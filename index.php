<?php include 'bin/config.php';

			if (!$_SESSION['login']) {
				header("Location: /login.html");
				exit;
			}
	// ============== Force Download
	if (isset($_REQUEST['action'])) {
		force_download($root.'/../'.$_ENV['DOWNLOAD_FILE']);
	}

?>
<!DOCTYPE html>
<html lang="en">
	<head>

		<meta charset="iso-8859-2" />
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2"/>

		<meta name="description" content="Language Translation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<title>TRS Flagship Store</title>

		<!--[if !IE]> -->
		<link rel="stylesheet" href="assets/1.3.3/css/pace.min.css" />

		<script data-pace-options='{ "ajax": true, "document": true, "eventLag": false, "elements": false }' src="assets/1.3.3/js/pace.min.js"></script>

		<!-- <![endif]-->

		<!-- bootstrap & fontawesome (from Bootstrap CDN) -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">



		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="assets/1.3.3/css/bootstrap.min.css" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="assets/1.3.3/css/ace-fonts.min.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="assets/1.3.3/css/ace.min.css" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/1.3.3/css/ace-part2.min.css" />
		<![endif]-->
<!--
		<link rel="stylesheet" href="assets/1.3.3/css/ace-skins.min.css" />
		<link rel="stylesheet" href="assets/1.3.3/css/ace-rtl.min.css" />
-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/1.3.3/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->
		<link rel="stylesheet" href="assets/css/style.css" />

		<!-- ace settings handler -->
		<script src="assets/1.3.3/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/1.3.3/js/html5shiv.js"></script>
		<script src="assets/1.3.3/js/respond.min.js"></script>
		<![endif]-->


		<!--[if !IE]> -->
		<script type="text/javascript">
			window.eery || document.write("<script src='/assets/1.3.3/js/jquery.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/1.3.3/js/jquery1x.min.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/1.3.3/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>

		<link rel="stylesheet" href="/assets/toastr/toastr.min.css" media="all" />

		<!--kwx library -->
		<link rel="stylesheet" href="assets/kwx/css/kwx.css" media="all" />
		<script type="text/javascript" src="assets/kwx/js/kwx.js"></script>
		<script type="text/javascript">
			var data = {'host':'http://com'};
			var kwx = new KWX(data);


		</script>



	</head>

	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default navbar-collapse  h-navbar trs-blue-bg white">


			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<!-- #section:basics/navbar.layout.brand -->
					<a href="#" class="navbar-brand">
						<small>
							<i class="fa fa-globe"></i>
							<?php echo ucfirst($_SESSION['site'])?> Store
						</small>
					</a>

					<!-- /section:basics/navbar.layout.brand -->

					<!-- #section:basics/navbar.toggle -->
					<button class="pull-right navbar-toggle navbar-toggle-img collapsed" type="button" data-toggle="collapse" data-target=".navbar-buttons,.navbar-menu">
						<span class="sr-only">Toggle user menu</span>

						<i class="fa fa-user fa-2x"></i>
					</button>

					<button class="pull-right navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".sidebar">
						<span class="sr-only">Toggle sidebar</span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>
					</button>

					<!-- /section:basics/navbar.toggle -->
				</div>

				<!-- #section:basics/navbar.dropdown -->
				<div class="navbar-buttons navbar-header pull-right  collapse navbar-collapse" role="navigation">
					<ul class="nav ace-nav">

						<!-- #section:basics/navbar.user_menu -->
						<li class="grey user-min">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<i class="fa fa-user fa-2x"></i>
								<span class="user-info">
									<small>Welcome, </small>
									 admin								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">


								<li>
									<a href="/index.html?action=logout">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>

						<!-- /section:basics/navbar.user_menu -->
					</ul>
				</div>

				<!-- /section:basics/navbar.dropdown -->

			</div><!-- /.navbar-container -->
		</div>

		<div id="sidebar" class="sidebar h-sidebar navbar-collapse collapse">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<button class="btn btn-success">
							<i class="ace-icon fa fa-signal"></i>
						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-pencil"></i>
						</button>

						<!-- #section:basics/sidebar.layout.shortcuts -->
						<button class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>
						</button>

						<button class="btn btn-danger">
							<i class="ace-icon fa fa-cogs"></i>
						</button>

						<!-- /section:basics/sidebar.layout.shortcuts -->
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div><!-- /.sidebar-shortcuts -->

				<ul class="nav nav-list">
					<!-- li class="hover">
						<a href="#dashboard" data-url="dashboard">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a>

						<b class="arrow"></b>
					</li -->
					<li class="hover">
						<a href="#translate" data-url="translate">
							<i class="menu-icon fa fa-globe"></i>
							<span class="menu-text"> Translate Language </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="hover">
						<a href="#email_tpl" data-url="email_tpl">
							<i class="menu-icon fa fa-envelope"></i>
							<span class="menu-text"> Email Templates </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="hover"></li>

					<li class="hover pull-right">
						<a href="#" data-url="dwn_lang" onclick="$.post('/bin/ajx.php', {action:'download_language'}, downloadLangHandler,'json')" >
							<i class="menu-icon fa fa-download"></i>
							<span class="menu-text"> Generate Language file </span>
						</a>

						<b class="arrow"></b>
					</li>

				</ul><!-- /.nav-list -->
</div><!-- /.sidebar -->

			<div class="main-content">
				<div class="main-content-inner">
					<!-- #section:basics/content.breadcrumbs -->

					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">
						<!-- #section:settings.box -->

						<div id="alert_display"></div>
						<!-- /section:settings.box -->
						<div class="page-content-area" data-ajax-content="false">
							<!-- ajax content goes here -->
						</div><!-- /.page-content-area -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->




<div class="footer">
				<div class="footer-inner">
					<!-- #section:basics/footer -->
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">TRS Flagship Store</span>
							Language Translation &copy; 2015
						</span>

					</div>

					<!-- /section:basics/footer -->
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>




<!-- ===================================== GOOGLE ANALYTICS ============================================== -->
<!--<?php //google_analytics(GOOGLE_ANALYTICS)?>-->
<!-- ===================================== GOOGLE ANALYTICS END============================================== -->

		<!-- basic scripts -->


		<script src="assets/1.3.3/js/bootstrap.min.js"></script>

		<!-- page specific plugin scripts -->

		<!-- ace scripts -->
		<script src="assets/1.3.3/js/ace-elements.min.js"></script>
		<script src="assets/1.3.3/js/ace.min.js"></script>
<!--		<script src="assets/1.3.3/js/ace.ajax-content.js"></script>-->


		<script src="/assets/toastr/toastr.min.js"></script>
		<script src="/assets/js/encode.js"></script>
		<script src="/assets/js/app.js"></script>
	</body>
</html>
