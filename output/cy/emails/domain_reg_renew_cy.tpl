Annwyl__NAME__,

Rydym yn falch o gadarnhau fod eich archeb ar gyfer yr enw(au) parth canlynol wedi ei gwblhau.

	__ITEMS__

Er mwyn rheoli eich gosodiadau enw parth, a phrynu gwasanaethau eraill, mewngofnodwch i'ch cyfrif yn __LOGIN__.

__LINKTEXT__ __LINK__

Os oes cwestiynau erall gennych, neu os gallwn fod o gymorth mewn unrhyw fater arall, peidiwch ag oedi i gysylltu â ni.

Diolch am eich busnes!

Tîm Cefnogaeth
__PARTNER__