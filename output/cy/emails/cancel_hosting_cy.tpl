Dear __NAME__,

Your hosting package for the following domain name has now expired, and any email accounts or uploaded website files have been deleted.

	__DOMAIN__

If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.

Best regards,

Support team
__PARTNER__