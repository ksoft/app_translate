Annwyl __NAME__,

Rydym ym falch gadarnhau fod eich archeb ar gyfer Google Apps wedi'i brosesu ac mae eich Cyfrif/on Google Apps erbyn hyn yn barod i'w sefydlu.

Er mwyn sefydlu cyfrif Google Apps, mewngofnodwch i'ch cyfrif yn __LOGIN__.

Bydd yn gyntaf angen i chi greu "Defnyddiwr Gweinyddol" ar gyfer pob cyfrif Google Apps.

Dilynwch y broses fel sydd wedi'i ddangos yn y sgr�n mewngofnodi.

Os oes unrhyw gwestiynau gennych, neu os gallwn fod o gymorth pellach o ran unrhyw fater arall, peidiwch ag oedi wrth gysylltu � ni.

Diolch am eich busnes!

Cofion Gorau,

T�m Cefnogi
__PARTNER__
