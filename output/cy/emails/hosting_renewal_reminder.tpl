Dear __NAME__,

Our records show that the __HOSTING__ hosting package for the following domain name is due to expire in __DAYS__ days, on __DATE__:

    __DOMAIN__

To renew your hosting package, please login to your account and click on the domain name.

__LOGIN__

If payment is not received by __DATE__, the hosting account will be deleted and your website will no longer be active.

If you require further assistance, please contact us immediately.

Best regards,

Support team
__PARTNER__