Annwyl __NAME__,

Rydym yn ysgrifennu atoch i gadarnhau fod y parth canlynol wedi ei restru ar gyfer ei ddileu, yn dilyn cais i'w ddileu gan ddeiliad cyfrif __ADDR__.

	__DOMAIN__

Os hoffech inni gadw'r parth gallwch ei adfer yn y 29 diwrnod nesaf am ffi ychwanegol, drwy fewngofnodi i'r cyfrif. Er mwyn gwneud hyn mewngofnodwch i'ch cyfrif a chliciwch 'Adfer' nesaf i'r enw parth. Mae ffioedd adfer wedi'u rhestru ar y dudalen cwestiynau cyffredin ar ein gwefan.

Cofion Gorau,

T�m Cymorth
_PARTNER_
