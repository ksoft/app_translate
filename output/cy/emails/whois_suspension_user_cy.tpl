Dear __NAME__,

As you have not verified the contact details associated with your account and it has been __DAYS__ days since this request was first made, we are now required by industry regulations to suspend your account.

If you wish to reactivate your account please click on this link:
	__LINK__

If you have any questions, or we can be of assistance in any other matter, please don't hesitate to contact us.

Best regards,

Support team
__PARTNER__