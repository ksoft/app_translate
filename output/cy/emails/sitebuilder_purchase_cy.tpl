Annwyl __NAME__,

Rydym yn falch o gadarnhau fod eich archeb wedi'i gwblhau ar gyfer y pecyn(nau) sitebuilder canlynol:

	__ITEMS__

Er mwyn cychwyn gydag adeiladu eich gwefan, mewngofnodwch i'ch cyfrif yn __LOGIN__, cliciwch ar eich parth a chliciwch y botwm 'Golygu Gwefan'.

Os oes unrhyw gwestiynau gennych, neu os gallwn fod o gymorth pellach o ran unrhyw fater arall, peidiwch ag oedi wrth gysylltu � ni.

Diolch am eich busnes!

Cofion Gorau,

T�m Cefnogi
__PARTNER__
