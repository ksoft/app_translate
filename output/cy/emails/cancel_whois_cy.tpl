Annwyl __NAME__,

Mae ein cofnodion yn dangos nad ydym wedi derbyn taliad ar gyfer adnewyddu "Private Whois" ar gyfer y parth canlynol, ac mae'r manylion cyswllt erbyn hyn wedi eu gwneud yn gyhoeddus:

	__DOMAIN__

Er mwyn galluogi "Privat Whois" ar gyfer y parth hwn, mewngofnodwch i'ch cyfrif a gwnewch bryniant newydd.

__LOGIN__

Os oes unrhyw gwestiynau gennych, neu os gallwn fod o gymorth pellach o ran unrhyw fater arall, peidiwch ag oedi wrth gysylltu � ni. 

Cofion Gorau,

T�m Cefnogi
__PARTNER__
