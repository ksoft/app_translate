Dear __NAME__,

The following domain name has now been transferred away from your account, and all associated services have been cancelled:

    __DOMAIN__

Best regards,

Support team
__PARTNER__