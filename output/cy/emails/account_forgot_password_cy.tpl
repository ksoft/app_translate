Annwyl __NAME__,

Mae rhywun (efallai chi) wedi gofyn inni ail-osod cyfrinair eich cyfrif. 

Eich cyfeiriad ebost cofrestredig yw: __EMAIL__

Eich cyfrinair newydd yw: __PASSWORD__

Cofiwch fod y cyfriair hwn yn SENSITIF I LYTHRENNAU BRAS NEU BACH. Er mwyn mewngofnodi, ewch i __LOGIN__.

Rydym yn argymell eich bod yn mewngofnodi i'ch cyfrif ac ail-osod eich cyfrinair ar fyrder.  Er mwyn gwneud hwn, cliciwch ar y linc "Newid Cyfrinair Cyfrif".

Cofion Gorau,

T�m Cymorth
__PARTNER__
