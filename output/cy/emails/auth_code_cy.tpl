Annwyl __NAME__,

Rydym wedi derbyn cais i ddanfon cod trosglwyddo ar gyfer yr enw parth canlynol, er mwyn eich galluogi i'w drosglwyddo i gofrestrydd arall:
	
Parth: __DOMAIN__
	Cod: __CODE__

Os ydych yn anymwybodol o'ch cais, cysylltwch � ni yn unionsyth. 

Nodwch: unwaith y bydd y parth wedi'i drosglwyddo, bydd unrhyw wasanaeth sy'n gysylltiedig �'r enw parth yn cael ei ganslo.  

Os oes unhyw gwestiynau gennych, neu gallwn fod o gymorth o ran unrhyw fater arall, peidiwch ac oedi wrth gysylltu.

Cofion Gorau,

T�m Cymorth
__PARTNER__
