Dear __NAME__,

As you have not verified the contact details associated with __DOMAIN__, and it has been __DAYS__ days since this request was first made, we are now required by industry regulations to suspend your domain.

If you wish to reactivate your domain please click on the below verification link:

	__LINK__

If you have any questions, or we can be of assistance in any other matter, please don't hesitate to contact us.

Best regards,

Support team
__PARTNER__