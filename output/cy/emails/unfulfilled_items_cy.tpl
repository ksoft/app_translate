Annwyl __NAME__,

Mae'n flin gen i ond nid oedd modd inni gofrestru'r enw(au) parth a'r/neu'r gwasanaethau canlynol:

	__ITEMS__

Efallai fod hyn oherwydd gwall yn ein system mewnol, ac felly byddwn yn ceisio cywiro'r broblem a chwblhau eich pryniant.

Rydym yn ymddiheuro am unrhyw anghyfleustra achoswyd wrth inni ymchwilio, a byddwn yn eich hysbysu o'r canlyniad cyn gynted a phosib.

Os oes unrhyw gwestiynau gennych, neu os gallwn fod o gymorth pellach o ran unrhyw fater arall, peidiwch ag oedi wrth gysylltu â ni.

Cofion Gorau,

Tîm Cefnogi
__PARTNER__
