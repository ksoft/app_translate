Dear __NAME__,

Our records show that the __PLAN__ website builder package for the following domain name is due to expire in __DAYS__ days, on __DATE__:

    __DOMAIN__

To renew or upgrade your website builder, please login to your account and click on the domain name.

__LOGIN__

If payment is not received by __DATE__, the website builder account will be downgraded to the default package and any premium features on your website will no longer work.

If you require further assistance, please contact us immediately.

Best regards,

Support team
__PARTNER__