Annwyl __NAME__,

Mae eich pecyn adeiladu gwefan ar gyfer y parth canlynol wedi dod i ben. Mae eich cyfrif wedi ei israddio i'r pecyn cyffredin ac ni fydd unrhyw nodwedd premiwm ar eich gwefan bellach yn weithredol.   

	__DOMAIN__

Os oes unrhyw gwestiynau gennych, neu os y gallwn fod o gymorth o ran unrhyw fater arall, peidiwch ac oedi wrth gysylltu. 

Cofion Gorau,

T�m Cymorth 
__PARTNER__
