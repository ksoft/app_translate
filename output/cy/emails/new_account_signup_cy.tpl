Annwyl __NAME__,

Diolch am ymuno � ni. Nodwch, fel rhan o rheoliadau'r diwydiant mae gofyn ichi ddilysu'r wybodaeth gysylltu rydych wedi ei rhoi inni o fewn __VERIFICATION_DAYS__ diwrnod neu bydd rhaid inni atal eich gwasanaethau.

Er mwyn dilysu eich manylion, cliciwch ar y ddolen dilysu isod, neu gop�wch a gludwch eich dolen yn ffenestr eich porwr:

__VERIFICATION_URL__

Er mwyn mewngofnodi, eiwch i__LOGIN__ a rhowch eich cyfeiriad ebost a chyfrinair y gwneathoch gyflwyno yn ystod y broses cofrestru.

Os ydych wedi anghofio eich cyfrinair, gallwch ddefnyddio'r ddolen "anghofio cyfrinair" ar y dudalen mewngofnodi ar ein gwefan er mwyn gofyn i ail-osod eich cyfrinair.

Cofion Gorau,

T�m Cefnogi
__PARTNER__
__PARTNER_EMAIL__
