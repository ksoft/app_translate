Annwyl __NAME__,

Nid ydym yn dymuno eich trafferthu, ond mae'n rhaid inni! Fel rhan o rheoliadau'r diwydiant mae gofyn ichi ddilysu'r wybodaeth gysylltu rydych wedi ei rhoi inni o fewn __VERIFICATION_DAYS__ diwrnod neu bydd rhaid inni atal eich gwasanaethau.

Er mwyn dilysu eich manylion, cliciwch ar y ddolen dilysu isod, neu gopïwch a gludwch eich dolen yn ffenestr eich porwr:

__VERIFICATION_URL__

Fe fyddwn yn eich atgoffa eto rhai troeon cyn inni atal eich gwasanaethau ond cliciwch ar y ddolen nawr thag ofn ichi fethu ein ebyst. Cysylltwch â ni ar unrhyw adeg os oes unrhyw gwestiynau gyda chi.

Cofion gorau,

Tîm Cefnogi
__PARTNER__
__PARTNER_EMAIL__
