Anwyl __NAME__,

Mae eich cais am yr enw parth canlynol wedi ei nodi fel __STATUS__ gan y gweithredwr cofrestrfa:

	__DOMAIN__

Mae nifer fawr o barthau arbennig yn dal i fod ar gael felly beth am ymweld â  __WEBSITE__ a cheisio eto.

Bydd cost cofrestru parth yn cael ei ad-dalu yn ôl i'ch carden, caniatewch hyd at 5 niwrnod gwaith er mwyn i hwn gael ei brosesu. Os oes cwestiynau pellach gennych, neu os gallwn fod o gymorth mewn unrhyw fater, cysylltwch â ni yn unionsyth. 


Cofion Gorau,
Y Tîm Cefnogi
Support team
__PARTNER__
