Annwyl __ENW__,

Rydym yn falch o roi gwbod i chi fod eich __TYPE__  cais ar gyfer y parth canlynol wedi bod yn llwyddiannus ac mae’r parth wedi ei gofrestru yn eich cyfrif:

	__DOMAIN__

Er mwyn rheoli eich gosodiadau enw parth, a gwasanaethau eraill, mewngofnodwch i'ch cyfrif yn __LOGIN__.

OS oes cwestiynau gennych, neu os gallwn fod o gymorth gydag unrhyw fater arall, cysylltwch â ni yn unionsyth. 

Diolch yn fawr am eich busnes!

Cofion gorau,
Y Tîm Cymorth

__PARTNER__
