
<title>Language File</title>
<link rel="stylesheet" href="../../assets/1.3.3/css/bootstrap-editable.min.css" />

<?php include '../bin/config.php' ;?>

<!-- ajax layout which only needs content area -->
<div class="page-header">
	<h1>Translating English >> <?php echo $_ENV['Lang_Name'] ?> </h1>
</div><!-- /.page-header -->

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="row">
			<div class="col-sm-7">
				<h3 class="header smaller lighter blue">English</h3>

				<div class="clearfix">
					<div class="pull-right tableTools-container"></div>
				</div>
				<div class="table-header">
					Please click on the targeted language text to edit"
				</div>

				<!-- div.dataTables_borderWrap -->
				<div>
					<table id="lang-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th class="center">
									<label class="pos-rel">
										<input type="checkbox" class="ace" />
										<span class="lbl"></span>
									</label>
								</th>
								<th>ID</th>
								<th>English</th>
								<th>Status</th>
							</tr>
						</thead>

						<tbody>

						<?php
							$from_file = true;
							$rs = $from_file ? $_ENV['data'] : getAllTranslations();
							$id = 1;
							$status_text = 'Not Translated';
							$status = 'default';

							foreach ($rs as $key => $value):
								if ($from_file) {
									$val = $value;
								}

								$translated = getTranslation($id);

								if (array_key_exists('from_lang', $translated)) {

									if ($translated['from_lang'] != null) {
										$status_text = 'Translated';
										$status = 'success';
										$val = trim($translated['from_lang']);
									}
								} else {
									$status_text = 'Not Translated';
									$status = 'default';
								}

						?>
						<tr>
							<td class="center">
								<label class="pos-rel">
									<input type="checkbox" class="ace" />
									<span class="lbl"></span>
								</label>
							</td>

							<td><?php echo $id ?></td>
							<td>
								<a  id="from_lang_<?php echo $id?>" class="selected_text" href="javascript:fetchTranslation(<?php echo $id?>)" data-trans_ref="<?php echo $id?>" >
									<?php echo htmlentities($val, ENT_QUOTES, 'UTF-8');?>
								</a>
							</td>
							<td>
								<div id="status_<?php echo $id ?>" class="label label-<?php echo $status ?>">
									<?php echo $status_text ?>
								</div>
							</td>
						</tr>
						<?php if ($from_file) {
										$id++;
									}
							 endforeach;?>
						</tbody>
					</table>
				</div>
			</div>


			<div class="col-sm-5">
				<div class="col-sm-12">
					<h4 id="ori_text" class="header blue">Original Text</h4>

					<div class="widget-box widget-color-blue">
						<div class="widget-header widget-header-small pad5"> You are editing: <span id="en_title" class="label label-lg label-light arrowed-right"></span> </div>

						<div class="widget-body">
							<form id="frm_translation" class="">
								<div class="widget-main no-padding">
									<div id="from_lang" class="pad30"></div>
								</div>

							</form>
						</div>
					</div>
				</div>
				<!-- .english -->


				<div class="col-sm-12">
					<h4 class="header green"><?php echo ucfirst($_ENV['Lang_Name']) ?> Editor</h4>

					<div class="widget-box widget-color-green">
						<div class="widget-header widget-header-small"> You are editing: <span id="target_lang_title" class="label label-lg label-purple arrowed-right"></span> </div>

						<div class="widget-body">
							<div class="widget-main no-padding">
								<div id="to_lang" name="to_lang" class="pad30 form-control"  style="height: 250px"></div>
							</div>

							<div class="widget-toolbox padding-4 clearfix">
								<div class="btn-group pull-left">
									<button class="btn btn-sm btn-info">
										<i class="ace-icon fa fa-times bigger-125"></i>
										Cancel
									</button>
								</div>

								<div class="btn-group pull-right">
									<button id="save_lang_text" class="btn btn-sm btn-purple">
										<i class="ace-icon fa fa-floppy-o bigger-125"></i>
										Save
									</button>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>



		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->



<!-- page specific plugin scripts -->
<script type="text/javascript">
	var scripts = [null,"../../assets/1.3.3/js/jquery.hotkeys.min.js","../../assets/1.3.3/js/bootstrap-wysiwyg.min.js","../../assets/1.3.3/js/dataTables/jquery.dataTables.min.js","../../assets/1.3.3/js/dataTables/jquery.dataTables.bootstrap.min.js","../../assets/1.3.3/js/dataTables/extensions/TableTools/js/dataTables.tableTools.min.js", null]
	$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
		//inline scripts related to this page
		jQuery(function($){
			kwx.store.csrf = "<?php echo $_SESSION['CSRF'] ?>" ;
			//initiate dataTables plugin
			var oTable1 = $('#lang-table')
			//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
			.dataTable( {
				bAutoWidth: false,
				"aoColumns": [
					{ "bSortable": false },
					null, null,
					{ "bSortable": true }
				],
						"columnDefs": [
							{ "width": "1%", "targets": 0 },
							{ "width": "3%", "targets": 0 },
							{ "width": "50%", "targets": 0 },
							{ "width": "45%", "targets": 0 }
						],
				//"aaSorting": [],

				//,
				//"sScrollY": "200px",
				"bPaginate": true,

				//"sScrollX": "100%",
				//"sScrollXInner": "120%",
				//"bScrollCollapse": true,
				//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
				//you may want to wrap the table inside a "div.dataTables_borderWrap" element

				"iDisplayLength": 25
			} );
			oTable1.fnAdjustColumnSizing();


			$('#from_lang').ace_wysiwyg();
			$('#to_lang').ace_wysiwyg();

			$('#save_lang_text').on('click', function(){
				var trans_ref = localStorage.selected_translet,
					  params = {trans_ref: trans_ref,
							to_lang: $('#to_lang').html(),
							from_lang: $('#from_lang').html(),
							action: 'save_translation',
							csrf:localStorage.csrf
					  };

				$.post('/bin/ajx.php', params, function(res){
					if (res.ok) {
						toastr.success(res.msg);
						$('#status_'+trans_ref).removeClass('label-default');
						$('#status_'+trans_ref).addClass('label-success');
						$('#status_'+trans_ref).html('Translated');
					} else {
						toastr.error(res.msg);
					}

				}, 'json');
			});

		});

	});



	function fetchTranslation(id) {
		var html = '';
		var params = {trans_ref: id, action: 'fetch_translation', csrf:localStorage.csrf};
		localStorage.selected_translet = id;

		$.post('/bin/ajx.php', params, function(res){

			$('html, body').animate({scrollTop: $('#ori_text').offset().top}, 1000);
			var from_lang = res.from_lang ? res.from_lang : $('#from_lang_'+id).html();
			$('#en_title').html('You are editing Translation Text ID:' + kwx.store.selected_translet );

			from_lang = Encoder.htmlDecode(from_lang);

			if (res.ok) {
				$('#to_lang').html(res.to_lang);
				$('#from_lang').html(from_lang);

			} else {
				$('#from_lang').html(from_lang);
				$('#to_lang').html(from_lang);
				toastr.error(res.msg);
			}

		}, 'json');
	}
</script>
