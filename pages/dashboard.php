
<title>Dashboard</title>

<!-- ajax layout which only needs content area -->
<div class="page-header">
	<h1>
		Dashboard
		<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			overview &amp; stats
		</small>
	</h1>
</div><!-- /.page-header -->

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="alert alert-block alert-success">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>

			<i class="ace-icon fa fa-check green"></i>

			Language file has been uploaded and is ready for <a href="#translate">translation</a>
			

		</div>

		<div class="row">
			<div class="space-6"></div>

			<div class="col-sm-6 infobox-container">
				<div>
					<form action="../../dummy.html" class="dropzone" id="dropzone">
						<div class="fallback">
							<input name="file" type="file" multiple="" />
						</div>
					</form>
				</div>
			</div>

			<div class="vspace-12-sm"></div>

			<div class="col-sm-5">
				
			</div><!-- /.col -->
		</div><!-- /.row -->


		<div class="hr hr32 hr-dotted"></div>

		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
	<script src="../../assets/js/excanvas.js"></script>
<![endif]-->
<script type="text/javascript">
	var scripts = [null,"../../assets/1.3.3/js/dropzone.min.js", null]
	$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
		//inline scripts related to this page
		jQuery(function($){

			try {
				Dropzone.autoDiscover = false;
				var myDropzone = new Dropzone("#dropzone" , {
					paramName: "file", // The name that will be used to transfer the file
					maxFilesize: 0.5, // MB

					addRemoveLinks : true,
					dictDefaultMessage :
					'<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Drop files</span> to upload \
<span class="smaller-80 grey">(or click)</span> <br /> \
<i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>'
					,
					dictResponseError: 'Error while uploading file!',

					//change the previewTemplate to use Bootstrap progress bars
					previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n  <div class=\"dz-details\">\n    <div class=\"dz-filename\"><span data-dz-name></span></div>\n    <div class=\"dz-size\" data-dz-size></div>\n    <img data-dz-thumbnail />\n  </div>\n  <div class=\"progress progress-small progress-striped active\"><div class=\"progress-bar progress-bar-success\" data-dz-uploadprogress></div></div>\n  <div class=\"dz-success-mark\"><span></span></div>\n  <div class=\"dz-error-mark\"><span></span></div>\n  <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n</div>"
				});

				$(document).one('ajaxloadstart.page', function(e) {
					try {
						myDropzone.destroy();
					} catch(e) {}
				});

			} catch(e) {
				alert('Dropzone.js does not support older browsers!');
			}

		});
	});
</script>
