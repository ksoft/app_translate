<title>Language File</title>
<link rel="stylesheet" href="../../assets/1.3.3/css/bootstrap-editable.min.css" />

<?php include '../bin/config.php' ?>

<!-- ajax layout which only needs content area -->
<div class="page-header">
	<h1>Translating English Email Templates >> <?php echo $_ENV['Lang_Name'] ?> Email Templates</h1>
</div><!-- /.page-header -->

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->

		<div class="row">
			<div class="col-sm-5">
				<h3 class="header smaller lighter blue">English</h3>

				<div class="clearfix">
					<div class="pull-right tableTools-container"></div>
				</div>
				<div class="table-header">
					Please click on the targeted language text to edit"
				</div>

				<!-- div.dataTables_borderWrap -->
				<div>
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th class="center">
									<label class="pos-rel">
										<input type="checkbox" class="ace" />
										<span class="lbl"></span>
									</label>
								</th>
								<th>ID</th>
								<th>English</th>
								<th><?php echo ucfirst($_ENV['Lang_Name']) ?> File Name</th>


							</tr>
						</thead>

						<tbody>
							<?php $i = 1;
								$ext = '_'.$_ENV['Lang'].'.tpl';
								$target_emails = getTargetLangEmails();
								foreach(getDefaultEmails() as $key=>$val):
								$target_email_file = str_replace('_en.tpl', $ext, $val['file_name']);
							?>
							<tr>
								<td class="center">
									<label class="pos-rel">
										<input type="checkbox" class="ace" />
										<span class="lbl"></span>
									</label>
								</td>

								<td><?php echo $i ?></td>
								<td>
									<a class="email_default" href="#" data-file_path="<?php echo $val['file_name']?>"
									   data-content="<?php echo $val['file_content']?>"
										data-target-email="<?php echo $target_email_file?>">
										<?php echo $val['file_name']?>
									</a>

								</td>
								<td>
									<!--<a class="email_target" href="#" data-file_path="<?php echo $target_email_file ?>">-->
										<?php echo $target_email_file  ?>
									<!--</a>-->
								</td>
							</tr>
							<?php $i++; endforeach;?>
						</tbody>
					</table>
				</div>
			</div>

			<div class="col-sm-7">
				<div class="col-sm-12">
					<h4 class="header blue">English Template</h4>

					<div class="widget-box widget-color-blue">
						<div class="widget-header widget-header-small pad5"> You are editing: <span id="en_title" class="label label-lg label-light arrowed-right"></span> </div>

						<div class="widget-body">
							<form id="frm_translation" class="">
								<div class="widget-main no-padding">
									<div id="from_lang" class="pad30"></div>
								</div>

							</form>
						</div>
					</div>
				</div>
				<!-- .english -->


				<div class="col-sm-12">
					<h4 class="header green"><?php echo ucfirst($_ENV['Lang_Name']) ?> Editor</h4>

					<div class="widget-box widget-color-green">
						<div class="widget-header widget-header-small"> You are editing: <span id="target_lang_title" class="label label-lg label-purple arrowed-right"></span> </div>

					<div class="widget-body">

							<div class="widget-main no-padding">
								<textarea id="to_lang" name="to_lang" class="pad30 form-control" rows="20"></textarea>
<!--								<div id="to_lang" class="pad30"></div>-->
							</div>

							<div class="widget-toolbox padding-4 clearfix">
								<div class="btn-group pull-left">
									<button class="btn btn-sm btn-info">
										<i class="ace-icon fa fa-times bigger-125"></i>
										Cancel
									</button>
								</div>

								<div class="btn-group pull-right">
									<button id="save_email_template" class="btn btn-sm btn-purple">
										<i class="ace-icon fa fa-floppy-o bigger-125"></i>
										Save
									</button>
								</div>
							</div>

					</div>
				</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			var $path_assets = "../../assets";//this will be used in loading jQuery UI if needed!
		</script>

		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->

<!-- page specific plugin scripts -->
<script type="text/javascript">
	var scripts = [null,"../../assets/1.3.3/js/bootstrap-wysiwyg.min.js","../../assets/1.3.3/js/dataTables/jquery.dataTables.min.js","../../assets/1.3.3/js/dataTables/jquery.dataTables.bootstrap.min.js","../../assets/1.3.3/js/dataTables/extensions/TableTools/js/dataTables.tableTools.min.js", null]
	$('.page-content-area').ace_ajax('loadScripts', scripts, function() {
		//inline scripts related to this page
		jQuery(function($){
			kwx.store.csrf = "<?php echo $_SESSION['CSRF'] ?>" ;
			function showErrorAlert (reason, detail) {
				var msg='';
				if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
				else {
					//console.log("error uploading file", reason, detail);
				}
				$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+
				  '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
			}

			//initiate dataTables plugin
			var oTable1 = $('#dynamic-table')
			.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
			.dataTable( {
				bAutoWidth: true,
				"aoColumns": [
					{ "bSortable": false },
					null, null,
					{ "bSortable": true }
				],
				//"aaSorting": [],

				//,
				//"sScrollY": "200px",
				"bPaginate": false,

				"sScrollX": "100%",
				//"sScrollXInner": "120%",
				//"bScrollCollapse": true,
				//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
				//you may want to wrap the table inside a "div.dataTables_borderWrap" element

				//"iDisplayLength": 50
			} );
			oTable1.fnAdjustColumnSizing();

			$('.email_default').on('click',function(e){
				kwx.showLoading();
				e.preventDefault();
				$('#en_title').html($(this).data('file_path'));
				$('#from_lang').html($(this).data('content'));


				// fetch content of corresponding language file
				var file_path = $(this).data('target-email');
				localStorage.setItem('selected_file', file_path);

				$('#target_lang_title').html(file_path);

				$.post('/bin/ajx.php', {action: 'get_email_tpl_content', file_name:file_path, csrf:kwx.store.csrf}, function(res){
					kwx.removeLoading();
					if (res.ok) {
						//var content = res.content.replace(/\n/g, "<br>");
						$('#to_lang').val(res.content);
					} else {
						toastr.error(res.msg);
					}
				}, 'json');

			});

			$('#from_lang').ace_wysiwyg({
				toolbar_place: function(toolbar) {
					//for example put the toolbar inside '.widget-header'
					return $(this).closest('.widget-box')
						.find('.widget-header').prepend(toolbar)
						.find('.wysiwyg-toolbar').addClass('inline');
				},
				toolbar: {		}

			});


			// Submitting email template
			$('#save_email_template').on('click', function(){
				kwx.showLoading();
				var content = $('#to_lang').val();

				if (typeof content == 'undefined' || content == '') {
					alert('Error! You cannot submit an empty template. Please make sure you are not in "View Source" mode');
					return false;
				}

				var	params = {file_content:content,
							file_path: localStorage.getItem('selected_file'),
							action:'save_email_tpl',
							csrf:kwx.store.csrf
						};

				$.post('/bin/ajx.php',params, function(res){
					kwx.removeLoading();

					if (res.ok) {
						toastr.success(res.msg);
						$('#to_lang').html(res.content);
					} else {
						toastr.error(res.msg);
					}

				}, 'json');
			})
		});
	});
</script>
