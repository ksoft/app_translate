<?php
session_start();

//include_once 'Sqlite.php';
include "NotORM.php";
error_reporting(0);
##########################################################3333333
$root = dirname(__FILE__);

$db_path = $root.'/lang_app.db';

 //create the sqlite database if it doesn't exist
        if(!file_exists($db_path)) {
                $db_path = $db_path. '.db';
                $db_conn = new SQLite3($db_path);

                //close the connection to allow NotORM to take over
                $db_conn->close();
        }


define('LANG', isset($_SESSION['lang']) > 0 ? $_SESSION['lang'] : 'cy');
$input_dir = $root.'/../input/'. (isset($_SESSION['site']) ? $_SESSION['site'].'/' : '');
$output_dir = $root.'/../output/'. (isset($_SESSION['site']) ? $_SESSION['site'].'/' : '');
$default_lang_file = $input_dir.'en.php';
$cache_file = $root."/notorm.cache";



################## NOTORM  CONNECTION SETTINGS #############################
$db_driver = 'mysql';
$localhost = '79.170.44.107';
$db_name = 'cl25-translate ';
$db_username = 'cl25-translate ';
$db_password = 'EHHXN7ZWVlM-';


switch($db_driver){
    case 'mysql':
        try {
            $pdo =  new PDO("mysql:host=$localhost;dbname=$db_name", $db_username,$db_password);

        } catch (PDOException $e) {
            print "Oops! Can't connect to the database "."<br/>";
            die();
        }
        break;

    case 'sqlite':

        try {
            $pdo = new PDO("sqlite:".$db_path, "");

        } catch (PDOException $e) {
            print "Oops! Can't connect to the database "."<br/>";
            die();
        }
        break;
}


$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
$cache = new NotORM_Cache_File($cache_file);
$db = new NotORM($pdo, null, $cache);

// global variables
$_ENV['DB'] = $db;

$_ENV['Lang'] = LANG;

// get language details
$current_lang = $db->language[LANG];

$_ENV['Lang_Name'] = $current_lang['name'];
$_ENV['Lang_Dir'] = $output_dir.LANG;
$_ENV['Email_Dir'] = $input_dir.'emails';
$_ENV['Output_Email_Dir'] = $_ENV['Lang_Dir'].'/emails/';
$_ENV['Output_Dir'] = $output_dir;
$_ENV['DOWNLOAD_FILE'] = '/output/'. (isset($_SESSION['site']) ? $_SESSION['site'].'/' : '').LANG.'/'.LANG.'.php';


//$_GET['action']='';
require_once 'common.php';
############################################################################

	if (isset($_GET) AND $_GET['action'] === 'logout') {
		$data['user_id'] = $id;
		$data['message'] = sprintf('%s logged out', $_SESSION['login']);
		$data['ip'] = $_SERVER['REMOTE_ADDR'];

		logActivity($data);

	   session_destroy();
	   header("Location: /login.html");
	   exit;
	}

if (!file_exists($_ENV['Output_Dir'])) {
	$out = mkdir($_ENV['Output_Dir'], 0777);
}

	// load language file
	if (file_exists($default_lang_file)){
		include_once $default_lang_file;
		$_ENV['data'] =  $STRINGS[$LANG];

	} else {
		echo alert(sprintf('Please upload the default language file (%s)', $default_lang_file));
	}


	// load language file
	if (file_exists( $root.'/../'.$_ENV['DOWNLOAD_FILE'])){
		include_once  $root.'/../'.$_ENV['DOWNLOAD_FILE'];
		$_ENV['download_data'] =  $STRINGS[$LANG];
	}



if (!file_exists($_ENV['Output_Dir'])) {
	$r = mkdir($_ENV['Output_Dir'], 0777);
}

	// create the targeted language directory and file
	if(!file_exists($_ENV['Lang_Dir'])){
		if (!is_writable($_ENV['Output_Dir'])) {
			$r = chmod($_ENV['Output_Dir'], 0777);

			if (!$r) {
				echo alert($_ENV['Output_Dir'] .' is not writable');
			}

		} else {

			$rs = mkdir($_ENV['Lang_Dir'], 0777);

			if (!$rs){
				echo alert('Language Directory failed to create');

			} else {
				echo alert('Language Directory was created successfully', 'success');

				$rs = populateTargetEmailDir();
				echo $rs ? alert('Target Language Email Templates were created successfully', 'success')
						 : alert('Target Language Email Templates: File failed to create');
			}

		}
	}



######################################## Unit Testing #########################

	//var_dump(getAllTranslations());


   // saveTranslatedEmail('test', 'did it work');

	// load defualt email templates
   // $emails = getDefaultEmails();
	//var_dump($emails);

	//reading email template file content
   /*  $content = '';
	$filename = $_ENV['Output_Email_Dir'].'sitebuilder_renewal_reminder_cy.tpl';
	$fh = fopen($filename, 'r');
	if ($fh) {
		while (($buffer = fgets($fh, filesize($filename))) !== false) {
			$content .= $buffer;
		   // exit;
		}
		if (!feof($fh)) {
			echo "Error: unexpected fgets() fail\n";
		}
		fclose($fh);
	}


	$rs['content'] = nl2br($content);


	 */

	// Test writing language file
  /*   $r = updateLanguageFile();
	if ($r) {
		$file_name = $_ENV['Lang_Dir'].'/'.$_ENV['Lang'].'.php';
		$rs['content'] = file_get_contents($file_name);
		$rs['file_name'] = $_ENV['Lang'].'.php';
		$rs['success'] = TRUE;
		$rs['msg'] = alert('Language File generated successfully', 'success');
	} */




