<?php

    // updateTranslation($lang_file, $db);
    

    /**
     * Detects the end-of-line character of a string.
     * @param string $str The string to check.
     * @param string $default Default EOL (if not detected).
     * @return string The detected EOL, or default one.
     */
    function detectEol($str, $default=''){
        static $eols = array(
            "\0x000D000A", // [UNICODE] CR+LF: CR (U+000D) followed by LF (U+000A)
            "\0x000A",     // [UNICODE] LF: Line Feed, U+000A
            "\0x000B",     // [UNICODE] VT: Vertical Tab, U+000B
            "\0x000C",     // [UNICODE] FF: Form Feed, U+000C
            "\0x000D",     // [UNICODE] CR: Carriage Return, U+000D
            "\0x0085",     // [UNICODE] NEL: Next Line, U+0085
            "\0x2028",     // [UNICODE] LS: Line Separator, U+2028
            "\0x2029",     // [UNICODE] PS: Paragraph Separator, U+2029
            "\0x0D0A",     // [ASCII] CR+LF: Windows, TOPS-10, RT-11, CP/M, MP/M, DOS, Atari TOS, OS/2, Symbian OS, Palm OS
            "\0x0A0D",     // [ASCII] LF+CR: BBC Acorn, RISC OS spooled text output.
            "\0x0A",       // [ASCII] LF: Multics, Unix, Unix-like, BeOS, Amiga, RISC OS
            "\0x0D",       // [ASCII] CR: Commodore 8-bit, BBC Acorn, TRS-80, Apple II, Mac OS <=v9, OS-9
            "\0x1E",       // [ASCII] RS: QNX (pre-POSIX)
            //"\0x76",       // [?????] NEWLINE: ZX80, ZX81 [DEPRECATED]
            "\0x15",       // [EBCDEIC] NEL: OS/390, OS/400
        );
        $cur_cnt = 0;
        $cur_eol = $default;
        foreach($eols as $eol){
            if(($count = substr_count($str, $eol)) > $cur_cnt){
                $cur_cnt = $count;
                $cur_eol = $eol;
            }
        }
        return $cur_eol;
    }


    function trs_nl2br($string){
        return preg_replace('/\<br(\s*)?\>/i', "\n", $string);
    }
    
    function br2nl($string)
    {
        return strip_tags($string);
        /* if (preg_match('/\<br(\s*)?\/?\>/i', $string)) {
            return preg_replace('/\<br(\s*)?\/?>/i', "\n", $string);
        } */
        
        return $string;
    }
    

    function alert($msg, $type="danger"){
        return  '<div class="alert alert-'.$type.'">
                <button type="button" class="close" data-dismiss="alert"> <i class="ace-icon fa fa-times"></i> </button>'.$msg.'</div>';
    
    }
    
    function directory_map($source_dir, $directory_depth = 0, $hidden = FALSE)
    {
        if ($fp = @opendir($source_dir))
        {
            $filedata	= array();
            $new_depth	= $directory_depth - 1;
            $source_dir	= rtrim($source_dir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
    
            while (FALSE !== ($file = readdir($fp)))
            {
                // Remove '.', '..', and hidden files [optional]
                if ( ! trim($file, '.') OR ($hidden == FALSE && $file[0] == '.'))
                {
                    continue;
                }
    
                if (($directory_depth < 1 OR $new_depth > 0) && @is_dir($source_dir.$file))
                {
                    $filedata[$file] = directory_map($source_dir.$file.DIRECTORY_SEPARATOR, $new_depth, $hidden);
                }
                else
                {
                    $filedata[] = $file;
                }
            }
    
            closedir($fp);
            return $filedata;
        }
    
        return FALSE;
    }

    function paginator(){
    	
    }
    
    /*
    function generateLanguageFile($file_name=null, $str=null){
        $file_name = $file_name == null ? $_ENV['Lang_Dir'].'/'.$_ENV['Lang'].'.php'
                                        : $file_name ;
	    $db = $_ENV['DB'];
	    $total_rows = $db->translation()->count('id'); //
	
	    for($i=300; $i <= $total_rows; $i = $i+300) {
		    echo "$i,";
	    	$offset = $total_rows - $i;
	    	echo "$offset,";
	    }
//	    $rows = $db->translation()->; // pull data from the database
//        $total_rows = $_ENV['data'];
	    
	    var_dump($total_rows);
	    
	    return $total_rows;
        $str = "<?php \n";
        $str .= sprintf('$LANG="%s";'."\n", $_ENV['Lang']);
        $str .= sprintf('$STRINGS[$LANG]["_CRC_I18N_LOCALE_NAME"]="%s";'."\n\n", $_ENV['Lang_Name']);

        $trans_ref = 1;
        
        foreach($rows as $key=>$val){
            //check which row has been translated
            $translated = getTranslation($trans_ref);
        		
            if ($translated) {
                $key = trim($translated['from_lang']);
                $val = trim($translated['to_lang']);

                $key = str_replace('"', "'", $key);
                $val = str_replace('"', "'", $val);
                $str .= sprintf('$STRINGS[$LANG]["%s"]="%s";'. "\n", $key, $val);
            }
            
            $trans_ref++;
        }        
        
      $rs = writeToFile($file_name, $str);
            
        return $rs;
    }
    */
	function generateLanguageFile($file_name=null, $str=null){
	        $file_name = $file_name == null ? $_ENV['Lang_Dir'].'/'.$_ENV['Lang'].'.php'
	                                        : $file_name ;
		    $db = $_ENV['DB'];
//		    $rows = $db->translation()->limit(400, 1001);
		    $rows =  $_ENV['data'];
//		var_dump($rows);
				    
	        $str = "<?php \n";
	        $str .= sprintf('$LANG="%s";'."\n", $_ENV['Lang']);
	        $str .= sprintf('$STRINGS[$LANG]["_CRC_I18N_LOCALE_NAME"]="%s";'."\n\n", $_ENV['Lang_Name']);
	
	        $trans_ref = 1;
	        
	        foreach($rows as $key=>$val){
	            //check which row has been translated
	            $translated = getTranslation($trans_ref);
	                
	            if ($translated) {
	                $key = trim($translated['from_lang']);
	                $val = trim($translated['to_lang']);
	
	                $key = str_replace('"', "'", $key);
	                $val = str_replace('"', "'", $val);
	                $str .= sprintf('$STRINGS[$LANG]["%s"]="%s";'. "\n", $key, $val);
	            }
	            
	            $trans_ref++;
	        }
	        
	      $rs = writeToFile($file_name, $str);
	            
	        return $rs;
	    }

    
    
    function writeToFile($file_name, $str=null, $file_mode='w'){
        $fh = fopen($file_name, $file_mode);
        $rs = fwrite($fh, $str);
        
        if ($rs) {
            $rs = fclose($fh);
            
        } else {
            return $rs['error'] = 'Writing to file failed';
        }
        
        return $rs;
        
    }
    
############################ email_templates ##########################

    function populateTargetEmailDir() {
        $dir = $_ENV['Output_Email_Dir'];
        $rs = TRUE;
        
        if(!file_exists($dir)){ 
            $rs = mkdir($dir, 0777);            
        }
        
        $files = getDirectoryContent($_ENV['Email_Dir'], 1, TRUE);
        foreach($files as $key=>$file) {
            $content = file_get_contents($_ENV['Email_Dir'].'/'.$file);
            
            if ($rs) {
                $ext = '_'.$_ENV['Lang'].'.tpl';
                $file_name = str_replace('_en.tpl', $ext, $file); 
                
                //create file if it doesn't exist
                if (!file_exists($dir.$file_name)){
                    $file = fopen($dir.$file_name,"w") or die("can't open file");
                    fwrite($file,$content);
                    $rs = fclose($file);
                }
            }
            
        }        
        
        return $rs;
    }

    function getDefaultEmails(){
        $rs = array();
        $files = getDirectoryContent($_ENV['Email_Dir'], 1, TRUE);
        foreach($files as $key=>$file) {
            $rs[$key]['file_name'] = $file;
            $rs[$key]['file_content'] = file_get_contents($_ENV['Email_Dir'].'/'.$file);
        }
        
        return $rs;
    }
    
    
    function getTargetLangEmails(){
        $rs = array();
        $files = getDirectoryContent($_ENV['Output_Email_Dir'], 1, TRUE);
        foreach($files as $key=>$file) {
            $rs[$key]['file_name'] = $file;
            $rs[$key]['file_content'] = file_get_contents($_ENV['Output_Email_Dir'].'/'.$file);
        }
    
        return $rs;
    }
    
    
    
    
    function saveTranslatedEmail($filename, $content){
        $dir = $_ENV['Output_Email_Dir'];
        $rs = TRUE;
       // $content = str_replace('<br>', '\n', $content);
        
        if(!file_exists($dir)){
            $rs = mkdir($dir, 0777);                       
        } 
        
        if ($rs) {
            $file = fopen($dir.$filename,"w") or die("Unable to open file!");
            fwrite($file,$content);
            
             $rs = fclose($file);
        }
        
        return $rs;
        
    }
    
    /**
     *  custome function to read the content of a file
     * @param unknown $filename
     */
    function trs_readFile($filename){
        $content = '';
        $fh = fopen($filename, 'r');
        if ($fh) {
            while (($buffer = fgets($fh, filesize($filename))) !== false) {
                $content .= $buffer;
            }
            if (!feof($fh)) {
                return false;
            }
            fclose($fh);
        }
        
        return $content;
        
    } 
    
    function getDirectoryContent($dir, $rootlevel=1, $onlyFiles=FALSE, $hidden=FALSE){
        
        if (file_exists($dir)){
            $dirData = directory_map($dir, $rootlevel, $hidden);
            $result= array();
             
            if(!empty($dirData)){
                foreach($dirData as $key=>$val){
                    if($onlyFiles){
                        $x = is_file($dir.'/'.$val) ? $val : '';
                        if($x)$result[$key] = $val;
        
                    }else{
                        if(!is_dir($dir.'/'.$val))continue; // eliminate all files and only display folders
                        else {
                            $x = strtolower(substr($val, -4));
                            if($x) $result[$key]="{$val}";
                        }
        
                    }
                }
            }
            else {
                $result = alert('<p class="notice"> The folder is empty</p>');
                 
            }
        }
        else
        {
            $result = alert("Oops! '$dir' does not exist");
        }
        
        return $result;
    }
    
    
    
    
    
############################ translation table ##########################

    function populateTranslation($trans_ref, $lang_text, $lang_file='') {
    		$db = $_ENV['DB'];
    		$translation = $db->translation()->where('trans_ref = ? AND site = ?', $trans_ref, $_SESSION['site']);

    		foreach($translation as $id=>$row) {
    			return $row['to_lang'];
    		}
    		

    	
  	  	return $to_lang === false ? $from_lang : $to_lang;
    }

   function getTranslation($trans_ref){
        $db = $_ENV['DB'];
        $translation = $db->translation()->where('trans_ref = ? AND site = ?', $trans_ref, $_SESSION['site']);
        
		foreach($translation as $id=>$row) {
			return iterator_to_array($row);
		}	
        
        return false;        
    }
    
    function translated($trans_ref, $from_lang=null){        
        $rs = getTranslation($trans_ref);
        
        return $rs == false ? $from_lang : $rs;
    }
    
    
    function isTranslated($trans_ref){
    		$rs = getTranslation($trans_ref);    
    		return $rs['from_lang'] == null ? false : true;
    }
    
    function insertTranslations($data){
        $db = $_ENV['DB'];
        $Translation = $db->translation();
        
            print_r( "updating the Translation table in the database");
            
            foreach($data as $key=>$val) {
                $dt['from_lang'] = $val;
                $trans = $Translation->insert($dt);
            
                if ($trans['id'] < 1) {
                    return "Oops! an error occurred whilst writing <i>$val</i>";
                }
            
                echo "records inserted: $key";
            }

    }
    
    
    /**
     *
     * @param array $data
     * @param object $db
     * @return boolean | array
     */
    function insertTranslation($data){
        $data['site'] = $_SESSION['site'];
        $data['target_lang'] = LANG;
        $data['created_at'] = date('Y-m-d h:i:s');
        $data['created_by'] = $_SESSION['login'];
        $db = $_ENV['DB'];

        $Translation = $db->translation();
        $rs = $Translation->insert($data);
    
        return $rs;
      }
      
      /**
       * 
       * @param array $data
       * @param object $db
       * @return boolean | array
       */
      function saveTranslation($data){
         $db = $_ENV['DB'];
		 $rs = false;
          $translation = $db->translation()->where('trans_ref = ? AND site = ?', $data['trans_ref'], $_SESSION['site']);

          foreach ($translation as $row) {
              $data['site'] = $_SESSION['site'];
              $data['target_lang'] = LANG;
              $data['modified_at'] = date('Y-m-d h:i:s');
              $data['modified_by'] = $_SESSION['login'];

              $rs = $translation->update($data);              
          } 

		 if ($rs === false) {              
             $rs = insertTranslation($data);      
          }
          
         return $rs;
      }
            

    function getAllTranslations($attr=''){

        $db = $_ENV['DB'];
        $rows = $db->translation()->where('target_lang = ? AND site = ?', LANG, $_SESSION['site']);
        
        $rs = array();
        
        foreach($rows as $key=>$row){
        		$rs[$key] = iterator_to_array($row);
        		$rs[$key]['status'] = $row['status'] ? 'Translated' : 'Not Translated';
        }
        
        return $rs;
        
    }

/**
 * @param $data array
 * @return array|string
 */
    function login($data){
       
        $db = $_ENV['DB'];
        $pword = md5($data['password']);
        $username = $data['username'];
        
        $row = $db->users()->where('username = ?', $username);

        foreach($row as $id=>$val) {
            if (!$id) {
                return 'User does not exist';

            } elseif ($pword === $val['password']) {
                $data['user_id'] = $id;
                $data['message'] = sprintf('%s logged in', $id);
                $data['ip'] = $_SERVER['REMOTE_ADDR'];

                $_SESSION['login'] = $id;
                $_SESSION['lang'] = $val['language_id'];
                $_SESSION['site'] = $val['site'];
                $_SESSION['CSRF'] = uniqid('TRS', TRUE);

                logActivity($data);
                return array($id);

            } else {
                return 'Password do not match!';
            }
        }
    }
    
    
    function logActivity($data){
        $db = $_ENV['DB'];
        $log = $db->logs();
		$data['user_id'] = $_SESSION['login'];
		$data['ip'] = $_SERVER['REMOTE_ADDR'];
        $data['logged_at'] = date('Y-m-d H:i:s');
        
        $rs = $log->insert($data);    
    
        return $rs;
    
    }
    
    
 //============================================ translation ends ==============

    

    function force_download($filename = '', $data = null)  {
	    	if ($filename == '' ) 	{
	    		return FALSE;
	    	}
     
	    	$data = $data == null ?  file_get_contents($filename) : $data;
	    	
    		$mime = 'application/x-httpd-php';
	    		    
	    	// Generate the server headers
	    	if (strpos($_SERVER['HTTP_USER_AGENT'], "MSIE") !== FALSE)
	    	{
	    		header('Content-Type: '.$mime);
	    		header('Content-Disposition: attachment; filename="'.$filename.'"');
	    		header('Expires: 0');
	    		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    		header("Content-Transfer-Encoding: binary");
	    		header('Pragma: public');
	    		header("Content-Length: ".strlen($data));
	    	}
	    	else
	    	{
	    		header('Content-Type: '.$mime);
	    		header('Content-Disposition: attachment; filename="'.$filename.'"');
	    		header("Content-Transfer-Encoding: binary");
	    		header('Expires: 0');
	    		header('Pragma: no-cache');
	    	//	header("Content-Length: ".strlen($data));
	    	}
    
    		exit($data);
    }

