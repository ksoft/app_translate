<?php

error_reporting(0);
/* AJAX check  */
if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
	/* special ajax here */
	die('PROHIBITED FILE ACCESS!');
}

/* not ajax, do more.... */


include "config.php";

$post = $_POST;
$rs['ok'] = false;

if (isset($post)){

	if ( $_SESSION['login'] AND $_SESSION['CSRF']  != $post['csrf']) {
// 		$rs['msg'] = 'Token mismatch, please login and try again!';

// 		echo json_encode($rs);
// 		exit;
	}

	unset($post['csrf']);


   //login
	if ($post['action'] == 'login') {
		$r = login($post);
		$rs['msg'] = ('Please enter the correct login details');

		if (intval($r)){
			$rs['ok'] = true;
			$rs['msg'] = "Login successful";
		}

		echo json_encode($rs);
		exit;
	}

	// commit changes to the translation table
	if ($post['action'] == 'save_translation') {
		unset($post['action']);
		$rs['msg'] = "Oops! an error occurred whilst saving data";

		if (empty($post['to_lang'])) {
			$rs['msg'] = 'You cannot save a blank translation';
			echo json_encode($rs);
			exit();
		}

		$post['status'] = 1;
		$res = saveTranslation($post);

		if ($res) {
			$rs['ok'] = TRUE;
			$rs['msg'] = is_object($res) ? 'Translation was added successfully' : 'Translation was saved successfully';
		}

		echo json_encode($rs);
		exit();
	}


	// commit changes to the translation table
	if ($post['action'] == 'fetch_translation') {
		$rs['msg'] = "No records found for this translation";
		$translation = getTranslation($post['trans_ref']);

		if ($translation) {
			$rs['ok'] = TRUE;
			$rs['msg'] = 'Record found';
			$rs['to_lang'] = html_entity_decode($translation['to_lang']);
			$rs['from_lang'] = html_entity_decode($translation['from_lang']);
		}

		echo json_encode($rs);
		exit();
	}


	// commit changes to the email template
	if ($post['action'] == 'save_email_tpl') {
		if (!empty($post)){
		   // strip off the html line breaks

			$r = saveTranslatedEmail($post['file_path'], $post['file_content']);
		}

		if ($r) {
			$rs['ok'] = TRUE;
			$rs['msg'] = sprintf('<b>%s</b> was successfully saved', $post['file_path']);
		}
	}
	// ------------- end



	// fetch email file content
	if ($post['action'] == 'get_email_tpl_content'){
		//$file = $_ENV['Output_Email_Dir'].$post['file_name'];
		$rs['ok'] = TRUE;
		$filename = $_ENV['Output_Email_Dir'].$post['file_name'];

	   // $rs['content'] = (trs_readFile($filename));

		 $str = file_get_contents($filename);
		 $rs['content'] = mb_convert_encoding($str, "UTF-8", "auto");
	}
	
	

	// =============== download language file
	if ($post['action'] == 'download_language'){

		$r = generateLanguageFile();

		if ($r === true) {
			$rs['file_name'] = $_ENV['DOWNLOAD_FILE'] ;
			$rs['ok'] = TRUE;
			$rs['msg'] = 'Language File generated successfully';
		}

		echo json_encode($rs);
		exit();
	}



	echo json_encode($rs);
}







