Dear __NAME__,

We are pleased to confirm the renewal of your hosting package for the following domain name(s):

	__ITEMS__

Thank you for your business!

Best regards,

Support team
__PARTNER__