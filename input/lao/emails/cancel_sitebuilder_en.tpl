Dear __NAME__,

Your website builder package for the following domain name has now expired. Your account has been downgraded to the default package and any premium features on your website will no longer work.

	__DOMAIN__

If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.

Best regards,

Support team
__PARTNER__