Dear __NAME__,

We are pleased to confirm your recent purchase(s) through our website, as listed below. 

	__ITEMS__

To manage your domain name settings, and other services, please login to your account at __LOGIN__.
__LINKTEXT__ __LINK__
__AMMSG__

If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.

Thank you for your business!

Support team
__PARTNER__