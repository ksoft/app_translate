Dear __NAME__,

Thank you for your recent purchase of Private Whois for the following domain name(s):

	__ITEMS__

You can disable Private Whois, if required, by logging into your account and clicking on the domain name.

__LOGIN__

If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.

Best regards,

Support team
__PARTNER__