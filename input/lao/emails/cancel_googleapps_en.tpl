Dear __NAME__,

Your __PLAN__ package for the following domain name has now expired and your Google APPS account is suspended. This means you will no longer have access to email communications via Google.
You MUST renew your Google APPS subscription within 4 days or your subscription will be cancelled; it will not be possible to reactivate your subscription after it has been cancelled.

You may also log into your Google Apps admin console to export all your emails and/or documents before the four day grace period expires.

__DOMAIN__

If you have any questions, or we can be of assistance in any other matter, please don't hesitate to contact us.

Best regards,

Support team
__PARTNER__