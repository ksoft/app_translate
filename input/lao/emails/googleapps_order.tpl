Dear __NAME__,

We are pleased to confirm your order for Google Apps has been completed for the following domain name:

__ITEMS__

To setup your Google Apps account or purchase additional services, please login to your account at __LOGIN__.

If you have any questions, or we can be of assistance in any other matter, please don't hesitate to contact us.

Thank you for your business!

Best regards,

Support team
__PARTNER__