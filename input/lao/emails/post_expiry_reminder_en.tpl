Dear __NAME__,

The following domain name expired __DAYS__ days ago and has been deactivated:

	__DOMAIN__

To make payment for the renewal of the domain name please login to your account at __LOGIN__. 

Failure to renew will result in it being scheduled for deletion, and you will be charged an additional fee to restore it. For further information on domain restore fees please visit the FAQs page of our website.

If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.

Best regards,

Support team
__PARTNER__