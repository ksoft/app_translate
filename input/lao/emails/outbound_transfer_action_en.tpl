Dear __NAME__,

Your pending request to transfer the following domain name to another registrar has been __ACTION__:

	__DOMAIN__

Please contact us if you have any questions regarding this notice.

Best regards,

Support team
__PARTNER__
