Dear __NAME__,

We are pleased to confirm your order has been completed for the following domain name(s):

	__ITEMS__

To manage your domain name settings, and purchase additional services, please login to your account at __LOGIN__.
__LINKTEXT__ __LINK__

If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.

Thank you for your business!

Best regards,

Support team
__PARTNER__