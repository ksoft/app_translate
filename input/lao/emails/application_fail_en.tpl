Dear __NAME__,

Your application for the following domain name has been marked as __STATUS__ by the registry operator:

	__DOMAIN__

There are still a number of great domains available so why not visit __WEBSITE__ and try again.

The domain registration cost will be refunded back to your card, please allow up to 5 working days for this to be processed. If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.


Best regards,

Support team
__PARTNER__