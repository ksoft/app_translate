Dear __NAME__

Re: Transfer of __DOMAIN__

__STORE__ received notification on __DATE__ that you have requested a transfer to another domain name registrar. If you would like to proceed with this transfer you can ignore this message. If you wish to cancel the transfer, please notify us before __ACTIONBY__ by:

Login to our website, as the account holder for this domain name, __TRANSFERPAGE__. If you are not the account holder, please forward this email to the account holder to cancel the transfer.

If we do not hear from you by __ACTIONBY__, the transfer will proceed.  

Best regards,

Support team
__PARTNER__