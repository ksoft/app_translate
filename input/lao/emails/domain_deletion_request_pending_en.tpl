Dear __NAME__,

We are writing to confirm that the following domain name has been scheduled for deletion, further to a deletion request from the account holder __ADDR__.

	__DOMAIN__

If you would like to keep the domain you can restore it for the next 29 days, through your account login, for an additional fee. To do this please login to your account and click 'Restore' next to the domain name. Restore fees are listed on the FAQs page of our website.

Best regards,

Support team
__PARTNER__