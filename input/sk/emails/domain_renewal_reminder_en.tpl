Dear __NAME__,

Our records show that the following domain name is due to expire in
__DAYS__ days, on __DATE__:

    __DOMAIN__

To renew this domain name please login to your account and click ‘Renew’ next to the domain name.

__LOGIN__

If payment is not received by __DELETE__ the domain will be scheduled for deletion.

If you require further assistance, please contact us immediately.

Best regards,

Support team
__PARTNER__