Dear __NAME__,

Thank you for your recent application for registration of the following domain name(s):

	__ITEMS__

Your request(s) have been successfully submitted. Once the current phase closes we will update you further on the success of your application(s).

__PHASE_TEXT__

You can view the status of your application(s) at any time by logging into your account.

__LOGIN__

__LINKTEXT__ __LINK__

If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.

Good luck!

Support team
__PARTNER__