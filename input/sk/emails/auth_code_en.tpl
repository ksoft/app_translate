Dear __NAME__,

We have received a request to send you the transfer code for the following domain name, in order for you to transfer it to another registrar:
	Domain: __DOMAIN__
	Code: __CODE__

If you were unaware of this request, please contact us immediately.

Please note: once the domain is transferred, any other services associated with the domain name will be cancelled. 

If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.

Best Regards,

Support team
__PARTNER__