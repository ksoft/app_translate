Dear __NAME__,

Our records show that the __PLAN__ package for the following domain name is due to expire in __DAYS__ days, on __DATE__:

__DOMAIN__

To renew or upgrade your Google Apps package, please login to your account and click on the renew button next to the domain name.

__LOGIN__

If payment is not received by __DATE__, the Google APPS account will be deleted and you will no longer be able to use the features of your Google APPS account.  This includes email if you have configured Gmail through your domain.

If you require further assistance, please contact us immediately.

Best regards,

Support team
__PARTNER__