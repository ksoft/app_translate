Dear __NAME__,

The following domain name has now expired and, as we have not received payment for its renewal, it has been deactivated:

	__DOMAIN__

To make payment for the renewal of the domain name please login to your account at __LOGIN__. 

Failure to renew will result in it being scheduled for deletion, after which it will become available for registration to other parties. 

If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.

Best regards,

Support team
__PARTNER__