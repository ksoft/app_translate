Dear __NAME__,

Your Google Apps account setup was unsuccessful because this domain is already registered on the Google platform, either to Google or to another reseller


Please kindly follow the instructions at https://support.google.com/work/reseller/answer/6182435?hl=en to generate a transfer token.
Log in to your domain account and click on "Apply Transfer Token" next to your domain.

If you have any questions, or we can be of assistance in any other matter, please don't hesitate to contact us.


Best regards,

Support team
__PARTNER__