Dear __NAME__,

The following domain name expired __DAYS__ days ago and has been deactivated:

	__DOMAIN__

To make payment for the renewal of the domain name please login to your account at __LOGIN__. 

Failure to renew will result in it being scheduled for deletion after which the domain will be made available for registration by another party.

If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.

Best regards,

Support team
__PARTNER__