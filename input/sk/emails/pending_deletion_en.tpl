Dear __NAME__,

Our records show we have not received payment for the renewal of the following domain name, and it has now been scheduled for deletion:

	__DOMAIN__

To restore the domain, and avoid deletion, payment must be made within 40 days. If payment is not received within 40 days your domain will be fully deleted and made available for registration by another party.

For details, please login to your account and click ‘Renew’ next to the domain name. 

__LOGIN__

Best regards,

Support team
__PARTNER__