Dear __NAME__,

Thanks for joining us. Please note you are required to verify the contact information you have given us within __VERIFICATION_DAYS__ days or we may have to suspend your services.

To verify your details, please click the verification link below, or copy and paste the link into your browser window:

__VERIFICATION_URL__

To login, please go to __LOGIN__ and enter the email address and password you entered during the sign up process.

If you forget your password, you can use the 'forgot password' link on the login page of our website to request a password reset.

Best regards,

Support team
__PARTNER__
__PARTNER_EMAIL__