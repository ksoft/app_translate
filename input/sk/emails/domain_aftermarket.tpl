Dear __NAME__,

We are pleased to confirm your order has been completed for the following domain name(s):

	__ITEMS__

As the domain name(s) was purchased from an aftermarket provider, it has been locked until the transfer to us is complete. This can take up to 10 working days. We will inform you once the transfer is completed.

__LINKTEXT__ __LINK__

If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.

Thank you for your business!

Best regards,

Support team
__PARTNER__