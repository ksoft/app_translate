<?php

$LANG='en';//setasappropriate


// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["Activate Advanced DNS Manager for [domain]"]="Activate Advanced DNS Manager for [domain]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_dns_info.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_dns_info.tpl
$STRINGS[$LANG]["Advanced DNS Manager"]="Advanced DNS Manager";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_policies.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_terms_advanced_dns.tpl
$STRINGS[$LANG]["Advanced DNS Terms & Conditions"]="Advanced DNS Terms & Conditions";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Please note: Activating Advanced DNS Manager.."]="Please note: Activating Advanced DNS Manager..";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["will remove any exisitng DNS server entries"]="will remove any exisitng DNS server entries";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["may make your website and e-mail unavailable, until they have been configured through the Advanced DNS Manager"]="may make your website and e-mail unavailable, until they have been configured through the Advanced DNS Manager";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["may take up to 24 hours to propagate"]="may take up to 24 hours to propagate";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["Before [domain] can be activated on Advanced DNS Manager, you must read and agree to the terms and conditions of service."]="Before [domain] can be activated on Advanced DNS Manager, you must read and agree to the terms and conditions of service.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["I have read and accept the Terms and Conditions"]="I have read and accept the Terms and Conditions";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["Please be patient as this can take a couple of minutes to set up"]="Please be patient as this can take a couple of minutes to set up";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["Activate Domain on Advanced DNS"]="Activate Domain on Advanced DNS";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["Current DNS Records for [domain]"]="Current DNS Records for [domain]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["As your domain is using the website builder, editing or deleting these records may cause your website to stop resolving."]="As your domain is using the website builder, editing or deleting these records may cause your website to stop resolving.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_dns.tpl
$STRINGS[$LANG]["As your domain is hosted with us, editing or deleting these records may cause your website to stop resolving or prevent emails being received and delivered."]="As your domain is hosted with us, editing or deleting these records may cause your website to stop resolving or prevent emails being received and delivered.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["Add New DNS Record"]="Add New DNS Record";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["the Fully-Qualified Domain Name (FQDN) such as <strong>www.example.uk.com</strong>"]="the Fully-Qualified Domain Name (FQDN) such as <strong>www.example.uk.com</strong>";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["just a hostname/subdomain, such as <strong>www</strong>, <strong>ftp</strong>, etc."]="just a hostname/subdomain, such as <strong>www</strong>, <strong>ftp</strong>, etc.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["<strong>@</strong> (alias), which will be replaced with the domain name"]="<strong>@</strong> (alias), which will be replaced with the domain name";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["Web Forwarding"]="Web Forwarding";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["Web Hosting Records"]="Web Hosting Records";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["The following records were setup automatically when you purchased web hosting with us.<br />These records are shown here for informational purposes, or if you change your DNS configuration and wish to revert back."]="The following records were setup automatically when you purchased web hosting with us.<br />These records are shown here for informational purposes, or if you change your DNS configuration and wish to revert back.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["Google Apps Records"]="Google Apps Records";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["The following records were setup automatically when you purchased 'Google Apps' with us.<br />These records are shown here for informational purposes, or if you change your DNS configuration and wish to revert back."]="The following records were setup automatically when you purchased 'Google Apps' with us.<br />These records are shown here for informational purposes, or if you change your DNS configuration and wish to revert back.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["Website Builder Records"]="Website Builder Records";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["The following records were setup automatically when you purchased 'Website Builder' with us.<br />These records are shown here for informational purposes, or if you change your DNS configuration and wish to revert back."]="The following records were setup automatically when you purchased 'Website Builder' with us.<br />These records are shown here for informational purposes, or if you change your DNS configuration and wish to revert back.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]["Additional Information"]="Additional Information";



// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_policies.tpl
$STRINGS[$LANG]["Policies & Agreements"]="Policies & Agreements";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_policies.tpl
$STRINGS[$LANG]["Below are policies, terms & conditions and information that apply to products available on this website. If you have any questions or queries, please <a href='/contact-us'>contact us</a>"]="Below are policies, terms & conditions and information that apply to products available on this website. If you have any questions or queries, please <a href='/contact-us'>contact us</a>";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_policies.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_terms_registration.tpl
$STRINGS[$LANG]["Registration Agreement"]="Registration Agreement";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_policies.tpl
$STRINGS[$LANG]["Deletion & Auto-Renew Policy"]="Deletion & Auto-Renew Policy";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_policies.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_dispute.tpl
$STRINGS[$LANG]["Dispute Policy"]="Dispute Policy";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_policies.tpl
$STRINGS[$LANG]["Registrant Benefits & Responsibilities"]="Registrant Benefits & Responsibilities";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_policies.tpl
$STRINGS[$LANG]["Privacy Policy"]="Privacy Policy";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_policies.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_terms.tpl
$STRINGS[$LANG]["Legal Information and Terms of Use"]="Legal Information and Terms of Use";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_policies.tpl
$STRINGS[$LANG]["Private Whois Agreement"]="Private Whois Agreement";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_policies.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Website Builder Terms & Conditions"]="Website Builder Terms & Conditions";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_policies.tpl
$STRINGS[$LANG]["Web Hosting Terms & Conditions"]="Web Hosting Terms & Conditions";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_cv2.tpl
$STRINGS[$LANG]["CV2 Information"]="CV2 Information";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_cv2.tpl
$STRINGS[$LANG]["About the Security Code"]="About the Security Code";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_cv2.tpl
$STRINGS[$LANG]["As an additional security measure, credit and debit cards have a 'CV2' code printed on the signature strip on the back of the card. We ask that you enter this code to prove that you are in physical possession of the card."]="As an additional security measure, credit and debit cards have a 'CV2' code printed on the signature strip on the back of the card. We ask that you enter this code to prove that you are in physical possession of the card.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_cv2.tpl
$STRINGS[$LANG]["Identifying the CV2 code"]="Identifying the CV2 code";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_cv2.tpl
$STRINGS[$LANG]["If you look at the back of the card you will see a series of digits on the signature strip. The CV2 code is the right-most block of numbers in this series."]="If you look at the back of the card you will see a series of digits on the signature strip. The CV2 code is the right-most block of numbers in this series.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_bulk_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_validation.tpl
$STRINGS[$LANG]["Domain Name"]="Domain Name";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_bulk_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Nameservers"]="Nameservers";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_bulk_dns.tpl
$STRINGS[$LANG]["Using Advanced DNS Manager"]="Using Advanced DNS Manager";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_bulk_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_dns_info.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_dns.tpl
$STRINGS[$LANG]["No Nameservers listed."]="No Nameservers listed.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_premiums.tpl
$STRINGS[$LANG]["Our premium domains now come with a low renewal rate of just [currency][price]. Better yet, they are fully transferable to your registrar of choice at any time. Take a look at the great names on offer below - there are over 3000 of them! Or use the search box above to find your preferred domain and register it instantly."]="Our premium domains now come with a low renewal rate of just [currency][price]. Better yet, they are fully transferable to your registrar of choice at any time. Take a look at the great names on offer below - there are over 3000 of them! Or use the search box above to find your preferred domain and register it instantly.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_premiums.tpl
$STRINGS[$LANG]["Take a look at the great names on offer below - there are over 3000 of them! Or use the search box above to find your preferred domain and register it instantly. All premiums are now they are fully transferable to your registrar of choice."]="Take a look at the great names on offer below - there are over 3000 of them! Or use the search box above to find your preferred domain and register it instantly. All premiums are now they are fully transferable to your registrar of choice.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
$STRINGS[$LANG]["Edit Contact Details"]="Edit Contact Details";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
$STRINGS[$LANG]["Any updates to this contact will be reflected in the whois record for all associated domains."]="Any updates to this contact will be reflected in the whois record for all associated domains.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
$STRINGS[$LANG]["<br/>As this contact is being used as an admin contact for a .Asia domain, you can only update the country to another in the Asia Pacific region."]="<br/>As this contact is being used as an admin contact for a .Asia domain, you can only update the country to another in the Asia Pacific region.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["First Name*:"]="First Name*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Last Name*:"]="Last Name*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["Organisation/Company:"]="Organisation/Company:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_country_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Street Address*:"]="Street Address*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["Town/City*:"]="Town/City*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["County:"]="County:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Postcode*:"]="Postcode*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_country_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["City*:"]="City*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["State, Region or Province:"]="State, Region or Province:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_country_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Postal/Zip code*:"]="Postal/Zip code*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Country/Territory:"]="Country/Territory:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_country_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Phone*:"]="Phone*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["Phone Number Format"]="Phone Number Format";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["Phone number should be in the format: +DDD.XXXXXXX. 'DDD' is the dial code, which should not exceed 3 digits. 'XXXXXXX' should not exceed 12 digits."]="Phone number should be in the format: +DDD.XXXXXXX. 'DDD' is the dial code, which should not exceed 3 digits. 'XXXXXXX' should not exceed 12 digits.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_country_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Fax:"]="Fax:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["Fax Number Format"]="Fax Number Format";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["Fax number should be in the format: +DDD.XXXXXXX. 'DDD' is the dial code, which should not exceed 3 digits. 'XXXXXXX' should not exceed 12 digits."]="Fax number should be in the format: +DDD.XXXXXXX. 'DDD' is the dial code, which should not exceed 3 digits. 'XXXXXXX' should not exceed 12 digits.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_forgot_password.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_country_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Email address*:"]="Email address*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
$STRINGS[$LANG]["CED Data"]="CED Data";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
$STRINGS[$LANG]["To update the data below please <a href='/e/contact'>contact us</a>"]="To update the data below please <a href='/e/contact'>contact us</a>";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
$STRINGS[$LANG]["Locality:"]="Locality:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
$STRINGS[$LANG]["Type of Entity:"]="Type of Entity:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
$STRINGS[$LANG]["Form of Identification:"]="Form of Identification:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
$STRINGS[$LANG]["Identification Number:"]="Identification Number:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_mailboxes.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_hosts.tpl
$STRINGS[$LANG]["Update"]="Update";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_edit_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_transfer_code.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_pending_transfers.tpl
$STRINGS[$LANG]["Cancel"]="Cancel";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_transfer_code.tpl
$STRINGS[$LANG]["Transfer Code"]="Transfer Code";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_transfer_code.tpl
$STRINGS[$LANG]["OK"]="OK";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_transfer_code.tpl
$STRINGS[$LANG]["The auth code will be emailed to the admin contact for the domain: [email]"]="The auth code will be emailed to the admin contact for the domain: [email]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_transfer_code.tpl
$STRINGS[$LANG]["Send Transfer Code"]="Send Transfer Code";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_transfer_code.tpl
$STRINGS[$LANG]["You cannot generate a transfer code as this domain is locked"]="You cannot generate a transfer code as this domain is locked";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_transfer_code.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_notifications.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Close"]="Close";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_menu.tpl
$STRINGS[$LANG]["Account Settings"]="Account Settings";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Your Account Details"]="Your Account Details";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["<span class='bold'>Please note:</span> For any new domain names registered in this account, we will take the account details below to populate the contact details for the new domain name(s)."]="<span class='bold'>Please note:</span> For any new domain names registered in this account, we will take the account details below to populate the contact details for the new domain name(s).";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["To change the contact details on your existing domain names, please use the <a href='/account/contacts'>Manage Contacts</a> page."]="To change the contact details on your existing domain names, please use the <a href='/account/contacts'>Manage Contacts</a> page.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["After editing your account details you will need to re-verify your account. An email has been sent to you with instructions on how to verify your account, we will have to suspend your account if you have not verified your account after [verify_days] days."]="After editing your account details you will need to re-verify your account. An email has been sent to you with instructions on how to verify your account, we will have to suspend your account if you have not verified your account after [verify_days] days.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Job Title*:"]="Job Title*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Hotel Name*:"]="Hotel Name*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Hotel Website*:"]="Hotel Website*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Organization:"]="Organization:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_country_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Organisation:"]="Organisation:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Vat Number:"]="Vat Number:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["A VAT Number is required, to qualify for VAT exemption. This is only applicable to businesses."]="A VAT Number is required, to qualify for VAT exemption. This is only applicable to businesses.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["City/Town*:"]="City/Town*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Post Code*:"]="Post Code*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_country_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Country/Territory*:"]="Country/Territory*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_country_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Phone number should contain digits only and not exceed 12 characters in length."]="Phone number should contain digits only and not exceed 12 characters in length.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_country_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Fax number should contain digits only and not exceed 12 characters in length."]="Fax number should contain digits only and not exceed 12 characters in length.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Confirm Email*:"]="Confirm Email*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_login.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Password*:"]="Password*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Enter your password to confirm the changes"]="Enter your password to confirm the changes";


// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Requested Domain Name*:"]="Requested Domain Name*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Requested Domain Name Corresponds To*:"]="Requested Domain Name Corresponds To*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Check all that apply"]="Check all that apply";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Legal trade name"]="Legal trade name";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Commonly known name, acronym, or trademark"]="Commonly known name, acronym, or trademark";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Ticker Symbol"]="Ticker Symbol";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Regulatory ID*:"]="Regulatory ID*:";


// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Select:"]="Select:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Stock Exchange Listed"]="Stock Exchange Listed";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Public Non-Listed"]="Public Non-Listed";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Private"]="Private";


// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Stock Exchange Listing:"]="Stock Exchange Listing:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Ticker Symbol:"]="Ticker Symbol:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Index Inclusion:"]="Index Inclusion:";


// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Other - please specify below:"]="Other - please specify below:";




// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Such items may include but are not limited to the following:"]="Such items may include but are not limited to the following:";


// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["National, Federal, State or Local Business License"]="National, Federal, State or Local Business License";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Articles of Incorporation"]="Articles of Incorporation";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Certificate of Formation"]="Certificate of Formation";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Corporate Operating Agreement"]="Corporate Operating Agreement";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Charter Documents"]="Charter Documents";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Attorney Opinion Letter"]="Attorney Opinion Letter";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Mission Statement"]="Mission Statement";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Update Details"]="Update Details";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Resend Verification Email"]="Resend Verification Email";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Change Password"]="Change Password";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Current Password*:"]="Current Password*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["New Password*:"]="New Password*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Password must be at least 8 characters long, contain at least one one upper case character, one lower case character and a number or non-alphanumeric character ."]="Password must be at least 8 characters long, contain at least one one upper case character, one lower case character and a number or non-alphanumeric character .";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Confirm New Password*:"]="Confirm New Password*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]["Save Password"]="Save Password";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_validation.tpl
$STRINGS[$LANG]["Domain Name Validation"]="Domain Name Validation";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["Search Results"]="Search Results";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["Validation"]="Validation";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["Trademark Claims Notice"]="Trademark Claims Notice";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["Private Whois"]="Private Whois";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/support_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["Website Builder"]="Website Builder";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["Services"]="Services";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["Shopping Cart"]="Shopping Cart";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["Country Validation"]="Country Validation";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["Checkout"]="Checkout";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_validation.tpl
$STRINGS[$LANG]["The domain name(s) below requires trademark validation, please upload the additional information for each domain using the 'Upload' buttons."]="The domain name(s) below requires trademark validation, please upload the additional information for each domain using the 'Upload' buttons.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_validation.tpl
$STRINGS[$LANG]["If you have already have a trademark for your organisation, you will need to register with <a href='http://www.trademark-clearinghouse.com/' target='_blank'>trademark clearing house</a> to obtain a SMD file which can be uploaded below."]="If you have already have a trademark for your organisation, you will need to register with <a href='http://www.trademark-clearinghouse.com/' target='_blank'>trademark clearing house</a> to obtain a SMD file which can be uploaded below.";




// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_validation.tpl
$STRINGS[$LANG]["You can only continue to the checkout once all required data has been submitted."]="You can only continue to the checkout once all required data has been submitted.";



// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_affiliate.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_menu.tpl
$STRINGS[$LANG]["Affiliate Sales"]="Affiliate Sales";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_affiliate.tpl
$STRINGS[$LANG]["Sales & Commission"]="Sales & Commission";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_affiliate.tpl
$STRINGS[$LANG]["Date"]="Date";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_affiliate.tpl
$STRINGS[$LANG]["Sales"]="Sales";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_affiliate.tpl
$STRINGS[$LANG]["Commission"]="Commission";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_affiliate.tpl
$STRINGS[$LANG]["Paid Date"]="Paid Date";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_affiliate.tpl
$STRINGS[$LANG]["Total"]="Total";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_affiliate.tpl
$STRINGS[$LANG]["Converted into your payment currency"]="Converted into your payment currency";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_forgot_password.tpl
$STRINGS[$LANG]["Forgot Password"]="Forgot Password";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_forgot_password.tpl
$STRINGS[$LANG]["If you've forgotten your password, enter the e-mail address you used to register with us into the box below or <br /> one of any domain names you maybe have registered and press the 'Reset' button, and we'll send you a new password in an e-mail."]="If you've forgotten your password, enter the e-mail address you used to register with us into the box below or <br /> one of any domain names you maybe have registered and press the 'Reset' button, and we'll send you a new password in an e-mail.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_forgot_password.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_forgot_password.tpl
$STRINGS[$LANG]["Reset"]="Reset";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_forgot_password.tpl
$STRINGS[$LANG]["Domain Name*:"]="Domain Name*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_forgot_password.tpl
$STRINGS[$LANG]["Click here to login"]="Click here to login";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_forgot_password.tpl
$STRINGS[$LANG]["or"]="or";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_forgot_password.tpl
$STRINGS[$LANG]["create a new account"]="create a new account";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Hello, [user_name]"]="Hello, [user_name]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Your Domain Launch Phase Requests"]="Your Domain Launch Phase Requests";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["All domain requests will be processed at the end of the phase. You will be notified once the domain has been registered."]="All domain requests will be processed at the end of the phase. You will be notified once the domain has been registered.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_pending_transfers.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_outgoing_transfers.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_transfer_history.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_hosts.tpl
$STRINGS[$LANG]["Domain"]="Domain";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Created"]="Created";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Phase"]="Phase";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_pending_transfers.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_transfer_history.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_hosts.tpl
$STRINGS[$LANG]["Status"]="Status";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Your Domain Names"]="Your Domain Names";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Renew"]="Renew";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Edit Nameservers"]="Edit Nameservers";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Expiry Date"]="Expiry Date";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/support_index.tpl
$STRINGS[$LANG]["Google Apps"]="Google Apps";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["This domain is locked"]="This domain is locked";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["This domain is scheduled for deletion"]="This domain is scheduled for deletion";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Pending Verification. Please refer to our FAQs for more information."]="Pending Verification. Please refer to our FAQs for more information.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Expired on [expiry]"]="Expired on [expiry]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Manage Google Apps"]="Manage Google Apps";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Setup in progress"]="Setup in progress";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Verification Failed"]="Verification Failed";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Pending Verification"]="Pending Verification";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Apply Transfer Token"]="Apply Transfer Token";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Token Applied"]="Token Applied";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Order Google Apps"]="Order Google Apps";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Pending Registration"]="Pending Registration";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Live"]="Live";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["On Hold"]="On Hold";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Pending Deletion"]="Pending Deletion";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["If your domain has not been renewed within 45 days of the expiry date it will be deactivated, and you will be charged a [fee] fee to restore it.<br /><br />After a further 30 days the domain will be fully deleted and made available for registration."]="If your domain has not been renewed within 45 days of the expiry date it will be deactivated, and you will be charged a [fee] fee to restore it.<br /><br />After a further 30 days the domain will be fully deleted and made available for registration.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Restore"]="Restore";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["You have no domains registered, please <a href='/'>click here</a> to register a domain"]="You have no domains registered, please <a href='/'>click here</a> to register a domain";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Add-on Services Overview"]="Add-on Services Overview";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Below is a list of services that you have purchased other than domain names."]="Below is a list of services that you have purchased other than domain names.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Service"]="Service";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Expires"]="Expires";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Edit Settings"]="Edit Settings";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["You are downgrading your package.."]="You are downgrading your package..";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Certain features available in your '<span id='current_plan'></span>' package will not be available in the '<span id='new_plan'></span>' package. You will need to remove these features in your 'Website Builder Editor' before you can republish your website."]="Certain features available in your '<span id='current_plan'></span>' package will not be available in the '<span id='new_plan'></span>' package. You will need to remove these features in your 'Website Builder Editor' before you can republish your website.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Are you sure you want to add this package to your cart?"]="Are you sure you want to add this package to your cart?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Continue"]="Continue";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Unlock your Google Apps account"]="Unlock your Google Apps account";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["This domain is already registered to a different reseller. To transfer your account you need to unlock it and provide the transfer token. Please follow this <a href='https://support.google.com/a/answer/71281?hl=en' target='_blank'>link</a> to do so. Come back here to complete your transaction once you've retrieved the transfer token."]="This domain is already registered to a different reseller. To transfer your account you need to unlock it and provide the transfer token. Please follow this <a href='https://support.google.com/a/answer/71281?hl=en' target='_blank'>link</a> to do so. Come back here to complete your transaction once you've retrieved the transfer token.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Your Transfer Token*:"]="Your Transfer Token*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]["Send"]="Send";


// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_completed.tpl
$STRINGS[$LANG]["An email will be sent to the email address you provided which will give you information on how to verify your contact details if required. If you do not verify your details within [lock] days your account/domain(s) will be suspended. This email may end up in your junk folder so make sure you check there too."]="An email will be sent to the email address you provided which will give you information on how to verify your contact details if required. If you do not verify your details within [lock] days your account/domain(s) will be suspended. This email may end up in your junk folder so make sure you check there too.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_completed.tpl
$STRINGS[$LANG]["Please note that only customers of <a href='http://www.reservalia.com' target='_blank'>reservalia.com</a> may register .hoteles domains through this site for free.  Your application will now be reviewed and if  you are found not to be a <a href='http://www.reservalia.com' target='_blank'>reservalia.com</a> customer your domain will be deleted."]="Please note that only customers of <a href='http://www.reservalia.com' target='_blank'>reservalia.com</a> may register .hoteles domains through this site for free.  Your application will now be reviewed and if  you are found not to be a <a href='http://www.reservalia.com' target='_blank'>reservalia.com</a> customer your domain will be deleted.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_completed.tpl
$STRINGS[$LANG]["This email will also confirm the completion of your order for the following domains and/or services:"]="This email will also confirm the completion of your order for the following domains and/or services:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_completed.tpl
$STRINGS[$LANG]["Your Google Apps package is pending provisioning. You will receive an email containing instructions on the next steps to take to complete your order"]="Your Google Apps package is pending provisioning. You will receive an email containing instructions on the next steps to take to complete your order";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_completed.tpl
$STRINGS[$LANG]["Please go to <a href='/account'>your account</a> to manage your domains and services."]="Please go to <a href='/account'>your account</a> to manage your domains and services.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_bulk_dns.tpl
$STRINGS[$LANG]["Bulk DNS Update"]="Bulk DNS Update";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_bulk_dns.tpl
$STRINGS[$LANG]["Current DNS Servers"]="Current DNS Servers";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_bulk_dns.tpl
$STRINGS[$LANG]["Update DNS Servers"]="Update DNS Servers";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_bulk_dns.tpl
$STRINGS[$LANG]["This will delete all existing DNS servers for the above domains and replace with new DNS Servers as specified below."]="This will delete all existing DNS servers for the above domains and replace with new DNS Servers as specified below.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_bulk_dns.tpl
$STRINGS[$LANG]["Override domain names that are using Advanced DNS."]="Override domain names that are using Advanced DNS.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_bulk_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_dns.tpl
$STRINGS[$LANG]["Hostname"]="Hostname";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_bulk_dns.tpl
$STRINGS[$LANG]["Another Nameserver"]="Another Nameserver";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_bulk_dns.tpl
$STRINGS[$LANG]["Update All Domains"]="Update All Domains";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
$STRINGS[$LANG]["Additional Services"]="Additional Services";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
$STRINGS[$LANG]["We offer a number of hosting packages to let you set up a website and/or email address for your chosen domain."]="We offer a number of hosting packages to let you set up a website and/or email address for your chosen domain.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["[emails] POP3 Mailbox Accounts"]="[emails] POP3 Mailbox Accounts";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["[storage] Storage Space"]="[storage] Storage Space";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["[bw] of Bandwidth"]="[bw] of Bandwidth";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["[curr][price]/month"]="[curr][price]/month";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["Add to Cart"]="Add to Cart";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["* Discounts are available on this package for selected TLDs"]="* Discounts are available on this package for selected TLDs";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
$STRINGS[$LANG]["<span class='bold'>Please note:</span> If you are purchasing or renewing multiple domains, any extra services selected will be added to EACH domain. You can remove any of these services for individual domains on the next page."]="<span class='bold'>Please note:</span> If you are purchasing or renewing multiple domains, any extra services selected will be added to EACH domain. You can remove any of these services for individual domains on the next page.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["No thanks"]="No thanks";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_private_registration.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Private Registration"]="Private Registration";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_private_registration.tpl
$STRINGS[$LANG]["When you register a domain your contact information, including name and postal address,<br />will be displayed in the public Whois record."]="When you register a domain your contact information, including name and postal address,<br />will be displayed in the public Whois record.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_private_registration.tpl
$STRINGS[$LANG]["Choose <span class='bold'>Go Private</span> to use Whois Privacy Ltd's contact information instead,<br />and protect yourself from spam, fraud and unsolicited emails."]="Choose <span class='bold'>Go Private</span> to use Whois Privacy Ltd's contact information instead,<br />and protect yourself from spam, fraud and unsolicited emails.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_private_registration.tpl
$STRINGS[$LANG]["<a href='http://www.whoisprivacy.la/agreement' target='_blank'>Privacy terms and conditions apply</a>"]="<a href='http://www.whoisprivacy.la/agreement' target='_blank'>Privacy terms and conditions apply</a>";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
$STRINGS[$LANG][".Asia domains are restricted to applicants with a presence in Asia, as defined by the list of approved Countries and Territories <a href='http://www.dot.asia/policies/DotAsia-Charter-Eligibility--COMPLETE-2010-09-01.pdf' target='_blank'>found here</a>. In order to complete your application we need you need to supply details of your presence in Asia by completing the below information. If you do not have a presence in Asia you may use our Local Presence service for a small fee by selecting the 'Use Local Presence' option below."]=".Asia domains are restricted to applicants with a presence in Asia, as defined by the list of approved Countries and Territories <a href='http://www.dot.asia/policies/DotAsia-Charter-Eligibility--COMPLETE-2010-09-01.pdf' target='_blank'>found here</a>. In order to complete your application we need you need to supply details of your presence in Asia by completing the below information. If you do not have a presence in Asia you may use our Local Presence service for a small fee by selecting the 'Use Local Presence' option below.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
$STRINGS[$LANG]["Use Local Presence"]="Use Local Presence";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
$STRINGS[$LANG]["If you dont have a postal address in Asia, you can use our local presence service for &dollar;10/year."]="If you dont have a postal address in Asia, you can use our local presence service for &dollar;10/year.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
$STRINGS[$LANG]["Administrative Contact"]="Administrative Contact";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
$STRINGS[$LANG]["Use this option if you have a postal address in the Asia Pacific region."]="Use this option if you have a postal address in the Asia Pacific region.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
$STRINGS[$LANG]["Add additional business information (CED)"]="Add additional business information (CED)";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
$STRINGS[$LANG]["Type of Entity*:"]="Type of Entity*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
$STRINGS[$LANG]["Form of Identification*:"]="Form of Identification*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
$STRINGS[$LANG]["Identification Number*:"]="Identification Number*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
$STRINGS[$LANG]["Please select either an option and complete the required information."]="Please select either an option and complete the required information.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_contacts.tpl
$STRINGS[$LANG]["Contact ID"]="Contact ID";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_contacts.tpl
$STRINGS[$LANG]["Name"]="Name";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_contacts.tpl
$STRINGS[$LANG]["Company"]="Company";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_contacts.tpl
$STRINGS[$LANG]["Contact Verified?"]="Contact Verified?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_contacts.tpl
$STRINGS[$LANG]["Verified"]="Verified";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_contacts.tpl
$STRINGS[$LANG]["Verify"]="Verify";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_contacts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_mailboxes.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_hosts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["Delete"]="Delete";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_contacts.tpl
$STRINGS[$LANG]["Edit"]="Edit";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_contacts.tpl
$STRINGS[$LANG]["There are no contacts associated with your account."]="There are no contacts associated with your account.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Unfortunately an error occured while receiving pricing information. You will be redirected back to the home page in a few seconds where you can restart your order."]="Unfortunately an error occured while receiving pricing information. You will be redirected back to the home page in a few seconds where you can restart your order.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["You are only allowed to purchase one domain per registered account. Please register another account if you wish to purchase additional domains. Please refer to our <a href='/support/'>FAQS</a> for naming rules and further details."]="You are only allowed to purchase one domain per registered account. Please register another account if you wish to purchase additional domains. Please refer to our <a href='/support/'>FAQS</a> for naming rules and further details.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Hosting"]="Hosting";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Site Builder"]="Site Builder";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Domain Registraton"]="Domain Registraton";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Hosting packages can be purchased for 1 year only, and are only renewable on an annual basis."]="Hosting packages can be purchased for 1 year only, and are only renewable on an annual basis.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Private Whois purchases will be billed up to the expiry date of the domain name."]="Private Whois purchases will be billed up to the expiry date of the domain name.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Website Builder packages can be purchased for 1 year only, and are only renewable on an annual basis."]="Website Builder packages can be purchased for 1 year only, and are only renewable on an annual basis.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Google Apps purchases will be billed up to the expiry date of the domain name, unless alternative year is chosen. Please contact support if you have any questions "]="Google Apps purchases will be billed up to the expiry date of the domain name, unless alternative year is chosen. Please contact support if you have any questions ";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Including restore fee"]="Including restore fee";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Premium domains are purchased with their existing expiry date, and will need renewing upon expiry at a cost of [curr][price]/year."]="Premium domains are purchased with their existing expiry date, and will need renewing upon expiry at a cost of [curr][price]/year.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Purchase only <br/>Expires [expires]"]="Purchase only <br/>Expires [expires]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG][" year"]=" year";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG][" years"]=" years";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Unlimited"]="Unlimited";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Until current expiry date"]="Until current expiry date";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG][" user"]=" user";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG][" users"]=" users";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["FREE"]="FREE";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Remove item"]="Remove item";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Approximate conversion rate. You will be billed in [currency]."]="Approximate conversion rate. You will be billed in [currency].";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Discount code '[code]' has been applied to this item"]="Discount code '[code]' has been applied to this item";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["If you have a promotional code which entitles you to a discount, please enter it below."]="If you have a promotional code which entitles you to a discount, please enter it below.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Discount applies to total years purchased at initial registration; subsequent renewals will be at full retail price."]="Discount applies to total years purchased at initial registration; subsequent renewals will be at full retail price.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Promo Code:"]="Promo Code:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Apply"]="Apply";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["If you are VAT registered and would like to claim back your VAT or receive exemption, please enter your VAT number below:"]="If you are VAT registered and would like to claim back your VAT or receive exemption, please enter your VAT number below:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["VAT Number:"]="VAT Number:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["If you are VAT registered and would like to claim back your VAT or receive exemption, please proceed to the checkout where you can apply your VAT number."]="If you are VAT registered and would like to claim back your VAT or receive exemption, please proceed to the checkout where you can apply your VAT number.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["If you are VAT registered and would like to claim back your VAT or receive exemption then please click 'Edit Cart' to apply your VAT number."]="If you are VAT registered and would like to claim back your VAT or receive exemption then please click 'Edit Cart' to apply your VAT number.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Proceed to Checkout"]="Proceed to Checkout";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_login.tpl
$STRINGS[$LANG]["Email Address*:"]="Email Address*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_login.tpl
$STRINGS[$LANG]["Log into account"]="Log into account";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_login.tpl
$STRINGS[$LANG]["Forgotten Password?"]="Forgotten Password?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_menu.tpl
$STRINGS[$LANG]["Add-on Services"]="Add-on Services";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Use this page to choose additional products/service to add to your domain name.<br />You can purchase multiple products/services, just click <span class='bold'>'Add to cart'</span> after choosing your domain name."]="Use this page to choose additional products/service to add to your domain name.<br />You can purchase multiple products/services, just click <span class='bold'>'Add to cart'</span> after choosing your domain name.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Add Services for:"]="Add Services for:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_notifications.tpl
$STRINGS[$LANG]["View Shopping Cart"]="View Shopping Cart";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Google Apps Plans"]="Google Apps Plans";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Show"]="Show";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Hide"]="Hide";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Website Builder Packages"]="Website Builder Packages";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Create your website within minutes. Unlimited FREE trial now available!"]="Create your website within minutes. Unlimited FREE trial now available!";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["month"]="month";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Setup Now"]="Setup Now";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["Back to top"]="Back to top";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Web Hosting Plans"]="Web Hosting Plans";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Choose one of our hosting plans which will enable you to setup a website with personal email addresses."]="Choose one of our hosting plans which will enable you to setup a website with personal email addresses.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["[curr][price] /month"]="[curr][price] /month";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Are you sure?"]="Are you sure?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Do you want to continue with the purchase?"]="Do you want to continue with the purchase?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Website Builder Setup"]="Website Builder Setup";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["To setup your free website builder package we will need to make changes to the DNS settings of your chosen domain name. If you have any web or email hosting services already setup for this domain name they may stop working."]="To setup your free website builder package we will need to make changes to the DNS settings of your chosen domain name. If you have any web or email hosting services already setup for this domain name they may stop working.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
$STRINGS[$LANG]["Before your domain can be activated on Advanced DNS Manager and Website Builder, you must read and agree to the terms and conditions of service."]="Before your domain can be activated on Advanced DNS Manager and Website Builder, you must read and agree to the terms and conditions of service.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_services.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Sitebuilder Service Agreement:"]="Sitebuilder Service Agreement:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["TLD Registrar Solutions does not guarantee the renewal of any domain. It is your responsibility to ensure that you arrange payment for renewal prior to the expiry date of your domain."]="TLD Registrar Solutions does not guarantee the renewal of any domain. It is your responsibility to ensure that you arrange payment for renewal prior to the expiry date of your domain.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["Before Expiry"]="Before Expiry";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["TLD Registrar Solutions will take issue reminder notices to the Registrant and Account Holder email addresses on record at the following times:"]="TLD Registrar Solutions will take issue reminder notices to the Registrant and Account Holder email addresses on record at the following times:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["[day] days prior to the domain expiry date"]="[day] days prior to the domain expiry date";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["After Expiry"]="After Expiry";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["TLD Registrar Solutions will take the following action if you have not made payment of your domain renewal:"]="TLD Registrar Solutions will take the following action if you have not made payment of your domain renewal:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["1 day after the domain expiry date we will suspend your domain and your website will cease functioning."]="1 day after the domain expiry date we will suspend your domain and your website will cease functioning.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["Between 74 and 77 days after the domain expiry date your domain will be deleted and will no longer appear in your account. The domain will be released to the public and available for re-registration."]="Between 74 and 77 days after the domain expiry date your domain will be deleted and will no longer appear in your account. The domain will be released to the public and available for re-registration.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Settings for [domain]"]="Settings for [domain]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["General Information"]="General Information";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["This domain name is locked."]="This domain name is locked.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Registered on:"]="Registered on:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Expires on:"]="Expires on:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Restore Now"]="Restore Now";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Private Whois:"]="Private Whois:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Your contact details are private until [expiry]. <a href='/account/private_registration/[domain]'>Edit Privacy Settings</a>"]="Your contact details are private until [expiry]. <a href='/account/private_registration/[domain]'>Edit Privacy Settings</a>";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Your contact details are public. <a href='/account/private_registration/[domain]'>Click here to make them private.</a>"]="Your contact details are public. <a href='/account/private_registration/[domain]'>Click here to make them private.</a>";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Auth/Transfer code:"]="Auth/Transfer code:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["This domain cannot be transferred"]="This domain cannot be transferred";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Generate Code"]="Generate Code";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Your contact details must be visible in the whois record before a transfer can be initiated. Any unused term will be lost upon transfer to another registrar."]="Your contact details must be visible in the whois record before a transfer can be initiated. Any unused term will be lost upon transfer to another registrar.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Nameserver Configuration"]="Nameserver Configuration";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Website Builder Settings"]="Website Builder Settings";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Website Builder account is locked."]="Website Builder account is locked.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["You are currently on the <strong>'[sitebuilder_plan]'</strong> website builder package, which expires on [sitebuilder_expires]"]="You are currently on the <strong>'[sitebuilder_plan]'</strong> website builder package, which expires on [sitebuilder_expires]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["You are currently on the <strong>'[sitebuilder_plan]'</strong> website builder package"]="You are currently on the <strong>'[sitebuilder_plan]'</strong> website builder package";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Check out our FAQ's section on 'How to use Website Builder'"]="Check out our FAQ's section on 'How to use Website Builder'";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Upgrade Package"]="Upgrade Package";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_notifications.tpl
$STRINGS[$LANG]["Renew Now"]="Renew Now";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Edit Your Website"]="Edit Your Website";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Manage Email Forwarding"]="Manage Email Forwarding";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Web Hosting Settings"]="Web Hosting Settings";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_files.tpl
$STRINGS[$LANG]["Hosting account is locked."]="Hosting account is locked.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Unfortunately we could not retrieve the usage information for your account at this time. Please try again later."]="Unfortunately we could not retrieve the usage information for your account at this time. Please try again later.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Advanced DNS Manager has been deactivated. Your hosting account is no longer configured for this domain. <a href='/account/advanced_dns/[domain]'>Click here to re-activate your hosting account settings</a>."]="Advanced DNS Manager has been deactivated. Your hosting account is no longer configured for this domain. <a href='/account/advanced_dns/[domain]'>Click here to re-activate your hosting account settings</a>.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Storage Used"]="Storage Used";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Bandwidth Used"]="Bandwidth Used";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Mailboxes"]="Mailboxes";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Manage"]="Manage";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["View hosting terms and conditions"]="View hosting terms and conditions";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Learn more about setting up your hosting"]="Learn more about setting up your hosting";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["For requests regarding any of the following please email <a href='mailto:[support]'>[support]</a>:"]="For requests regarding any of the following please email <a href='mailto:[support]'>[support]</a>:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Changing your overage policy to charge on a per GB basis"]="Changing your overage policy to charge on a per GB basis";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Setting up a database"]="Setting up a database";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Enabling PHP, PHPSafe, SSI, CGI, FCGI, Phyton, ASP, WebStats and ErrorDocs"]="Enabling PHP, PHPSafe, SSI, CGI, FCGI, Phyton, ASP, WebStats and ErrorDocs";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Contact Associations"]="Contact Associations";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Below are your contact(s) associated with your domain.<br /><span class='bold'>To update domain contacts, they must be public. To do this disable private whois above.</span>"]="Below are your contact(s) associated with your domain.<br /><span class='bold'>To update domain contacts, they must be public. To do this disable private whois above.</span>";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Below are your contact(s) associated with your domain.<br />To change contact(s), tick the appropiate checkboxes and click 'Change Contacts'."]="Below are your contact(s) associated with your domain.<br />To change contact(s), tick the appropiate checkboxes and click 'Change Contacts'.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_menu.tpl
$STRINGS[$LANG]["Payment History"]="Payment History";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Transaction ID"]="Transaction ID";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_payments.tpl
$STRINGS[$LANG]["Payment Date"]="Payment Date";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_payments.tpl
$STRINGS[$LANG]["Amount"]="Amount";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["View Payment"]="View Payment";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Domain Deletion"]="Domain Deletion";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["If you no longer require your domain you can schedule it for deletion. Once deleted, the status will change to 'Pending Deletion' and, should you wish to restore it, you will be charged a [restore_fee] fee. If the domain is within 5 days of registration it will be fully deleted, and you will not be able to restore it. No refund will be given for the remainder of the registration."]="If you no longer require your domain you can schedule it for deletion. Once deleted, the status will change to 'Pending Deletion' and, should you wish to restore it, you will be charged a [restore_fee] fee. If the domain is within 5 days of registration it will be fully deleted, and you will not be able to restore it. No refund will be given for the remainder of the registration.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Delete Domain"]="Delete Domain";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Restore Domain"]="Restore Domain";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Send Reminders"]="Send Reminders";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Allow to auto-expire"]="Allow to auto-expire";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Website Builder Renewal"]="Website Builder Renewal";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Would you like to renew your current package, or change your package before renewing?"]="Would you like to renew your current package, or change your package before renewing?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Renew Current Package"]="Renew Current Package";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]["Change Package"]="Change Package";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_country_contact.tpl
$STRINGS[$LANG]["Name*:"]="Name*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_country_contact.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["State or Province:"]="State or Province:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
$STRINGS[$LANG]["When you register a domain, details like your name and postal address can be seen in a public database. WHOIS privacy hides these details from only [curr][price]/year per domain name. Choose 'Go Private' to protect yourself from spam, fraud and unsolicited emails."]="When you register a domain, details like your name and postal address can be seen in a public database. WHOIS privacy hides these details from only [curr][price]/year per domain name. Choose 'Go Private' to protect yourself from spam, fraud and unsolicited emails.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
$STRINGS[$LANG]["Stay Public"]="Stay Public";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
$STRINGS[$LANG]["Keep your information on the pubic WHOIS database."]="Keep your information on the pubic WHOIS database.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
$STRINGS[$LANG]["Go Private"]="Go Private";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
$STRINGS[$LANG]["Hide your information in the public WHOIS database from [curr][price]/year.<br />Protection from spam and unsolicited emails."]="Hide your information in the public WHOIS database from [curr][price]/year.<br />Protection from spam and unsolicited emails.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
$STRINGS[$LANG]["<span class='bold'>Please note:</span> If you are purchasing or renewing multiple domains, selecting private WHOIS will be added to EACH domain. You can remove private WHOIS for individual domains on the shopping cart page."]="<span class='bold'>Please note:</span> If you are purchasing or renewing multiple domains, selecting private WHOIS will be added to EACH domain. You can remove private WHOIS for individual domains on the shopping cart page.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
$STRINGS[$LANG]["pr_active_box"]="pr_active_box";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
$STRINGS[$LANG]["pr_inactive_box"]="pr_inactive_box";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_dns_info.tpl
$STRINGS[$LANG]["DNS Loading Error"]="DNS Loading Error";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_dns_info.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Add"]="Add";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["Changes will affect the following contacts: [types]"]="Changes will affect the following contacts: [types]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["You can only select a contact that is verified."]="You can only select a contact that is verified.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["The admin contact for a .Asia domain must have a postal address in the DotAsia Community."]="The admin contact for a .Asia domain must have a postal address in the DotAsia Community.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["New Contact:"]="New Contact:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["Save"]="Save";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["OR"]="OR";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["Create New Contact"]="Create New Contact";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_change_contacts.tpl
$STRINGS[$LANG]["After creating a new contact below, a verification email will be sent to the supplied email address. If you do not verify your contact details within [verify_days] days any domain which uses this contact as a Registrant will be suspended."]="After creating a new contact below, a verification email will be sent to the supplied email address. If you do not verify your contact details within [verify_days] days any domain which uses this contact as a Registrant will be suspended.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_verification.tpl
$STRINGS[$LANG]["WHOIS Verification"]="WHOIS Verification";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_verification.tpl
$STRINGS[$LANG]["Thanks for clicking the link! Your contact details have now been verified and the below are ready for use."]="Thanks for clicking the link! Your contact details have now been verified and the below are ready for use.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_contacts.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_menu.tpl
$STRINGS[$LANG]["Manage Contacts"]="Manage Contacts";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_contacts.tpl
$STRINGS[$LANG]["These are the contacts that are associated with domains in your account."]="These are the contacts that are associated with domains in your account.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_contacts.tpl
$STRINGS[$LANG]["Clicking the 'Verify' button will send a verification link via email to the email address of the contact."]="Clicking the 'Verify' button will send a verification link via email to the email address of the contact.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_contacts.tpl
$STRINGS[$LANG]["Editing a verified contact may mean you need to re-verify the contact data. Any domain which uses the contact as a Registrant will be suspended after [verify_days] days if you haven’t done this so we will send you an email with information on how to re-verify the contact data if it is required."]="Editing a verified contact may mean you need to re-verify the contact data. Any domain which uses the contact as a Registrant will be suspended after [verify_days] days if you haven’t done this so we will send you an email with information on how to re-verify the contact data if it is required.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
$STRINGS[$LANG]["Pay with PayPal"]="Pay with PayPal";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
$STRINGS[$LANG]["Payer's Name:"]="Payer's Name:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
$STRINGS[$LANG]["Payer's Email:"]="Payer's Email:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
$STRINGS[$LANG]["Total Price:"]="Total Price:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Terms & Conditions"]="Terms & Conditions";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Please read and accept the terms and conditions to complete your purchase."]="Please read and accept the terms and conditions to complete your purchase.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Registration Agreement:"]="Registration Agreement:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Web Hosting and Email Service Agreement:"]="Web Hosting and Email Service Agreement:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["I have read and accepted the Terms and Conditions"]="I have read and accepted the Terms and Conditions";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_paypal.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Complete Purchase"]="Complete Purchase";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_error.tpl
$STRINGS[$LANG]["An error has occured"]="An error has occured";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_error.tpl
$STRINGS[$LANG]["An unexpected error has occured or page could not be found."]="An unexpected error has occured or page could not be found.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_error.tpl
$STRINGS[$LANG]["An email has been sent to the technical team. If you require further assistence, please contact <a href='mailto:[support]'>[support]</a>"]="An email has been sent to the technical team. If you require further assistence, please contact <a href='mailto:[support]'>[support]</a>";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_error.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_permission.tpl
$STRINGS[$LANG]["Return to the homepage"]="Return to the homepage";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_error.tpl
$STRINGS[$LANG]["Return to previous page"]="Return to previous page";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_records.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_records.tpl
$STRINGS[$LANG]["Remove"]="Remove";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_records.tpl
$STRINGS[$LANG]["these records are required for parking/web forwarding."]="these records are required for parking/web forwarding.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_pending_transfers.tpl
$STRINGS[$LANG]["Pending Transfers In"]="Pending Transfers In";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_pending_transfers.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_outgoing_transfers.tpl
$STRINGS[$LANG]["Action Date"]="Action Date";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]["Contact"]="Contact";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]["Full Name*:"]="Full Name*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]["Subject*:"]="Subject*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]["Question*:"]="Question*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]["Send Enquiry"]="Send Enquiry";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]["Customer Support"]="Customer Support";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]["TLD Registrar Solutions is committed to providing our customers with first class service. If you need support or have a question about any of our products please send an email to <a href='mailto:[support]'>[support]</a> and we will respond promptly. Whilst we endeavor to answer all of your questions ourselves, on occasion we may have to refer you to one of our partners for help, if we do this we will let you know."]="TLD Registrar Solutions is committed to providing our customers with first class service. If you need support or have a question about any of our products please send an email to <a href='mailto:[support]'>[support]</a> and we will respond promptly. Whilst we endeavor to answer all of your questions ourselves, on occasion we may have to refer you to one of our partners for help, if we do this we will let you know.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]["Complaints"]="Complaints";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]["If you would like to report abuse please email <a href='mailto:[abuse]'>[abuse]</a> and we will deal with your correspondence promptly."]="If you would like to report abuse please email <a href='mailto:[abuse]'>[abuse]</a> and we will deal with your correspondence promptly.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]["Postal Address"]="Postal Address";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]["The [site_url] website is operated by TLD Registrar Solutions (TRS), a company registered in England and Wales with Company Number 07629187. TLD Registrar Solutions is a wholly owned subsidiary of CentralNic Group PLC. The company officers include Ben Crawford, Chairman and Glenn Hayward, Director."]="The [site_url] website is operated by TLD Registrar Solutions (TRS), a company registered in England and Wales with Company Number 07629187. TLD Registrar Solutions is a wholly owned subsidiary of CentralNic Group PLC. The company officers include Ben Crawford, Chairman and Glenn Hayward, Director.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]["If you would like to correspond with us by post you can get in touch at:"]="If you would like to correspond with us by post you can get in touch at:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_menu.tpl
$STRINGS[$LANG]["Account Summary"]="Account Summary";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_menu.tpl
$STRINGS[$LANG]["Notifications"]="Notifications";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_menu.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfers.tpl
$STRINGS[$LANG]["Manage Transfers"]="Manage Transfers";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_menu.tpl
$STRINGS[$LANG]["Register Nameservers"]="Register Nameservers";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_menu.tpl
$STRINGS[$LANG]["active"]="active";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_menu.tpl
$STRINGS[$LANG]["text-danger"]="text-danger";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["<u>User:</u> the User is defined as an individual who is accessing the Services."]="<u>User:</u> the User is defined as an individual who is accessing the Services.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["DISCLAIMER OF WARANTEES: LIMITATION OF LIABILITY"]="DISCLAIMER OF WARANTEES: LIMITATION OF LIABILITY";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["CentralNic USA Ltd shall endeavor to provide the Services in accordance with the standards of the industry."]="CentralNic USA Ltd shall endeavor to provide the Services in accordance with the standards of the industry.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["the User acknowledges that CentralNic USA Ltd has not made any representations or warrantees with respect to the quality of the service or that the service will be accessible on a continuous basis or that such accessibility will not be limited or interrupted from time to time, and CentralNic USA Ltd specifically disclaims any such representations, warrantees, or covenants including without limitation the warrantee of merchantability or fitness for a particular purpose. The User acknowledges that CentralNic USA Ltd shall not be liable or responsible for consequential damages, lost profits, lost email or other damages of any type or nature, even if CentralNic USA Ltd has been advised of such damages. The User acknowledges that sole remedy for any such failure is cancellation of the Service. Hereof the User acknowledges that CentralNic USA Ltd does not monitor or review any of the User's material (as hereafter defined); However, the User acknowledges that CentralNic USA Ltd may, upon receipt of a complaint, with respect to any of the User material or any other reason, prevent the User material from being accessible through the service; provided that CentralNic USA Ltd shall have no obligation to prevent such access and no liability to the User or any third party for preventing access to or failure to prevent access to such material."]="the User acknowledges that CentralNic USA Ltd has not made any representations or warrantees with respect to the quality of the service or that the service will be accessible on a continuous basis or that such accessibility will not be limited or interrupted from time to time, and CentralNic USA Ltd specifically disclaims any such representations, warrantees, or covenants including without limitation the warrantee of merchantability or fitness for a particular purpose. The User acknowledges that CentralNic USA Ltd shall not be liable or responsible for consequential damages, lost profits, lost email or other damages of any type or nature, even if CentralNic USA Ltd has been advised of such damages. The User acknowledges that sole remedy for any such failure is cancellation of the Service. Hereof the User acknowledges that CentralNic USA Ltd does not monitor or review any of the User's material (as hereafter defined); However, the User acknowledges that CentralNic USA Ltd may, upon receipt of a complaint, with respect to any of the User material or any other reason, prevent the User material from being accessible through the service; provided that CentralNic USA Ltd shall have no obligation to prevent such access and no liability to the User or any third party for preventing access to or failure to prevent access to such material.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["the User Materials: Indemnification: Policies"]="the User Materials: Indemnification: Policies";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["the User represents and warrants that any material which the User places on the service, permits to be placed on the service with or without the Users authority, or on the Internet and/or the Web through CentralNic USA Ltd's service, including the User's domain name(s), (&quot;the User Material&quot;) will not:"]="the User represents and warrants that any material which the User places on the service, permits to be placed on the service with or without the Users authority, or on the Internet and/or the Web through CentralNic USA Ltd's service, including the User's domain name(s), (&quot;the User Material&quot;) will not:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["violate the rights of any 3rd party and will not give rise to any claim of such violation,"]="violate the rights of any 3rd party and will not give rise to any claim of such violation,";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["violate any law, rule or regulation of any type of any nature (civil or criminal), including without limitation, laws with respect to obscenity, indecency, harassment, or export controls, or"]="violate any law, rule or regulation of any type of any nature (civil or criminal), including without limitation, laws with respect to obscenity, indecency, harassment, or export controls, or";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["contain or transmit any software disabling devices or internal controls including, without limitation, time bombs, viruses or the like."]="contain or transmit any software disabling devices or internal controls including, without limitation, time bombs, viruses or the like.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["Termination:"]="Termination:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["CentralNic USA Ltd may terminate the User's right to use the service at any time without cause by sending notice to the User. CentralNic USA Ltd may Terminate the User's right to use the service immediately without prior notice on a breach of these Terms And Conditions by the User."]="CentralNic USA Ltd may terminate the User's right to use the service at any time without cause by sending notice to the User. CentralNic USA Ltd may Terminate the User's right to use the service immediately without prior notice on a breach of these Terms And Conditions by the User.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["Changes in Services and Terms And Conditions:"]="Changes in Services and Terms And Conditions:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["Miscellaneous"]="Miscellaneous";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["These Terms and Conditions shall be governed and construed under the laws under the State of Nevada. Any dispute arising out of these Terms and conditions or any Services provided by CentralNic USA Ltd shall be resolved in the Federal or State courts located in Nevada county and Subscriber consents to the jurisdiction of such courts over the User. All notices given by the User shall be in writing and addressed to: 112 North Curry St., Carson City, Nevada 89703, or by fax to +1-775-841-4747. All notices to the User shall be given either in writing, via email, via Fax, or via telephone to the address, email address, fax number or telephone number indicated on the User Signup Form."]="These Terms and Conditions shall be governed and construed under the laws under the State of Nevada. Any dispute arising out of these Terms and conditions or any Services provided by CentralNic USA Ltd shall be resolved in the Federal or State courts located in Nevada county and Subscriber consents to the jurisdiction of such courts over the User. All notices given by the User shall be in writing and addressed to: 112 North Curry St., Carson City, Nevada 89703, or by fax to +1-775-841-4747. All notices to the User shall be given either in writing, via email, via Fax, or via telephone to the address, email address, fax number or telephone number indicated on the User Signup Form.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]["the User acknowledges and agrees that the User will permit CentralNic USA Ltd to use its name and trademarks for promotional and advertising purposes: provided that such use accurately reflects the relationship of the parties. By signing up for services, the User has agreed to these Terms And Conditions. These Terms And Conditions represent the parties entire agreement and understanding, supercede any previous oral or written agreement and may only be modified pursuant to a written agreement or as permitted by paragraph 6 hereof."]="the User acknowledges and agrees that the User will permit CentralNic USA Ltd to use its name and trademarks for promotional and advertising purposes: provided that such use accurately reflects the relationship of the parties. By signing up for services, the User has agreed to these Terms And Conditions. These Terms And Conditions represent the parties entire agreement and understanding, supercede any previous oral or written agreement and may only be modified pursuant to a written agreement or as permitted by paragraph 6 hereof.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_zonefile.tpl
$STRINGS[$LANG]["Zone File for [domain]"]="Zone File for [domain]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_dns.tpl
$STRINGS[$LANG]["Use <a href='/account/advanced_dns/[domain]'>Advanced DNS Manager</a> to configure your own DNS records<br /> (web forwarding, A, MX, CNAME, TXT and SRV Records)."]="Use <a href='/account/advanced_dns/[domain]'>Advanced DNS Manager</a> to configure your own DNS records<br /> (web forwarding, A, MX, CNAME, TXT and SRV Records).";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_dns.tpl
$STRINGS[$LANG]["Save All Changes"]="Save All Changes";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_dns.tpl
$STRINGS[$LANG]["This domain is using the Advanced DNS Manager"]="This domain is using the Advanced DNS Manager";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_dns.tpl
$STRINGS[$LANG]["Edit Records"]="Edit Records";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_dns.tpl
$STRINGS[$LANG]["Deactivate Advanced DNS Manager"]="Deactivate Advanced DNS Manager";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_create.tpl
$STRINGS[$LANG]["Account Sign up"]="Account Sign up";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_domain_contacts.tpl
$STRINGS[$LANG]["Unable to retrieve contact information at this time, please try again later"]="Unable to retrieve contact information at this time, please try again later";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_domain_contacts.tpl
$STRINGS[$LANG]["Change Contacts"]="Change Contacts";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_notifications.tpl
$STRINGS[$LANG]["Your Notifications"]="Your Notifications";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_notifications.tpl
$STRINGS[$LANG]["Expiring in the next [renew_days] days:"]="Expiring in the next [renew_days] days:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_notifications.tpl
$STRINGS[$LANG]["Transfer Requests:"]="Transfer Requests:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_notifications.tpl
$STRINGS[$LANG]["You have received a transfer request(s)."]="You have received a transfer request(s).";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_notifications.tpl
$STRINGS[$LANG]["View Requests"]="View Requests";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_notifications.tpl
$STRINGS[$LANG]["Account Verification:"]="Account Verification:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_notifications.tpl
$STRINGS[$LANG]["Contact(s) Verification:"]="Contact(s) Verification:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_notifications.tpl
$STRINGS[$LANG]["Your Shopping Cart:"]="Your Shopping Cart:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_notifications.tpl
$STRINGS[$LANG]["You have [cart_count] item(s) in your shopping cart."]="You have [cart_count] item(s) in your shopping cart.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
$STRINGS[$LANG]["Domains with Public Whois Information"]="Domains with Public Whois Information";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
$STRINGS[$LANG]["Private whois is available for purchase 90+ days before the domain expiry date. If your domain expires in less than 90 days, you must renew it in order to purchase private whois."]="Private whois is available for purchase 90+ days before the domain expiry date. If your domain expires in less than 90 days, you must renew it in order to purchase private whois.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
$STRINGS[$LANG]["Price *"]="Price *";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
$STRINGS[$LANG]["Added to cart"]="Added to cart";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
$STRINGS[$LANG][" from [currency][price]/year"]=" from [currency][price]/year";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
$STRINGS[$LANG]["* You may be charged less or more than the stated amount as the cost is calculated pro rata depending on when the domain name expires. The exact price will be shown in the shopping cart."]="* You may be charged less or more than the stated amount as the cost is calculated pro rata depending on when the domain name expires. The exact price will be shown in the shopping cart.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
$STRINGS[$LANG]["Your Private Whois Domains"]="Your Private Whois Domains";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
$STRINGS[$LANG]["If you would like to make your whois information public, you can do so below. You can re-enable your privacy settings anytime before the expiry date."]="If you would like to make your whois information public, you can do so below. You can re-enable your privacy settings anytime before the expiry date.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
$STRINGS[$LANG]["Privacy Expiry"]="Privacy Expiry";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
$STRINGS[$LANG]["Re-enable"]="Re-enable";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_private_registration.tpl
$STRINGS[$LANG]["Go Public"]="Go Public";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_mailboxes.tpl
$STRINGS[$LANG]["[used] of [quota] email accounts are currently being used."]="[used] of [quota] email accounts are currently being used.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_mailboxes.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["Email Address"]="Email Address";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_mailboxes.tpl
$STRINGS[$LANG]["New Password"]="New Password";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_mailboxes.tpl
$STRINGS[$LANG]["Administrator"]="Administrator";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_mailboxes.tpl
$STRINGS[$LANG]["Use the 'Update' button to update the password or administrator for that record."]="Use the 'Update' button to update the password or administrator for that record.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_outgoing_transfers.tpl
$STRINGS[$LANG]["Outgoing Transfers"]="Outgoing Transfers";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_outgoing_transfers.tpl
$STRINGS[$LANG]["A request has been received to transfer the following domain(s) away from your account.<br />If you consent to the transfer please click 'Approve' below. If you ignore this request, the domain will automatically be removed from your account and transferred to the account/registrar shown on the action date."]="A request has been received to transfer the following domain(s) away from your account.<br />If you consent to the transfer please click 'Approve' below. If you ignore this request, the domain will automatically be removed from your account and transferred to the account/registrar shown on the action date.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_outgoing_transfers.tpl
$STRINGS[$LANG]["Requested by"]="Requested by";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_outgoing_transfers.tpl
$STRINGS[$LANG]["Approve"]="Approve";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_outgoing_transfers.tpl
$STRINGS[$LANG]["Reject"]="Reject";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_transfer_history.tpl
$STRINGS[$LANG]["Transfer History"]="Transfer History";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_transfer_history.tpl
$STRINGS[$LANG]["Action"]="Action";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_transfer_history.tpl
$STRINGS[$LANG]["Completed"]="Completed";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_qrcode.tpl
$STRINGS[$LANG]["Free QR Code"]="Free QR Code";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_qrcode.tpl
$STRINGS[$LANG]["QR (short for Quick Response) codes are two-dimensional bar codes that can be scanned using a smartphone or other personal media device with a QR Code Reader application."]="QR (short for Quick Response) codes are two-dimensional bar codes that can be scanned using a smartphone or other personal media device with a QR Code Reader application.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_qrcode.tpl
$STRINGS[$LANG]["Below we will generate a QR code for your domain name which will have the URL of your domain encoded in it. You can then save this QR code and place it on flyers, posters, signs, business cards etc. so your customers can scan it and  no longer have to remember or write down your URL."]="Below we will generate a QR code for your domain name which will have the URL of your domain encoded in it. You can then save this QR code and place it on flyers, posters, signs, business cards etc. so your customers can scan it and  no longer have to remember or write down your URL.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_qrcode.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_cart_search_multiple.tpl
$STRINGS[$LANG]["www."]="www.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_qrcode.tpl
$STRINGS[$LANG]["Get Code"]="Get Code";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_qrcode.tpl
$STRINGS[$LANG]["QR Code for "]="QR Code for ";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_qrcode.tpl
$STRINGS[$LANG]["To download this QR code right click on the image and select 'Save image As'. Then navigate to the directory in which you want to save the image and click 'Save'"]="To download this QR code right click on the image and select 'Save image As'. Then navigate to the directory in which you want to save the image and click 'Save'";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_qrcode.tpl
$STRINGS[$LANG]["HTML embed code:"]="HTML embed code:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_qrcode.tpl
$STRINGS[$LANG]["Use this HTML code to diplay your QR code on your website."]="Use this HTML code to diplay your QR code on your website.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_validation.tpl
$STRINGS[$LANG]["Required Data"]="Required Data";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_validation.tpl
$STRINGS[$LANG]["Required Action"]="Required Action";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_validation.tpl
$STRINGS[$LANG]["Signed Marked Data"]="Signed Marked Data";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_validation.tpl
$STRINGS[$LANG]["Uploaded"]="Uploaded";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_validation.tpl
$STRINGS[$LANG]["Upload SMD File"]="Upload SMD File";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_validation.tpl
$STRINGS[$LANG]["Thank you for submitting your validation data, you may now continue with your purchase."]="Thank you for submitting your validation data, you may now continue with your purchase.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_validation.tpl
$STRINGS[$LANG]["Please be aware that your validation data has only passed initial checks. A more thorough check will be performed by the operating registry of the TLD. Should validation checks fail, you will be notified by email and payment will be refunded."]="Please be aware that your validation data has only passed initial checks. A more thorough check will be performed by the operating registry of the TLD. Should validation checks fail, you will be notified by email and payment will be refunded.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["The information below is required to register domain name(s):"]="The information below is required to register domain name(s):";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["About you:"]="About you:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Password must be at least 8 characters long, contain at least one one upper case character, one lower case character and a number or non-alphanumeric character."]="Password must be at least 8 characters long, contain at least one one upper case character, one lower case character and a number or non-alphanumeric character.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Confirm Password*:"]="Confirm Password*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Contact information:"]="Contact information:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["From time to time we may notify you of relevant new products and features on [website] and of promotions from us and our affiliates. You consent to receiving such communications from us."]="From time to time we may notify you of relevant new products and features on [website] and of promotions from us and our affiliates. You consent to receiving such communications from us.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["[client], may use the information you provided above to contact you about domain names and other services which may be of interest to you."]="[client], may use the information you provided above to contact you about domain names and other services which may be of interest to you.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["You will need to verify your details within [verify_days] days. We'll send you an email with the details on how to, so make sure you add us to your safe list."]="You will need to verify your details within [verify_days] days. We'll send you an email with the details on how to, so make sure you add us to your safe list.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]["Agree & Continue"]="Agree & Continue";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["invalid"]="invalid";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["is available"]="is available";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["is unavailable"]="is unavailable";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Premium Domain Names"]="Premium Domain Names";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Premium"]="Premium";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Aftermarket"]="Aftermarket";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Purchase through our aftermarket provider, renew upon expiry at a cost of [curr][price]/year."]="Purchase through our aftermarket provider, renew upon expiry at a cost of [curr][price]/year.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["IDN Domain Names"]="IDN Domain Names";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["The IDN system allows Internet users to use the full alphabet of their language in their domain names. They are no longer restricted to the English A-Z, and can use the full latin character set, as well as characters from other locales, including Chinese and Japanese."]="The IDN system allows Internet users to use the full alphabet of their language in their domain names. They are no longer restricted to the English A-Z, and can use the full latin character set, as well as characters from other locales, including Chinese and Japanese.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["IDN"]="IDN";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Suggestion"]="Suggestion";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Purchase: <b>[curr][price]</b><br />Expires: [expires]"]="Purchase: <b>[curr][price]</b><br />Expires: [expires]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["[year] years for [curr][price]"]="[year] years for [curr][price]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["[year] year for [curr][price]"]="[year] year for [curr][price]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["[year] years"]="[year] years";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["[year] year"]="[year] year";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Added"]="Added";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["$reason"]="$reason";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Already registered"]="Already registered";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Choose your registration phase:"]="Choose your registration phase:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Non-refundable application fee:"]="Non-refundable application fee:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Learn more"]="Learn more";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Renewal price for [domain] is [currency][price]"]="Renewal price for [domain] is [currency][price]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Add all domains to cart"]="Add all domains to cart";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Can't find what you're looking for? Search again..."]="Can't find what you're looking for? Search again...";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Non-refundable application fee of [currency][fee] is included in the price"]="Non-refundable application fee of [currency][fee] is included in the price";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["You can register for this phase between the following dates:"]="You can register for this phase between the following dates:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Opens: [open] UTC"]="Opens: [open] UTC";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Closes: [close] UTC"]="Closes: [close] UTC";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["error"]="error";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]["disabled"]="disabled";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]["Manage Email Accounts"]="Manage Email Accounts";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]["How to Access your Email"]="How to Access your Email";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]["Access your email through this link: <a href='http://webmail.[domain]'>http://webmail.[domain]</a>."]="Access your email through this link: <a href='http://webmail.[domain]'>http://webmail.[domain]</a>.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]["Login with the email address you want to use and type in the password used when setting up the address. If you have forgotten the password you can reset it below."]="Login with the email address you want to use and type in the password used when setting up the address. If you have forgotten the password you can reset it below.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]["To view the DNS records for setting up your email accounts in another client please <a href='/account/advanced_dns/[domain]#records'>click here</a>."]="To view the DNS records for setting up your email accounts in another client please <a href='/account/advanced_dns/[domain]#records'>click here</a>.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]["Only letters and numbers are allowed and it must contain at least one number and one upper case character."]="Only letters and numbers are allowed and it must contain at least one number and one upper case character.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]["Display Name ('From:'):"]="Display Name ('From:'):";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]["When an administrator logs on to Webmail and goes to the admin page, <br/>he or she can see all mailboxes for this domain name in this Web hosting account,<br/> can reset passwords for any or all mailboxes, and can delete any or all mailboxes"]="When an administrator logs on to Webmail and goes to the admin page, <br/>he or she can see all mailboxes for this domain name in this Web hosting account,<br/> can reset passwords for any or all mailboxes, and can delete any or all mailboxes";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]["Create Email Account"]="Create Email Account";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_terms_deletion_renewal.tpl
$STRINGS[$LANG]["Deletion & Auto Renewal Policy"]="Deletion & Auto Renewal Policy";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_view_shopping_cart.tpl
$STRINGS[$LANG]["Your Shopping Cart"]="Your Shopping Cart";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Edit Cart"]="Edit Cart";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Choose Payment Method"]="Choose Payment Method";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["We accept the following credit and debit cards. We do not accept Switch or Maestro cards."]="We accept the following credit and debit cards. We do not accept Switch or Maestro cards.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Pay with Paypal"]="Pay with Paypal";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["These details must match the information that appears on your statement."]="These details must match the information that appears on your statement.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Name on Card*:"]="Name on Card*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Address*:"]="Address*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["State/Provice:"]="State/Provice:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Card Number*:"]="Card Number*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Expiry Date*:"]="Expiry Date*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Security Code*:"]="Security Code*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Our site uses SSL Encryption which allows data to be transmitted over computer networks in a secure manner."]="Our site uses SSL Encryption which allows data to be transmitted over computer networks in a secure manner.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["Important Purchase Notice"]="Important Purchase Notice";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["We urge you to review the .tickets application process and Registry Rules fully <u>before completing your purchase</u>. Note that your payment includes a <u>non-refundable application fee</u> included in the Registration price, there is no guarantee that your chosen domain name/s will be granted if you do not meet the requirements for .tickets or if your application is successfully challenged. (For more information go to <a href='http://tickets.domains/faqs/' target='_blank'>http://tickets.domains/faqs/</a>)."]="We urge you to review the .tickets application process and Registry Rules fully <u>before completing your purchase</u>. Note that your payment includes a <u>non-refundable application fee</u> included in the Registration price, there is no guarantee that your chosen domain name/s will be granted if you do not meet the requirements for .tickets or if your application is successfully challenged. (For more information go to <a href='http://tickets.domains/faqs/' target='_blank'>http://tickets.domains/faqs/</a>).";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]["I have read and accepted the Purchase Notice"]="I have read and accepted the Purchase Notice";


// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfer_request.tpl
$STRINGS[$LANG]["Transfer of [domain]"]="Transfer of [domain]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfer_request.tpl
$STRINGS[$LANG]["Transfer confirmation"]="Transfer confirmation";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfer_request.tpl
$STRINGS[$LANG]["I confirm that I have read the Domain Name Transfer Request Message."]="I confirm that I have read the Domain Name Transfer Request Message.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfer_request.tpl
$STRINGS[$LANG]["I confirm that I wish to proceed with the transfer of <span class='bold'>[domain]</span> from <span class='bold'>[losing]</span> to <span class='bold'>[gaining]</span>."]="I confirm that I wish to proceed with the transfer of <span class='bold'>[domain]</span> from <span class='bold'>[losing]</span> to <span class='bold'>[gaining]</span>.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfer_request.tpl
$STRINGS[$LANG]["Your Name:"]="Your Name:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfer_request.tpl
$STRINGS[$LANG]["Approve Request"]="Approve Request";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfer_request.tpl
$STRINGS[$LANG]["Reject Request"]="Reject Request";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_checkout_login.tpl
$STRINGS[$LANG]["Register Your Domain"]="Register Your Domain";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_checkout_login.tpl
$STRINGS[$LANG]["Are you new to [store]?"]="Are you new to [store]?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_checkout_login.tpl
$STRINGS[$LANG]["Join & continue to checkout"]="Join & continue to checkout";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_checkout_login.tpl
$STRINGS[$LANG]["Log into existing [store] account"]="Log into existing [store] account";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_nameservers.tpl
$STRINGS[$LANG]["Manage Nameservers"]="Manage Nameservers";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_nameservers.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_nameservers.tpl
$STRINGS[$LANG]["Register Nameserver"]="Register Nameserver";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_nameservers.tpl
$STRINGS[$LANG]["The registration of this nameserver requires manual processing. This should take no longer than 48 hours."]="The registration of this nameserver requires manual processing. This should take no longer than 48 hours.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_nameservers.tpl
$STRINGS[$LANG]["You can only register nameservers under domains that you control and you must enter a valid IP address.<br/>If you have been given nameservers by your hosting company and want to add those to your domain you can do so on the settings page for the domain."]="You can only register nameservers under domains that you control and you must enter a valid IP address.<br/>If you have been given nameservers by your hosting company and want to add those to your domain you can do so on the settings page for the domain.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_nameservers.tpl
$STRINGS[$LANG]["Please be aware creating or changing a nameserver can take up to 48 hours."]="Please be aware creating or changing a nameserver can take up to 48 hours.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_nameservers.tpl
$STRINGS[$LANG]["Server Name:"]="Server Name:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_nameservers.tpl
$STRINGS[$LANG]["IP Address:"]="IP Address:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_nameservers.tpl
$STRINGS[$LANG]["Sorry, you cannot manage your nameservers if you haven't registered any domains."]="Sorry, you cannot manage your nameservers if you haven't registered any domains.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_files.tpl
$STRINGS[$LANG]["FTP Client"]="FTP Client";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_files.tpl
$STRINGS[$LANG]["[domain]"]="[domain]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_files.tpl
$STRINGS[$LANG]["Use the FTP Client below to manage your files and folders on your hosting account.<br />You can also your manage your files and folder using an alternative FTP client using the access information below."]="Use the FTP Client below to manage your files and folders on your hosting account.<br />You can also your manage your files and folder using an alternative FTP client using the access information below.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_files.tpl
$STRINGS[$LANG]["FTP Account Access"]="FTP Account Access";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_files.tpl
$STRINGS[$LANG]["FTP Address:"]="FTP Address:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_files.tpl
$STRINGS[$LANG]["Username:"]="Username:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_files.tpl
$STRINGS[$LANG]["Password:"]="Password:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_files.tpl
$STRINGS[$LANG]["Port Number:"]="Port Number:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["Web Forwardings"]="Web Forwardings";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["Your domain name can be forwarded to any URL you choose."]="Your domain name can be forwarded to any URL you choose.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["[domain] is configured to [wfwd_type] to [wfwd_domain]."]="[domain] is configured to [wfwd_type] to [wfwd_domain].";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["Please make sure you enter the full URL including http:// or https://."]="Please make sure you enter the full URL including http:// or https://.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["Domain Redirect - The address you are forwarding to will be shown in the browser"]="Domain Redirect - The address you are forwarding to will be shown in the browser";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["Domain Masking - Your [store] domain will be shown in the browser"]="Domain Masking - Your [store] domain will be shown in the browser";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["Disable Forwarding "]="Disable Forwarding ";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["Your [store] currently configured to redirect to your Blue Book profile page."]="Your [store] currently configured to redirect to your Blue Book profile page.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["Redirect [domain] to your Blue Book profile page."]="Redirect [domain] to your Blue Book profile page.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["Point [domain] to your Houzz website."]="Point [domain] to your Houzz website.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["[domain] is currently configured to point to your Houzz website."]="[domain] is currently configured to point to your Houzz website.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["We have setup your domain to forward to the Houzz web servers, once you have completed your Houzz Seller profile and Site Designer setup, you will need to publish your site to your .build domain.  Your .build domain will begin to display your Houzz page to users.<br> <br>	For instructions on how to publish your Houzz Site Designer to your .build domain please visit the Houzz (support pages) - Link to  <a href=https://support.houzz.com/hc/en-us/articles/201922083-How-do-I-point-my-Site-Designer-site-to-my-domain- target=_blank>Site Designer</a>"]="We have setup your domain to forward to the Houzz web servers, once you have completed your Houzz Seller profile and Site Designer setup, you will need to publish your site to your .build domain.  Your .build domain will begin to display your Houzz page to users.<br> <br>	For instructions on how to publish your Houzz Site Designer to your .build domain please visit the Houzz (support pages) - Link to  <a href=https://support.houzz.com/hc/en-us/articles/201922083-How-do-I-point-my-Site-Designer-site-to-my-domain- target=_blank>Site Designer</a>";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["Houzz – Not registered with Houzz? "]="Houzz – Not registered with Houzz? ";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["Houzz - Registered with Houzz not published"]="Houzz - Registered with Houzz not published";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]["Houzz - Registered with Houzz and published"]="Houzz - Registered with Houzz and published";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfers.tpl
$STRINGS[$LANG]["To request the transfer of a domain name to this account, please enter the domain and auth/transfer code in the form below. An email will then be sent to the domain's administrative contact to validate the request. Once validated the transfer will be initiated."]="To request the transfer of a domain name to this account, please enter the domain and auth/transfer code in the form below. An email will then be sent to the domain's administrative contact to validate the request. Once validated the transfer will be initiated.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfers.tpl
$STRINGS[$LANG]["The transfer will be automatically APPROVED after five calendar days and the domain transferred to this account. If the losing account holder approves the transfer within the five days it will be transferred immediately."]="The transfer will be automatically APPROVED after five calendar days and the domain transferred to this account. If the losing account holder approves the transfer within the five days it will be transferred immediately.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfers.tpl
$STRINGS[$LANG]["You will be notified by email when the transfer is complete."]="You will be notified by email when the transfer is complete.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfers.tpl
$STRINGS[$LANG]["The above rules also apply to transfers between user accounts"]="The above rules also apply to transfers between user accounts";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfers.tpl
$STRINGS[$LANG]["Domain Name:"]="Domain Name:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfers.tpl
$STRINGS[$LANG]["Transfer Code:"]="Transfer Code:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfers.tpl
$STRINGS[$LANG]["Request Transfer"]="Request Transfer";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_transfers.tpl
$STRINGS[$LANG]["Transferring into this account is currently unavailable, please try again later."]="Transferring into this account is currently unavailable, please try again later.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]["Payment ID #[id]"]="Payment ID #[id]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]["(Refunded)"]="(Refunded)";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]["(Cancelled)"]="(Cancelled)";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]["VAT Number"]="VAT Number";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]["Payment Date:"]="Payment Date:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]["Payment Type:"]="Payment Type:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]["Card Information:"]="Card Information:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]["Service Type"]="Service Type";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]["Domain (if applicable)"]="Domain (if applicable)";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]["Year(s)"]="Year(s)";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]["(Deleted)"]="(Deleted)";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]["VAT exemption claimed using VAT number: [vat_number]"]="VAT exemption claimed using VAT number: [vat_number]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]["The transaction will be in the name of [company] on your credit card statement."]="The transaction will be in the name of [company] on your credit card statement.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]["Print Payment"]="Print Payment";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_hosts.tpl
$STRINGS[$LANG]["Current Nameservers"]="Current Nameservers";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_hosts.tpl
$STRINGS[$LANG]["Updating this nameserver requires manual processing. This should take no longer than 48 hours."]="Updating this nameserver requires manual processing. This should take no longer than 48 hours.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_hosts.tpl
$STRINGS[$LANG]["IP Address"]="IP Address";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_hosts.tpl
$STRINGS[$LANG]["Couldn't retrieve nameserver information at this time"]="Couldn't retrieve nameserver information at this time";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
$STRINGS[$LANG]["You have received the following Trademark Notice(s) because you have applied for domain name(s) which matches at least one trademark record submitted to the Trademark Clearinghouse."]="You have received the following Trademark Notice(s) because you have applied for domain name(s) which matches at least one trademark record submitted to the Trademark Clearinghouse.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
$STRINGS[$LANG]["Please read and accept the 'Trademark Claim Notice(s)' for all your domains below. You can only continue with your purchase once all trademark claim notice(s) have been accepted or declined. If you decline a Trademark Notice for a domain, the domain name will be removed from your shopping cart."]="Please read and accept the 'Trademark Claim Notice(s)' for all your domains below. You can only continue with your purchase once all trademark claim notice(s) have been accepted or declined. If you decline a Trademark Notice for a domain, the domain name will be removed from your shopping cart.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
$STRINGS[$LANG]["Continue with purchase"]="Continue with purchase";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_trademark_claim.tpl
$STRINGS[$LANG]["collapsed"]="collapsed";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/support_index.tpl
$STRINGS[$LANG]["FAQs"]="FAQs";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/support_index.tpl
$STRINGS[$LANG]["If you are unable to find an answer to your query, and require further assistance, please email our support team at <a class='bold' href='mailto:[support]'>[support]</a>. We will endeavour to respond within 24 hours but, if you don't see a reply from us, please check your spam filter."]="If you are unable to find an answer to your query, and require further assistance, please email our support team at <a class='bold' href='mailto:[support]'>[support]</a>. We will endeavour to respond within 24 hours but, if you don't see a reply from us, please check your spam filter.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/support_index.tpl
$STRINGS[$LANG]["Support FAQs"]="Support FAQs";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/support_index.tpl
$STRINGS[$LANG]["About [store]"]="About [store]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/support_index.tpl
$STRINGS[$LANG]["How to Setup Email"]="How to Setup Email";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_cart_search.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_cart_search.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_cart_search_multiple.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_cart_search_multiple.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_cart_search_multiple.tpl
$STRINGS[$LANG]["input-blurred"]="input-blurred";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_login.tpl
$STRINGS[$LANG]["Account Login"]="Account Login";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["Email Forwarding"]="Email Forwarding";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["Enter the email address you would like to set up.  "]="Enter the email address you would like to set up.  ";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["Enter the existing email adddress you would like all emails to forward to and click Save Changes."]="Enter the existing email adddress you would like all emails to forward to and click Save Changes.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["Changes take up to 1 hour to take effect."]="Changes take up to 1 hour to take effect.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["You have [rem] email forwarding allowance(s) remaining"]="You have [rem] email forwarding allowance(s) remaining";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["Forward to"]="Forward to";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["Save Changes"]="Save Changes";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_payments.tpl
$STRINGS[$LANG]["Your Payment History"]="Your Payment History";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_payments.tpl
$STRINGS[$LANG]["Click 'View Payments' to see details of the payment in a printable invoice format."]="Click 'View Payments' to see details of the payment in a printable invoice format.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_payments.tpl
$STRINGS[$LANG]["ID"]="ID";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_payments.tpl
$STRINGS[$LANG]["View"]="View";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_payments.tpl
$STRINGS[$LANG]["You have no payments"]="You have no payments";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["(Accepted)"]="(Accepted)";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["You have received this Trademark Notice because you have applied for a domain name which match at least one trademark record submitted to the Trademark Clearinghouse."]="You have received this Trademark Notice because you have applied for a domain name which match at least one trademark record submitted to the Trademark Clearinghouse.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["You may or may not be entitled to register the domain name depending on your intended use and whether it is the same or significantly overlaps with the trademarks listed below. Your rights to register this domain name may or may not be protected as noncommercial use or “fair use” by the laws of your country."]="You may or may not be entitled to register the domain name depending on your intended use and whether it is the same or significantly overlaps with the trademarks listed below. Your rights to register this domain name may or may not be protected as noncommercial use or “fair use” by the laws of your country.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Please read the trademark information below carefully, including the trademarks, jurisdictions, and goods and service for which the trademarks are registered. Please be aware that not all jurisdictions review trademark applications closely, so some of the trademark information below may exist in a national or regional registry which does not conduct a thorough or substantive review of trademark rights prior to registration."]="Please read the trademark information below carefully, including the trademarks, jurisdictions, and goods and service for which the trademarks are registered. Please be aware that not all jurisdictions review trademark applications closely, so some of the trademark information below may exist in a national or regional registry which does not conduct a thorough or substantive review of trademark rights prior to registration.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["If you have questions, you may want to consult an attorney or legal expert on trademarks and intellectual property for guidance."]="If you have questions, you may want to consult an attorney or legal expert on trademarks and intellectual property for guidance.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["If you continue with this registration, you represent that, you have received and you understand this notice and to the best of your knowledge, your registration and use of the requested domain name will not infringe on the trademark rights listed below."]="If you continue with this registration, you represent that, you have received and you understand this notice and to the best of your knowledge, your registration and use of the requested domain name will not infringe on the trademark rights listed below.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["The following [number] Trademarks are listed in the Trademark Clearinghouse:"]="The following [number] Trademarks are listed in the Trademark Clearinghouse:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["The following Trademark is listed in the Trademark Clearinghouse:"]="The following Trademark is listed in the Trademark Clearinghouse:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Mark:"]="Mark:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Jurisdiction:"]="Jurisdiction:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Goods:"]="Goods:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["International Class of Goods and Services or Equivalent if applicable:"]="International Class of Goods and Services or Equivalent if applicable:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["N/A"]="N/A";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Trademark Registrant:"]="Trademark Registrant:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Trademark Registrant Contact:"]="Trademark Registrant Contact:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["You cannot register this [domain] because an error occured while retrieving the necessary trademark claim information."]="You cannot register this [domain] because an error occured while retrieving the necessary trademark claim information.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["If you decline the trademark notice, the domain name will be removed from your shopping cart"]="If you decline the trademark notice, the domain name will be removed from your shopping cart";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Decline"]="Decline";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["You cannot register [domain] because the trademark claim information appears to be invalid."]="You cannot register [domain] because the trademark claim information appears to be invalid.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Please accept or decline the trademark claim notice for [domain]"]="Please accept or decline the trademark claim notice for [domain]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Accept"]="Accept";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Trademark claim notice for [domain], has been successfully accepted."]="Trademark claim notice for [domain], has been successfully accepted.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["Add Website Builder"]="Add Website Builder";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["Easy to use website builder. Get started today!"]="Easy to use website builder. Get started today!";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["Choose from a great range of packages. Unlimited FREE trial now available!"]="Choose from a great range of packages. Unlimited FREE trial now available!";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["More Info"]="More Info";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["All packages are purchasable on an annual basis only."]="All packages are purchasable on an annual basis only.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_site_builder.tpl
$STRINGS[$LANG]["All packages are available via the 'Add-on Services' page if you decided to purchase at a later date."]="All packages are available via the 'Add-on Services' page if you decided to purchase at a later date.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_cart_search_multiple.tpl
$STRINGS[$LANG]["To check the availability of multiple domains, enter each name on a new line in the box below."]="To check the availability of multiple domains, enter each name on a new line in the box below.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_cart_search_multiple.tpl
$STRINGS[$LANG]["Domain Search"]="Domain Search";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_cart_search_multiple.tpl
$STRINGS[$LANG]["Search"]="Search";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_permission.tpl
$STRINGS[$LANG]["Permission Denied"]="Permission Denied";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_permission.tpl
$STRINGS[$LANG]["You do not have access to this page."]="You do not have access to this page.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['Please note any changes made on this page may take up to ']='Please note any changes made on this page may take up to ';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG][' hours to propagate.']=' hours to propagate.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['Host Name*']='Host Name*';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_records.tpl
$STRINGS[$LANG]['Record Type']='Record Type';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_records.tpl
$STRINGS[$LANG]['Priority']='Priority';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_records.tpl
$STRINGS[$LANG]['Value']='Value';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['Add']='Add';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['* The following input is allowed:']='* The following input is allowed:';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['Please consult the Notes below for the valid values for this field.']='Please consult the Notes below for the valid values for this field.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['Web forwarding configuration has been moved to the <a href="/account/domain_settings/[domain]">domain settings</a> page.']='Web forwarding configuration has been moved to the <a href="/account/domain_settings/[domain]">domain settings</a> page.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_records.tpl
$STRINGS[$LANG]['Host Name']='Host Name';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['Host names may only contain the characters a-z, 0-9, periods (to create sub-domains), underscores, and hyphens. If you want to create a <strong>wildcard</strong> record, use the "*" character. If you want to create a record for the domain name itself, use the "@" character.']='Host names may only contain the characters a-z, 0-9, periods (to create sub-domains), underscores, and hyphens. If you want to create a <strong>wildcard</strong> record, use the "*" character. If you want to create a record for the domain name itself, use the "@" character.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>CNAME records:</strong> a given host name may only have a <strong>single</strong> CNAME record - our system will reject multiple CNAMEs for the same host name. You cannot set a CNAME record for the domain name itself. <strong>CNAME records must point to fully-qualified domain names.</strong>']='<strong>CNAME records:</strong> a given host name may only have a <strong>single</strong> CNAME record - our system will reject multiple CNAMEs for the same host name. You cannot set a CNAME record for the domain name itself. <strong>CNAME records must point to fully-qualified domain names.</strong>';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>MX records:</strong> all MX records should have the <em>priority</em> field set. Mail servers will try to deliver mail to the server with the lowest priority listed, moving on to the next one if the first is unavailable. <strong>MX records should point to fully-qualified domain names, not IP addresses.</strong>']='<strong>MX records:</strong> all MX records should have the <em>priority</em> field set. Mail servers will try to deliver mail to the server with the lowest priority listed, moving on to the next one if the first is unavailable. <strong>MX records should point to fully-qualified domain names, not IP addresses.</strong>';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>SPF records:</strong> to add an SPF record to your domain, <a href="http://old.openspf.org/wizard.html?mydomain=']='<strong>SPF records:</strong> to add an SPF record to your domain, <a href="http://old.openspf.org/wizard.html?mydomain=';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['">click here</a> to use the OpenSPF website to generate your SPF record, and then add it as a <strong>TXT</strong> record to your domain.']='">click here</a> to use the OpenSPF website to generate your SPF record, and then add it as a <strong>TXT</strong> record to your domain.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>TXT records:</strong> for security reasons, we limit the maximum size of TXT records to 255 characters.']='<strong>TXT records:</strong> for security reasons, we limit the maximum size of TXT records to 255 characters.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['Key to DNS Record Types']='Key to DNS Record Types';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>Start Of Authority</strong> - the presence of this record in a zone indicates that the server is <em>authoritative</em> for the domain name and includes some basic information, such as the master nameserver, and the e-mail address of the administrator.']='<strong>Start Of Authority</strong> - the presence of this record in a zone indicates that the server is <em>authoritative</em> for the domain name and includes some basic information, such as the master nameserver, and the e-mail address of the administrator.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>Name Server</strong> - NS records indicate the name servers that are authoritative for the given host name. If the host name is "@" then the servers are authoritative for this domain.']='<strong>Name Server</strong> - NS records indicate the name servers that are authoritative for the given host name. If the host name is "@" then the servers are authoritative for this domain.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>IPv4 Address</strong> - specifies the IPv4 address of the given host name. If the host name is "@" then the domain name will resolve to this IP address.']='<strong>IPv4 Address</strong> - specifies the IPv4 address of the given host name. If the host name is "@" then the domain name will resolve to this IP address.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>IPv6 Address</strong> - specifies the IPv6 address of the given host name. If the host name is "@" then the domain name will resolve to this IP address.']='<strong>IPv6 Address</strong> - specifies the IPv6 address of the given host name. If the host name is "@" then the domain name will resolve to this IP address.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>Canonical Name (Alias)</strong> - specifies that the given host name is an <em>alias</em> for the host name specified.']='<strong>Canonical Name (Alias)</strong> - specifies that the given host name is an <em>alias</em> for the host name specified.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>Mail Exchanger</strong> - specifies the mail server for the given host/domain name. Multiple MX records can be specified, with mail servers sorting them by priority.']='<strong>Mail Exchanger</strong> - specifies the mail server for the given host/domain name. Multiple MX records can be specified, with mail servers sorting them by priority.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>Text</strong> - allows the zone operator to insert an arbitrary message into the DNS. This is most commonly used to set SPF records.']='<strong>Text</strong> - allows the zone operator to insert an arbitrary message into the DNS. This is most commonly used to set SPF records.';


// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_cv2.tpl
$STRINGS[$LANG]['The security code is usually the last three digits on the signature strip on the back of your card:']='The security code is usually the last three digits on the signature strip on the back of your card:';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_cv2.tpl
$STRINGS[$LANG]['On American Express cards, the security code is the four digits to the right of the long embossed number on the <strong>front</strong> of the card:']='On American Express cards, the security code is the four digits to the right of the long embossed number on the <strong>front</strong> of the card:';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_premiums.tpl
$STRINGS[$LANG]['Premium Domains']='Premium Domains';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]['Stock Exchange Listed']='Stock Exchange Listed';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]['Public Non-Listed']='Public Non-Listed';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_settings.tpl
$STRINGS[$LANG]['Private']='Private';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]['Use the check boxes to perform quick actions to your domain(s):']='Use the check boxes to perform quick actions to your domain(s):';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]['div.panel']='div.panel';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_index.tpl
$STRINGS[$LANG]['center']='center';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout_completed.tpl
$STRINGS[$LANG]['Checkout Completed']='Checkout Completed';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
$STRINGS[$LANG]['hide']='hide';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_country_validation.tpl
$STRINGS[$LANG]['display']='display';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]['Vat:']='Vat:';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_view_shopping_cart.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_view_payment.tpl
$STRINGS[$LANG]['Total:']='Total:';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]['[pending_deletion] days after the domain expiry date your domain will be placed on "Pending Delete" status and it will enter the redemption period. If you wish to renew your domain whilst the domain is in the redemption period you will be required to pay a redemption fee:']='[pending_deletion] days after the domain expiry date your domain will be placed on "Pending Delete" status and it will enter the redemption period. If you wish to renew your domain whilst the domain is in the redemption period you will be required to pay a redemption fee:';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_domain_settings.tpl
$STRINGS[$LANG]['You can only use verified contact(s) as your contact data, please visit the <a href="/account/contacts">manage contacts</a> page for information on how to verify your contacts.']='You can only use verified contact(s) as your contact data, please visit the <a href="/account/contacts">manage contacts</a> page for information on how to verify your contacts.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_private_whois.tpl
$STRINGS[$LANG]['Add Private WHOIS']='Add Private WHOIS';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_verification.tpl
$STRINGS[$LANG]['An error has occured while trying to verify your contact details. Please <a href="/e/contact">contact our support team</a> to resolve this issue.']='An error has occured while trying to verify your contact details. Please <a href="/e/contact">contact our support team</a> to resolve this issue.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_records.tpl
$STRINGS[$LANG]['* these records are managed by our system.']='* these records are managed by our system.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_records.tpl
$STRINGS[$LANG]['Save Changes']='Save Changes';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_records.tpl
$STRINGS[$LANG]['View Zone File']='View Zone File';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]['For information about what will happen if your domain expires please see our <a href="/terms-deletion-renewal">Deletion & Auto-Renewal Policy</a>']='For information about what will happen if your domain expires please see our <a href="/terms-deletion-renewal">Deletion & Auto-Renewal Policy</a>';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]['If you want to submit a complaint about any of our services please submit your request via the email above with the subject line "Complaint about your service".']='If you want to submit a complaint about any of our services please submit your request via the email above with the subject line "Complaint about your service".';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/e_contact.tpl
$STRINGS[$LANG]['Know your rights as a Registrant? Go to <a href="http://www.icann.org/en/resources/registrars/registrant-rights/benefits" target="_blank">ICANN&#39;s Registrants&#39; Benefits and Responsibilities</a> and <a href="http://www.icann.org/en/resources/registrars/registrant-rights/educational" target="_blank">Registrant Rights and Responsibilities</a> for more information.']='Know your rights as a Registrant? Go to <a href="http://www.icann.org/en/resources/registrars/registrant-rights/benefits" target="_blank">ICANN&#39;s Registrants&#39; Benefits and Responsibilities</a> and <a href="http://www.icann.org/en/resources/registrars/registrant-rights/educational" target="_blank">Registrant Rights and Responsibilities</a> for more information.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]['<u>Services:</u> The following are the Terms And Conditions for use of the service including without limitation DNS, web parking, email and other services which may be offered from time to time by CentralNic USA Ltd for use with your User ID (each feature individually and collectively referred to as the "Services"). Please read them carefully.']='<u>Services:</u> The following are the Terms And Conditions for use of the service including without limitation DNS, web parking, email and other services which may be offered from time to time by CentralNic USA Ltd for use with your User ID (each feature individually and collectively referred to as the "Services"). Please read them carefully.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]['the User shall indemnify and hold CentralNic USA Ltd and its employees, agents, shareholders, officers, directors and successors and assigns harmless from and against any and all claims, damages, liabilities, costs and expenses (including attorneys&#39; fees and costs and settlement costs) ("Damages") arising out of any breach by the Users of any of the representations, warrantees or agreements set forth in the Signup Form or these Terms And Conditions.']='the User shall indemnify and hold CentralNic USA Ltd and its employees, agents, shareholders, officers, directors and successors and assigns harmless from and against any and all claims, damages, liabilities, costs and expenses (including attorneys&#39; fees and costs and settlement costs) ("Damages") arising out of any breach by the Users of any of the representations, warrantees or agreements set forth in the Signup Form or these Terms And Conditions.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]['the User agrees to comply with CentralNic USA Ltd&#39;s policies with respect to the Services, as such policies may be modified from time to time including, but not limited to, CentralNic USA Ltd&#39;s policy against "spamming".']='the User agrees to comply with CentralNic USA Ltd&#39;s policies with respect to the Services, as such policies may be modified from time to time including, but not limited to, CentralNic USA Ltd&#39;s policy against "spamming".';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_terms_advanced_dns.tpl
$STRINGS[$LANG]['CentralNic USA Ltd may modify these Terms And Conditions and may discontinue or revise any and all other aspects of the service in CentralNic USA Ltd&#39;s sole discretion with no notice to the User. The User shall be deemed to have accepted any such changes or modifications by continuing to use the Service. A copy of CentralNic USA Ltd&#39;s standard Terms And Conditions including any such changes made to such Terms And Conditions from time-to-time is available at <a href="http://www.centralnic.com/support/terms/managed-dns">http://www.centralnic.com/support/terms/managed-dns</a> or a new address of which CentralNic USA Ltd gives the User notice.']='CentralNic USA Ltd may modify these Terms And Conditions and may discontinue or revise any and all other aspects of the service in CentralNic USA Ltd&#39;s sole discretion with no notice to the User. The User shall be deemed to have accepted any such changes or modifications by continuing to use the Service. A copy of CentralNic USA Ltd&#39;s standard Terms And Conditions including any such changes made to such Terms And Conditions from time-to-time is available at <a href="http://www.centralnic.com/support/terms/managed-dns">http://www.centralnic.com/support/terms/managed-dns</a> or a new address of which CentralNic USA Ltd gives the User notice.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_notifications.tpl
$STRINGS[$LANG]['You have not verified your account information yet. You may have already recieved an email to do so. If not, you can resend another verification email on the <a href="/account/settings">account settings</a> page.']='You have not verified your account information yet. You may have already recieved an email to do so. If not, you can resend another verification email on the <a href="/account/settings">account settings</a> page.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_notifications.tpl
$STRINGS[$LANG]['You have not verified some of your domain contacts yet. You may have already recieved an email to do so. If not, you can resend another verification email on the <a href="/account/contacts">manage contacts</a> page.']='You have not verified some of your domain contacts yet. You may have already recieved an email to do so. If not, you can resend another verification email on the <a href="/account/contacts">manage contacts</a> page.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_html_view_mailboxes.tpl
$STRINGS[$LANG]['You currently have no email accounts setup. Use the form below to create a new account.']='You currently have no email accounts setup. Use the form below to create a new account.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_create.tpl
$STRINGS[$LANG]['By clicking "Agree and Continue" you agree to our <a href="/terms" target="_blank">Terms of Service</a> including all TLD Registrar Solutions policies, in addition you may agree to the following:']='By clicking "Agree and Continue" you agree to our <a href="/terms" target="_blank">Terms of Service</a> including all TLD Registrar Solutions policies, in addition you may agree to the following:';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]['You can choose to purchase multiple years for any domain on the "Shopping Cart" page']='You can choose to purchase multiple years for any domain on the "Shopping Cart" page';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]['in_cart']='in_cart';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]['div.result_row']='div.result_row';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]['div.phase_row']='div.phase_row';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]['active']='active';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_search_results.tpl
$STRINGS[$LANG]['label.phase_box']='label.phase_box';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]['Existing Email Accounts']='Existing Email Accounts';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]['Create Email Account']='Create Email Account';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG][' Denotes a mandatory field']=' Denotes a mandatory field';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]['Email:']='Email:';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]['@']='@';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]['Password:']='Password:';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]['Repeat Password:']='Repeat Password:';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/account_manage_emails.tpl
$STRINGS[$LANG]['Administrator:']='Administrator:';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]['Payment & Checkout']='Payment & Checkout';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]['Company:']='Company:';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_checkout.tpl
$STRINGS[$LANG]['More information about security codes']='More information about security codes';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]['Warning: If you have a live website on your "[domain]" address setting web forwarding will affect this site. Visitors to your site will be redirected to the new website you choose to forward to. Please be aware of how these changes will affect your existing setup.']='Warning: If you have a live website on your "[domain]" address setting web forwarding will affect this site. Visitors to your site will be redirected to the new website you choose to forward to. Please be aware of how these changes will affect your existing setup.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]['Save']='Save';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]['You will now be redirected to the Houzz account sign up page.']='You will now be redirected to the Houzz account sign up page.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]['You will now be redirected to the Houzz Site Designer publishing page.']='You will now be redirected to the Houzz Site Designer publishing page.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]['We have setup your domain to forward to the Houzz web servers, once you have completed your Houzz Site Designer setup and published it to your .build domain, your .build domain will begin to display your Houzz page to users.<br><br>To publish your Houzz site click on the “Publish” button from within the Site Designer application page, select “Use my domain” and enter your .build domain.']='We have setup your domain to forward to the Houzz web servers, once you have completed your Houzz Site Designer setup and published it to your .build domain, your .build domain will begin to display your Houzz page to users.<br><br>To publish your Houzz site click on the “Publish” button from within the Site Designer application page, select “Use my domain” and enter your .build domain.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]['Domain was successfully setup']='Domain was successfully setup';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]['Your [domain] is now pointing to  your Houzz web servers']='Your [domain] is now pointing to  your Houzz web servers';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_account_webforwarding.tpl
$STRINGS[$LANG]['By clicking the <b>Save changes</b> button we will setup your <b>[domain]</b> to forward to the Houzz web servers.']='By clicking the <b>Save changes</b> button we will setup your <b>[domain]</b> to forward to the Houzz web servers.';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/inc_cart_search.tpl
$STRINGS[$LANG]['table']='table';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/default_duplicate_for_hoteles/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]['For more information concerning the records included in this notice, please visit <a href="http://claims.clearinghouse.org" target="_blank">http://claims.clearinghouse.org</a>']='For more information concerning the records included in this notice, please visit <a href="http://claims.clearinghouse.org" target="_blank">http://claims.clearinghouse.org</a>';









########## HOTELES TRANS SECTION #########################################



// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_policies.tpl
$STRINGS[$LANG]["Policies & Agreements"]="Policies & Agreements";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_policies.tpl
$STRINGS[$LANG]["Below are policies, terms & conditions and information that apply to products available on this website. If you have any questions or queries, please <a href='/contact-us'>contact us</a>"]="Below are policies, terms & conditions and information that apply to products available on this website. If you have any questions or queries, please <a href='/contact-us'>contact us</a>";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_policies.tpl
$STRINGS[$LANG]["Registration Agreement"]="Registration Agreement";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_policies.tpl
$STRINGS[$LANG]["Deletion & Auto-Renew Policy"]="Deletion & Auto-Renew Policy";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_policies.tpl
$STRINGS[$LANG]["Dispute Policy"]="Dispute Policy";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_policies.tpl
$STRINGS[$LANG]["Registrant Benefits & Responsibilities"]="Registrant Benefits & Responsibilities";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_policies.tpl
$STRINGS[$LANG]["Advanced DNS Terms & Conditions"]="Advanced DNS Terms & Conditions";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_policies.tpl
$STRINGS[$LANG]["Privacy Policy"]="Privacy Policy";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_policies.tpl
$STRINGS[$LANG]["Legal Information and Terms of Use"]="Legal Information and Terms of Use";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_policies.tpl
$STRINGS[$LANG]["Private Whois Agreement"]="Private Whois Agreement";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_policies.tpl
$STRINGS[$LANG]["Website Builder Terms & Conditions"]="Website Builder Terms & Conditions";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_policies.tpl
$STRINGS[$LANG]["Web Hosting Terms & Conditions"]="Web Hosting Terms & Conditions";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["Who are TLD Registrar Solutions?"]="Who are TLD Registrar Solutions?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["Can I register a domain name if I'm not yet ready to use it?"]="Can I register a domain name if I'm not yet ready to use it?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["How do I update the contact information for my account?"]="How do I update the contact information for my account?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["How do I modify the details of a Contact associated with a domain in my account?"]="How do I modify the details of a Contact associated with a domain in my account?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["How do I renew a domain name?"]="How do I renew a domain name?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["Can I set up automatic renewal payments?"]="Can I set up automatic renewal payments?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["How do I contact customer support?"]="How do I contact customer support?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: Who are TLD Registrar Solutions?"]="Q: Who are TLD Registrar Solutions?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["A: TLD Registrar Solutions Ltd (TRS) is an ICANN Accredited Registrar who provides domain registration and management services through the [url] website. All agreements entered into on this site, unless otherwise stated, are made with TRS."]="A: TLD Registrar Solutions Ltd (TRS) is an ICANN Accredited Registrar who provides domain registration and management services through the [url] website. All agreements entered into on this site, unless otherwise stated, are made with TRS.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: Can I register a domain name if I'm not yet ready to use it?"]="Q: Can I register a domain name if I'm not yet ready to use it?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["A: Yes, you can leave it parked with [store] for the length of your registration."]="A: Yes, you can leave it parked with [store] for the length of your registration.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: How do I update the contact information for my account?"]="Q: How do I update the contact information for my account?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["A: To update your contact information:"]="A: To update your contact information:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["login using your email address and password"]="login using your email address and password";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["go to 'Account Settings' from the menu"]="go to 'Account Settings' from the menu";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["enter your new contact information into the form fields and click 'Update Details'"]="enter your new contact information into the form fields and click 'Update Details'";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["PLEASE NOTE: If you update the contact information associated with your account you will be asked to re-verify your contact information via email."]="PLEASE NOTE: If you update the contact information associated with your account you will be asked to re-verify your contact information via email.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: How do I modify the details of a Contact associated with a domain in my account?"]="Q: How do I modify the details of a Contact associated with a domain in my account?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["A: To update your Contacts:"]="A: To update your Contacts:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["go to 'Manage Contacts' from the menu"]="go to 'Manage Contacts' from the menu";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["your Contacts will be listed and can be edited from this page. Please note: any changes to a Contact will be reflected across all domain associations"]="your Contacts will be listed and can be edited from this page. Please note: any changes to a Contact will be reflected across all domain associations";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["PLEASE NOTE: In certain cases, if you update the contact information associated with your domain you will be asked to re-verify your contact information via email."]="PLEASE NOTE: In certain cases, if you update the contact information associated with your domain you will be asked to re-verify your contact information via email.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: How do I renew a domain name?"]="Q: How do I renew a domain name?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["A: To renew a domain name:"]="A: To renew a domain name:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["click on the domain name you wish to renew"]="click on the domain name you wish to renew";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["click 'Renew' next to the domain name's expiry date at the top of the page"]="click 'Renew' next to the domain name's expiry date at the top of the page";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["Complete the renewal application form"]="Complete the renewal application form";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["please note: domain names cannot be renewed for 7 days after their initial registration"]="please note: domain names cannot be renewed for 7 days after their initial registration";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: Can I set up automatic renewal payments?"]="Q: Can I set up automatic renewal payments?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["A: No, you must login to your account and make payment each time a domain name needs renewing. We will send you reminders by email before the expiry date."]="A: No, you must login to your account and make payment each time a domain name needs renewing. We will send you reminders by email before the expiry date.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: How do I contact customer support?"]="Q: How do I contact customer support?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["A: Email your enquiry to <a href='mailto:[support]'>[support]</a>."]="A: Email your enquiry to <a href='mailto:[support]'>[support]</a>.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_general_index.tpl
$STRINGS[$LANG]["We endeavour to respond to all enquiries within 24 hours."]="We endeavour to respond to all enquiries within 24 hours.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_index.tpl
$STRINGS[$LANG]["Get your .hoteles name for free*"]="Get your .hoteles name for free*";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_index.tpl
$STRINGS[$LANG]["The new TLD exclusively dedicated to hotels worldwide"]="The new TLD exclusively dedicated to hotels worldwide";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_index.tpl
$STRINGS[$LANG]["What is .hoteles?"]="What is .hoteles?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_index.tpl
$STRINGS[$LANG][".hoteles is a new top-level domain (such as .com or .net) which is exclusively dedicated to hotels and their organizations worldwide."]=".hoteles is a new top-level domain (such as .com or .net) which is exclusively dedicated to hotels and their organizations worldwide.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_index.tpl
$STRINGS[$LANG]["Thanks to .hoteles, now you can have the perfect domain name for your hotel. It is ideal for members of the global hotel community that aim at building a trustworthy and clear online identity, while also advertising their services to a wider audience."]="Thanks to .hoteles, now you can have the perfect domain name for your hotel. It is ideal for members of the global hotel community that aim at building a trustworthy and clear online identity, while also advertising their services to a wider audience.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_index.tpl
$STRINGS[$LANG]["Why .hoteles?"]="Why .hoteles?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_index.tpl
$STRINGS[$LANG]["Having a .hoteles top level domain enables hotel site owners to:"]="Having a .hoteles top level domain enables hotel site owners to:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_index.tpl
$STRINGS[$LANG]["Get .hoteles for free with Reservalia"]="Get .hoteles for free with Reservalia";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_index.tpl
$STRINGS[$LANG]["Reservalia enables hotels to enhance their online presence through a dedicated, a brand new website with a built-in booking engine to handle reservations directly, independently from OTAs. Websites are fully responsive for smartphones and tablets and SEO optimised. By joining Reservalia, hotels will be able to accept multiple payment methods and process online bookings 24/7."]="Reservalia enables hotels to enhance their online presence through a dedicated, a brand new website with a built-in booking engine to handle reservations directly, independently from OTAs. Websites are fully responsive for smartphones and tablets and SEO optimised. By joining Reservalia, hotels will be able to accept multiple payment methods and process online bookings 24/7.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_index.tpl
$STRINGS[$LANG]["For more information go to <a href='http://www.reservalia.com' target='_blank'>www.reservalia.com</a>."]="For more information go to <a href='http://www.reservalia.com' target='_blank'>www.reservalia.com</a>.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_index.tpl
$STRINGS[$LANG]["How to obtain .hoteles"]="How to obtain .hoteles";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_index.tpl
$STRINGS[$LANG]["Members of the global hotel community such as hoteliers, hotel chains and hotel associations are eligible to register a domain .hoteles. To do so, the applicant must follow these steps:"]="Members of the global hotel community such as hoteliers, hotel chains and hotel associations are eligible to register a domain .hoteles. To do so, the applicant must follow these steps:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_login.tpl
$STRINGS[$LANG]["Email Address*:"]="Email Address*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_login.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Password*:"]="Password*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_login.tpl
$STRINGS[$LANG]["Log into account"]="Log into account";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_login.tpl
$STRINGS[$LANG]["Forgotten Password?"]="Forgotten Password?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
$STRINGS[$LANG][".HOTELES"]=".HOTELES";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
$STRINGS[$LANG]["Please wait..."]="Please wait...";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
$STRINGS[$LANG]["Menu"]="Menu";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
$STRINGS[$LANG]["Search"]="Search";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
$STRINGS[$LANG]["Spanish"]="Spanish";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
$STRINGS[$LANG]["English"]="English";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
$STRINGS[$LANG]["Home"]="Home";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/footer.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/support_index.tpl
$STRINGS[$LANG]["FAQs"]="FAQs";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/footer.tpl
$STRINGS[$LANG]["Policies"]="Policies";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
$STRINGS[$LANG]["Contact"]="Contact";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
$STRINGS[$LANG]["Login"]="Login";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
$STRINGS[$LANG]["Sign Up"]="Sign Up";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
$STRINGS[$LANG]["Account"]="Account";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
$STRINGS[$LANG]["Logout"]="Logout";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
$STRINGS[$LANG]["Cart (<span id='cart_count'>0</span>)"]="Cart (<span id='cart_count'>0</span>)";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/footer.tpl
$STRINGS[$LANG]["Please enter your allocated domain Token to continue."]="Please enter your allocated domain Token to continue.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/footer.tpl
$STRINGS[$LANG]["This service is only available to Resevalia customers"]="This service is only available to Resevalia customers";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/footer.tpl
$STRINGS[$LANG]["Please contact your registrar to proceed."]="Please contact your registrar to proceed.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/footer.tpl
$STRINGS[$LANG]["By browsing our site, you're agreeing to the use of cookies. Cookies are tiny bits of data websites store in your web browser to make your online experience better."]="By browsing our site, you're agreeing to the use of cookies. Cookies are tiny bits of data websites store in your web browser to make your online experience better.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/footer.tpl
$STRINGS[$LANG]["Whois Lookup"]="Whois Lookup";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/footer.tpl
$STRINGS[$LANG]["Terms of Use"]="Terms of Use";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/footer.tpl
$STRINGS[$LANG]["Privacy"]="Privacy";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/footer.tpl
$STRINGS[$LANG]["Contact Us"]="Contact Us";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/footer.tpl
$STRINGS[$LANG]["Report Abuse"]="Report Abuse";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/footer.tpl
$STRINGS[$LANG]["&copy; [year] www.registrar.hoteles. All rights reserved."]="&copy; [year] www.registrar.hoteles. All rights reserved.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_create.tpl
$STRINGS[$LANG]["Account Sign up"]="Account Sign up";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["The information below is required to register domain name(s)."]="The information below is required to register domain name(s).";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Items marked with an * are required."]="Items marked with an * are required.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["About you:"]="About you:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["First Name*:"]="First Name*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Last Name*:"]="Last Name*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Job Title*:"]="Job Title*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Email address*:"]="Email address*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Confirm Email*:"]="Confirm Email*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Password must be at least 8 characters long, contain at least one one upper case character, one lower case character and a number or non-alphanumeric character."]="Password must be at least 8 characters long, contain at least one one upper case character, one lower case character and a number or non-alphanumeric character.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Confirm Password*:"]="Confirm Password*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Contact information:"]="Contact information:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Hotel Name*:"]="Hotel Name*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Hotel Website*:"]="Hotel Website*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Street Address*:"]="Street Address*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["City/Town*:"]="City/Town*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["City*:"]="City*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["County:"]="County:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["State or Province:"]="State or Province:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Post Code*:"]="Post Code*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Postal/Zip code*:"]="Postal/Zip code*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Country/Territory*:"]="Country/Territory*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Vat Number:"]="Vat Number:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["A VAT Number is required, to qualify for VAT exemption. This is only applicable to businesses."]="A VAT Number is required, to qualify for VAT exemption. This is only applicable to businesses.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Phone*:"]="Phone*:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Phone number should contain digits only and not exceed 12 characters in length."]="Phone number should contain digits only and not exceed 12 characters in length.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Fax:"]="Fax:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Fax number should contain digits only and not exceed 12 characters in length."]="Fax number should contain digits only and not exceed 12 characters in length.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["From time to time we may notify you of relevant new products and features on [website] and of promotions from us and our affiliates. You consent to receiving such communications from us."]="From time to time we may notify you of relevant new products and features on [website] and of promotions from us and our affiliates. You consent to receiving such communications from us.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["[client], may use the information you provided above to contact you about domain names and other services which may be of interest to you."]="[client], may use the information you provided above to contact you about domain names and other services which may be of interest to you.";


// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]["Agree & Continue"]="Agree & Continue";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Search Results"]="Search Results";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Validation"]="Validation";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Trademark Claims Notice"]="Trademark Claims Notice";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Private Whois"]="Private Whois";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/support_index.tpl
$STRINGS[$LANG]["Website Builder"]="Website Builder";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Services"]="Services";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Shopping Cart"]="Shopping Cart";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Country Validation"]="Country Validation";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Checkout"]="Checkout";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Continue"]="Continue";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["invalid"]="invalid";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["is available"]="is available";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["is unavailable"]="is unavailable";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Premium Domain Names"]="Premium Domain Names";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Premium domains are purchased with their existing expiry date, and will need renewing upon expiry at a cost of [curr][price]/year."]="Premium domains are purchased with their existing expiry date, and will need renewing upon expiry at a cost of [curr][price]/year.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Premium"]="Premium";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Aftermarket"]="Aftermarket";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Purchase through our aftermarket provider, renew upon expiry at a cost of [curr][price]/year."]="Purchase through our aftermarket provider, renew upon expiry at a cost of [curr][price]/year.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["IDN Domain Names"]="IDN Domain Names";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["The IDN system allows Internet users to use the full alphabet of their language in their domain names. They are no longer restricted to the English A-Z, and can use the full latin character set, as well as characters from other locales, including Chinese and Japanese."]="The IDN system allows Internet users to use the full alphabet of their language in their domain names. They are no longer restricted to the English A-Z, and can use the full latin character set, as well as characters from other locales, including Chinese and Japanese.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["IDN"]="IDN";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Suggestion"]="Suggestion";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Purchase: <b>[curr][price]</b><br />Expires: [expires]"]="Purchase: <b>[curr][price]</b><br />Expires: [expires]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["[year] years for [curr][price]"]="[year] years for [curr][price]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["[year] year for [curr][price]"]="[year] year for [curr][price]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["[year] years"]="[year] years";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["[year] year"]="[year] year";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Added"]="Added";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Add"]="Add";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Please enter a valid token for this domain"]="Please enter a valid token for this domain";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["You may have entered an invalid token"]="You may have entered an invalid token";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Already registered"]="Already registered";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Choose your registration phase:"]="Choose your registration phase:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Non-refundable application fee:"]="Non-refundable application fee:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Learn more"]="Learn more";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Renewal price for [domain] is [currency][price]"]="Renewal price for [domain] is [currency][price]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Add all domains to cart"]="Add all domains to cart";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Can't find what you're looking for? Search again..."]="Can't find what you're looking for? Search again...";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Non-refundable application fee of [currency][fee] is included in the price"]="Non-refundable application fee of [currency][fee] is included in the price";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["You can register for this phase between the following dates:"]="You can register for this phase between the following dates:";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Opens: [open] UTC"]="Opens: [open] UTC";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Closes: [close] UTC"]="Closes: [close] UTC";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["Close"]="Close";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["error"]="error";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]["disabled"]="disabled";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_about_index.tpl
$STRINGS[$LANG]["How is .hoteles different from .com?"]="How is .hoteles different from .com?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_about_index.tpl
$STRINGS[$LANG]["What if I already have a website on .com or other TLD?"]="What if I already have a website on .com or other TLD?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_about_index.tpl
$STRINGS[$LANG]["Is a .hoteles name secure?"]="Is a .hoteles name secure?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_about_index.tpl
$STRINGS[$LANG]["Q: How is .hoteles different from .com?"]="Q: How is .hoteles different from .com?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_about_index.tpl
$STRINGS[$LANG]["A: As you may have noticed recently .com is full and for anything and everything from donuts to doorknobs. Whereas .hoteles is an online namespace focused solely on the needs of the building community."]="A: As you may have noticed recently .com is full and for anything and everything from donuts to doorknobs. Whereas .hoteles is an online namespace focused solely on the needs of the building community.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_about_index.tpl
$STRINGS[$LANG]["Q: What if I already have a website on .com or other TLD?"]="Q: What if I already have a website on .com or other TLD?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_about_index.tpl
$STRINGS[$LANG]["A: No problem, keep your existing site. But upgrade to a .hoteles website. Apart from having a great name, which you may have missed out in the .com space. You basically have three options. You can upgrade to a better website at your .hoteles address. We can help you with specialized templates or you can provide your own custom solution. You can redirect your existing .com website to your new .hoteles website and benefit from a larger footprint on the web."]="A: No problem, keep your existing site. But upgrade to a .hoteles website. Apart from having a great name, which you may have missed out in the .com space. You basically have three options. You can upgrade to a better website at your .hoteles address. We can help you with specialized templates or you can provide your own custom solution. You can redirect your existing .com website to your new .hoteles website and benefit from a larger footprint on the web.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_about_index.tpl
$STRINGS[$LANG]["Q: Is a .hoteles name secure?"]="Q: Is a .hoteles name secure?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_support_about_index.tpl
$STRINGS[$LANG]["A: Absolutely. In fact it is even more secure than .com and all the other existing gTLDs. Why is that? The new application requirements mandate that the new gTLDs employ the latest and most advanced security protocols on the web."]="A: Absolutely. In fact it is even more secure than .com and all the other existing gTLDs. Why is that? The new application requirements mandate that the new gTLDs employ the latest and most advanced security protocols on the web.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_checkout_login.tpl
$STRINGS[$LANG]["Register Your Domain"]="Register Your Domain";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_checkout_login.tpl
$STRINGS[$LANG]["Are you new to [store]?"]="Are you new to [store]?";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_checkout_login.tpl
$STRINGS[$LANG]["Log into existing [store] account"]="Log into existing [store] account";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/support_index.tpl
$STRINGS[$LANG]["If you are unable to find an answer to your query, and require further assistance, please email our support team at <a class='bold' href='mailto:[support]'>[support]</a>. We will endeavour to respond within 24 hours but, if you don't see a reply from us, please check your spam filter."]="If you are unable to find an answer to your query, and require further assistance, please email our support team at <a class='bold' href='mailto:[support]'>[support]</a>. We will endeavour to respond within 24 hours but, if you don't see a reply from us, please check your spam filter.";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/support_index.tpl
$STRINGS[$LANG]["About [store]"]="About [store]";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/support_index.tpl
$STRINGS[$LANG]["Support FAQs"]="Support FAQs";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/support_index.tpl
$STRINGS[$LANG]["Google Apps"]="Google Apps";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/support_index.tpl
$STRINGS[$LANG]["How to Setup Email"]="How to Setup Email";

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/e_index.tpl
$STRINGS[$LANG]['Type your hotel name here']='Type your hotel name here';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/header.tpl
$STRINGS[$LANG]['Search for your name here']='Search for your name here';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/footer.tpl
$STRINGS[$LANG]['Domain Token']='Domain Token';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/inc_create.tpl
$STRINGS[$LANG]['By clicking "Agree and Continue" you agree to our <a href="/terms" target="_blank">Terms of Service</a> including all TLD Registrar Solutions policies, in addition you may agree to the following:']='By clicking "Agree and Continue" you agree to our <a href="/terms" target="_blank">Terms of Service</a> including all TLD Registrar Solutions policies, in addition you may agree to the following:';

// File: /home/kwaks/webdev/cnic/registrar-platform/www/application/views/hoteles/cart_search_results.tpl
$STRINGS[$LANG]['You can choose to purchase multiple years for any domain on the "Shopping Cart" page']='You can choose to purchase multiple years for any domain on the "Shopping Cart" page';


$STRINGS[$LANG]['by joining and hosting your website with booking engine service Reservalia.com']='by joining and hosting your website with booking engine service Reservalia.com';



