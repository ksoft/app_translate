Dear __NAME__,

I’m sorry but we were unable to register the following domain name(s) and/or services:

	__ITEMS__

This may be because of an internal system error, in which case we will attempt to fix the problem and complete your purchase. 

We apologise for any inconvenience caused while we investigate, and will notify you as soon as possible of the outcome.

If you have any questions, or would like to cancel this registration request, please let us know.

Support team
__PARTNER__