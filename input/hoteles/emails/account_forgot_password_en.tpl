Dear __NAME__,

Someone (possibly you) requested that we reset your account password.

Your registered email address is: __EMAIL__
Your new password is: __PASSWORD__

Remember that this password is CASE SENSITIVE. To login, go to __LOGIN__.

We recommend you login to your account and reset your password immediately. To do this, click on the "Change Account Password" link.

Best regards,

Support team
__PARTNER__