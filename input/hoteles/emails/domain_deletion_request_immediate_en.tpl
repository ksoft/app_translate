Dear __NAME__,

We are writing to confirm that the following domain name has been deleted, further to a deletion request from the account holder __ADDR__.

	__DOMAIN__

If you believe this deletion was in error please contact us immediately.

Best regards,

Support team
__PARTNER__