Dear __NAME__,

We are pleased to inform you that your __TYPE__ application for the following domain name was successful and the domain is now registered in your account:

	__DOMAIN__

To manage your domain name settings, and other services, please login to your account at __LOGIN__.

If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.

Thank you for your business!

Best regards,

Support team
__PARTNER__