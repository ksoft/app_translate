Attention: __NAME__

Re: Your VAT information

You have recently provided us with VAT information which we have been unable to verify as correct.

As we have been unable to verify this information as correct you are at risk of paying VAT amounts which may not be applicable. Please login to your account and confirm that your VAT information is correct at your earliest convenience.

If you have any questions about this process, please contact __CONTACT__.

Best regards,
Support team
__PARTNER__