Dear __NAME__,

We are pleased to confirm your order has been completed for the following sitebuilder package(s):

	__ITEMS__

To get started with building your website, please login to your account at __LOGIN__, click on your domain and click the 'Edit Website' button.

If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.

Thank you for your business!

Best regards,

Support team
__PARTNER__