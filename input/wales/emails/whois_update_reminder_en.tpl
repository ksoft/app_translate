Dear __NAME__,

You recently made a change to the contact information associated with your __PARTNER__ services. Due to industry regulations we need to verify that this information is correct.

Please help us to do this by clicking on the below link:
	__LINK__

If you don't verify the information your services will be suspended in __DAYS__ day(s).

If you have any questions, or we can be of assistance in any other matter, please don't hesitate to contact us.

Best regards,

Support team
__PARTNER__