Dear __NAME__,

We are pleased to confirm your order for Google Apps has been processed and your Google Apps account/s are now ready for setup.


To setup your Google Apps account please login to your account at __LOGIN__.

You will first need to create an "Administrative User" for each Google Apps account. Please follow the process as shown in the login screen.

If you have any questions, or we can be of assistance in any other matter, please don't hesitate to contact us.

Thank you for your business!

Best regards,

Support team
__PARTNER__