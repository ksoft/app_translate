Dear __NAME__,

We are pleased to inform you that your request to transfer the following domain name to your __STORE__ account has been completed:

  __DOMAIN__

To manage your domain name settings, and purchase additional services, please login to your account at __LOGIN__.

Best regards,

Support team
__PARTNER__