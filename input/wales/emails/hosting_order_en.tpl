Dear __NAME__,

We are pleased to confirm your order has been completed for the following hosting package(s):

	__ITEMS__

To get started with your new hosting package, create email accounts, or upload website files, login to your account and click on the domain name.

__LOGIN__


If you have any questions, or we can be of assistance in any other matter, please don’t hesitate to contact us.

Best regards,

Support team
__PARTNER__