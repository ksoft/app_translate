Dear __NAME__,

We wrote to you recently to inform you that your domain name had expired, and been scheduled for deletion. Our records show we have still not received payment for its renewal, and it has now been deleted. 

	__DOMAIN__

Best regards,

Support team
__PARTNER__