Attention: __NAME__

Re: Transfer of __DOMAIN__

__STORE__ received a request from __REQUESTOR__ on __DATE__ for us to become the new registrar of record.

You have received this message because you are listed as the Registered Name Holder or Administrative contact for this domain name in the WHOIS database.
 
Please read the following important information about transferring your domain name:
	* You must agree to enter into a new Registration Agreement with us. You can review the full terms and conditions of the Agreement at __TC__
	* Once you have entered into the Agreement, the transfer will take place within __AUTO__ calendar days unless the current registrar of record denies the request.
	* Once a transfer takes place, you will not be able to transfer to another registrar for __BLOCK__ days, apart from a transfer back to the original registrar, in cases where both registrars so agree or where a decision in the dispute resolution process so directs. 

If you WISH TO PROCEED with the transfer please login to our website as the account holder for this domain name, using the link below, to confirm. 
__URL__
If you are not the account holder, please forward this email to the account holder to confirm the transfer. 

(Please note: if you do not respond by __EXPIRES__, __DOMAIN__ will not be transferred to us)

If you DO NOT WANT the transfer to proceed, then you can ignore this message.

If you have any questions about this process, please contact __CONTACT__.

Best regards,
Support team
__PARTNER__