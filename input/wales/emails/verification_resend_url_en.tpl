Dear __NAME__,

As part of industry regulations you are required to verify the contact information you have given us or we may have to suspend your services.

To verify your details, please click the verification link below, or copy and paste the link into your browser window:

__VERIFICATION_URL__

We'll remind you again a couple of times before we suspend your services but please do click the link now in case you miss our emails. Please feel free to contact us if you have any questions.

Best regards,

Support team
__PARTNER__
__PARTNER_EMAIL__