<?php

$LANG='cy';//setasappropriate


// File: /registrar-platform/lib/Platform/CardPaymentEngine.php
$STRINGS[$LANG]["No transaction to fulfill"]="No transaction to fulfill";

// File: /registrar-platform/lib/Platform/CardPaymentEngine.php
$STRINGS[$LANG]["Can't find transaction"]="Can't find transaction";

// File: /registrar-platform/bin/process_queue.php
// File: /registrar-platform/bin/submit_ga_applications.php
$STRINGS[$LANG]["\n\nPlease note, as part of industry regulations you are required to verify the contact information you have given us within {$lock} days or we may have to suspend your services.\n\nWe'll remind you again a couple of times before we suspend your services but please do click the link now in case you miss our emails. Please feel free to contact us if you have any questions.\n\nTo verify your details, please click the verification link below, or copy and paste the link into your browser window:\n\n\t"]="\n\nPlease note, as part of industry regulations you are required to verify the contact information you have given us within {$lock} days or we may have to suspend your services.\n\nWe'll remind you again a couple of times before we suspend your services but please do click the link now in case you miss our emails. Please feel free to contact us if you have any questions.\n\nTo verify your details, please click the verification link below, or copy and paste the link into your browser window:\n\n\t";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["\n\nAs one or more of the domain name(s) was purchased from an aftermarket provider, it has been locked until the transfer to us is complete. This can take up to 10 working days. We will inform you once the transfer is completed.\n"]="\n\nAs one or more of the domain name(s) was purchased from an aftermarket provider, it has been locked until the transfer to us is complete. This can take up to 10 working days. We will inform you once the transfer is completed.\n";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Web Hosting for %s"]="Web Hosting for %s";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Your order is complete!"]="Your order is complete!";

// File: /registrar-platform/bin/process_queue.php
// File: /registrar-platform/bin/process_queue.php
// File: /registrar-platform/bin/submit_ga_applications.php
$STRINGS[$LANG]["Your domain name order is complete!"]="Your domain name order is complete!";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Confirmation of hosting renewal!"]="Confirmation of hosting renewal!";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Your hosting order is complete!"]="Your hosting order is complete!";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Confirmation of private whois purchase!"]="Confirmation of private whois purchase!";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Your website builder order is complete!"]="Your website builder order is complete!";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Your Google Apps order is complete!"]="Your Google Apps order is complete!";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["%s - Your application has been submitted"]="%s - Your application has been submitted";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Couldn't fulfill payment for order #%d: %s, items have been provisioned"]="Couldn't fulfill payment for order #%d: %s, items have been provisioned";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Couldn't fulfill payment for order #%d, items have been provisioned"]="Couldn't fulfill payment for order #%d, items have been provisioned";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Couldn't fulfill paypal payment (get) for order #%d: %s, items have been provisioned"]="Couldn't fulfill paypal payment (get) for order #%d: %s, items have been provisioned";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Couldn't authorise paypal payment for order #%d: %s, items have been provisioned"]="Couldn't authorise paypal payment for order #%d: %s, items have been provisioned";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Couldn't authorise paypal payment for order #%s, items have been provisioned for user %s, price %d. Response:%s"]="Couldn't authorise paypal payment for order #%s, items have been provisioned for user %s, price %d. Response:%s";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Couldn't capture paypal payment for order #%d: %s"]="Couldn't capture paypal payment for order #%d: %s";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Couldn't capture paypal payment for order #%s, user %s, price %d. Response:%s"]="Couldn't capture paypal payment for order #%s, user %s, price %d. Response:%s";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Please fix this manually.\n"]="Please fix this manually.\n";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Web Hosting Renewal"]="Web Hosting Renewal";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Renewal of %s for %s"]="Renewal of %s for %s";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Premium Domain Registration of {$object->name}"]="Premiwm Cofrestru Parth o {$object->name}";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Domain Registration of {$object->name}"]="Cofrestru Parth o {$object->name}";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG][" Request"]=" Request";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["%s Registration Request of %s"]="%s Registration Request of %s";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Web Hosting Registration"]="Web Hosting Registration";

// File: /registrar-platform/bin/process_queue.php
// File: /registrar-platform/bin/process_queue.php
// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["%s for %s"]="%s for %s";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Sitebuilder Registration"]="Sitebuilder Registration";

// File: /registrar-platform/bin/process_queue.php
// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["%s Website Builder Package for %s"]="%s Website Builder Package for %s";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Sitebuilder Renewal"]="Sitebuilder Renewal";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Sitebuilder Upgrade"]="Sitebuilder Upgrade";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Upgrade to %s Website Builder Package for %s"]="Upgrade to %s Website Builder Package for %s";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Google Apps Registration"]="Google Apps Registration";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Google Apps Renewal"]="Google Apps Renewal";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Google Apps Upgrade"]="Google Apps Upgrade";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Upgrade to %s for %s"]="Upgrade to %s for %s";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Hosting info not returned: %s"]="Hosting info not returned: %s";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["The %s record was not dropped"]="The %s record was not dropped";

// File: /registrar-platform/bin/process_queue.php
// File: /registrar-platform/bin/process_queue.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error setting up domain {$domain->name} on MDNS"]="Error setting up domain {$domain->name} on MDNS";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["The FTP value was not set"]="The FTP value was not set";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["The new www record was not added for hosting account #%d: %s"]="The new www record was not added for hosting account #%d: %s";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["The www record was not retrieved from hosting info"]="The www record was not retrieved from hosting info";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["The new @ record was not added for hosting account #%d: %s"]="The new @ record was not added for hosting account #%d: %s";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["The @ record was not retrieved from hosting info"]="The @ record was not retrieved from hosting info";

// File: /registrar-platform/bin/process_queue.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error removing conflicting 'CNAME' record for {$type}"]="Error removing conflicting 'CNAME' record for {$type}";

// File: /registrar-platform/bin/process_queue.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error removing conflicting 'A' record for {$type}"]="Error removing conflicting 'A' record for {$type}";

// File: /registrar-platform/bin/process_queue.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["A new DNS record was not added for sitebuilder account #%d: %s"]="A new DNS record was not added for sitebuilder account #%d: %s";

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]["Error mapping domain {$domain->name} to sitebuilder:%s"]="Error mapping domain {$domain->name} to sitebuilder:%s";

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["Please review the following EPP messages and action where necessary: %s"]="Please review the following EPP messages and action where necessary: %s";

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["Please review: error registering allocated application {$id}"]="Please review: error registering allocated application {$id}";

// File: /registrar-platform/bin/process_epp_msgs.php
// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["ERROR in allocation application"]="ERROR in allocation application";

// File: /registrar-platform/bin/process_epp_msgs.php
// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["%s - Your application was successful for %s"]="%s - Your application was successful for %s";

// File: /registrar-platform/bin/process_epp_msgs.php
// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["%s - We're sorry but your application has been unsuccessful"]="%s - We're sorry but your application has been unsuccessful";

// File: /registrar-platform/bin/process_epp_msgs.php
// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["Application for %s (request #%d) for user #%d was unsuccessful. Please refund the registration cost to the customer."]="Application for %s (request #%d) for user #%d was unsuccessful. Please refund the registration cost to the customer.";

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["Please review: error allocating application {$id}"]="Please review: error allocating application {$id}";

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["We've received an EPP message to transfer in '%s' but we don't have an uncompleted transfer in the system."]="We've received an EPP message to transfer in '%s' but we don't have an uncompleted transfer in the system.";

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["No open transfer in system for {$name}"]="No open transfer in system for {$name}";

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["Domain created from transfer"]="Domain created from transfer";

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["Transfer completed, domain record created for '%s'"]="Transfer completed, domain record created for '%s'";

// File: /registrar-platform/bin/process_epp_msgs.php
// File: /registrar-platform/bin/process_inbound_internal.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Transfer completed for %s"]="Transfer completed for %s";

// File: /registrar-platform/bin/process_epp_msgs.php
// File: /registrar-platform/bin/process_inbound_internal.php
// File: /registrar-platform/bin/process_inbound_internal.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Transfer approval email sent to  %s"]="Transfer approval email sent to  %s";

// File: /registrar-platform/bin/process_epp_msgs.php
// File: /registrar-platform/bin/process_epp_msgs.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Important domain transfer update for %s"]="Important domain transfer update for %s";

// File: /registrar-platform/bin/process_epp_msgs.php
// File: /registrar-platform/bin/process_epp_msgs.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Transfer action email sent to  %s"]="Transfer action email sent to  %s";

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["We've received an EPP message to transfer out '%s' but it's not registered with us."]="We've received an EPP message to transfer out '%s' but it's not registered with us.";

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["We were unable to auto reject the outgoing transfer for %s: %s"]="We were unable to auto reject the outgoing transfer for %s: %s";

// File: /registrar-platform/bin/process_epp_msgs.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Transfer Initiated for %s"]="Transfer Initiated for %s";

// File: /registrar-platform/bin/process_epp_msgs.php
// File: /registrar-platform/bin/process_inbound_internal.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Confirmation of domain transfer for %s"]="Confirmation of domain transfer for %s";

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["Transferred away"]="Transferred away";

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]["We were unable to cancel the hosting account for the outgoing domain %s: %s"]="We were unable to cancel the hosting account for the outgoing domain %s: %s";

// File: /registrar-platform/bin/expired_pending_delete.php
// File: /registrar-platform/bin/expired_delete.php
$STRINGS[$LANG]["Domain deletion notice for %s"]="Domain deletion notice for %s";

// File: /registrar-platform/bin/cancel_googleapps.php
// File: /registrar-platform/bin/cancel_sitebuilder.php
$STRINGS[$LANG]["Sitebuilder cancelled for %s"]="Sitebuilder cancelled for %s";

// File: /registrar-platform/bin/import_conversion_rates.php
$STRINGS[$LANG]["Conversion rate error"]="Conversion rate error";

// File: /registrar-platform/bin/expired_hold.php
// File: /registrar-platform/bin/expired_hold.php
// File: /registrar-platform/bin/set_transfer_prohibited.php
// File: /registrar-platform/bin/set_transfer_prohibited.php
$STRINGS[$LANG]["Failed to set transfer lock for %s: %s"]="Failed to set transfer lock for %s: %s";

// File: /registrar-platform/bin/expired_hold.php
// File: /registrar-platform/bin/expired_hold.php
$STRINGS[$LANG]["Failed to put %s on hold: %s"]="Failed to put %s on hold: %s";

// File: /registrar-platform/bin/expired_hold.php
$STRINGS[$LANG]["%s put on hold"]="%s put on hold";

// File: /registrar-platform/bin/expired_hold.php
$STRINGS[$LANG]["Domain expiry notice for %s"]="Domain expiry notice for %s";

// File: /registrar-platform/bin/log_epp_msgs.php
$STRINGS[$LANG]["Processing EPP messages from %s"]="Processing EPP messages from %s";

// File: /registrar-platform/bin/log_epp_msgs.php
$STRINGS[$LANG]["Please investigate error received when polling for epp message with %s: %s"]="Please investigate error received when polling for epp message with %s: %s";

// File: /registrar-platform/bin/log_epp_msgs.php
$STRINGS[$LANG]["No messages to process with %s"]="No messages to process with %s";

// File: /registrar-platform/bin/log_epp_msgs.php
// File: /registrar-platform/bin/log_epp_msgs.php
$STRINGS[$LANG]["Please investigate error received when acking epp message %d with %s: %s"]="Please investigate error received when acking epp message %d with %s: %s";

// File: /registrar-platform/bin/cancel_whois.php
$STRINGS[$LANG]["Private Whois cancelled for %s"]="Private Whois cancelled for %s";

// File: /registrar-platform/bin/verify_googleapps.php
$STRINGS[$LANG]["%s - Your Google Apps Failed Verification"]="%s - Your Google Apps Failed Verification";

// File: /registrar-platform/bin/set_transfer_prohibited.php
$STRINGS[$LANG]["Transfer prohibited flag set for %s"]="Transfer prohibited flag set for %s";

// File: /registrar-platform/bin/whois_data_reminder.php
$STRINGS[$LANG]["%s - Verify your information"]="%s - Verify your information";

// File: /registrar-platform/bin/cancel_hosting.php
$STRINGS[$LANG]["Hosting cancelled for %s"]="Hosting cancelled for %s";

// File: /registrar-platform/bin/renew_premiums.php
$STRINGS[$LANG]["Premium domain %s renewed at the registry"]="Premium domain %s renewed at the registry";

// File: /registrar-platform/bin/renewal_reminders.php
$STRINGS[$LANG]["Domain name expiry email for %s"]="Domain name expiry email for %s";

// File: /registrar-platform/bin/renewal_reminders.php
$STRINGS[$LANG]["Website builder expiry email for %s"]="Website builder expiry email for %s";

// File: /registrar-platform/bin/renewal_reminders.php
$STRINGS[$LANG]["Hosting expiry email for %s"]="Hosting expiry email for %s";

// File: /registrar-platform/bin/whois_on_hold.php
$STRINGS[$LANG]["Failed to set transferProhibited for %s"]="Failed to set transferProhibited for %s";

// File: /registrar-platform/bin/whois_on_hold.php
$STRINGS[$LANG]["transferProhibited set for %s due to non verification of registrant data"]="transferProhibited set for %s due to non verification of registrant data";

// File: /registrar-platform/bin/whois_on_hold.php
$STRINGS[$LANG]["Failed to set clientHold for %s"]="Failed to set clientHold for %s";

// File: /registrar-platform/bin/whois_on_hold.php
$STRINGS[$LANG]["clientHold set for %s due to non verification of registrant data"]="clientHold set for %s due to non verification of registrant data";

// File: /registrar-platform/bin/whois_on_hold.php
$STRINGS[$LANG]["%s - Your domain has been suspended"]="%s - Your domain has been suspended";

// File: /registrar-platform/bin/whois_on_hold.php
$STRINGS[$LANG]["%s - Your account has been suspended"]="%s - Your account has been suspended";

// File: /registrar-platform/bin/process_inbound_internal.php
$STRINGS[$LANG]["Review inbound transfer #%s for domain %s. The status is not requested or submitted but it doesn't have a completion date."]="Review inbound transfer #%s for domain %s. The status is not requested or submitted but it doesn't have a completion date.";

// File: /registrar-platform/bin/process_inbound_internal.php
$STRINGS[$LANG]["Transfer status not matching completion"]="Transfer status not matching completion";

// File: /registrar-platform/bin/submit_ga_applications.php
$STRINGS[$LANG]["Domain %s registered with %s for %d years for user #%d (submit_ga_application script)"]="Domain %s registered with %s for %d years for user #%d (submit_ga_application script)";

// File: /registrar-platform/bin/validate_vat_numbers.php
$STRINGS[$LANG]["Invalid VAT Number"]="Invalid VAT Number";

// File: /registrar-platform/www/application/views/wales/e_index.tpl
$STRINGS[$LANG]["Redefine Wales - Register your true online identity today"]="Ail luniwch Cymru – Cofrestrwch eich hunaniaeth go iawn heddiw";

// File: /registrar-platform/www/application/views/wales/e_index.tpl
$STRINGS[$LANG]["Use your Cymru.Domains name as your personal email address"]="Defnyddiwch eich enw Cymru.Domains fel eich ebost personol";

// File: /registrar-platform/www/application/views/wales/e_index.tpl
$STRINGS[$LANG]["A new Cymru.Domains email address or web address redefines you."]="Bydd cyfeiriad ebost Cymru.Domains newydd yn eich ailddiffinio.";

// File: /registrar-platform/www/application/views/wales/e_index.tpl
$STRINGS[$LANG]["It's easy to setup using your current email.."]="Mae’n hawdd ei osod gan ddefnyddio eich ebost presennol";

// File: /registrar-platform/www/application/views/wales/e_index.tpl
$STRINGS[$LANG]["Cymru.Domains is the solution for you or your business"]="Cymru.Domains yw’r ateb i chi neu eich busnes";

// File: /registrar-platform/www/application/views/wales/e_index.tpl
$STRINGS[$LANG]["Cymru.Domains is a one stop shop for getting your business online.  Along with our easy to use <a href='/sitebuilder'>website builder tool</a>, we have partnered with Google to bring you professional email, online storage, shared calendars, video meetings and more. Add <a href='/googleapps'>Google Apps</a> to your cymru.domains name order and join thousands of other Welsh businesses who have all the tools to get more done."]="Cymru.Domains yw’r fangre ar gyfer rhoi eich busnes arlein.  Ynghyd â’n teclyn adeiladu gwefan hawdd ei ddefnyddio, rydym wedi dechrau partneriaeth gyda Google er mwyn darparu ebost proffesiynol, storfa arlein, calendrau i’w rhannu, cyfarfodydd fideo a mwy. Ychwanegwch <a href='/googleapps'>Google Apps</a> i’ch archeb enw cymru.domains ac ymunwch â’r miloedd o fusnesau Cymreig sydd â’r holl offer sy’n eu galluogi i gyflawni mwy.";

// File: /registrar-platform/www/application/views/wales/e_index.tpl
$STRINGS[$LANG]["If you live in Wales or away from home Cymru.Domains is for you. <span style='color: #0da554';><b>This change is historic</b></span>, Wales will never have another domain on the World Wide Web."]="Os ydych chi’n byw yng Nghymru neu oddi cartref mae Cymru.Domains i chi. Mae’r newid hwn yn hanesyddol, ni fydd gan Cymru barth arall ar y We Fyd-Eang.";

// File: /registrar-platform/www/application/views/wales/header.tpl
$STRINGS[$LANG]["Cymru.Domains - Get your Welsh online identity"]="Cymru.Domains - Hawliwch eich hunaniaeth Gymreig arlein";

// File: /registrar-platform/www/application/views/wales/header.tpl
$STRINGS[$LANG]["Please wait..."]="Please wait...";

// File: /registrar-platform/www/application/views/wales/header.tpl
$STRINGS[$LANG]["Menu"]="Dewislen";

// File: /registrar-platform/www/application/views/wales/header.tpl
$STRINGS[$LANG]["Home"]="Hafan";

// File: /registrar-platform/www/application/views/wales/header.tpl
$STRINGS[$LANG]["Blog"]="Blog";

// File: /registrar-platform/www/application/views/wales/header.tpl
// File: /registrar-platform/www/application/views/wales/footer.tpl
// File: /registrar-platform/www/application/views/default/support_index.tpl
$STRINGS[$LANG]["FAQs"]="Cwestiynau Cyffredinol";

// File: /registrar-platform/www/application/views/wales/header.tpl
// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]["Contact"]="Cysylltu â ni";

// File: /registrar-platform/www/application/views/wales/header.tpl
$STRINGS[$LANG]["Tools"]="Offer";

// File: /registrar-platform/www/application/views/wales/header.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
// File: /registrar-platform/www/application/views/default/cart_country_validation.tpl
// File: /registrar-platform/www/application/views/default/support_index.tpl
// File: /registrar-platform/www/application/views/default/cart_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["Website Builder"]="Adeiladydd Gwefan";

// File: /registrar-platform/www/application/views/wales/header.tpl
// File: /registrar-platform/www/application/views/default/support_index.tpl
// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Google Apps"]="Google Apps";

// File: /registrar-platform/www/application/views/wales/header.tpl
$STRINGS[$LANG]["Login"]="Mewngofnodi";

// File: /registrar-platform/www/application/views/wales/header.tpl
$STRINGS[$LANG]["Sign Up"]="Cofrestru";

// File: /registrar-platform/www/application/views/wales/header.tpl
$STRINGS[$LANG]["Account"]="Cyfrif";

// File: /registrar-platform/www/application/views/wales/header.tpl
$STRINGS[$LANG]["Logout"]="Allgofnodi";

// File: /registrar-platform/www/application/views/wales/header.tpl
$STRINGS[$LANG]["Cart (<span id='cart_count'>0</span>)"]="Cert Siopa (<span id='cart_count'>0</span>)";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Who are TLD Registrar Solutions?"]="Pwy yw TLD Registrar Solutions?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Can I register a domain name if I'm not yet ready to use it?"]="Gallaf gofrestru enw parth os nad ydwyf eto’n barod i’w ddefnyddio?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["How do I modify the nameservers for my domain name?"]="Sut ydw i’n addasu’r gweinydd enw (nameserver) ar gyfer fy enw parth?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["How do I update the contact information for my account?"]="Sut ydw i’n diweddaru’r wybodaeth gysylltu ar gyfer fy nghyfrif?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["How do I modify the details of a Contact associated with a domain in my account?"]="Sut ydw i’n addasu manylion Cyswllt sy’n gysylltiedig â pharth yn fy nghyfrif?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["How do I renew a domain name?"]="Sut ydw i’n adnewyddu enw parth?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Can I set up automatic renewal payments?"]="Gallaf osod taliadau adnewyddu awtomatig?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["What is domain forwarding?"]="Beth yw dargyfeirio parth?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["How do I transfer my domain name to another registrar?"]="Sut ydw i’n trosglwyddo fy enw parth i gofrestrydd arall?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["How do I transfer my domain name to another [url] account?"]="Sut ydw i’n trosglwyddo fy enw parth i gyfrif [url] arall?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Does it cost anything to transfer my domain?"]="Oes cost yn gysylltiedig âg unrhywbeth sy’n ymwneud â throsglwyddo fy mharth?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["What are 'Premium' Domain Names?"]="Beth yw Enwau Parth ‘Premiwm’?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["How do I contact customer support?"]="Sut ydw i’n cysylltu Cymorth i Gwsmeriaid?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["How do I submit a trademark infringement claim against a [store] domain name?"]="Sut ydw i’n cyflwyno cais ar drosedd nod masnach yn erbyn enw parth gan [store]?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["What is Advanced DNS Manager?"]="Beth yw Advanced DNS Manager?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["What is Whois Privacy?"]="Beth yw Preifatrwydd Whois?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["How do I delete my domain?"]="Sut ydw i’n dileu fy mharth?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["My domain is pending deletion, how do I make it live?"]="Mae fy mharth yn aros i gael ei ddileu, sut ydw i’n ei wneud yn fyw?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["How much does it cost to restore a domain after expiry?"]="Beth yw’r gost o adfer parth wedi iddo ddod i ben?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: Who are TLD Registrar Solutions?"]="Q: Pwy yw TLD Registrar Solutions?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: TLD Registrar Solutions Ltd (TRS) are an ICANN Accredited Registrar who are providing a domain registration and management service through the [url] website.  All agreements entered into on this site, unless otherwise stated, are made with TRS."]="A: Mae TLD Registrar Solutions Ltd (TRS) yn Gofrestrydd sydd wedi’i achredu gan ICANN sy’n darparu gwasanaeth cofrestru a rheoli parth drwy’r wefan test.cymru.domains.  Mae pob cytundeb a wneir ar y safle hwn, heblaw ei fod wedi ei ddatgan fel arall, yn cael ei wneud gyda TRS.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: Can I register a domain name if I'm not yet ready to use it?"]="Q: Gallaf gofrestru enw parth os nad ydwyf eto’n barod i’w ddefnyddio?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: Yes, you can leave it parked with [store] for the length of your registration."]="A: Gallwch, gallwch ei adael yn llonydd gyda [store] am gyfod eich cofrestriad.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: How do I modify the nameservers for my domain name?"]="Q: Sut ydw i’n addasu’r gweinydd enw (nameserver) ar gyfer fy enw parth?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: To modify the nameservers for your domain name:"]="A: Er mwyn addasu’r gweinyddwyr enw ar gyfer eich enw parth:";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["login using your email address and password"]="Mewngofnodwch yn defnyddio eich cyfeiriad ebost a chyfrinair ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Click on the domain name you wish to modify"]="Cliciwch ar yr enw parth os hoffech ei addasu ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Enter the new Nameservers into the Hostname fields"]="Rhowch y Gweinyddwyr Enw newydd i fewn i’r meysydd enw Gwesteiwr (Hostname)";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Click 'Add Nameserver' if you need to add additional nameservers"]="Cliciwch 'Add Namevserver’ os oes angen ychwanegu gweinyddwyr enw ychwanegol";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Click 'Save changes' to save your changes to the nameserver details"]="Cliciwch 'Save changes' er mwyn cadw eich newidiadau i’r manylion gweinyddwr enw ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Remember, changes may take up to 24 hours to propagate."]="Cofiwch, gall newidiadau gymryd hyd at 24 awr i ymddangos.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: How do I update the contact information for my account?"]="Q:  Sut ydw i’n diweddaru’r wybodaeth gysylltu ar gyfer fy nghyfrif?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: To update your contact information:"]="A: Er mwyn diweddaru fy ngwybodaeth gysylltu: ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["go to 'Account Settings' from the menu"]="Ewch i 'Account Settings' yn y fwydlen
 ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["enter your new contact information into the form fields and click 'Update Details'"]="Rhowch eich gwybodaeth gysylltu newydd i fewn i’r meysydd ar y ffurflen a chliciwch 'Update Details'";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["PLEASE NOTE: If you update the contact information associated with your account you will be asked to re-verify your contact information via email."]="NODWCH: Os ydych yn diweddaru’r wybodaeth gysylltu sy’n gysylltiedig â’ch cyfrif bydd gofyn i chi ail-gadarnhau eich gwybodaeth gysylltu drwy ebost.  ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: How do I modify the details of a Contact associated with a domain in my account?"]="Q: Sut ydw i’n addasu manylion Cyswllt sy’n gysylltiedig â pharth yn fy nghyfrif?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: To update your Contacts:"]="A: Er mwyn diweddaru fy Ngysylltiadau:";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["go to 'Manage Contacts' from the menu"]="Ewch at 'Manage Contacts' o’r fwydlen";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["your Contacts will be listed and can be edited from this page. Please note: any changes to a Contact will be reflected across all domain associations"]="Bydd eich Cysylltiadau wedi’u rhestru a gallant gael eu golygu o’r dudalen hon. Nodwch: bydd unrhyw newidiadau i Gyswllt yn cael ei adlewyrchu ar draws pob cysylltiad parth. ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["PLEASE NOTE: In certain cases, if you update the contact information associated with your domain you will be asked to re-verify your contact information via email."]="NODWCH: Mewn rhai achosion, os ydych yn diweddaru’r wybodaeth gysylltu sy’n gysylltiedig â’ch parth, byddwn yn gofyn i chi ail-gadarnhau eich manylion cysylltu drwy ebost.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: How do I renew a domain name?"]="Q: Sut ydw i’n adnewyddu enw parth? ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: To renew a domain name:"]="A:  Er mwyn adnewyddu enw parth: ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["click on the domain name you wish to renew"]="Cliciwch ar yr enw parth yr hoffech ei adnewyddu ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["click 'Renew' next to the domain name's expiry date at the top of the page"]="Cliciwch 'Renew' nesaf i’r dyddiad dod i ben ar frig y dudalen";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["please note: domain names cannot be renewed for 7 days after their initial registration"]="NODWCH: ni all enwau parth gael eu hadnewyddi am 7 diwrnod wedi eu cofrestriad cychwynnol ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["to perform bulk renewals, use the check boxes next to the domain names on the 'Account Summary' page, and click 'renew'"]="er mwyn perfformio adnewyddiadau swmpus, defnyddwch y bocsys gwirio nesaf i’r enwau parth ar y dudalen 'Account Summary’, a chliciwch 'renew'";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: Can I set up automatic renewal payments?"]="Q: Gallaf osod taliadau adnewyddu awtomatig?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: No, you must login to your account and make payment each time a domain name needs renewing. We will send you reminders by email before the expiry date."]="A: Na, rhaid i chi fewngofnodi i’ch cyfrif a gwneud taliad bob tro y bydd angen adnewyddu enw parth.  Byddwn yn eich atgoffa drwy ebost cyn y dyddiad dod i ben.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: What is domain forwarding?"]="Q: Beth yw dargyfeirio parth?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: Our forwarding service allows you to re-direct your domain name to an already existing website. To set web forwarding for your domain name you will need to use the Advanced DNS Manager."]="A: Mae ein gwasanaeth dargyfeirio yn eich caniatáu i ail-gyfeirio eich enw parth i wefan sydd eisioes yn bodoli.  Er mwyn gosod dargyfeirio gwe ar gyfer eich enw parth byd angen i chi ddefnyddio’r Advanced DNS Manager. ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: How do I transfer my domain name to another registrar?"]="Q: Sut ydw i’n trosglwyddo fy enw parth i gofrestrydd arall?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: To transfer your domain name you will need to generate the Auth/Transfer code. Once logged in, click on the domain name and then use the 'Generate code' button at the top of the page. The Auth/Transfer code will be emailed to the Administrative Contact for the domain name."]="A: Er mwyn trosglwyddo eich enw parth bydd angen i chi gynhyrchu cod Dilysu/Trosglwyddo. Unwaith i chi fewngofnodi, cliciwch ar yr enw parth ac wedyn defnyddiwch y botwm ‘Genereat code’ ar frig y dudalen. Bydd y cod Dilysu/Trosglwydo yn cael ei ebostio i’r Cyswllt Gweinyddol ar gyfer yr enw parth.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["This Auth/Transfer code can be given to your new registrar in order for them to initiate a transfer."]="Gall y cod Dilysu/Trosglwyddo gael ei rhoi i’ch cofrestrydd newydd er mwyn iddynt ddechrau trosglwyddiad.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: How do I transfer my domain name to another [url] account?"]="Q: Sut ydw i’n trosglwyddo fy enw parth i gyfrif test.cymru arall? ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: To transfer your domain name to another [url] account you will need to generate the Auth/Transfer code. Once logged in, click on the domain name and then use the 'Generate code' button at the top of the page. The Auth/Transfer code will be emailed to the Administrative Contact for the domain name."]="A: Er mwyn trosglwyddo eich enw parth i gyfrif test.cymru.domains bydd angen i chi gynhyrchu cod Dilysu/Trosglwyddo. Unwaith i chi fewngofnodi, cliciwch ar yr enw parth ac yna defnyddiwch y botwm 'Generate code' ar frig y dudalen. Bydd y cod Dilysu/Trosglwydo yn cael ei ebostio i’r Cyswllt Gweinyddol ar gyfer yr enw parth.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["This Auth/Transfer code can be given to the new account holder in order for them to initiate a transfer."]="Gall y cod Dilysu/Trosglwyddo gael ei rhoi i’ch cofrestrydd newydd er mwyn iddynt ddechrau trosglwyddiad.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: Does it cost anything to transfer my domain?"]="Q: Oes cost yn gysylltiedig âg unrhywbeth sy’n ymwneud â throsglwyddo fy mharth? ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: No, transfers are free of charge."]="A: Nacoes, mae trosglwyddiadau yn rhad ac am ddim. ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: What are 'Premium' Domain Names?"]="Q: Beth yw Enwau Parth ‘Premiwm’?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: Premium Domain Names are high value names available purchase at a premium price, renewals may be at a higher rate, and transfers are permitted."]="A: Mae Enwau Parth Premiwm yn enwau gwerth uchel sydd ar gael i’w prynu am bris premiwm, gall adnewyddu fod ar raddfa uwch, a chaniateir trosglwyddiadau. ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: How do I contact customer support?"]="Q: Sut ydw i’n cysylltu Cymorth i Gwsmeriaid?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: Email your enquiry to [support]"]="A: Ebostiwch eich ymholiad at [support]";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["We endeavour to respond to all enquiries within 24 hours."]="Fe geisiwn ymateb i bob ymholiad o fewn 24 awr.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: How do I submit a trademark infringement claim against a [store] domain name?"]="Q: Sut ydw i’n cyflwyno cais ar drosedd nod masnach yn erbyn enw parth gan [store]?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: What is Advanced DNS Manager?"]="Q: Beth yw Advanced DNS Manager? ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: Advanced DNS Manager is a free service, which enables you to put and manage your domains on our registry service provider, CentralNic USA's, well proven DNS infrastructure."]="A: Mae Advanced DNS Manager yn wasanaeth am ddim, sy’n eich caniatáu i osod a rheoli eich parthau ar ein darparwr gwasanaeth cofrestrfa, sef tanadeliadd CentralNic yn yr UDA, sydd wedi ei brofi’n helaeth.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["To setup your domain on Advanced DNS Manager:"]="Er mwyn gosod eich parth ar Advanced DNS Manager:";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Login to your account and click on the domain name"]="Mewngofnodwch i’ch cyfrif a chliciwch ar yr enw parth";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Click 'Advanced DNS Manager' under Nameserver Configuration"]="Cliciwch 'Advanced DNS Manager' o dan ‘Nameserver Configuration’ ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Confirm you have read and accept the terms and conditions"]="Cadarnhewch eich bod wedi darllen a derbyn y telerau ac amodau ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Click 'Activate domain on Advanced DNS'"]="Cliciwch 'Activate domain on Advanced DNS'";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Once the Advanced DNS Manager is active for your domain you will be able to manage your own DNS records:"]="Unwaith i’r Advanced DNS Manager ddod yn fyw ar gyfer eich parth bydd modd i chi rheoli eich cofnodion DNS eich hun:";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["You are fully in control of your zonedata"]="Mae rheolaeth lwyr gennych ar eich zonedata ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["You can add as many A/AAAA/MX/CNAME/TXT records to your zonefile as you'd like"]="Gallwch ychwanegu gymaint o gofnodion A/AAAA/MX/CNAME/TXT i’ch zonefile ag y dymunwch. ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["You can setup Web Forwarding and redirect your domain to an already existing URL"]="Gallwch osod Dargyfeirio Gwe (Web Forwarding) ac ail-gyfeirio eich parth i URL sydd eisioes yn bodoli.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: What is Whois Privacy?"]="Q: Beth yw Preifatrwydd Whois?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: When you register a domain name your contact details are displayed in the public Whois record. If you would like to keep your personal details private and protected, Whois Privacy Ltd can provide their contact details in the Whois record in place of your own. The Whois Privacy Ltd service can be purchased through your account manager:"]="A: Pan fyddwch yn cofrestru enw path bydd eich manylion cyswllt yn cael eu dangos yn y cofnod Whois cyhoeddus. Os hoffech gadw eich manylion personol a’u hamddiffyn, gall Whois Privacy Ltd ddarparu eu manylion cyswllt yn y cofnod Whois yn hytrach na’ch manylion eich hun. Gellir prynu’r gwasanaeth Whois Privacy Ltd drwy eich rheolwr cyfrif: ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["To purchase whois privacy select the domain name(s) from the 'Account Summary' page and click 'Private Whois'."]="Er mwyn prynu preifatrwydd whois dewisiwch enw(au) o’r dudalen 'Account Summary' a chliciwch 'Private Whois'.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["By purchasing private whois you are agreeing to the Whois Privacy Ltd agreement as detailed here: <a href='http://www.whoisprivacy.la/agreement'>http://www.whoisprivacy.la/agreement</a>"]="Drwy brynu whois preifat rydychyn cytuno i gytundeb Whois Privacy Ltd a fanylir yma: <a href='http://www.whoisprivacy.la/agreement'>http://www.whoisprivacy.la/agreement</a>";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: How do I delete my domain?"]="Q: Sut ydw i’n dileu fy mharth?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["A: To delete your domain:"]="A: Er mwyn dileu eich parth:";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Login to your account and click on the domain name you'd like to delete."]="Mewngofnodwch i’ch cyfrif a chliciwch ar yr enw parth yr hoffech ei ddileu.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Go to 'Domain Cancellation' at the bottom of the page and click 'Delete'."]="Ewch i 'Domain Cancellation' ar waelod y dudalen a chliciwch 'Delete'.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["This action will schedule the domain for deletion, and also delete any associated services."]="Bydd y weithred hon yn rhestru’r parth ar gyfer e ddileu, a hefyd yn dileu unrhyw wasanaethau cysylltiedig. ";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["This action cannot be undone, and no refund will be given."]="Ni ellir dad-wneud y weithred hon, ac ni fydd ad-daliad.";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: My domain is pending deletion, how do I make it live?"]="Q: Mae fy mharth yn aros i gael ei ddileu, sut ydw i’n ei wneud yn fyw?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["Q: How much does it cost to restore a domain after expiry?"]="Q: Beth yw’r gost o adfer parth wedi iddo ddod i ben?";

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]["There is a [currency][fee] restoration fee for [store] domain names."]="Mae ffi adfer o [currency][fee] ar gyfer enwau parth [store].";

// File: /registrar-platform/www/application/views/wales/footer.tpl
$STRINGS[$LANG]["By browsing our site, you're agreeing to the use of cookies. Cookies are tiny bits of data websites store in your web browser to make your online experience better."]="By browsing our site, you're agreeing to the use of cookies. Cookies are tiny bits of data websites store in your web browser to make your online experience better.";

// File: /registrar-platform/www/application/views/wales/footer.tpl
// File: /registrar-platform/www/application/views/default/e_whois.tpl
$STRINGS[$LANG]["Whois Lookup"]="Chwilio Whois";

// File: /registrar-platform/www/application/views/wales/footer.tpl
$STRINGS[$LANG]["Policies"]="Polisïau";

// File: /registrar-platform/www/application/views/wales/footer.tpl
$STRINGS[$LANG]["Terms of Use"]="Telerau Defnyddio";

// File: /registrar-platform/www/application/views/wales/footer.tpl
$STRINGS[$LANG]["Privacy"]="Preifatrwydd";

// File: /registrar-platform/www/application/views/wales/footer.tpl
$STRINGS[$LANG]["Contact Us"]="Cysylltu â ni";

// File: /registrar-platform/www/application/views/wales/footer.tpl
$STRINGS[$LANG]["Report Abuse"]="Adrodd ar ddifrïo";

// File: /registrar-platform/www/application/views/wales/footer.tpl
$STRINGS[$LANG]["&copy; [year] Cymru.Domains. All rights reserved."]="&copy; [year] Cymru.Domains. Cedwir Holl Hawliau.";

// File: /registrar-platform/www/application/views/wales/e_blog.tpl
$STRINGS[$LANG]["Cymru.Domains Blog"]="Cymru.Domains Blog";

// File: /registrar-platform/www/application/views/wales/e_blog.tpl
$STRINGS[$LANG]["General Availability is now on for .cymru and .wales"]="General Availability is now on for .cymru and .wales";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["It really is this simple..."]="It really is this simple...";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["1. Select your template"]="1. Select your template";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Use our template picker to choose from over 200 professionally designed and fully customizable templates – there’s something for every type of website imaginable!"]="Use our template picker to choose from over 200 professionally designed and fully customizable templates – there’s something for every type of website imaginable!";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["2. Add content"]="2. Add content";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Customize your site by adding text, images and even video clips. And integrate your favorite social channels to encourage visitor interaction."]="Customize your site by adding text, images and even video clips. And integrate your favorite social channels to encourage visitor interaction.";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["3. Publish"]="3. Publish";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Publish your fabulous website to the internet for the world to see (and love)!"]="Publish your fabulous website to the internet for the world to see (and love)!";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Why Choose Website Builder?"]="Why Choose Website Builder?";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Website builder is a simple, user-friendly and affordable solution to building your website. It creates fast, impressive, 'design agency quality' results and requires no technical knowledge on your behalf."]="Website builder is a simple, user-friendly and affordable solution to building your website. It creates fast, impressive, 'design agency quality' results and requires no technical knowledge on your behalf.";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["All in all, it is the ultimate answer to your site building needs, enabling you to customize your website exactly as you like. You can even build your own online store! For all you techies and designers out there, go ahead and edit the HTML, CSS and Javascript to create your very own customized platform."]="All in all, it is the ultimate answer to your site building needs, enabling you to customize your website exactly as you like. You can even build your own online store! For all you techies and designers out there, go ahead and edit the HTML, CSS and Javascript to create your very own customized platform.";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Website builder offers a range of cool features to help you build an amazing site with minimal fuss:"]="Website builder offers a range of cool features to help you build an amazing site with minimal fuss:";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Drag & Drop"]="Drag & Drop";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Simply drag and drop content onto your website to create the perfect design. This feature also allows you to reposition text boxes and resize images to your exact specifications. Give it your own signature style by selecting the fonts, styles, colours and layouts of your choice."]="Simply drag and drop content onto your website to create the perfect design. This feature also allows you to reposition text boxes and resize images to your exact specifications. Give it your own signature style by selecting the fonts, styles, colours and layouts of your choice.";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Widgets"]="Widgets";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Make your website dynamic and interactive by integrating a host of web widgets including Facebook, Twitter, Google+, Flickr, RSS feeds, Google Maps and even forms. This can be done in seconds by using the drag and drop feature."]="Make your website dynamic and interactive by integrating a host of web widgets including Facebook, Twitter, Google+, Flickr, RSS feeds, Google Maps and even forms. This can be done in seconds by using the drag and drop feature.";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["E-commerce"]="E-commerce";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Sell your products and services online! Our ecommerce features such as “add to cart”, “buy now” and “view cart” will make your user journey a treat. Plus you can choose between Google and PayPal cart features for ultimate safety and security."]="Sell your products and services online! Our ecommerce features such as “add to cart”, “buy now” and “view cart” will make your user journey a treat. Plus you can choose between Google and PayPal cart features for ultimate safety and security.";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Help & Support"]="Help & Support";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Help is available 24/7 via our <a href='/support#sitebuilder'>support section</a> which includes downloadable guides, FAQs and videos. If you’d like one-on-one assistance, simply get in touch via our support site – most questions are answered within four hours."]="Help is available 24/7 via our <a href='/support#sitebuilder'>support section</a> which includes downloadable guides, FAQs and videos. If you’d like one-on-one assistance, simply get in touch via our support site – most questions are answered within four hours.";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Choose your package"]="Choose your package";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
// File: /registrar-platform/www/application/views/default/inc_google.tpl
// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]["FREE"]="FREE";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]["month"]="month";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Choose Package"]="Choose Package";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
// File: /registrar-platform/www/application/views/default/inc_google.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
$STRINGS[$LANG]["All packages are purchasable on an annual basis only."]="All packages are purchasable on an annual basis only.";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
$STRINGS[$LANG]["Your website will need a domain name..."]="Your website will need a domain name...";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
// File: /registrar-platform/www/application/views/default/inc_cart_search_multiple.tpl
$STRINGS[$LANG]["Search"]="Chwilio";

// File: /registrar-platform/www/application/views/wales/e_sitebuilder.tpl
// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]["Already have a domain name with us? <a href='/e/login'>Login here</a>, and choose 'Add-on Services'."]="Already have a domain name with us? <a href='/e/login'>Login here</a>, and choose 'Add-on Services'.";

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]["Google Apps for Business"]="Google Apps ar gyfer Busnes";

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]["All you need to work on the go"]="Y cyfan sydd angen er mwyn gweithio wrth deithio";

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]["Enjoy a consistent experience from your computer, tablet or phone. Draft a proposal in Docs at the office, review it on the train, then make final edits from your phone right before the meeting. Get more out of your workday."]="Mwynhewch brofiad cyson drwy eich cyfrifiadur, tabled neu ffôn. Lluniwch gynnig yn Docs yn y swydda, adolygwch ar y trên, yna gwneud golygiadau terfynol ar eich ffôn cyn y cyfarfod. Gwnewch y gorau o’ch diwrnod gwaith.";

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]["Get started"]="Cychwyn";

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]["Bring the ease and reliability of Gmail to work"]="Dewch â hwylustod a dibynadwyedd Gmail i’ch gwaith.";

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]["Get a professional email address with the same familiar interface hundreds of millions of people use every day. Business features include 30GB of storage and 24/7 support."]="Get a professional email address with the same familiar interface hundreds of millions of people use every day. Business features include 30GB of storage and 24/7 support.";

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]["More productive meetings"]="Cyfarfodydd mwy cynhyrchiol";

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]["Schedule events in Calendar at times that work for everyone. Get meeting reminders directly to your Gmail inbox. With one-click, join a video meeting through Hangouts and share your Slides to review as a team. Less prep, fewer next steps."]="Rhestrwch ddigwyddiadau yn Calendar ar amserau sy’n hwylus i bawb. Derbyniwch nodiadau atgoffa’n uniongyrchol i’ch mewnflwch Gmail. Gydag un clic, ymunwch â chyfarfod fideo drwy Hangouts a rhannwch eich sleidiau i’w adolygu fel tîm. Llai o baratoi, llai o rhwystrau. ";

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]["Teamwork that works"]="Cyd-weithio effeithiol";

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]["Create a budget tracker in Sheets, share it with teammates and edit it in real-time. It's automatically stored in Drive so everyone always has the latest version. No more sending attachments to get on the same page."]="Creu traciwr cyllid yn Sheets, ei rhannu gydag aelodau eich tîm a’i olygu’n fyw. Mae wedi ei storio’n awtomatig yn Drive fel bod gan bawb y fersiwn ddiweddaraf ohyd.  Dim mwy o ddanfon atodiadau er mwyn cydsynio. ";

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]["Get started with Google Apps today"]="Cychwynwch gyda Google Apps heddiw";

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]["Your Google Apps will need a domain name..."]="Your Google Apps will need a domain name...";

// File: /registrar-platform/www/application/views/default/e_whois.tpl
$STRINGS[$LANG]["You can use our WHOIS form to check the details of any domain name registered with us."]="Gallwch ddefyddio ein ffurflen WHOIS er mwyn gwirio manylion unrhyw enw parth sydd wedi’i gofrestru gyda ni.";

// File: /registrar-platform/www/application/views/default/e_whois.tpl
$STRINGS[$LANG]["Simply enter a domain name in the text field below and press the 'Lookup' button to retrieve the available information."]="Yn syml, gosodwch yr enw parth yn y maes testun isod a gwasgwch y botwm 'Chwilio' er mwyn adalw y wybodaeth sydd ar gael.";

// File: /registrar-platform/www/application/views/default/e_whois.tpl
// File: /registrar-platform/www/application/views/default/account_transfers.tpl
$STRINGS[$LANG]["Domain Name:"]="Enw Parth:";

// File: /registrar-platform/www/application/views/default/e_whois.tpl
$STRINGS[$LANG]["Lookup"]="Chwilio";

// File: /registrar-platform/www/application/views/default/account_html_view_records.tpl
// File: /registrar-platform/www/application/views/default/account_html_view_records.tpl
$STRINGS[$LANG]["Remove"]="Remove";

// File: /registrar-platform/www/application/views/default/account_html_view_records.tpl
$STRINGS[$LANG]["these records are required for parking/web forwarding."]="these records are required for parking/web forwarding.";

// File: /registrar-platform/www/application/views/default/cart_checkout_completed.tpl
$STRINGS[$LANG]["An email will be sent to the email address you provided which will give you information on how to verify your contact details if required. If you do not verify your details within [lock] days your account/domain(s) will be suspended. This email may end up in your junk folder so make sure you check there too."]="Bydd ebost yn cael ei ddanfon i’ch cyfeiriad ebost a ddarparwyd a fydd yn rhoi gwybodaeth ar sut I ddilysu eich manylion cyswllt os fydd angen. Os nad ydych yn dilysu eich manylion o fewn [lock] diwrnod bydd eich cyfrif / parth(au) yn cael eu atal.  Efallai bydd yr ebost yn diweddu yn eich post sothach felly sicrhewch eich bod yn gwirio’r ffolder hwn hefyd. ";

// File: /registrar-platform/www/application/views/default/cart_checkout_completed.tpl
$STRINGS[$LANG]["This email will also confirm the completion of your order for the following domains and/or services:"]="Mae’r eost hwn hefyd yn cadarnhau gorffen eich archeb ar gyfer y parthau a/neu (g)wasanaethau:";

// File: /registrar-platform/www/application/views/default/cart_checkout_completed.tpl
$STRINGS[$LANG]["Your Google Apps package is pending provisioning. You will receive an email containing instructions on the next steps to take to complete your order"]="Your Google Apps package is pending provisioning. You will receive an email containing instructions on the next steps to take to complete your order";

// File: /registrar-platform/www/application/views/default/cart_checkout_completed.tpl
$STRINGS[$LANG]["Please go to <a href='/account'>your account</a> to manage your domains and services."]="Ewch at <a href='/account'>eich cyfrif</a> er mwyn rheoli eich parthau a’ch gwasanaethau.";

// File: /registrar-platform/www/application/views/default/e_terms_hosting.tpl
$STRINGS[$LANG]["Web Hosting and Email Service Agreement"]="Web Hosting and Email Service Agreement";

// File: /registrar-platform/www/application/views/default/e_parked.tpl
// File: /registrar-platform/www/application/views/default/e_parked.tpl
$STRINGS[$LANG]["This domain has already been registered."]="This domain has already been registered.";

// File: /registrar-platform/www/application/views/default/e_parked.tpl
$STRINGS[$LANG]["This domain is parked"]="This domain is parked";

// File: /registrar-platform/www/application/views/default/e_error.tpl
$STRINGS[$LANG]["An error has occured"]="An error has occured";

// File: /registrar-platform/www/application/views/default/e_error.tpl
$STRINGS[$LANG]["An unexpected error has occured or page could not be found."]="An unexpected error has occured or page could not be found.";

// File: /registrar-platform/www/application/views/default/e_error.tpl
$STRINGS[$LANG]["An email has been sent to the technical team. If you require further assistants, please contact <a href='mailto:[support]'>[support]</a>"]="An email has been sent to the technical team. If you require further assistants, please contact <a href='mailto:[support]'>[support]</a>";

// File: /registrar-platform/www/application/views/default/e_error.tpl
// File: /registrar-platform/www/application/views/default/e_permission.tpl
$STRINGS[$LANG]["Return to the homepage"]="Return to the homepage";

// File: /registrar-platform/www/application/views/default/e_error.tpl
$STRINGS[$LANG]["Return to previous page"]="Return to previous page";

// File: /registrar-platform/www/application/views/default/inc_account_menu.tpl
$STRINGS[$LANG]["Account Summary"]="Crynodeb Cyfrif";

// File: /registrar-platform/www/application/views/default/inc_account_menu.tpl
$STRINGS[$LANG]["Notifications"]="Hysbysiadau";

// File: /registrar-platform/www/application/views/default/inc_account_menu.tpl
// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["Account Settings"]="Gosodiadau Cyfrif";

// File: /registrar-platform/www/application/views/default/inc_account_menu.tpl
// File: /registrar-platform/www/application/views/default/account_contacts.tpl
$STRINGS[$LANG]["Manage Contacts"]="Rheoli Cysylltiadau";

// File: /registrar-platform/www/application/views/default/inc_account_menu.tpl
// File: /registrar-platform/www/application/views/default/account_transfers.tpl
$STRINGS[$LANG]["Manage Transfers"]="Rheoli Trosglwyddiadau";

// File: /registrar-platform/www/application/views/default/inc_account_menu.tpl
$STRINGS[$LANG]["Register Nameservers"]="Cofrestru Gweinyddwyr Enw";

// File: /registrar-platform/www/application/views/default/inc_account_menu.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Payment History"]="Hanes Taliadau";

// File: /registrar-platform/www/application/views/default/inc_account_menu.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Add-on Services"]="Gwasanaethau Ychwanegol";

// File: /registrar-platform/www/application/views/default/inc_account_menu.tpl
// File: /registrar-platform/www/application/views/default/account_affiliate.tpl
$STRINGS[$LANG]["Affiliate Sales"]="Affiliate Sales";

// File: /registrar-platform/www/application/views/default/inc_account_menu.tpl
$STRINGS[$LANG]["active"]="active";

// File: /registrar-platform/www/application/views/default/inc_account_menu.tpl
$STRINGS[$LANG]["text-danger"]="text-danger";

// File: /registrar-platform/www/application/views/default/account_html_view_hosts.tpl
$STRINGS[$LANG]["Current Nameservers"]="Current Nameservers";

// File: /registrar-platform/www/application/views/default/account_html_view_hosts.tpl
$STRINGS[$LANG]["Updating this nameserver requires manual processing. This should take no longer than 48 hours."]="Updating this nameserver requires manual processing. This should take no longer than 48 hours.";

// File: /registrar-platform/www/application/views/default/account_html_view_hosts.tpl
// File: /registrar-platform/www/application/views/default/account_html_outgoing_transfers.tpl
// File: /registrar-platform/www/application/views/default/account_html_transfer_history.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
// File: /registrar-platform/www/application/views/default/account_html_pending_transfers.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Domain"]="Domain";

// File: /registrar-platform/www/application/views/default/account_html_view_hosts.tpl
// File: /registrar-platform/www/application/views/default/account_html_transfer_history.tpl
// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
// File: /registrar-platform/www/application/views/default/account_html_pending_transfers.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Status"]="Status";

// File: /registrar-platform/www/application/views/default/account_html_view_hosts.tpl
$STRINGS[$LANG]["IP Address"]="IP Address";

// File: /registrar-platform/www/application/views/default/account_html_view_hosts.tpl
$STRINGS[$LANG]["Couldn't retrieve nameserver information at this time"]="Couldn't retrieve nameserver information at this time";

// File: /registrar-platform/www/application/views/default/account_html_view_hosts.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
// File: /registrar-platform/www/application/views/default/account_html_view_mailboxes.tpl
$STRINGS[$LANG]["Update"]="Update";

// File: /registrar-platform/www/application/views/default/account_html_view_hosts.tpl
// File: /registrar-platform/www/application/views/default/account_html_domain_forwarding.tpl
// File: /registrar-platform/www/application/views/default/account_html_view_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_html_view_mailboxes.tpl
$STRINGS[$LANG]["Delete"]="Delete";

// File: /registrar-platform/www/application/views/default/account_html_view_zonefile.tpl
$STRINGS[$LANG]["Zone File for [domain]"]="Zone File for [domain]";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
$STRINGS[$LANG]["Manage Your Google Apps Account for $domain_name "]="Manage Your Google Apps Account for $domain_name ";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
$STRINGS[$LANG]["Google Apps Account Information"]="Google Apps Account Information";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
$STRINGS[$LANG]["Alternate Email:"]="Alternate Email:";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/inc_google_view_user.tpl
// File: /registrar-platform/www/application/views/default/inc_google_view_user.tpl
$STRINGS[$LANG]["Google Customer ID:"]="Google Customer ID:";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
$STRINGS[$LANG]["Domain status:"]="Domain status:";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
$STRINGS[$LANG]["Verify Domain"]="Verify Domain";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
$STRINGS[$LANG]["Google Apps Subscription Details"]="Google Apps Subscription Details";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/inc_google_view_user.tpl
$STRINGS[$LANG]["Registered On:"]="Registered On:";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/inc_google_view_user.tpl
$STRINGS[$LANG]["Expires On:"]="Expires On:";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/account_html_notifications.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Renew Now"]="Renew Now";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/inc_google_view_user.tpl
$STRINGS[$LANG]["Plan Name:"]="Plan Name:";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/inc_google_view_user.tpl
$STRINGS[$LANG]["Subscription ID:"]="Subscription ID:";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/inc_google_view_user.tpl
$STRINGS[$LANG]["Plan Type:"]="Plan Type:";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/inc_google_view_user.tpl
$STRINGS[$LANG]["Total Users:"]="Total Users:";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/inc_google_view_user.tpl
$STRINGS[$LANG]["Status:"]="Status:";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
$STRINGS[$LANG]["Make Admin"]="Make Admin";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
$STRINGS[$LANG]["Google Apps Panel"]="Google Apps Panel";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
$STRINGS[$LANG]["Delete User"]="Delete User";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
$STRINGS[$LANG]["Add User"]="Add User";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
$STRINGS[$LANG]["Google Apps Cancellation"]="Google Apps Cancellation";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
$STRINGS[$LANG]["If you no longer require your Google Apps account you can schedule it for deletion. Once deleted the status will change to 'Pending Deletion' for four days giving you the chance to restore. The account will then be deleted permanently after the four days and this will in effect terminate all user licences held under this account."]="If you no longer require your Google Apps account you can schedule it for deletion. Once deleted the status will change to 'Pending Deletion' for four days giving you the chance to restore. The account will then be deleted permanently after the four days and this will in effect terminate all user licences held under this account.";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
$STRINGS[$LANG]["Delete Google Apps Account"]="Delete Google Apps Account";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Restore Domain"]="Restore Domain";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Send Reminders"]="Send Reminders";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Allow to auto-expire"]="Allow to auto-expire";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Website Builder Renewal"]="Website Builder Renewal";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Would you like to renew your current package, or change your package before renewing?"]="Would you like to renew your current package, or change your package before renewing?";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Renew Current Package"]="Renew Current Package";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Change Package"]="Change Package";

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
// File: /registrar-platform/www/application/views/default/account_html_transfer_code.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
// File: /registrar-platform/www/application/views/default/account_html_pending_transfers.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Cancel"]="Cancel";

// File: /registrar-platform/www/application/views/default/e_permission.tpl
$STRINGS[$LANG]["Permission Denied"]="Permission Denied";

// File: /registrar-platform/www/application/views/default/e_permission.tpl
$STRINGS[$LANG]["You do not have access to this page."]="You do not have access to this page.";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
// File: /registrar-platform/www/application/views/default/cart_country_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["Search Results"]="Canlyniadau Chwilio";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
// File: /registrar-platform/www/application/views/default/cart_country_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["Validation"]="Validation";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
// File: /registrar-platform/www/application/views/default/cart_country_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["Trademark Claims Notice"]="Trademark Claims Notice";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
// File: /registrar-platform/www/application/views/default/cart_country_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Private Whois"]="Whois Preifat";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
// File: /registrar-platform/www/application/views/default/cart_country_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["Services"]="Gwasanaethau";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
// File: /registrar-platform/www/application/views/default/cart_country_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["Shopping Cart"]="Cert Siopa";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
// File: /registrar-platform/www/application/views/default/cart_country_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_country_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["Country Validation"]="Country Validation";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
// File: /registrar-platform/www/application/views/default/cart_country_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["Checkout"]="Desg Dalu";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_view_shopping_cart.tpl
$STRINGS[$LANG]["Your Shopping Cart"]="Eich Cert Siopa";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]["Edit Cart"]="Golygu Cert";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]["Choose Payment Method"]="Dewis dull talu";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]["Pay with Paypal"]="Pay with Paypal";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]["We accept the following credit and debit cards. We do not accept Switch or Maestro cards."]="Rydym yn derbyn y cardiau credyd a debyd canlynol. Nid ydym yn derbyn cardiau Switch na Maestro";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]["These details must match the information that appears on your statement."]="Mae’n rhaid i’r manylion yma gyfateb i’r wybodaeth sydd ar eich datganiad.";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]["Name on Card*:"]="Enw ar y Garden*:";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]["Address*:"]="Cyfeiriad*:";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["City/Town*:"]="Dinas/Tref*:";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]["State/Provice:"]="Talaith:";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
$STRINGS[$LANG]["Postcode*:"]="Cod Post*:";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
$STRINGS[$LANG]["Country/Territory:"]="Gwlad/Tiriogaeth:";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]["Card Number*:"]="Rhif Carden*:";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]["Expiry Date*:"]="Dyddiad dod i ben*:";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]["Security Code*:"]="Cod Diogelwch*:";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]["Our site uses SSL Encryption which allows data to be transmitted over computer networks in a secure manner."]="Mae ein gwefan yn defnyddio Amgryptiad SSL sy’n caniatáu data gael ei drosglwyddo dros rhwydweithau cyfrifiadur mewn dull diogel.";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
$STRINGS[$LANG]["Terms & Conditions"]="Telerau ac Amodau";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
$STRINGS[$LANG]["Please read and accept the terms and conditions to complete your purchase."]="Darllenwch a derbyniwch y telerau ac amodau er mwyn cwblhau eich pryniant.";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
$STRINGS[$LANG]["Registration Agreement:"]="Registration Agreement:";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
$STRINGS[$LANG]["Web Hosting and Email Service Agreement:"]="Web Hosting and Email Service Agreement:";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Sitebuilder Service Agreement:"]="Sitebuilder Service Agreement:";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
$STRINGS[$LANG]["I have read and accepted the Terms and Conditions"]="Rwyf wedi darllen a derbyn y Telerau ac Amodau";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
$STRINGS[$LANG]["Complete Purchase"]="Cwblhau Adbryniant";

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG][":checked"]=":checked";

// File: /registrar-platform/www/application/views/default/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["Email Forwarding"]="Dargyfeirio Ebost";

// File: /registrar-platform/www/application/views/default/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["Enter the email address you would like to set up.  "]="Enter the email address you would like to set up.  ";

// File: /registrar-platform/www/application/views/default/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["Enter the existing email adddress you would like all emails to forward to and click Save Changes."]="Enter the existing email adddress you would like all emails to forward to and click Save Changes.";

// File: /registrar-platform/www/application/views/default/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["Changes take up to 1 hour to take effect."]="Changes take up to 1 hour to take effect.";

// File: /registrar-platform/www/application/views/default/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["You have [rem] email forwarding allowance(s) remaining"]="You have [rem] email forwarding allowance(s) remaining";

// File: /registrar-platform/www/application/views/default/account_html_domain_forwarding.tpl
// File: /registrar-platform/www/application/views/default/account_html_view_mailboxes.tpl
$STRINGS[$LANG]["Email Address"]="Email Address";

// File: /registrar-platform/www/application/views/default/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["Forward to"]="Forward to";

// File: /registrar-platform/www/application/views/default/account_html_domain_forwarding.tpl
$STRINGS[$LANG]["Save Changes"]="Save Changes";

// File: /registrar-platform/www/application/views/default/account_contacts.tpl
$STRINGS[$LANG]["These are the contacts that are associated with domains in your account."]="These are the contacts that are associated with domains in your account.";

// File: /registrar-platform/www/application/views/default/account_contacts.tpl
$STRINGS[$LANG]["Clicking the 'Verify' button will send a verification link via email to the email address of the contact."]="Clicking the 'Verify' button will send a verification link via email to the email address of the contact.";

// File: /registrar-platform/www/application/views/default/account_contacts.tpl
$STRINGS[$LANG]["Editing a verified contact may mean you need to re-verify the contact data. Any domain which uses the contact as a Registrant will be suspended after [verify_days] days if you haven’t done this so we will send you an email with information on how to re-verify the contact data if it is required."]="Editing a verified contact may mean you need to re-verify the contact data. Any domain which uses the contact as a Registrant will be suspended after [verify_days] days if you haven’t done this so we will send you an email with information on how to re-verify the contact data if it is required.";

// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
$STRINGS[$LANG]["Pay with PayPal"]="Pay with PayPal";

// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
$STRINGS[$LANG]["Payer's Name:"]="Payer's Name:";

// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
$STRINGS[$LANG]["Payer's Email:"]="Payer's Email:";

// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
$STRINGS[$LANG]["Total Price:"]="Total Price:";

// File: /registrar-platform/www/application/views/default/cart_cv2.tpl
$STRINGS[$LANG]["CV2 Information"]="CV2 Information";

// File: /registrar-platform/www/application/views/default/cart_cv2.tpl
$STRINGS[$LANG]["About the Security Code"]="About the Security Code";

// File: /registrar-platform/www/application/views/default/cart_cv2.tpl
$STRINGS[$LANG]["As an additional security measure, credit and debit cards have a 'CV2' code printed on the signature strip on the back of the card. We ask that you enter this code to prove that you are in physical possession of the card."]="As an additional security measure, credit and debit cards have a 'CV2' code printed on the signature strip on the back of the card. We ask that you enter this code to prove that you are in physical possession of the card.";

// File: /registrar-platform/www/application/views/default/cart_cv2.tpl
$STRINGS[$LANG]["Identifying the CV2 code"]="Identifying the CV2 code";

// File: /registrar-platform/www/application/views/default/cart_cv2.tpl
$STRINGS[$LANG]["If you look at the back of the card you will see a series of digits on the signature strip. The CV2 code is the right-most block of numbers in this series."]="If you look at the back of the card you will see a series of digits on the signature strip. The CV2 code is the right-most block of numbers in this series.";

// File: /registrar-platform/www/application/views/default/e_premiums.tpl
$STRINGS[$LANG]["Our premium domains now come with a low renewal rate of just [currency][price]. Better yet, they are fully transferable to your registrar of choice at any time. Take a look at the great names on offer below - there are over 3000 of them! Or use the search box above to find your preferred domain and register it instantly."]="Our premium domains now come with a low renewal rate of just [currency][price]. Better yet, they are fully transferable to your registrar of choice at any time. Take a look at the great names on offer below - there are over 3000 of them! Or use the search box above to find your preferred domain and register it instantly.";

// File: /registrar-platform/www/application/views/default/e_premiums.tpl
$STRINGS[$LANG]["Take a look at the great names on offer below - there are over 3000 of them! Or use the search box above to find your preferred domain and register it instantly. All premiums are now they are fully transferable to your registrar of choice."]="Take a look at the great names on offer below - there are over 3000 of them! Or use the search box above to find your preferred domain and register it instantly. All premiums are now they are fully transferable to your registrar of choice.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_test_form.tpl
$STRINGS[$LANG]["Using Widgets: How do I test a form?"]="Using Widgets: How do I test a form?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_test_form.tpl
$STRINGS[$LANG]["How to check if a form is working correctly"]="How to check if a form is working correctly";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_test_form.tpl
$STRINGS[$LANG]["1. Forms can be tested in Preview mode (1)"]="1. Forms can be tested in Preview mode (1)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_test_form.tpl
$STRINGS[$LANG]["Click the <strong>Preview</strong> button (1) to enter preview mode"]="Click the <strong>Preview</strong> button (1) to enter preview mode";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_test_form.tpl
$STRINGS[$LANG]["2. Enter some data into the form and click submit"]="2. Enter some data into the form and click submit";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_test_form.tpl
$STRINGS[$LANG]["3. Click the Design button (1) to return to Design mode"]="3. Click the Design button (1) to return to Design mode";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_test_form.tpl
$STRINGS[$LANG]["4. Click the edit button to open the form settings panel"]="4. Click the edit button to open the form settings panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_test_form.tpl
$STRINGS[$LANG]["5. Click the Database tab (1) and check the data is correct (2)"]="5. Click the Database tab (1) and check the data is correct (2)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_search.tpl
$STRINGS[$LANG]["Using Widgets: Site Search"]="Using Widgets: Site Search";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_search.tpl
$STRINGS[$LANG]["Add site search widget"]="Add site search widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_search.tpl
$STRINGS[$LANG]["The site search widget makes it easy for people to search your site"]="The site search widget makes it easy for people to search your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_search.tpl
$STRINGS[$LANG]["1. Drag and drop site search widget"]="1. Drag and drop site search widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_search.tpl
$STRINGS[$LANG]["Click the <strong>widgets tab (1)</strong> and then drag the<strong> site search widget (2)</strong> on to your page"]="Click the <strong>widgets tab (1)</strong> and then drag the<strong> site search widget (2)</strong> on to your page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_search.tpl
$STRINGS[$LANG]["2. Positioning and scaling the site search widget"]="2. Positioning and scaling the site search widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_search.tpl
$STRINGS[$LANG]["Click and drag within the widget (1) to move it. Scale the widget by dragging one of the corner handles (2)."]="Click and drag within the widget (1) to move it. Scale the widget by dragging one of the corner handles (2).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_search.tpl
$STRINGS[$LANG]["3. Set the default text that appears in the search field"]="3. Set the default text that appears in the search field";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_search.tpl
$STRINGS[$LANG]["Enter text in the <strong>Default Text field (1) </strong>and click <strong>OK (2)</strong>"]="Enter text in the <strong>Default Text field (1) </strong>and click <strong>OK (2)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_search.tpl
$STRINGS[$LANG]["4. Testing the site search widget"]="4. Testing the site search widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_search.tpl
$STRINGS[$LANG]["The site search widget can be tested in <strong>preview mode (1)</strong>. Type a <strong>search term (2)</strong> and a few seconds later the search result will appear below the widget (3)."]="The site search widget can be tested in <strong>preview mode (1)</strong>. Type a <strong>search term (2)</strong> and a few seconds later the search result will appear below the widget (3).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["Text, Images, Files: Images panel"]="Text, Images, Files: Images panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["Using the images panel"]="Using the images panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["1. With this button you'll be able to see all the images you have in your library and change the current image for another."]="1. With this button you'll be able to see all the images you have in your library and change the current image for another.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["2. You can click on the image configuration button so the settings panel will be displayed."]="2. You can click on the image configuration button so the settings panel will be displayed.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["3. Click the reset button to reset the image to it's original size."]="3. Click the reset button to reset the image to it's original size.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["4. Click on this icon so you can move the image across the different rowns and columns in your site."]="4. Click on this icon so you can move the image across the different rowns and columns in your site.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["5. Click here if you want to delete the image from this page."]="5. Click here if you want to delete the image from this page.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["The images panel"]="The images panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["Use the <strong>Alt Tag</strong> field (1) to provide an alternative description of your image. This will be displayed in browsers that can not display the image (e.g. a mobile phone browser) and is important for accessibility with speech reading devices."]="Use the <strong>Alt Tag</strong> field (1) to provide an alternative description of your image. This will be displayed in browsers that can not display the image (e.g. a mobile phone browser) and is important for accessibility with speech reading devices.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["The <strong>Title Tag</strong> field (2) can be used to give the image a title. This may be displayed in search engines."]="The <strong>Title Tag</strong> field (2) can be used to give the image a title. This may be displayed in search engines.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["Click the <strong>Reset button </strong>(3) to reset the size of the image"]="Click the <strong>Reset button </strong>(3) to reset the size of the image";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["Click the <strong>Aspect ratio</strong> <strong>button</strong> (4) so you can make changes on the height or the width of the image. If you lock this function you can change both height and width at the same time."]="Click the <strong>Aspect ratio</strong> <strong>button</strong> (4) so you can make changes on the height or the width of the image. If you lock this function you can change both height and width at the same time.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["Drag an image onto the <strong>rollover target</strong> (5) to create a rollover."]="Drag an image onto the <strong>rollover target</strong> (5) to create a rollover.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["Drag an image onto the <strong>lightbox target </strong>(6) to create a lightbox. When clicked on, the image will open in a new window. Note: this can only be tested in <strong>Preview mode</strong>. It will not work in the site editor."]="Drag an image onto the <strong>lightbox target </strong>(6) to create a lightbox. When clicked on, the image will open in a new window. Note: this can only be tested in <strong>Preview mode</strong>. It will not work in the site editor.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_panels.tpl
$STRINGS[$LANG]["To add a link to an image click the <strong>Add Link </strong>button (7). To remove a link click <strong>Clear Link</strong> (8)"]="To add a link to an image click the <strong>Add Link </strong>button (7). To remove a link click <strong>Clear Link</strong> (8)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["Text, Images, Files: Text editing"]="Text, Images, Files: Text editing";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["Before your start: Text basics"]="Before your start: Text basics";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["Text editing options"]="Text editing options";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["<strong>Drag handle</strong><br />Use this to drag a text block a different row"]="<strong>Drag handle</strong><br />Use this to drag a text block a different row";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["<strong>Settings button</strong><br />Click to open the text block for editing"]="<strong>Settings button</strong><br />Click to open the text block for editing";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["<strong>Delete button</strong><br />Remove text block from page"]="<strong>Delete button</strong><br />Remove text block from page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["<strong>Help button</strong>"]="<strong>Help button</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["<strong>Click and drag</strong><br />Click and drag within the content to position the text block within the row. You can also use the arrow keys on your keyboard to nudge the position."]="<strong>Click and drag</strong><br />Click and drag within the content to position the text block within the row. You can also use the arrow keys on your keyboard to nudge the position.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["Text Formatting"]="Text Formatting";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["Formatting"]="Formatting";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["Select all or some of the text in the text block and click any of the <strong>formatting controls (1)</strong> to format the selected text."]="Select all or some of the text in the text block and click any of the <strong>formatting controls (1)</strong> to format the selected text.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["Styling"]="Styling";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["Click the <strong>styles menu (1) </strong>and click on <strong>style name (2) </strong>to apply a style. You can also <strong>add a new type style (3) </strong>"]="Click the <strong>styles menu (1) </strong>and click on <strong>style name (2) </strong>to apply a style. You can also <strong>add a new type style (3) </strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["Adding Links"]="Adding Links";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["Select some text and click the link button"]="Select some text and click the link button";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["To link to a page on your site use the <strong>Internal link </strong>menu (1). To link to an external page, paste the URL in to the <strong>External link</strong> field (2). To make the link open in a new window use the <strong>Link Target</strong> menu (3)."]="To link to a page on your site use the <strong>Internal link </strong>menu (1). To link to an external page, paste the URL in to the <strong>External link</strong> field (2). To make the link open in a new window use the <strong>Link Target</strong> menu (3).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["Adding Mailto/Email Links"]="Adding Mailto/Email Links";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["The link editor can also be used add 'mailto' links (1). With a mailto link, clicking on the link will open the user's email application and create an email to the address in the link."]="The link editor can also be used add 'mailto' links (1). With a mailto link, clicking on the link will open the user's email application and create an email to the address in the link.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["The syntax is <strong>mailto:<a href='mailto:someone@somewhere.com'>someone@somewhere.com</a></strong>"]="The syntax is <strong>mailto:<a href='mailto:someone@somewhere.com'>someone@somewhere.com</a></strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_editing_text.tpl
$STRINGS[$LANG]["Learn more about the mailto link here: <a href='http://www.w3schools.com/html/tryit.asp?filename=tryhtml_mailto' target='_blank'>http://www.w3schools.com/html/tryit.asp?filename=tryhtml_mailto</a>"]="Learn more about the mailto link here: <a href='http://www.w3schools.com/html/tryit.asp?filename=tryhtml_mailto' target='_blank'>http://www.w3schools.com/html/tryit.asp?filename=tryhtml_mailto</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_analytics.tpl
$STRINGS[$LANG]["Publish & Optimise: Google Analytics"]="Publish & Optimise: Google Analytics";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_analytics.tpl
$STRINGS[$LANG]["Setup Google Analytics to analyse the usage of your site"]="Setup Google Analytics to analyse the usage of your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_analytics.tpl
$STRINGS[$LANG]["Sign up for Google Analytics"]="Sign up for Google Analytics";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_analytics.tpl
$STRINGS[$LANG]["Sign up at <a href='http://www.google.com/analytics/'>http://www.google.com/analytics/</a>"]="Sign up at <a href='http://www.google.com/analytics/'>http://www.google.com/analytics/</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_analytics.tpl
$STRINGS[$LANG]["Find the Google Analytics Website Profile ID for the domain you want to track"]="Find the Google Analytics Website Profile ID for the domain you want to track";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_analytics.tpl
$STRINGS[$LANG]["The ID is a string of letters in the form of XX-11111111-1"]="The ID is a string of letters in the form of XX-11111111-1";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_analytics.tpl
$STRINGS[$LANG]["Click the Manage tab inside the editor"]="Click the Manage tab inside the editor";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_analytics.tpl
$STRINGS[$LANG]["Enter your <strong>Google Analytics Website Profile ID</strong> (&quot;XX-11111111-1&quot;) in the integrations section (1). <strong>DO NOT PASTE ANY CODE into this field.</strong>"]="Enter your <strong>Google Analytics Website Profile ID</strong> (&quot;XX-11111111-1&quot;) in the integrations section (1). <strong>DO NOT PASTE ANY CODE into this field.</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_analytics.tpl
$STRINGS[$LANG]["Click the <strong>Save Settings button (2)</strong>"]="Click the <strong>Save Settings button (2)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["Using Widgets: Google Checkout Widgets"]="Using Widgets: Google Checkout Widgets";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["How to add e-commerce using the Google Checkout widgets"]="How to add e-commerce using the Google Checkout widgets";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["Important Information"]="Important Information";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["Known limitations for the Google Checkout Widgets:"]="Known limitations for the Google Checkout Widgets:";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["1: These widgets are currently only available for customers with UK or US bank accounts<br /> 2: For best performance, we recommend a store size below 200 items<br /> 3: Complex options such as weight-based shipping charges are not supported"]="1: These widgets are currently only available for customers with UK or US bank accounts<br /> 2: For best performance, we recommend a store size below 200 items<br /> 3: Complex options such as weight-based shipping charges are not supported";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["1. Before you start"]="1. Before you start";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["You need a Google merchant ID."]="You need a Google merchant ID.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["Get a merchant ID from <a href='http://checkout.google.com/sell/' target='_blank'>http://checkout.google.com/sell/</a>"]="Get a merchant ID from <a href='http://checkout.google.com/sell/' target='_blank'>http://checkout.google.com/sell/</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["2. Copy your Merchant ID"]="2. Copy your Merchant ID";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["Your Merchant ID will be visible at the top right of the screen when you are logged-in to your Google Checkout account"]="Your Merchant ID will be visible at the top right of the screen when you are logged-in to your Google Checkout account";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["3. Choose what kind of store you want"]="3. Choose what kind of store you want";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["Use the <strong>Add To Cart widget </strong>(1) for creating stores where users can add multiple items to a cart and purchase them in a single transaction. Use the <strong>Shopping Cart widget </strong>(3) on the same page as the Add to Cart widget.<br /> Use the <strong>Buy It Now widget </strong>(2) for allowing a user to purchase a single item immediately."]="Use the <strong>Add To Cart widget </strong>(1) for creating stores where users can add multiple items to a cart and purchase them in a single transaction. Use the <strong>Shopping Cart widget </strong>(3) on the same page as the Add to Cart widget.<br /> Use the <strong>Buy It Now widget </strong>(2) for allowing a user to purchase a single item immediately.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["The first time you click any of these widgets you will be prompted to set your Google Merchant ID."]="The first time you click any of these widgets you will be prompted to set your Google Merchant ID.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["4. Set your Google Merchant ID"]="4. Set your Google Merchant ID";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["The first time you drag a checkout widget on to the page you will be prompted for your <strong>Google Merchant ID</strong> (1). You can change this later in the Manage panel. Select your currency (2) and click <strong>Save</strong> (3)"]="The first time you drag a checkout widget on to the page you will be prompted for your <strong>Google Merchant ID</strong> (1). You can change this later in the Manage panel. Select your currency (2) and click <strong>Save</strong> (3)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["If your merchant ID fails check for a space character at the end of the numbers."]="If your merchant ID fails check for a space character at the end of the numbers.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["5. Drag the Add to Cart widget to the page"]="5. Drag the Add to Cart widget to the page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["6. Click the Edit icon"]="6. Click the Edit icon";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["7. Add the details for your item"]="7. Add the details for your item";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["Enter name, price and description (1). Optionally add an image by dragging an image of the product from the images tab to the drop box (2). Click the <strong>Save</strong> button (3)."]="Enter name, price and description (1). Optionally add an image by dragging an image of the product from the images tab to the drop box (2). Click the <strong>Save</strong> button (3).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["8. If you used the Add To Cart Widget, Add a Shopping Cart Widget to the page"]="8. If you used the Add To Cart Widget, Add a Shopping Cart Widget to the page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["9. To see your cart, got to Preview Mode"]="9. To see your cart, got to Preview Mode";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["To test your cart, click the Add to Cart Button (1). Click the disclosure triangle (2) to see the items in the cart."]="To test your cart, click the Add to Cart Button (1). Click the disclosure triangle (2) to see the items in the cart.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["How to add Google Checkout 'Buy Now' button"]="How to add Google Checkout 'Buy Now' button";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["1. Add Buy It Now button"]="1. Add Buy It Now button";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["Drag the <strong>Buy It Now widget </strong>onto the page (1) and click the <strong>edit icon </strong>(2). Enter the item details (3) and click the <strong>Save button</strong> (4)"]="Drag the <strong>Buy It Now widget </strong>onto the page (1) and click the <strong>edit icon </strong>(2). Enter the item details (3) and click the <strong>Save button</strong> (4)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_checkout_widgets.tpl
$STRINGS[$LANG]["2. Place the Buy Now button underneath the item you wish to sell"]="2. Place the Buy Now button underneath the item you wish to sell";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_get_image_url.tpl
$STRINGS[$LANG]["Text, Images, Files: Get an image URL"]="Text, Images, Files: Get an image URL";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_get_image_url.tpl
// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["Get the URL to an image on your website"]="Get the URL to an image on your website";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_get_image_url.tpl
$STRINGS[$LANG]["Click on the MEDIA tab (1)"]="Click on the MEDIA tab (1)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_get_image_url.tpl
$STRINGS[$LANG]["Click on the info icon (2) to see the image URL."]="Click on the info icon (2) to see the image URL.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_get_image_url.tpl
$STRINGS[$LANG]["Get the URL"]="Get the URL";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_get_image_url.tpl
$STRINGS[$LANG]["You will be able to see the image URL. You can copy this URL as text."]="You will be able to see the image URL. You can copy this URL as text.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_get_image_url.tpl
$STRINGS[$LANG]["A complete URL will look something like this:"]="A complete URL will look something like this:";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_get_image_url.tpl
$STRINGS[$LANG]["Be careful not to accidentally miss any of the characters at the start or end when copying and pasting."]="Be careful not to accidentally miss any of the characters at the start or end when copying and pasting.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["Using Widgets: Flickr widget"]="Using Widgets: Flickr widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["How to add a Flickr widget"]="How to add a Flickr widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["The Flickr widget displays thumbnails from Flickr that match a search term. Clicking on a thumbnail will display a larger image."]="The Flickr widget displays thumbnails from Flickr that match a search term. Clicking on a thumbnail will display a larger image.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["1. Locate the Flickr widget"]="1. Locate the Flickr widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["Click on the widgets tab and then <strong>Social</strong> (1). You may need to scroll down to see the <strong>Flickr widget</strong> (2)"]="Click on the widgets tab and then <strong>Social</strong> (1). You may need to scroll down to see the <strong>Flickr widget</strong> (2)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["2. Drag and drop the Flickr Widget to an edit zone"]="2. Drag and drop the Flickr Widget to an edit zone";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["After a few seconds the widget will appear with a default image selection"]="After a few seconds the widget will appear with a default image selection";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["3. Click the edit icon to change the default settings"]="3. Click the edit icon to change the default settings";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["4. Enter a new search term"]="4. Enter a new search term";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["Change the<strong> search term (1)</strong>. Click the <strong>OK button (2) </strong>to update the widget. It may take up to 20 seconds to update the widget. Adjust the number of pictures to be shown using the <strong>Amount</strong> <strong>slider</strong> (3)"]="Change the<strong> search term (1)</strong>. Click the <strong>OK button (2) </strong>to update the widget. It may take up to 20 seconds to update the widget. Adjust the number of pictures to be shown using the <strong>Amount</strong> <strong>slider</strong> (3)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["5. Show a specific group of pictures"]="5. Show a specific group of pictures";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["To use the Flickr widget with a specific set of photos you first need to give them a unique tag in Flickr."]="To use the Flickr widget with a specific set of photos you first need to give them a unique tag in Flickr.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["Use the same tag in the <strong>search field (1) </strong>on the Flickr widget panel and your photos will appear in the widget."]="Use the same tag in the <strong>search field (1) </strong>on the Flickr widget panel and your photos will appear in the widget.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["<strong>Note:</strong> Flickr may take hours or even days to propogate new tags."]="<strong>Note:</strong> Flickr may take hours or even days to propogate new tags.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["6. Change the size of the thumbnail"]="6. Change the size of the thumbnail";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["1. Click on the CSS tab"]="1. Click on the CSS tab";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["2. Change the line of CSS to read"]="2. Change the line of CSS to read";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["3. Change the width and height values"]="3. Change the width and height values";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_flickr_widget.tpl
$STRINGS[$LANG]["4. Click the save changes button"]="4. Click the save changes button";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_choose_new_theme.tpl
$STRINGS[$LANG]["Design, Themes & Styles: Choose new theme"]="Design, Themes & Styles: Choose new theme";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_choose_new_theme.tpl
$STRINGS[$LANG]["Choosing a new theme"]="Choosing a new theme";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_choose_new_theme.tpl
$STRINGS[$LANG]["Warning: Choosing a new theme will change the appearance of your site."]="Warning: Choosing a new theme will change the appearance of your site.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_choose_new_theme.tpl
$STRINGS[$LANG]["Open the theme browser"]="Open the theme browser";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_choose_new_theme.tpl
$STRINGS[$LANG]["Click the <strong>themes tab (1) </strong>to display the theme browser. Select <strong>Global Themes (2) </strong>to see all themes available to your account. Select L<strong>ocal theme </strong>to see themes you have created or edited."]="Click the <strong>themes tab (1) </strong>to display the theme browser. Select <strong>Global Themes (2) </strong>to see all themes available to your account. Select L<strong>ocal theme </strong>to see themes you have created or edited.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_upload_file.tpl
$STRINGS[$LANG]["Text, Images, Files: Upload a file"]="Text, Images, Files: Upload a file";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_upload_file.tpl
// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Upload a file"]="Upload a file";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_upload_file.tpl
$STRINGS[$LANG]["Upload .MP3 .PDF .MP4 .MOV or similar files"]="Upload .MP3 .PDF .MP4 .MOV or similar files";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_upload_file.tpl
$STRINGS[$LANG]["Click the <strong>MEDIA </strong>tab<strong> (1) </strong>and then the <strong>ADD</strong> button <strong>(2)</strong>"]="Click the <strong>MEDIA </strong>tab<strong> (1) </strong>and then the <strong>ADD</strong> button <strong>(2)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_upload_file.tpl
$STRINGS[$LANG]["You can upload these file types:"]="You can upload these file types:";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_upload_file.tpl
$STRINGS[$LANG]["Documents:"]="Documents:";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_upload_file.tpl
$STRINGS[$LANG]["Media:"]="Media:";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_upload_file.tpl
$STRINGS[$LANG]["Archive:"]="Archive:";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_upload_file.tpl
$STRINGS[$LANG]["Select a compatible file"]="Select a compatible file";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_upload_file.tpl
$STRINGS[$LANG]["Drag a file onto your page"]="Drag a file onto your page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_upload_file.tpl
$STRINGS[$LANG]["Note: movie files will be automatically presented in a player where possible"]="Note: movie files will be automatically presented in a player where possible";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_upload_file.tpl
$STRINGS[$LANG]["Add a description (optional)"]="Add a description (optional)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_upload_file.tpl
$STRINGS[$LANG]["Optional step: Click in the description line <strong>(1) </strong>to add a file description. After you publish this file will be downloadable from your site."]="Optional step: Click in the description line <strong>(1) </strong>to add a file description. After you publish this file will be downloadable from your site.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
$STRINGS[$LANG]["Pages & Navigation: Page Navigation"]="Pages & Navigation: Page Navigation";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
$STRINGS[$LANG]["Add a Navigation Menu widget"]="Add a Navigation Menu widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
$STRINGS[$LANG]["When you add a new page to your site any existing page navigation menus will be updated automatically."]="When you add a new page to your site any existing page navigation menus will be updated automatically.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
$STRINGS[$LANG]["Drag and drop the navigation widget<strong> (1)</strong> on to your page. If you want the widget to appear on other pages drag it into a template widget. See <a class='bold' href='/support/sitebuilder/template_widget'>Template Widgets</a>"]="Drag and drop the navigation widget<strong> (1)</strong> on to your page. If you want the widget to appear on other pages drag it into a template widget. See <a class='bold' href='/support/sitebuilder/template_widget'>Template Widgets</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
$STRINGS[$LANG]["Change the order that pages appear in the menu"]="Change the order that pages appear in the menu";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
$STRINGS[$LANG]["Pages can be placed in any order by dragging and dropping"]="Pages can be placed in any order by dragging and dropping";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
// File: /registrar-platform/www/application/views/default/sitebuilder/support_navigation_panel.tpl
$STRINGS[$LANG]["Using the Navigation Panel"]="Using the Navigation Panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
// File: /registrar-platform/www/application/views/default/sitebuilder/support_navigation_panel.tpl
$STRINGS[$LANG]["Use the <strong>Pages menu</strong> (1) to choose which group of pages this navigation bar applies to. This is only active if you have more than one folder of pages."]="Use the <strong>Pages menu</strong> (1) to choose which group of pages this navigation bar applies to. This is only active if you have more than one folder of pages.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
// File: /registrar-platform/www/application/views/default/sitebuilder/support_navigation_panel.tpl
$STRINGS[$LANG]["Use the <strong>orientation buttons</strong> (2) to choose between vertical and horizontal navigation."]="Use the <strong>orientation buttons</strong> (2) to choose between vertical and horizontal navigation.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
// File: /registrar-platform/www/application/views/default/sitebuilder/support_navigation_panel.tpl
$STRINGS[$LANG]["The <strong>alignment buttons</strong> (3) set left, centre or right alignment."]="The <strong>alignment buttons</strong> (3) set left, centre or right alignment.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
$STRINGS[$LANG]["The <strong>Reset button </strong>(4) return all the settings to the default state."]="The <strong>Reset button </strong>(4) return all the settings to the default state.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
$STRINGS[$LANG]["To set the colour and background for each state click the <strong>Styles</strong> menu (5)."]="To set the colour and background for each state click the <strong>Styles</strong> menu (5).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
// File: /registrar-platform/www/application/views/default/sitebuilder/support_navigation_panel.tpl
$STRINGS[$LANG]["Set padding and spacing for the navigation items using the sliders (6)."]="Set padding and spacing for the navigation items using the sliders (6).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
$STRINGS[$LANG]["Set Page Name and Title"]="Set Page Name and Title";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
$STRINGS[$LANG]["Click the page settings icon <strong>(1)</strong> and select <strong>Name and URL (2)</strong>."]="Click the page settings icon <strong>(1)</strong> and select <strong>Name and URL (2)</strong>.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
$STRINGS[$LANG]["The <strong>Page Title (1) </strong>will appear in the browsers tab. Enter the menu title in the <strong>Menu Title</strong> field (1), this will appear in your navigation menu. If you exclude this page from the navigation menu untick (2)"]="The <strong>Page Title (1) </strong>will appear in the browsers tab. Enter the menu title in the <strong>Menu Title</strong> field (1), this will appear in your navigation menu. If you exclude this page from the navigation menu untick (2)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
$STRINGS[$LANG]["Styling the Navigation Menu"]="Styling the Navigation Menu";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
// File: /registrar-platform/www/application/views/default/sitebuilder/support_vertical_page_navigation.tpl
$STRINGS[$LANG]["Click the edit icon"]="Click the edit icon";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
$STRINGS[$LANG]["Click the styles tab (1)"]="Click the styles tab (1)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_navigation.tpl
$STRINGS[$LANG]["Click the edit button <strong>(2) </strong>to edit the link style."]="Click the edit button <strong>(2) </strong>to edit the link style.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_link_to_image.tpl
$STRINGS[$LANG]["Text, Images, Files: How do I add a link to an image?"]="Text, Images, Files: How do I add a link to an image?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_link_to_image.tpl
// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["How do I add a link to an image?"]="How do I add a link to an image?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Getting started: Adding content to your site"]="Getting started: Adding content to your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["Before you start"]="Before you start";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Website builder has four modes:"]="Website builder has four modes:";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["<strong>Design</strong>: build and edit your site"]="<strong>Design</strong>: build and edit your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["<strong>Preview</strong>: check what site will look like"]="<strong>Preview</strong>: check what site will look like";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["<strong>Theme</strong>: browse and select themes"]="<strong>Theme</strong>: browse and select themes";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["<strong>Manage</strong>: change the settings for your site"]="<strong>Manage</strong>: change the settings for your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["When you click <strong>Publish</strong>, your site will become visible on the World Wide Web."]="When you click <strong>Publish</strong>, your site will become visible on the World Wide Web.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Click <strong>design</strong> to start adding content."]="Click <strong>design</strong> to start adding content.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Part 1: Adding text"]="Part 1: Adding text";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Drag a text block widget onto the page"]="Drag a text block widget onto the page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Click the <strong>settings icon</strong> to edit the text"]="Click the <strong>settings icon</strong> to edit the text";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Click in the text (1) to select it."]="Click in the text (1) to select it.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Type some text into the field. You can copy and paste text too but for now just type something simple."]="Type some text into the field. You can copy and paste text too but for now just type something simple.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Select the text."]="Select the text.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Click the <strong>Styles drop down (1)</strong> and then click the <strong>Heading 1 style (2) </strong>"]="Click the <strong>Styles drop down (1)</strong> and then click the <strong>Heading 1 style (2) </strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["To finish editing, click anywhere on the page outside of the text block."]="To finish editing, click anywhere on the page outside of the text block.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Part 2: Adding an image"]="Part 2: Adding an image";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Click the media tab (1) and then click add image (2)"]="Click the media tab (1) and then click add image (2)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Select a JPG or PNG file to upload"]="Select a JPG or PNG file to upload";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["After a few seconds, the image will appear in the image listing."]="After a few seconds, the image will appear in the image listing.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["After a few seconds, the image will appear in the image panel. Drag the image onto the page."]="After a few seconds, the image will appear in the image panel. Drag the image onto the page.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Drag any <strong>corner handle (1)</strong> to scale the image. Click and drag on the image to move it."]="Drag any <strong>corner handle (1)</strong> to scale the image. Click and drag on the image to move it.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_adding_content.tpl
$STRINGS[$LANG]["Next step: Customise the appearance"]="Next step: Customise the appearance";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_color_picker.tpl
$STRINGS[$LANG]["Design, Themes & Styles: The colour picker"]="Design, Themes & Styles: The colour picker";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_color_picker.tpl
$STRINGS[$LANG]["Introduction to the colour picker"]="Introduction to the colour picker";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_color_picker.tpl
$STRINGS[$LANG]["The colour picker has four parts:"]="The colour picker has four parts:";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_color_picker.tpl
$STRINGS[$LANG]["<strong>Custom colour selector</strong>: Use this to choose a colour."]="<strong>Custom colour selector</strong>: Use this to choose a colour.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_color_picker.tpl
$STRINGS[$LANG]["<strong>Colour swatch</strong>: Use this to choose a colour from the current colour swatch."]="<strong>Colour swatch</strong>: Use this to choose a colour from the current colour swatch.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_color_picker.tpl
$STRINGS[$LANG]["<strong>Selected colour display</strong>: Enter a hex value on the left to set the colour."]="<strong>Selected colour display</strong>: Enter a hex value on the left to set the colour.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_color_picker.tpl
$STRINGS[$LANG]["<strong>Picked colours storage</strong>: Store picked colours for later use"]="<strong>Picked colours storage</strong>: Store picked colours for later use";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_color_picker.tpl
$STRINGS[$LANG]["Using the colour picker"]="Using the colour picker";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_color_picker.tpl
$STRINGS[$LANG]["Click and drag to select a colour (1). Move the slider (2) to select a different hue. Hex colour values can be typed or pasted into the text box (3). Click the + button (4) to store the colour in the picked colours grid (5).  Pick a colour from the colour swatch (6) if you want the colour to change when ever you change the colour swatch. To set the colour to Transparent, click (7)."]="Click and drag to select a colour (1). Move the slider (2) to select a different hue. Hex colour values can be typed or pasted into the text box (3). Click the + button (4) to store the colour in the picked colours grid (5).  Pick a colour from the colour swatch (6) if you want the colour to change when ever you change the colour swatch. To set the colour to Transparent, click (7).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_color_picker.tpl
$STRINGS[$LANG]["Click the OK button (8) to close the colour picker and apply the selected colour"]="Click the OK button (8) to close the colour picker and apply the selected colour";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Pages & Navigation: Setting Page options"]="Pages & Navigation: Setting Page options";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["How to set page options"]="How to set page options";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["The <strong>Edit Page Options </strong>panel is used for setting the page title, page navigation, SEO and Scripting for each page."]="The <strong>Edit Page Options </strong>panel is used for setting the page title, page navigation, SEO and Scripting for each page.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Rollover the page so you can see the page options button (1)"]="Rollover the page so you can see the page options button (1)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Name and URL"]="Name and URL";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Select Name and URL (1) in the drop down menu"]="Select Name and URL (1) in the drop down menu";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Change Name and URL settings"]="Change Name and URL settings";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["The <strong>Page title </strong>(1) is the name of the page that will appear in the page selection panel in the site editor."]="The <strong>Page title </strong>(1) is the name of the page that will appear in the page selection panel in the site editor.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["The <strong>Page URL</strong> (2) is the name of the page used in URLs. The home page URL can not be changed."]="The <strong>Page URL</strong> (2) is the name of the page used in URLs. The home page URL can not be changed.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["The <strong>Menu Title</strong> (3) is the name used within any site Navigation widget"]="The <strong>Menu Title</strong> (3) is the name used within any site Navigation widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Uncheck the <strong>Show in Navigation Menu (4) </strong>tick box to exclude the page from a navigation menu"]="Uncheck the <strong>Show in Navigation Menu (4) </strong>tick box to exclude the page from a navigation menu";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["The <strong>Page Status </strong>menu (5) determines if the page will be published the next time the Publish button is clicked. <strong>Active</strong> pages are published. <strong>Inactive</strong> pages are not published and any previous versions are removed from the active site i.e. they are not visible outside of the site editor.  <strong>Draft</strong> pages are not published but any previous version remains visible."]="The <strong>Page Status </strong>menu (5) determines if the page will be published the next time the Publish button is clicked. <strong>Active</strong> pages are published. <strong>Inactive</strong> pages are not published and any previous versions are removed from the active site i.e. they are not visible outside of the site editor.  <strong>Draft</strong> pages are not published but any previous version remains visible.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["The <strong>Move page to (6) </strong>selection menu is only available if you have created page folders."]="The <strong>Move page to (6) </strong>selection menu is only available if you have created page folders.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["SEO Settings (Search Engine Optimisation)"]="SEO Settings (Search Engine Optimisation)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Select SEO (1) in the drop down menu"]="Select SEO (1) in the drop down menu";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Change SEO settings"]="Change SEO settings";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Use the SEO tab to enter keywords and a short description of your page."]="Use the SEO tab to enter keywords and a short description of your page.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Keywords should be separated with commas.<br />The keywords are placed within the &lt;head&gt; tags of the page as keywords e.g. &lt;meta name=&quot;keywords&quot; content=&quot;my, brand, new, website&quot; /&gt;"]="Keywords should be separated with commas.<br />The keywords are placed within the &lt;head&gt; tags of the page as keywords e.g. &lt;meta name=&quot;keywords&quot; content=&quot;my, brand, new, website&quot; /&gt;";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["The description may be used by search engines in Search Results pages. The content of the description field is placed within the &lt;head&gt; tags of the page <br />as description e.g. <strong>&lt;meta name=&quot;description&quot; content=&quot;Welcome to my new website.&quot; /&gt; </strong>"]="The description may be used by search engines in Search Results pages. The content of the description field is placed within the &lt;head&gt; tags of the page <br />as description e.g. <strong>&lt;meta name=&quot;description&quot; content=&quot;Welcome to my new website.&quot; /&gt; </strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Find out more about how SEO at Google Webmaster Central: <a href='http://bit.ly/9F2cDU' target='_blank'>http://bit.ly/9F2cDU</a>"]="Find out more about how SEO at Google Webmaster Central: <a href='http://bit.ly/9F2cDU' target='_blank'>http://bit.ly/9F2cDU</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Body/Page Scripts"]="Body/Page Scripts";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Note: Inappropriate use of Javascript may result in damage to your site. This feature is intended for use by people already familiar with Javascript and we cannot provide support for solving Javascript issues."]="Note: Inappropriate use of Javascript may result in damage to your site. This feature is intended for use by people already familiar with Javascript and we cannot provide support for solving Javascript issues.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Pages Script"]="Pages Script";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["The Pages Script tab allows you to place scripts in the page header."]="The Pages Script tab allows you to place scripts in the page header.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Any script placed in the <strong>Site Wide Scripts</strong> field (1) will be applied to every page across the whole site."]="Any script placed in the <strong>Site Wide Scripts</strong> field (1) will be applied to every page across the whole site.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Any script placed in the <strong>Page Specific Scripts</strong> field (2) will be applied to the current page only."]="Any script placed in the <strong>Page Specific Scripts</strong> field (2) will be applied to the current page only.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["Body Script"]="Body Script";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_setting_page_options.tpl
$STRINGS[$LANG]["The Body Scripts tab allows you to place scripts in the page body."]="The Body Scripts tab allows you to place scripts in the page body.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["Using Widgets: How do I create a form?"]="Using Widgets: How do I create a form?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["How do I create a form?"]="How do I create a form?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["Add forms to your site and have the captured form data sent to you by email or stored online for downloading later. You can also set the results/confirmation page after the form is submitted."]="Add forms to your site and have the captured form data sent to you by email or stored online for downloading later. You can also set the results/confirmation page after the form is submitted.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["1. Drag the Forms widget into an edit zone"]="1. Drag the Forms widget into an edit zone";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["Click the widgets tab (1) and scroll down to the Form section (2). Drag the form widget (3) onto your page."]="Click the widgets tab (1) and scroll down to the Form section (2). Drag the form widget (3) onto your page.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["2. Give your form a unique name"]="2. Give your form a unique name";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["Give your form a unique name (1) -<strong> it's important to not have two forms with the same name</strong> as this may cause problems with data capture."]="Give your form a unique name (1) -<strong> it's important to not have two forms with the same name</strong> as this may cause problems with data capture.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["Click the <strong>Create Form </strong>button to continue."]="Click the <strong>Create Form </strong>button to continue.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["3. The Form widget will be visible on the page"]="3. The Form widget will be visible on the page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["4. Drag a form field on to the Form widget"]="4. Drag a form field on to the Form widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["5. Click the edit button to edit the form"]="5. Click the edit button to edit the form";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["6. In edit mode, rollover any form field to see the controls for that field"]="6. In edit mode, rollover any form field to see the controls for that field";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["Click and type over the labels (1) to change the labels<br /> Click the <strong>settings</strong> button (2) to set form validation and other options<br /> Use the <strong>field drag</strong> icon (3) to move the field within the form widget<br /> Click the <strong>delete</strong> button (4) to remove the field"]="Click and type over the labels (1) to change the labels<br /> Click the <strong>settings</strong> button (2) to set form validation and other options<br /> Use the <strong>field drag</strong> icon (3) to move the field within the form widget<br /> Click the <strong>delete</strong> button (4) to remove the field";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["7. Set form field validation (optional)"]="7. Set form field validation (optional)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["Click the field settings button (see previous step) and the select the validation tab. Choose the kind of validation and a custom error message."]="Click the field settings button (see previous step) and the select the validation tab. Choose the kind of validation and a custom error message.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["8. Configure the form widget"]="8. Configure the form widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["Bring up the form settings panel by clicking the edit button on the form (See step 5)"]="Bring up the form settings panel by clicking the edit button on the form (See step 5)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["The form actions settings determine what happens when the the form <strong>Submit</strong>  button is clicked. Typically, clicking the Submit button will take the user to a Confirmation or Thank you page. All these settings are optional and your form will work without them."]="The form actions settings determine what happens when the the form <strong>Submit</strong>  button is clicked. Typically, clicking the Submit button will take the user to a Confirmation or Thank you page. All these settings are optional and your form will work without them.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["To link to a page within the current site, select a page from the <strong>Internal Link </strong>menu (7). To link to page on a different site enter the full URL (including <a href='http://www/'>http://www</a>) in the <strong>External Link</strong> field (4). To have the form data sent to an email address use the <strong>Email</strong> field (5)"]="To link to a page within the current site, select a page from the <strong>Internal Link </strong>menu (7). To link to page on a different site enter the full URL (including <a href='http://www/'>http://www</a>) in the <strong>External Link</strong> field (4). To have the form data sent to an email address use the <strong>Email</strong> field (5)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["<strong>Title</strong> (1) and D<strong>escription</strong> (2) fields are optional. It's good practice to give a form a meaningful title and description.<br /><strong>Button Text </strong>(3) is the label that will appear on the button that submits the form.<br /><strong>Internal page (7) / External Page (4) </strong>is the results page the user will see after clicking the submit button.<br /><strong>Email To </strong>(5) is the email address form data will be mailed to.<br /><strong>Message </strong>(6) is the message displayed after the user has clicked the submit button."]="<strong>Title</strong> (1) and D<strong>escription</strong> (2) fields are optional. It's good practice to give a form a meaningful title and description.<br /><strong>Button Text </strong>(3) is the label that will appear on the button that submits the form.<br /><strong>Internal page (7) / External Page (4) </strong>is the results page the user will see after clicking the submit button.<br /><strong>Email To </strong>(5) is the email address form data will be mailed to.<br /><strong>Message </strong>(6) is the message displayed after the user has clicked the submit button.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["9. Editing the style of a form"]="9. Editing the style of a form";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["Click the form <strong>Styles</strong> tab (1) and then select the form element you wish to edit (2)."]="Click the form <strong>Styles</strong> tab (1) and then select the form element you wish to edit (2).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["10. Testing a form"]="10. Testing a form";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_form.tpl
$STRINGS[$LANG]["To test a form see <a class='bold' href='/support/sitebuilder/test_form'>How do I test a form?</a>"]="To test a form see <a class='bold' href='/support/sitebuilder/test_form'>How do I test a form?</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["Text, Images, Files: Image lightbox and rollover"]="Text, Images, Files: Image lightbox and rollover";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["1. Add your images"]="1. Add your images";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_uploads.tpl
$STRINGS[$LANG]["Click on the MEDIA tab"]="Click on the MEDIA tab";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["Click on the <strong>Add </strong>button to add images"]="Click on the <strong>Add </strong>button to add images";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["In this example we will upload 3 photos:"]="In this example we will upload 3 photos:";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["<strong>Lips 100x80 -</strong> This will be the image I'll add to my page. We advise our users to reduce the size of their images to the size they need them to be on their websites, otherwise it will take them a long time to load."]="<strong>Lips 100x80 -</strong> This will be the image I'll add to my page. We advise our users to reduce the size of their images to the size they need them to be on their websites, otherwise it will take them a long time to load.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["<strong>Lips 100x80 Black&amp;White -</strong> This is the same image as before with the same size, but in black and white. I will use this image to make the rollover."]="<strong>Lips 100x80 Black&amp;White -</strong> This is the same image as before with the same size, but in black and white. I will use this image to make the rollover.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["<strong>Lips 500x400 -</strong> This is the same image as the first one in color, but with an increased size. I will use this image to make the lightbox."]="<strong>Lips 500x400 -</strong> This is the same image as the first one in color, but with an increased size. I will use this image to make the lightbox.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["The uploaded images will show in your <strong>Images</strong> tab."]="The uploaded images will show in your <strong>Images</strong> tab.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["2. Drag your image to your page"]="2. Drag your image to your page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["Drag the small coloured image to your page."]="Drag the small coloured image to your page.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["3. Set the image options"]="3. Set the image options";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["Click the options icon so the Options panel displays."]="Click the options icon so the Options panel displays.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["In the options panel you can see these two boxes where we are going to drag our images."]="In the options panel you can see these two boxes where we are going to drag our images.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["Drag the small black and white image to the first box. After doing this, when you are in Preview or Live mode and you move your mouse over the original image, you will see the black and white photo."]="Drag the small black and white image to the first box. After doing this, when you are in Preview or Live mode and you move your mouse over the original image, you will see the black and white photo.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["Drag the larger image to the second box to create a lightbox. When you go to Preview or Live mode and click on the image a pop up will show you the larger image."]="Drag the larger image to the second box to create a lightbox. When you go to Preview or Live mode and click on the image a pop up will show you the larger image.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_lightbox_and_rollover.tpl
$STRINGS[$LANG]["This is an example of a Lightbox image. You will need to be in the Preview mode to test these options."]="This is an example of a Lightbox image. You will need to be in the Preview mode to test these options.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Getting started: Optimising and publishing your site"]="Getting started: Optimising and publishing your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Before you start: What is SEO?"]="Before you start: What is SEO?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Optimising your site will help Google and other search engines to choose when to show our site in search results. The most important thing to know about Site Engine Optimisation (SEO) is that your site must have relevant and original content with links from other sites."]="Optimising your site will help Google and other search engines to choose when to show our site in search results. The most important thing to know about Site Engine Optimisation (SEO) is that your site must have relevant and original content with links from other sites.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Everyone wants their site to be listed at the top of a Google search but this can only be achieved through good content, optimisation and linkage. If you want your site to be successful it's well worth spending time learning more about how SEO works and google has an excellent guide in PDF format here : <a href='http://www.google.com/webmasters/docs/search-engine-optimization-starter-guide.pdf' target='_blank'>http://www.google.com/webmasters/docs/search-engine-optimization-starter-guide.pdf</a>"]="Everyone wants their site to be listed at the top of a Google search but this can only be achieved through good content, optimisation and linkage. If you want your site to be successful it's well worth spending time learning more about how SEO works and google has an excellent guide in PDF format here : <a href='http://www.google.com/webmasters/docs/search-engine-optimization-starter-guide.pdf' target='_blank'>http://www.google.com/webmasters/docs/search-engine-optimization-starter-guide.pdf</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Google has plenty of other information to help you learn more and this is place to start: <a href='http://www.google.com/support/webmasters/bin/answer.py?answer=7089' target='_blank'>http://www.google.com/support/webmasters/bin/answer.py?answer=7089</a>"]="Google has plenty of other information to help you learn more and this is place to start: <a href='http://www.google.com/support/webmasters/bin/answer.py?answer=7089' target='_blank'>http://www.google.com/support/webmasters/bin/answer.py?answer=7089</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Step 1: Your Domain Name"]="Step 1: Your Domain Name";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["You can publish your site at any time by clicking the <strong>Publish button (1).</strong>  When you publish your site, it will be appear with a name like <strong>http://yoursite.site.com</strong> unless you have an external domain."]="You can publish your site at any time by clicking the <strong>Publish button (1).</strong>  When you publish your site, it will be appear with a name like <strong>http://yoursite.site.com</strong> unless you have an external domain.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["If you want your site to have a name like <strong>www.yoursite.com</strong> you will need an external domain and a paid-for account."]="If you want your site to have a name like <strong>www.yoursite.com</strong> you will need an external domain and a paid-for account.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["You can use any domain name you already own or you can purchase a domain from us."]="You can use any domain name you already own or you can purchase a domain from us.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Step 2: Search engine optimisation"]="Step 2: Search engine optimisation";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Click the <strong>Manage tab (1) </strong>and then set the title and description of your site. The site title should include the name of your organisation and what you do. The site description should be one or two short sentences that you would like to appear in a search listing for your site."]="Click the <strong>Manage tab (1) </strong>and then set the title and description of your site. The site title should include the name of your organisation and what you do. The site description should be one or two short sentences that you would like to appear in a search listing for your site.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Click <strong>Save settings (3) </strong>when you are done."]="Click <strong>Save settings (3) </strong>when you are done.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["To set the SEO details for any other pages, Click the <strong>design tab (1)</strong> and then click the <strong>page settings button (2)</strong>. Click <strong>SEO (3)</strong> to open the Page options dialogue."]="To set the SEO details for any other pages, Click the <strong>design tab (1)</strong> and then click the <strong>page settings button (2)</strong>. Click <strong>SEO (3)</strong> to open the Page options dialogue.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Set the keywords for this page (1). Use short words that describe what is on this page that makes it different from other pages. Use a few words - there is no right number but there is advantage in using more than your need or repeating words."]="Set the keywords for this page (1). Use short words that describe what is on this page that makes it different from other pages. Use a few words - there is no right number but there is advantage in using more than your need or repeating words.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["The Description field should describe what is on the page in one or two short sentences (2)"]="The Description field should describe what is on the page in one or two short sentences (2)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Click <strong>Save options (3)</strong>"]="Click <strong>Save options (3)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Click the page settings button and then click <strong>Name and URL (1)</strong>"]="Click the page settings button and then click <strong>Name and URL (1)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Set the<strong> page title (1)</strong>. The contents of the page title field will usually appear on a Google search results page, so this should be short descriptive name. Click <strong>save options (2)</strong>."]="Set the<strong> page title (1)</strong>. The contents of the page title field will usually appear on a Google search results page, so this should be short descriptive name. Click <strong>save options (2)</strong>.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Step 3: Publishing your site"]="Step 3: Publishing your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["To publish your site, click <strong>Publish (1)</strong>"]="To publish your site, click <strong>Publish (1)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Step 4: Index your site on Google"]="Step 4: Index your site on Google";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["If you have published your site to an external domain now is a good time to invite Google to add your site to it's index. Go to <a href='http://www.google.com/addurl/?continue=/addurl' target='_blank'>http://www.google.com/addurl/?continue=/addurl</a> and add your full domain."]="If you have published your site to an external domain now is a good time to invite Google to add your site to it's index. Go to <a href='http://www.google.com/addurl/?continue=/addurl' target='_blank'>http://www.google.com/addurl/?continue=/addurl</a> and add your full domain.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_optimizing_publishing_your_site.tpl
$STRINGS[$LANG]["Well done! You have now created and published your first site."]="Well done! You have now created and published your first site.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_form_field_selection_menu.tpl
$STRINGS[$LANG]["Using Widgets: Form Field Selection Menu"]="Using Widgets: Form Field Selection Menu";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_form_field_selection_menu.tpl
$STRINGS[$LANG]["Add form field selection menu"]="Add form field selection menu";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_form_field_selection_menu.tpl
$STRINGS[$LANG]["If you havent already created a form, please see <a class='bold' href='/support/sitebuilder/create_form'>How to create a form</a>"]="If you havent already created a form, please see <a class='bold' href='/support/sitebuilder/create_form'>How to create a form</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_form_field_selection_menu.tpl
$STRINGS[$LANG]["Edit the existing form"]="Edit the existing form";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_form_field_selection_menu.tpl
$STRINGS[$LANG]["Click the edit form icon (1) and then <strong>double-click </strong>the field settings icon (2)."]="Click the edit form icon (1) and then <strong>double-click </strong>the field settings icon (2).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_form_field_selection_menu.tpl
$STRINGS[$LANG]["Add values to a form selection menu"]="Add values to a form selection menu";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_form_field_selection_menu.tpl
$STRINGS[$LANG]["Enter a title (1) for the menu and optionally a description."]="Enter a title (1) for the menu and optionally a description.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_form_field_selection_menu.tpl
$STRINGS[$LANG]["In the left column (2) enter the values you want saved in the form database and in the right column (3) enter the values that you want the user to see. Click the <strong>Add new value to list </strong>button<strong> </strong>(4) to add additional values."]="In the left column (2) enter the values you want saved in the form database and in the right column (3) enter the values that you want the user to see. Click the <strong>Add new value to list </strong>button<strong> </strong>(4) to add additional values.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_form_field_selection_menu.tpl
$STRINGS[$LANG]["When you are done, click <strong>OK (5)</strong>."]="When you are done, click <strong>OK (5)</strong>.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_form_field_selection_menu.tpl
$STRINGS[$LANG]["Example of form Selection Menu"]="Example of form Selection Menu";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_uploads.tpl
$STRINGS[$LANG]["Text, Images, Files: Image uploads"]="Text, Images, Files: Image uploads";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_uploads.tpl
$STRINGS[$LANG]["Importing images"]="Importing images";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_uploads.tpl
$STRINGS[$LANG]["Click the <strong>Add Image </strong>button (1) to add images. PNG and JPEG files are supported. Website Builder does support GIFs but we recommend you <strong>do not use GIF format images</strong>. Unless you need animation, use JPEG instead."]="Click the <strong>Add Image </strong>button (1) to add images. PNG and JPEG files are supported. Website Builder does support GIFs but we recommend you <strong>do not use GIF format images</strong>. Unless you need animation, use JPEG instead.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_image_uploads.tpl
$STRINGS[$LANG]["Click the sort toggle (2) to put the sort images by most recent first or least recent first."]="Click the sort toggle (2) to put the sort images by most recent first or least recent first.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["Text, Images, Files: How do i add Flash content?"]="Text, Images, Files: How do i add Flash content?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["How do i add Flash content?"]="How do i add Flash content?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["1. Import your .SWF file"]="1. Import your .SWF file";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["Click the <strong>Media</strong> tab (1) and the click the <strong>Add</strong> button (2)"]="Click the <strong>Media</strong> tab (1) and the click the <strong>Add</strong> button (2)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["2. Get the path for the file"]="2. Get the path for the file";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["After the file has finished importing, double click on the file to reveal the path (1). The path can be selected and copied as text. Click and drag over the text or triple-click to select it."]="After the file has finished importing, double click on the file to reveal the path (1). The path can be selected and copied as text. Click and drag over the text or triple-click to select it.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["Note: it can be tricky to double click the file and get the path to appear especially if you are using a touch pad. It does work but you might need to have several goes."]="Note: it can be tricky to double click the file and get the path to appear especially if you are using a touch pad. It does work but you might need to have several goes.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["3. Paste the path into a text editor"]="3. Paste the path into a text editor";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["Paste the path into a text editor like Notepad (Windows) or TextEdit (Mac)."]="Paste the path into a text editor like Notepad (Windows) or TextEdit (Mac).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["4. Add the embed widget to your page"]="4. Add the embed widget to your page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["Click the <strong>widgets</strong> tab (1) and drag the Embed widget onto the page"]="Click the <strong>widgets</strong> tab (1) and drag the Embed widget onto the page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["5. Edit the widget settings"]="5. Edit the widget settings";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["Click the <strong>edit button</strong> (1) and then delete any text in the embed field (2)"]="Click the <strong>edit button</strong> (1) and then delete any text in the embed field (2)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["6. Copy and paste the embed code"]="6. Copy and paste the embed code";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["You can find this code at <a href='http://www.w3schools.com/flash/flash_inhtml.asp' target='_blank'>http://www.w3schools.com/flash/flash_inhtml.asp</a>"]="You can find this code at <a href='http://www.w3schools.com/flash/flash_inhtml.asp' target='_blank'>http://www.w3schools.com/flash/flash_inhtml.asp</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["It looks like this:"]="It looks like this:";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["7. Edit the embed code to match the path name for you file"]="7. Edit the embed code to match the path name for you file";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["Replace both occurrences of <strong>somefilename.swf</strong> (1) with the path to your file e.g. <strong>http://basekit-file.s3.amazonaws.com/staging14709_skywrite.swf</strong>"]="Replace both occurrences of <strong>somefilename.swf</strong> (1) with the path to your file e.g. <strong>http://basekit-file.s3.amazonaws.com/staging14709_skywrite.swf</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["Change the width and height values to a size appropriate for your page (2)"]="Change the width and height values to a size appropriate for your page (2)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["Click the <strong>Save Changes </strong>button (3)."]="Click the <strong>Save Changes </strong>button (3).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["8. Go to preview mode to see your Flash content"]="8. Go to preview mode to see your Flash content";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["If the Flash movie does not appear, try refreshing your browser window."]="If the Flash movie does not appear, try refreshing your browser window.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_flash_content.tpl
$STRINGS[$LANG]["If it still doesn't work check carefully that you pasted the correct path inside the enclosing quotation marks. It's easy to accidentally delete a character."]="If it still doesn't work check carefully that you pasted the correct path inside the enclosing quotation marks. It's easy to accidentally delete a character.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["Design, Themes & Styles: Editing a theme"]="Design, Themes & Styles: Editing a theme";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["Introduction to theme editing"]="Introduction to theme editing";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["A theme is a collection of styles and settings"]="A theme is a collection of styles and settings";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["You can edit the styles and settings associated with any theme to change it's appearance. It's also possible to to create a theme from scratch."]="You can edit the styles and settings associated with any theme to change it's appearance. It's also possible to to create a theme from scratch.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["Use the <strong>Background button (1)</strong> to set the background for the theme. Then click a style in the <strong>Row styles panel (2) </strong>to apply it to the current row. Click the Edit Row style button to edit a style. Styles can be <strong>cloned </strong>using the buttons at the bottom of the panel (4) . <strong>The column styles button (5)</strong> is used to edit a Column Style."]="Use the <strong>Background button (1)</strong> to set the background for the theme. Then click a style in the <strong>Row styles panel (2) </strong>to apply it to the current row. Click the Edit Row style button to edit a style. Styles can be <strong>cloned </strong>using the buttons at the bottom of the panel (4) . <strong>The column styles button (5)</strong> is used to edit a Column Style.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["Setting a theme background"]="Setting a theme background";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["<strong>1) Theme width</strong> should normally be set to 960px. Warning: changing the width may impact the layout of your site."]="<strong>1) Theme width</strong> should normally be set to 960px. Warning: changing the width may impact the layout of your site.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["<strong>2) Theme position</strong> will place your site on the left, centre or right of the page. It should normally be left set to centre."]="<strong>2) Theme position</strong> will place your site on the left, centre or right of the page. It should normally be left set to centre.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["<strong>3) Colour mode: </strong>use a solid colour background"]="<strong>3) Colour mode: </strong>use a solid colour background";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["<strong>4) Image mode:</strong> use an image as the background an optionally repeat the image horizontally or vertically"]="<strong>4) Image mode:</strong> use an image as the background an optionally repeat the image horizontally or vertically";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["<strong>5) Flex mode: </strong>use an image and scale the image flexibly so that top and bottom third are kept unchanged and the middle portion is scaled."]="<strong>5) Flex mode: </strong>use an image and scale the image flexibly so that top and bottom third are kept unchanged and the middle portion is scaled.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["<strong>6) Linear gradient: </strong>create a blend between two colours"]="<strong>6) Linear gradient: </strong>create a blend between two colours";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["Row styles editor"]="Row styles editor";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["<strong>1) Each row style</strong> has an 'inner' and 'outer' style. The inner style is always in front of the outer style. The outer style extends to the edge of the browser window."]="<strong>1) Each row style</strong> has an 'inner' and 'outer' style. The inner style is always in front of the outer style. The outer style extends to the edge of the browser window.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["<strong>2) A background colour</strong>, gradation or image can be set. If the row contains multiple columns, the gutter slider can be used to change the widths between them."]="<strong>2) A background colour</strong>, gradation or image can be set. If the row contains multiple columns, the gutter slider can be used to change the widths between them.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["<strong>3) Row Border width</strong>, radius and colour can be set. Click the check box to apply the same value to each group of sliders.  Note: you must choose a colour to make the border visible."]="<strong>3) Row Border width</strong>, radius and colour can be set. Click the check box to apply the same value to each group of sliders.  Note: you must choose a colour to make the border visible.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["<strong>4) Set Margin and Padding</strong> for the row. Click the check box to apply the same value to each group of sliders."]="<strong>4) Set Margin and Padding</strong> for the row. Click the check box to apply the same value to each group of sliders.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["<strong>5) Shadow settings.</strong> Set the colour for the shadow to black and then move the other sliders to see that shadow."]="<strong>5) Shadow settings.</strong> Set the colour for the shadow to black and then move the other sliders to see that shadow.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["Column styles editor"]="Column styles editor";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_edit_theme.tpl
$STRINGS[$LANG]["The column style panels works like the row styles panel."]="The column style panels works like the row styles panel.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_navigation_panel.tpl
$STRINGS[$LANG]["Pages & Navigation: Navigation panel"]="Pages & Navigation: Navigation panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_navigation_panel.tpl
$STRINGS[$LANG]["The Navigation panel sets the appearance and orientation of a Navigation widget"]="The Navigation panel sets the appearance and orientation of a Navigation widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_navigation_panel.tpl
$STRINGS[$LANG]["The <strong>Reset button </strong>(4) returns all the settings to their default state."]="The <strong>Reset button </strong>(4) returns all the settings to their default state.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_navigation_panel.tpl
$STRINGS[$LANG]["Colour and Background for the whole navigation widget can be set with the <strong>Colour buttons</strong> (5). To set the colour and background for each state click the <strong>Styles</strong> menu (7)."]="Colour and Background for the whole navigation widget can be set with the <strong>Colour buttons</strong> (5). To set the colour and background for each state click the <strong>Styles</strong> menu (7).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_set_color_swatch.tpl
$STRINGS[$LANG]["Design, Themes & Styles: Set Colour Swatch"]="Design, Themes & Styles: Set Colour Swatch";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_set_color_swatch.tpl
$STRINGS[$LANG]["Set swatch colours for a theme"]="Set swatch colours for a theme";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_set_color_swatch.tpl
$STRINGS[$LANG]["Select a Colour Swatch to change all the colours in the current theme"]="Select a Colour Swatch to change all the colours in the current theme";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_set_color_swatch.tpl
$STRINGS[$LANG]["Click the <strong>Theme Colors (1) </strong>button and then click on a swatch to preview it. Click anywhere outside of the Swatch panel to confirm the change."]="Click the <strong>Theme Colors (1) </strong>button and then click on a swatch to preview it. Click anywhere outside of the Swatch panel to confirm the change.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_set_color_swatch.tpl
$STRINGS[$LANG]["To change the colour of a row, column or background see <a class='bold' href='/support/sitebuilder/customizing_your_site's>Getting started: Customising your site</a>"]="To change the colour of a row, column or background see <a class='bold' href='/support/sitebuilder/customizing_your_site's>Getting started: Customising your site</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_font_sets.tpl
$STRINGS[$LANG]["Design, Themes & Styles: Font sets"]="Design, Themes & Styles: Font sets";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_font_sets.tpl
$STRINGS[$LANG]["How to switch the font set for your site"]="How to switch the font set for your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_font_sets.tpl
$STRINGS[$LANG]["The font set panel"]="The font set panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_font_sets.tpl
$STRINGS[$LANG]["Click the <strong>font set icon (1) </strong>to open the <strong>font set panel (2)</strong>. Click any font set on the list to change the font set."]="Click the <strong>font set icon (1) </strong>to open the <strong>font set panel (2)</strong>. Click any font set on the list to change the font set.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_font_sets.tpl
$STRINGS[$LANG]["Editing a font style"]="Editing a font style";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_font_sets.tpl
$STRINGS[$LANG]["Font sets give you a quick way to change all the fonts across your site. If you want to have more control, you can create and edit type styles directly using the styles editor. See <a class='bold' href='/support/sitebuilder/creating_editing_type_style'>Editing a type style</a>."]="Font sets give you a quick way to change all the fonts across your site. If you want to have more control, you can create and edit type styles directly using the styles editor. See <a class='bold' href='/support/sitebuilder/creating_editing_type_style'>Editing a type style</a>.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_twitter_widget.tpl
$STRINGS[$LANG]["Using Widgets: Twitter Widget"]="Using Widgets: Twitter Widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_twitter_widget.tpl
$STRINGS[$LANG]["Add a Twitter feed to your site"]="Add a Twitter feed to your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_twitter_widget.tpl
$STRINGS[$LANG]["<strong>Q: Why am I not seeing any tweets when I set the Twitter widget to my username?</strong>"]="<strong>Q: Why am I not seeing any tweets when I set the Twitter widget to my username?</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_twitter_widget.tpl
$STRINGS[$LANG]["A: In most cases this is caused by not tweeting anything for 7 days or more. Only tweets up to 7 days old can be searched for. This is a restriction of the Twitter API, and is completely out of our control, sorry."]="A: In most cases this is caused by not tweeting anything for 7 days or more. Only tweets up to 7 days old can be searched for. This is a restriction of the Twitter API, and is completely out of our control, sorry.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_twitter_widget.tpl
$STRINGS[$LANG]["Locate the Twitter widget"]="Locate the Twitter widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_twitter_widget.tpl
$STRINGS[$LANG]["Click on the <strong>widgets tab (1)</strong> and then on the <strong>Social tab (2)</strong>. You may need to scroll down to see the Twitter widget."]="Click on the <strong>widgets tab (1)</strong> and then on the <strong>Social tab (2)</strong>. You may need to scroll down to see the Twitter widget.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_twitter_widget.tpl
$STRINGS[$LANG]["Drag and drop the Twitter widget onto an edit zone"]="Drag and drop the Twitter widget onto an edit zone";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_twitter_widget.tpl
// File: /registrar-platform/www/application/views/default/sitebuilder/support_twitter_widget.tpl
$STRINGS[$LANG]["When you drop the widget, the default Twitter feed will appear."]="When you drop the widget, the default Twitter feed will appear.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_twitter_widget.tpl
$STRINGS[$LANG]["Click the edit icon to change the settings"]="Click the edit icon to change the settings";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_twitter_widget.tpl
$STRINGS[$LANG]["Set the search term"]="Set the search term";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_twitter_widget.tpl
$STRINGS[$LANG]["Type a search term into the search field (1) and hit the return key on your keyboard."]="Type a search term into the search field (1) and hit the return key on your keyboard.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_twitter_widget.tpl
$STRINGS[$LANG]["Change the appearance of the widget"]="Change the appearance of the widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_twitter_widget.tpl
// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["Click on the styles tab (1) to display the widget styles controls. Click on a text style <strong>Edit </strong>button (2) and then use the controls to adjust the style."]="Click on the styles tab (1) to display the widget styles controls. Click on a text style <strong>Edit </strong>button (2) and then use the controls to adjust the style.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_maps_widget.tpl
$STRINGS[$LANG]["Using Widgets: Google Maps widget"]="Using Widgets: Google Maps widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_maps_widget.tpl
$STRINGS[$LANG]["How to add Google Maps widget"]="How to add Google Maps widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_maps_widget.tpl
$STRINGS[$LANG]["1. Locate the Google Maps widget"]="1. Locate the Google Maps widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_maps_widget.tpl
$STRINGS[$LANG]["Click the <strong>Widgets tab (1)</strong> and then drag the <strong>Google Maps widget (2) </strong>onto the page."]="Click the <strong>Widgets tab (1)</strong> and then drag the <strong>Google Maps widget (2) </strong>onto the page.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_maps_widget.tpl
$STRINGS[$LANG]["2. Edit the Google Maps widget settings"]="2. Edit the Google Maps widget settings";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_maps_widget.tpl
$STRINGS[$LANG]["You can drag the corner handles to resize the map."]="You can drag the corner handles to resize the map.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_maps_widget.tpl
$STRINGS[$LANG]["Click the <strong>edit icon (1) </strong>to open the settings panel"]="Click the <strong>edit icon (1) </strong>to open the settings panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_maps_widget.tpl
$STRINGS[$LANG]["3. Configure Settings"]="3. Configure Settings";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_maps_widget.tpl
$STRINGS[$LANG]["To centre to the map on a given location, type the address into the <strong>Find address</strong> field (1) and press the return key on your keyboard. Hint: for non UK addresses try typing the country name in first and then pressing the return key on your keyboard to centre the map on a given country."]="To centre to the map on a given location, type the address into the <strong>Find address</strong> field (1) and press the return key on your keyboard. Hint: for non UK addresses try typing the country name in first and then pressing the return key on your keyboard to centre the map on a given country.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_maps_widget.tpl
$STRINGS[$LANG]["The <strong>options buttons </strong>(2) will add buttons to the map so the user can zoom and select map/satellite/hybrid views."]="The <strong>options buttons </strong>(2) will add buttons to the map so the user can zoom and select map/satellite/hybrid views.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_google_maps_widget.tpl
$STRINGS[$LANG]["To add a pointer to the map, drag and drop a pointer on to the map (3). To add your own image to the map, drag and drop an image into the <strong>drop target </strong>(4) and then drag it from the pointer area (3) onto the map."]="To add a pointer to the map, drag and drop a pointer on to the map (3). To add your own image to the map, drag and drop an image into the <strong>drop target </strong>(4) and then drag it from the pointer area (3) onto the map.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_colour_row_column.tpl
$STRINGS[$LANG]["Design, Themes & Styles: Change the colour of a row or column"]="Design, Themes & Styles: Change the colour of a row or column";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_colour_row_column.tpl
$STRINGS[$LANG]["Part 1: Use styles to set colours"]="Part 1: Use styles to set colours";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_colour_row_column.tpl
$STRINGS[$LANG]["You can set the colour of a row or column by creating a style."]="You can set the colour of a row or column by creating a style.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_colour_row_column.tpl
$STRINGS[$LANG]["To create a row style, click the <strong>row settings button (1) </strong>and then click the <strong>add style button (2).</strong>"]="To create a row style, click the <strong>row settings button (1) </strong>and then click the <strong>add style button (2).</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_colour_row_column.tpl
$STRINGS[$LANG]["Give the row a meaningful name (1) and click the <strong>Add row style button (2)</strong>"]="Give the row a meaningful name (1) and click the <strong>Add row style button (2)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_colour_row_column.tpl
$STRINGS[$LANG]["Click the <strong>colour chip (1) </strong>and then select a colour. You can click and drag on the <strong>colour selector (2)</strong> or paste a <strong>hex value (3). </strong> Click the <strong>OK button (4) </strong>to confirm the selection."]="Click the <strong>colour chip (1) </strong>and then select a colour. You can click and drag on the <strong>colour selector (2)</strong> or paste a <strong>hex value (3). </strong> Click the <strong>OK button (4) </strong>to confirm the selection.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_colour_row_column.tpl
$STRINGS[$LANG]["Part 2: To change the colour of a column"]="Part 2: To change the colour of a column";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_colour_row_column.tpl
$STRINGS[$LANG]["To split a row into columns use the row split menu"]="To split a row into columns use the row split menu";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_colour_row_column.tpl
$STRINGS[$LANG]["Click the add style button (1) and then follow the same procedure as for a row."]="Click the add style button (1) and then follow the same procedure as for a row.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Getting started: Glossary"]="Getting started: Glossary";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Site Editor"]="Site Editor";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["The <strong>Site editor </strong>is where you create, edit and publish websites."]="The <strong>Site editor </strong>is where you create, edit and publish websites.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["The Site Editor has 6 main parts:"]="The Site Editor has 6 main parts:";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Site navigation panel"]="Site navigation panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Every page in your site is listed here. Click a page to edit it."]="Every page in your site is listed here. Click a page to edit it.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Theme settings panel"]="Theme settings panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Select a Colour swatch or Font set to change the appearance of your current theme. Edit the background for the site, show / hide rows and switch guides on and off."]="Select a Colour swatch or Font set to change the appearance of your current theme. Edit the background for the site, show / hide rows and switch guides on and off.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Row settings"]="Row settings";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Apply a row style, split the row into columns or drag to change the row order."]="Apply a row style, split the row into columns or drag to change the row order.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Widget panel"]="Widget panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Drag widgets on to the page to add content to your site."]="Drag widgets on to the page to add content to your site.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Top bar"]="Top bar";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Switch between modes. Click the help link at top right for support and help documents."]="Switch between modes. Click the help link at top right for support and help documents.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Column Style selector"]="Column Style selector";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Apply a style to a column"]="Apply a style to a column";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["The four modes: Design, Preview, Theme, Manage"]="The four modes: Design, Preview, Theme, Manage";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["We have four modes:"]="We have four modes:";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["<strong>Desgn: </strong>build and edit your site"]="<strong>Desgn: </strong>build and edit your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["<strong>Preview:</strong> check what site will look like"]="<strong>Preview:</strong> check what site will look like";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["<strong>Theme:</strong> browse and select themes"]="<strong>Theme:</strong> browse and select themes";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["<strong>Manage:</strong> change the settings for your site"]="<strong>Manage:</strong> change the settings for your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Themes and Templates"]="Themes and Templates";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["A <strong>theme</strong> is a collection of styles and other settings that together define the appearance of a website. When a theme is applied to a site, style and settings are replaced but content is left unchanged."]="A <strong>theme</strong> is a collection of styles and other settings that together define the appearance of a website. When a theme is applied to a site, style and settings are replaced but content is left unchanged.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["A <strong>template</strong> defines the layout of one or more pages. Every page has it's own unique template."]="A <strong>template</strong> defines the layout of one or more pages. Every page has it's own unique template.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["A template widget is used to place content (e.g. a header or footer) that you want to repeat over many pages."]="A template widget is used to place content (e.g. a header or footer) that you want to repeat over many pages.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Widget"]="Widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["A Widget is a piece of content or functionality that can be added to a page. Widgets include text, images, forms, social networking, maps and video players."]="A Widget is a piece of content or functionality that can be added to a page. Widgets include text, images, forms, social networking, maps and video players.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Template widget"]="Template widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["A template widget is a container for items that you want to share across multiple pages. When you edit a template widget on one page, it will update on every other page too."]="A template widget is a container for items that you want to share across multiple pages. When you edit a template widget on one page, it will update on every other page too.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Colour swatch"]="Colour swatch";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["A colour swatch is a set of 5 colours used to set the colours for a theme. If you use the colours in a swatch and then change the swatch the colours will update. If you choose colour outside of the swatch, your colour will stay fixed."]="A colour swatch is a set of 5 colours used to set the colours for a theme. If you use the colours in a swatch and then change the swatch the colours will update. If you choose colour outside of the swatch, your colour will stay fixed.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["Font set"]="Font set";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_glossary.tpl
$STRINGS[$LANG]["A font set is used to set all the font styles used within a theme. If you use styles within a font set and then change the set the related font styles will change. If you choose a font style not included in the font set the style will stay fixed."]="A font set is used to set all the font styles used within a theme. If you use styles within a font set and then change the set the related font styles will change. If you choose a font style not included in the font set the style will stay fixed.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_sitemap_widget.tpl
$STRINGS[$LANG]["Using Widgets: Sitemap widget"]="Using Widgets: Sitemap widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_sitemap_widget.tpl
$STRINGS[$LANG]["How to add sitemap widget"]="How to add sitemap widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_sitemap_widget.tpl
$STRINGS[$LANG]["Use the sitemap widget to add a sitemap to your page"]="Use the sitemap widget to add a sitemap to your page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_sitemap_widget.tpl
$STRINGS[$LANG]["The Sitemap widget creates a dynamically updated list of every page in your site to your page. Whenever you update your site the the sitemap will update automatically. Use the sitemap widget if you have a site with lots of pages to help users see the site structure and navigate the content."]="The Sitemap widget creates a dynamically updated list of every page in your site to your page. Whenever you update your site the the sitemap will update automatically. Use the sitemap widget if you have a site with lots of pages to help users see the site structure and navigate the content.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["Using Widgets: Add Tweet Button"]="Using Widgets: Add Tweet Button";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["Add tweet button to any page"]="Add tweet button to any page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["Add this button to your website to let people share content on Twitter without having to leave the page."]="Add this button to your website to let people share content on Twitter without having to leave the page.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["You will need a Twitter account."]="You will need a Twitter account.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["1. Create the button"]="1. Create the button";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["Go to Twitter page <a href='http://twitter.com/about/resources'>http://twitter.com/about/resources</a>"]="Go to Twitter page <a href='http://twitter.com/about/resources'>http://twitter.com/about/resources</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["Click <strong>Create Tweet Button </strong>(1)"]="Click <strong>Create Tweet Button </strong>(1)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["2. Customize the button &amp; get the code"]="2. Customize the button &amp; get the code";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["Choose the button type (1), enter your Twitter Username (2), optionally recommend two other Twitter accounts (3), and copy the code (4)."]="Choose the button type (1), enter your Twitter Username (2), optionally recommend two other Twitter accounts (3), and copy the code (4).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["3. Login into Website Builder, drag an Embed Widget into your page"]="3. Login into Website Builder, drag an Embed Widget into your page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["4. Edit the Embed Widget"]="4. Edit the Embed Widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["Click the edit icon (1)"]="Click the edit icon (1)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["5. Paste the code into the widget"]="5. Paste the code into the widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["Paste the code into the Settings Tab of the Embed Widget (1), and click <strong>Save Changes </strong>(2)"]="Paste the code into the Settings Tab of the Embed Widget (1), and click <strong>Save Changes </strong>(2)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["The Twitter Button will appear. If it does not appear, it is probably because you did not click the save changes button in the previous step."]="The Twitter Button will appear. If it does not appear, it is probably because you did not click the save changes button in the previous step.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["6. Go to preview mode to try out your button"]="6. Go to preview mode to try out your button";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_add_tweet_button.tpl
$STRINGS[$LANG]["Click the preview button (1)."]="Click the preview button (1).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["Using Widgets: RSS feed widget"]="Using Widgets: RSS feed widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["How to use the RSS feed widget to add a dynamic RSS feed to your page"]="How to use the RSS feed widget to add a dynamic RSS feed to your page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["Locate the RSS feed widget"]="Locate the RSS feed widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["Click on the widgets tab (1) and then click on the Social group (2). You may need to scroll down to see the RSS feed widget"]="Click on the widgets tab (1) and then click on the Social group (2). You may need to scroll down to see the RSS feed widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["Drag and drop the widget onto an edit zone"]="Drag and drop the widget onto an edit zone";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["When you drop the widget, it will show the default RSS feed"]="When you drop the widget, it will show the default RSS feed";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["Edit the default settings"]="Edit the default settings";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["Click and drag to position the widget. Click the edit button to show the RSS feed settings panel."]="Click and drag to position the widget. Click the edit button to show the RSS feed settings panel.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["Change the RSS feed source"]="Change the RSS feed source";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["Paste a feed URL into the field. Press the <strong>return key</strong> on your keyboard or click the <strong>OK </strong>button<strong> </strong>to set the feed. After a few sconds the RSS widget will update."]="Paste a feed URL into the field. Press the <strong>return key</strong> on your keyboard or click the <strong>OK </strong>button<strong> </strong>to set the feed. After a few sconds the RSS widget will update.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["You can find feeds at <a class='bold' href='http://feeds.feedburner.com/'>http://feeds.feedburner.com</a>"]="You can find feeds at <a class='bold' href='http://feeds.feedburner.com/'>http://feeds.feedburner.com</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["<strong>Note: A Feed URL is not the same as a website URL.</strong>"]="<strong>Note: A Feed URL is not the same as a website URL.</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_rss_feed_widget.tpl
$STRINGS[$LANG]["Change the appearence of the RSS feed"]="Change the appearence of the RSS feed";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_background_widget.tpl
$STRINGS[$LANG]["Using Widgets: Background widget"]="Using Widgets: Background widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_background_widget.tpl
$STRINGS[$LANG]["Using the Background widget"]="Using the Background widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_background_widget.tpl
$STRINGS[$LANG]["The Background widget is used to provide a coloured background for other widgets. To edit the background for the whole site, see <a class='bold' href='/support/sitebuilder/site_background'>Site background</a>"]="The Background widget is used to provide a coloured background for other widgets. To edit the background for the whole site, see <a class='bold' href='/support/sitebuilder/site_background'>Site background</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_background_widget.tpl
$STRINGS[$LANG]["Drag a background widget onto the page"]="Drag a background widget onto the page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_background_widget.tpl
$STRINGS[$LANG]["Drag a widget onto the background widget"]="Drag a widget onto the background widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_background_widget.tpl
$STRINGS[$LANG]["Click the settings icon to change the settings for the background widget"]="Click the settings icon to change the settings for the background widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_background_widget.tpl
$STRINGS[$LANG]["Background widget panel"]="Background widget panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_background_widget.tpl
$STRINGS[$LANG]["The Background widget creates boxes which can contain other widgets such as text or images."]="The Background widget creates boxes which can contain other widgets such as text or images.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_background_widget.tpl
$STRINGS[$LANG]["Use the <strong>Background colour picker </strong>(1) to choose a background a colour and the <strong>sliders</strong> (2) to set corner radius, opacity and padding."]="Use the <strong>Background colour picker </strong>(1) to choose a background a colour and the <strong>sliders</strong> (2) to set corner radius, opacity and padding.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_background_widget.tpl
$STRINGS[$LANG]["To use an image for a background, drag an image into the <strong>image drop box</strong> (3)"]="To use an image for a background, drag an image into the <strong>image drop box</strong> (3)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_background_widget.tpl
$STRINGS[$LANG]["Using the Background widget with an image"]="Using the Background widget with an image";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_background_widget.tpl
$STRINGS[$LANG]["<strong>Flex</strong> will attempt to scale the image intelligently so that the upper and lower thirds of the image are left unscaled. This preserves rounded corners or other edge effect as the widget is scaled."]="<strong>Flex</strong> will attempt to scale the image intelligently so that the upper and lower thirds of the image are left unscaled. This preserves rounded corners or other edge effect as the widget is scaled.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_background_widget.tpl
$STRINGS[$LANG]["<strong>Tile</strong> will repeat the image - use the Repeat X and Repeat Y check boxes to control in which direction the tiling takes place."]="<strong>Tile</strong> will repeat the image - use the Repeat X and Repeat Y check boxes to control in which direction the tiling takes place.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_background_widget.tpl
$STRINGS[$LANG]["<strong>Stretch</strong> will scale the image to fill the box."]="<strong>Stretch</strong> will scale the image to fill the box.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Design, Themes & Styles: Creating or editing a type style"]="Design, Themes & Styles: Creating or editing a type style";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Part 1: Create a new type style"]="Part 1: Create a new type style";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Create a new type style to define the appearance of one or more text blocks."]="Create a new type style to define the appearance of one or more text blocks.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Select or create a text block."]="Select or create a text block.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Click the <strong>edit icon (1)</strong> to open the text block for editing"]="Click the <strong>edit icon (1)</strong> to open the text block for editing";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Click the <strong>Styles drop down (1) </strong>and then click <strong>Add new type style (2)</strong>"]="Click the <strong>Styles drop down (1) </strong>and then click <strong>Add new type style (2)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Give your style a unique<strong> title (1). </strong>Select a <strong>clone class (2). </strong>If you are creating  a style for use as a heading, select one of the heading styles (Heading 1, Heading 2 etc). If you are creating a style to be used as a paragraph style select 'p'."]="Give your style a unique<strong> title (1). </strong>Select a <strong>clone class (2). </strong>If you are creating  a style for use as a heading, select one of the heading styles (Heading 1, Heading 2 etc). If you are creating a style to be used as a paragraph style select 'p'.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["<strong>Element (3) </strong>and <strong>Class name (4) </strong>should not normally need to be changed. Unless you understand enough about CSS to know what these terms mean, you should leave them alone."]="<strong>Element (3) </strong>and <strong>Class name (4) </strong>should not normally need to be changed. Unless you understand enough about CSS to know what these terms mean, you should leave them alone.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Click <strong>Add style (5)</strong> to create the style."]="Click <strong>Add style (5)</strong> to create the style.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Your new type style will be added to the list. You may have to <strong>scroll down (1)</strong> to see it."]="Your new type style will be added to the list. You may have to <strong>scroll down (1)</strong> to see it.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Part 2: Editing a type style"]="Part 2: Editing a type style";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Click the <strong>edit button (2) </strong>to edit the style."]="Click the <strong>edit button (2) </strong>to edit the style.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Click the <strong>custom tab (1)</strong>"]="Click the <strong>custom tab (1)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Choose a <strong>typeface (1)</strong>, <strong>size (2)</strong> and <strong>line height (3)</strong>. Click the <strong>lock (4)</strong> to lock the relationship between size and line height."]="Choose a <strong>typeface (1)</strong>, <strong>size (2)</strong> and <strong>line height (3)</strong>. Click the <strong>lock (4)</strong> to lock the relationship between size and line height.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Use the options on the left (1) to change the appearance of the style. You can preview the change on the right (2)."]="Use the options on the left (1) to change the appearance of the style. You can preview the change on the right (2).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["If you use the <strong>Shadows and VIsual FX </strong>options (1) your type style may not display correctly in older browsers like Internet Explorer 6 &amp; 7. This is an inherent limitation of obsolete browser technology."]="If you use the <strong>Shadows and VIsual FX </strong>options (1) your type style may not display correctly in older browsers like Internet Explorer 6 &amp; 7. This is an inherent limitation of obsolete browser technology.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["Click the <strong>save changes button (1) </strong>when you finished editing."]="Click the <strong>save changes button (1) </strong>when you finished editing.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["To apply the style, select some text and click on the <strong>style name (1)</strong>"]="To apply the style, select some text and click on the <strong>style name (1)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_creating_editing_type_style.tpl
$STRINGS[$LANG]["The new style is now applied and can be used anywhere on your site."]="The new style is now applied and can be used anywhere on your site.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Text, Images, Files: How do I change the favicon?"]="Text, Images, Files: How do I change the favicon?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Before you get started: Favicon FAQs"]="Before you get started: Favicon FAQs";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["You can replace the standard favicon with your own design"]="You can replace the standard favicon with your own design";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Q: What is a favicon?"]="Q: What is a favicon?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["A: A favicon (short for favorites icon), also known as a shortcut icon, website icon, URL icon, or bookmark icon is a 16&times;</i>16 or 32&times;32 pixel square icon associated with a particular website or webpage. Definition from wikipedia: <a href='http://en.wikipedia.org/wiki/Favicon' target='_blank'>http://en.wikipedia.org/wiki/Favicon</a>"]="A: A favicon (short for favorites icon), also known as a shortcut icon, website icon, URL icon, or bookmark icon is a 16&times;</i>16 or 32&times;32 pixel square icon associated with a particular website or webpage. Definition from wikipedia: <a href='http://en.wikipedia.org/wiki/Favicon' target='_blank'>http://en.wikipedia.org/wiki/Favicon</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Q: I'm in too much of a hurry to read these instructions so I'm just going to try a line of code I found somewhere else. Will that work?"]="Q: I'm in too much of a hurry to read these instructions so I'm just going to try a line of code I found somewhere else. Will that work?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["A: No, it won't. The only way to change the favicon on your site is the one shown in this tutorial"]="A: No, it won't. The only way to change the favicon on your site is the one shown in this tutorial";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Q: How can I make a favicon?"]="Q: How can I make a favicon?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["A: We make it for you. You just need to add the image you want to your media tab"]="A: We make it for you. You just need to add the image you want to your media tab";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Q: Why do some browsers not display my favicon?"]="Q: Why do some browsers not display my favicon?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["A: Some versions of Internet Explorer may not display the favicon correctly. This is not a fault with Website Builder. Some problems may be resolved by clearing browser cache. There is nothing we can do to resolve problems with old browsers."]="A: Some versions of Internet Explorer may not display the favicon correctly. This is not a fault with Website Builder. Some problems may be resolved by clearing browser cache. There is nothing we can do to resolve problems with old browsers.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Q: Why do I sometimes see a brief flash of another favicon before my own favicon appears?"]="Q: Why do I sometimes see a brief flash of another favicon before my own favicon appears?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["A: It's probably a browser caching issue. There is nothing we can do about this."]="A: It's probably a browser caching issue. There is nothing we can do about this.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Q: Will you change the favicon on my site for me?"]="Q: Will you change the favicon on my site for me?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["A: No."]="A: No.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Q: Can I disable the favicon so no favicon at all is displayed?"]="Q: Can I disable the favicon so no favicon at all is displayed?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["A: No. You can create a blank favicon and use that but you can not remove the favicon."]="A: No. You can create a blank favicon and use that but you can not remove the favicon.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Step 1. Add an image to your editor"]="Step 1. Add an image to your editor";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Click the <strong>Media</strong> tab (1) and click on the <strong>Add</strong> (2) button. Upload your image (3)"]="Click the <strong>Media</strong> tab (1) and click on the <strong>Add</strong> (2) button. Upload your image (3)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Step 2. Set the image as your favicon"]="Step 2. Set the image as your favicon";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Click on <strong>MANAGE (1)</strong> at the top of the editor and you will be able to see your site settings <strong>(2)</strong>"]="Click on <strong>MANAGE (1)</strong> at the top of the editor and you will be able to see your site settings <strong>(2)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Scroll down until you find the <strong>Custom Favicon</strong> options and click on the <strong>Choose (1) </strong>button and select the image you want for your favicon."]="Scroll down until you find the <strong>Custom Favicon</strong> options and click on the <strong>Choose (1) </strong>button and select the image you want for your favicon.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Once you've selected the image reload the page so you can see the changes"]="Once you've selected the image reload the page so you can see the changes";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Step 3. Publish your site"]="Step 3. Publish your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["Click on the PUBLISH button to see the changes in your published site."]="Click on the PUBLISH button to see the changes in your published site.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_change_fav_icon.tpl
$STRINGS[$LANG]["This is how your favicon will look like after you finish."]="This is how your favicon will look like after you finish.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_social_widget.tpl
$STRINGS[$LANG]["Using Widgets: Social Bookmarking Widget"]="Using Widgets: Social Bookmarking Widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_social_widget.tpl
$STRINGS[$LANG]["How to add social bookmarking widget"]="How to add social bookmarking widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_social_widget.tpl
$STRINGS[$LANG]["Locate the Social bookmarking widget"]="Locate the Social bookmarking widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_social_widget.tpl
$STRINGS[$LANG]["Click the widgets tab (1) and drag the social bookmarks widget on to the page (2)"]="Click the widgets tab (1) and drag the social bookmarks widget on to the page (2)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_social_widget.tpl
$STRINGS[$LANG]["Enter preview mode to test the widget"]="Enter preview mode to test the widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_social_widget.tpl
$STRINGS[$LANG]["Hover over the widget in preview mode to test it"]="Hover over the widget in preview mode to test it";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_social_widget.tpl
$STRINGS[$LANG]["Click any of the icons to bookmark the current page with selected social bookmark service."]="Click any of the icons to bookmark the current page with selected social bookmark service.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_vertical_page_navigation.tpl
$STRINGS[$LANG]["Pages & Navigation: Vertical Page Navigation"]="Pages & Navigation: Vertical Page Navigation";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_vertical_page_navigation.tpl
$STRINGS[$LANG]["Add a navigation widget to your page"]="Add a navigation widget to your page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_vertical_page_navigation.tpl
$STRINGS[$LANG]["Page navigation can be vertical or horizontal"]="Page navigation can be vertical or horizontal";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_vertical_page_navigation.tpl
$STRINGS[$LANG]["Drag the navigation widget onto your page"]="Drag the navigation widget onto your page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_vertical_page_navigation.tpl
$STRINGS[$LANG]["Click the orientation button in the widget settings panel (1)"]="Click the orientation button in the widget settings panel (1)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_vertical_page_navigation.tpl
$STRINGS[$LANG]["The orienation of the navigation menu will change to vertical"]="The orienation of the navigation menu will change to vertical";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Text, Images, Files: Working with images"]="Text, Images, Files: Working with images";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Before you start: Image FAQ's"]="Before you start: Image FAQ's";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Q: I have imported some large PNG images and now my site seems very slow. Why is this?"]="Q: I have imported some large PNG images and now my site seems very slow. Why is this?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["A: PNG files are not an appropriate format for large images. PNG files are best used for small graphics where you need transparency. You should try to use JPEGs where possible.  For an 800 x 600 image the difference between a JPEG may be 90% smaller than a PNG without appreciable loss of visual quality."]="A: PNG files are not an appropriate format for large images. PNG files are best used for small graphics where you need transparency. You should try to use JPEGs where possible.  For an 800 x 600 image the difference between a JPEG may be 90% smaller than a PNG without appreciable loss of visual quality.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Q: Why does my PNG file not work properly?"]="Q: Why does my PNG file not work properly?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["A: Check that your PNG is a RGB format image.We support 24bit and 32 bit PNG files. We do not support 8 bit PNG files. For best results we recommend you use Photoshop's 'Save for web and devices' option to create the files."]="A: Check that your PNG is a RGB format image.We support 24bit and 32 bit PNG files. We do not support 8 bit PNG files. For best results we recommend you use Photoshop's 'Save for web and devices' option to create the files.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Q: What size should my images be?"]="Q: What size should my images be?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["A: For best performance always use 72 dpi images and scale them to the correct size in Photoshop or some other image editor before you import them. If you import a large image and then scale it down the image still retains it's full file size and this may make your web page slow to load."]="A: For best performance always use 72 dpi images and scale them to the correct size in Photoshop or some other image editor before you import them. If you import a large image and then scale it down the image still retains it's full file size and this may make your web page slow to load.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Q: Can I use GIFs?"]="Q: Can I use GIFs?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["A: Website Builder does support GIFs but we recommend you do <strong>not</strong> use this format. Unless you need animation, use JPEG instead. If you do need to use GIFs, avoid scaling them."]="A: Website Builder does support GIFs but we recommend you do <strong>not</strong> use this format. Unless you need animation, use JPEG instead. If you do need to use GIFs, avoid scaling them.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Q: Is there a limit to how many images I can upload?"]="Q: Is there a limit to how many images I can upload?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["A: No, there is no limit. You can upload as many images as will fit within your storage allowance. Demo accounts have 10Mb of storage space, paid-for accounts have 1Gb or more."]="A: No, there is no limit. You can upload as many images as will fit within your storage allowance. Demo accounts have 10Mb of storage space, paid-for accounts have 1Gb or more.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Q: How many images can I upload simultaneously?"]="Q: How many images can I upload simultaneously?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["A: You can upload up to 5 images in a single go."]="A: You can upload up to 5 images in a single go.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Q: Can I place one image on top of another?"]="Q: Can I place one image on top of another?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["A: You may be able to use the Background Widget to do this."]="A: You may be able to use the Background Widget to do this.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Importing an image"]="Importing an image";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Select the <strong>Media tab (1)</strong> and then click<strong> Add (2)</strong> to add an image."]="Select the <strong>Media tab (1)</strong> and then click<strong> Add (2)</strong> to add an image.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["You can import JPEG or PNG format images. Generally it's best to use JPEG for photographs and PNG for graphics."]="You can import JPEG or PNG format images. Generally it's best to use JPEG for photographs and PNG for graphics.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["<strong>Note 1: </strong>If the Add Image button is not visible, this may indicate you need to re-install Adobe Flash player. Logout of the editor, go to <a href='http://get.adobe.com/flashplayer/' target='_blank'>http://get.adobe.com/flashplayer/</a>  and follow the installation instructions."]="<strong>Note 1: </strong>If the Add Image button is not visible, this may indicate you need to re-install Adobe Flash player. Logout of the editor, go to <a href='http://get.adobe.com/flashplayer/' target='_blank'>http://get.adobe.com/flashplayer/</a>  and follow the installation instructions.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["<strong>Note 2: </strong>IE6 does not support  PNG transparency. If you want IE6 compatibility do not use PNG transparency."]="<strong>Note 2: </strong>IE6 does not support  PNG transparency. If you want IE6 compatibility do not use PNG transparency.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Positioning an image"]="Positioning an image";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Click and drag within the image (1) to move the image."]="Click and drag within the image (1) to move the image.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["<strong>Do not click on the borders or the handles (2) to move the image."]="<strong>Do not click on the borders or the handles (2) to move the image.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["The image will not move if the settings panel is open. Use the close button (3) to close the panel"]="The image will not move if the settings panel is open. Use the close button (3) to close the panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Scaling an image"]="Scaling an image";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Click and drag on any of the handles to scale the image"]="Click and drag on any of the handles to scale the image";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["The image will not scale if the settings panel is open. Use the close button (1) to close the panel."]="The image will not scale if the settings panel is open. Use the close button (1) to close the panel.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Positioning two images next to each other"]="Positioning two images next to each other";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Use the <strong>columns layout widget </strong>to place images or any other widget alongside each other, or you could just drag one image by the side of other and the column will appear."]="Use the <strong>columns layout widget </strong>to place images or any other widget alongside each other, or you could just drag one image by the side of other and the column will appear.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Deleting an image"]="Deleting an image";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["To delete an image from the page, click the delete button (1). To remove the image from your site you must also delete it from the Images Tab."]="To delete an image from the page, click the delete button (1). To remove the image from your site you must also delete it from the Images Tab.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Reset the scale of an image"]="Reset the scale of an image";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Click the reset button (1) to reset the image to it's original size"]="Click the reset button (1) to reset the image to it's original size";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Change the image for another"]="Change the image for another";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["With this button (1) you'll be able to see all the images you have in your library and change the current image for another."]="With this button (1) you'll be able to see all the images you have in your library and change the current image for another.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Add a link to an image"]="Add a link to an image";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["First click on the image configuration button<strong> (1) </strong>so the settings panel will be displayed."]="First click on the image configuration button<strong> (1) </strong>so the settings panel will be displayed.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["Click the Add link button<strong> (1).</strong>"]="Click the Add link button<strong> (1).</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_working_with_images.tpl
$STRINGS[$LANG]["A window will display. Paste in the URL you want the image to link to or select one from the drop down menu."]="A window will display. Paste in the URL you want the image to link to or select one from the drop down menu.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_manage_database.tpl
$STRINGS[$LANG]["Using Widgets: Manage databases"]="Using Widgets: Manage databases";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_manage_database.tpl
$STRINGS[$LANG]["Managing Databases"]="Managing Databases";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_manage_database.tpl
$STRINGS[$LANG]["Click on Manage (1) and then click on the Database (2) button."]="Click on Manage (1) and then click on the Database (2) button.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_manage_database.tpl
$STRINGS[$LANG]["Databases"]="Databases";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_manage_database.tpl
$STRINGS[$LANG]["If your site contains one or more forms, the database for each one can be selected here <strong>(1).</strong> The <strong>Add Database</strong> function is not implemented yet <strong>(2)</strong>. Click the <strong>Delete Database</strong> button to remove a database <strong>(3)</strong>."]="If your site contains one or more forms, the database for each one can be selected here <strong>(1).</strong> The <strong>Add Database</strong> function is not implemented yet <strong>(2)</strong>. Click the <strong>Delete Database</strong> button to remove a database <strong>(3)</strong>.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_manage_database.tpl
$STRINGS[$LANG]["You can also click the <strong>Export Data</strong> button to save the data to a spreadsheet."]="You can also click the <strong>Export Data</strong> button to save the data to a spreadsheet.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["Using Widgets: Template widgets"]="Using Widgets: Template widgets";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["Create a template widget"]="Create a template widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["Use a template widget as a container for content that you want to share across many pages, like a header or a footer. When you edit the content of a template widget on one page it will change on every page."]="Use a template widget as a container for content that you want to share across many pages, like a header or a footer. When you edit the content of a template widget on one page it will change on every page.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["1. Create new template widget"]="1. Create new template widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["Click the <strong>widgets tab (1) </strong>and then scroll down to find the template<strong> widgets tab (2)</strong>. Click <strong>Add new template widget (3)</strong>"]="Click the <strong>widgets tab (1) </strong>and then scroll down to find the template<strong> widgets tab (2)</strong>. Click <strong>Add new template widget (3)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["Give the template widget a descriptive name (1) and click the <strong>Create Template widget button (2)</strong>"]="Give the template widget a descriptive name (1) and click the <strong>Create Template widget button (2)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["2. Drag the widget onto the page"]="2. Drag the widget onto the page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["The <strong>template widget handle (1) </strong>can be click to select the widget. Click the <strong>lock icon (2)</strong> to prevent content within the widget from being changed."]="The <strong>template widget handle (1) </strong>can be click to select the widget. Click the <strong>lock icon (2)</strong> to prevent content within the widget from being changed.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["3. Drag and drop image or text content into the Template widget."]="3. Drag and drop image or text content into the Template widget.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["You can use most kinds of content widget inside a template widget but you can not place one template widget inside another."]="You can use most kinds of content widget inside a template widget but you can not place one template widget inside another.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["4. Lock the widget to prevent changes"]="4. Lock the widget to prevent changes";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["When you have finished adding content, click the <strong>template widget handle (1)</strong>"]="When you have finished adding content, click the <strong>template widget handle (1)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["Click the <strong>lock icon (1)</strong> to prevent accidental changes to the contents of the template widget. Once the template widget is locked you can position it like any other widget"]="Click the <strong>lock icon (1)</strong> to prevent accidental changes to the contents of the template widget. Once the template widget is locked you can position it like any other widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["5. Place the template widget on other pages where you want the same content to appear"]="5. Place the template widget on other pages where you want the same content to appear";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_template_widget.tpl
$STRINGS[$LANG]["Navigate to another page (1) and drag the template widget into place (2)."]="Navigate to another page (1) and drag the template widget into place (2).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["FAQs for New Users"]="FAQs for New Users";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["Common questions"]="Common questions";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["How do I modify the background?"]="How do I modify the background?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["If the Background was created using the template PSD, you will need to edit the original PSD file. If the Background was created in the editor, you will be able to edit it directly. See: <a class='bold' href='/support/sitebuilder/site_background'>Site background</a>"]="If the Background was created using the template PSD, you will need to edit the original PSD file. If the Background was created in the editor, you will be able to edit it directly. See: <a class='bold' href='/support/sitebuilder/site_background'>Site background</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["How do I set up a sub-navigation menu?"]="How do I set up a sub-navigation menu?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["A Navigation widget can be confined to any group of pages in a folder. For more information see <a class='bold' href='/support/sitebuilder/create_sub_navigation_menu'>How to set up a sub-navigation menu.</a>"]="A Navigation widget can be confined to any group of pages in a folder. For more information see <a class='bold' href='/support/sitebuilder/create_sub_navigation_menu'>How to set up a sub-navigation menu.</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["How do I change the type style"]="How do I change the type style";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["The Style settings panel allows you to edit global text styles. You can create a new style for your text or modify one of the list.<br />Follow the <a class='bold' href='/support/sitebuilder/creating_editing_type_style'>Creating or editing a type style</a> tutorial to learn more about it."]="The Style settings panel allows you to edit global text styles. You can create a new style for your text or modify one of the list.<br />Follow the <a class='bold' href='/support/sitebuilder/creating_editing_type_style'>Creating or editing a type style</a> tutorial to learn more about it.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["I already started my site but now I want to change the theme, how do I do that?"]="I already started my site but now I want to change the theme, how do I do that?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["Click on the <strong>Themes</strong> button on top of the editor."]="Click on the <strong>Themes</strong> button on top of the editor.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["Click on top of the new theme to see a preview."]="Click on top of the new theme to see a preview.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["Select the theme by clicking on the <strong>Select </strong>button."]="Select the theme by clicking on the <strong>Select </strong>button.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["How do I edit a form?"]="How do I edit a form?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["We have several tutorials that could help you while editing your forms. Take a look at <a class='bold' href='/support/sitebuilder/create_form'>How to create a form</a>."]="We have several tutorials that could help you while editing your forms. Take a look at <a class='bold' href='/support/sitebuilder/create_form'>How to create a form</a>.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["What do I need to do to add my first online store?"]="What do I need to do to add my first online store?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["Ecommerce is supported for most packages except Demo and Basic. To use our Google Checkout widgets, you will only need a <strong>Merchant ID</strong> from Google. You can take a look at how this works and to our <a class='bold' href='/support/sitebuilder/google_checkout_widgets'>Google Checkout Widgets</a> here."]="Ecommerce is supported for most packages except Demo and Basic. To use our Google Checkout widgets, you will only need a <strong>Merchant ID</strong> from Google. You can take a look at how this works and to our <a class='bold' href='/support/sitebuilder/google_checkout_widgets'>Google Checkout Widgets</a> here.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["PayPal widgets work the same as the Google Checkout Widgets, but instead of a Merchant ID you will need to add the email for your PayPal account."]="PayPal widgets work the same as the Google Checkout Widgets, but instead of a Merchant ID you will need to add the email for your PayPal account.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["If you do not wish to use Google Checkout or Paypal, you can also use other platforms such as Ecwid or Wazala. You can add them all to your site using the<strong> Embed Widget</strong>"]="If you do not wish to use Google Checkout or Paypal, you can also use other platforms such as Ecwid or Wazala. You can add them all to your site using the<strong> Embed Widget</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["Can I embed flash?"]="Can I embed flash?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["Yes, you can <a class='bold' href='/support/sitebuilder/add_flash_content'>embed flash</a></strong></u></font> using the Embed Widget."]="Yes, you can <a class='bold' href='/support/sitebuilder/add_flash_content'>embed flash</a></strong></u></font> using the Embed Widget.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["Some of my content is missing or incorrectly displayed - What do I do?"]="Some of my content is missing or incorrectly displayed - What do I do?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["Make sure that"]="Make sure that";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["You are using a recent version of a supported browser like Firefox, Chrome, Safari or Internet Explorer 9"]="You are using a recent version of a supported browser like Firefox, Chrome, Safari or Internet Explorer 9";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["You have cleared your cache"]="You have cleared your cache";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["If this does not work, please email our support team at <a class='bold' href='mailto:[support]'>[support]</a>"]="If this does not work, please email our support team at <a class='bold' href='mailto:[support]'>[support]</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["Can I make a drop-down menu for navigation?"]="Can I make a drop-down menu for navigation?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["No. We are working on this feature but we don't have a release date yet."]="No. We are working on this feature but we don't have a release date yet.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["Is the editor accessible from an mobile device such as iPhone or iPad?"]="Is the editor accessible from an mobile device such as iPhone or iPad?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["No."]="No.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["Can you provide me a FTP access?"]="Can you provide me a FTP access?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["We do not provide FTP access. Files up to 20Mb in size can be uploaded within the editor."]="We do not provide FTP access. Files up to 20Mb in size can be uploaded within the editor.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["If I upload my professional pictures will they be automatically encrypted against copyright theft?"]="If I upload my professional pictures will they be automatically encrypted against copyright theft?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_faqs_for_new_users.tpl
$STRINGS[$LANG]["We do not provide any watermarking on encryption features. If you are concerned about image theft we recommend you upload images at a small size and at 72dpi."]="We do not provide any watermarking on encryption features. If you are concerned about image theft we recommend you upload images at a small size and at 72dpi.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_cloning.tpl
$STRINGS[$LANG]["Pages & Navigation: Page cloning"]="Pages & Navigation: Page cloning";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_cloning.tpl
$STRINGS[$LANG]["How to clone a page"]="How to clone a page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_cloning.tpl
$STRINGS[$LANG]["Use page cloning to create an exact copy of a page and it's contents"]="Use page cloning to create an exact copy of a page and it's contents";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_cloning.tpl
$STRINGS[$LANG]["1. Click the page options button"]="1. Click the page options button";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_cloning.tpl
$STRINGS[$LANG]["2. Clone the page"]="2. Clone the page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_page_cloning.tpl
$STRINGS[$LANG]["3. Name the new page"]="3. Name the new page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["Using Widgets: Video Panel"]="Using Widgets: Video Panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["Copyright material"]="Copyright material";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["Our terms of use explicitly prohibit the publishing of copyright material or anything we consider to be offensive. Your site may be removed without warning if you use the editor to publish any content that infringes our terms of use."]="Our terms of use explicitly prohibit the publishing of copyright material or anything we consider to be offensive. Your site may be removed without warning if you use the editor to publish any content that infringes our terms of use.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["Supported sites"]="Supported sites";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["Supported video sites include YouTube, Vimeo, Facebook, UStream live video feeds, MetaCafe, Veoh, Blip.TV, Google."]="Supported video sites include YouTube, Vimeo, Facebook, UStream live video feeds, MetaCafe, Veoh, Blip.TV, Google.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["Adding a video"]="Adding a video";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["In this example, we will import a youtube video. Other sites are supported (see above)."]="In this example, we will import a youtube video. Other sites are supported (see above).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["Are you uploading or embedding?"]="Are you uploading or embedding?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["If you want to add video to your site from YouTube, Vimeo or similar sites, you are <strong>embedding</strong> the video. You can use the video widget or embed widget for this."]="If you want to add video to your site from YouTube, Vimeo or similar sites, you are <strong>embedding</strong> the video. You can use the video widget or embed widget for this.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["If you want to add a video file to your site from your local hard drive you are <strong>uploading</strong> the video. You need to upload the video file from the Files tab. See: <a class='bold' href='/support/sitebuilder/upload_file'>Uploading a file</a>"]="If you want to add a video file to your site from your local hard drive you are <strong>uploading</strong> the video. You need to upload the video file from the Files tab. See: <a class='bold' href='/support/sitebuilder/upload_file'>Uploading a file</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["Do you need the Video widget or Embed widget?"]="Do you need the Video widget or Embed widget?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["If the embed code for a video starts with <strong>&lt;iframe&gt; </strong>you should use the embed widget. If the embed code starts with <strong>&lt;object&gt; </strong>use the video widget."]="If the embed code for a video starts with <strong>&lt;iframe&gt; </strong>you should use the embed widget. If the embed code starts with <strong>&lt;object&gt; </strong>use the video widget.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["1. Get the embed code from YouTube"]="1. Get the embed code from YouTube";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["Click the <strong>Embed</strong> <strong>button (1) </strong>and select the <strong>Use old embed code button (2). </strong>Select a size (3) and then copy the code (4)."]="Click the <strong>Embed</strong> <strong>button (1) </strong>and select the <strong>Use old embed code button (2). </strong>Select a size (3) and then copy the code (4).";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["2. Open the settings panel and paste embed code"]="2. Open the settings panel and paste embed code";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_video_panel.tpl
$STRINGS[$LANG]["Click the edit icon (1) to open the settings panel (2). Cut and paste video embed code from YouTube, Vimeo or any other video site into the Embed Code field (3)"]="Click the edit icon (1) to open the settings panel (2). Cut and paste video embed code from YouTube, Vimeo or any other video site into the Embed Code field (3)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_selective_page_publication.tpl
$STRINGS[$LANG]["Pages & Navigation: Selective page publication"]="Pages & Navigation: Selective page publication";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_selective_page_publication.tpl
$STRINGS[$LANG]["Select which pages get published when you publish your site"]="Select which pages get published when you publish your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_selective_page_publication.tpl
$STRINGS[$LANG]["Selective publication is set using the page options panel"]="Selective publication is set using the page options panel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_selective_page_publication.tpl
$STRINGS[$LANG]["The <strong>Page Status </strong>menu (1) determines if the page will be published the next time the Publish button is clicked. <strong>Active</strong> pages are always published. By default, all pages are set to Active when they are created. <strong>Inactive</strong> pages are not published and any previous versions are removed from the active site i.e. they are not visible outside of the site editor.  <strong>Draft</strong> pages are not published but any previously published version of the page remains visible."]="The <strong>Page Status </strong>menu (1) determines if the page will be published the next time the Publish button is clicked. <strong>Active</strong> pages are always published. By default, all pages are set to Active when they are created. <strong>Inactive</strong> pages are not published and any previous versions are removed from the active site i.e. they are not visible outside of the site editor.  <strong>Draft</strong> pages are not published but any previously published version of the page remains visible.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_selective_page_publication.tpl
$STRINGS[$LANG]["Note: The home page is always published and cannot be set to draft to or inactive."]="Note: The home page is always published and cannot be set to draft to or inactive.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Getting started: Customising your site"]="Getting started: Customising your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Before you start..."]="Before you start...";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["What's the difference between a theme and template?"]="What's the difference between a theme and template?";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Website builder uses a <strong>theme</strong> to define the appearance of a site. A <strong>template </strong>is used to define the layout of each page in a site. Every page has it's own template which is updated whenever you edit your page."]="Website builder uses a <strong>theme</strong> to define the appearance of a site. A <strong>template </strong>is used to define the layout of each page in a site. Every page has it's own template which is updated whenever you edit your page.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Part 1: Choosing a theme"]="Part 1: Choosing a theme";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Click <strong>Themes</strong> to show the <strong>Theme Browser. </strong>"]="Click <strong>Themes</strong> to show the <strong>Theme Browser. </strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Click any theme in the browser to preview it. To apply the theme to your site, click <strong>Confirm</strong>. To return to your original theme, click <strong>Cancel.</strong>"]="Click any theme in the browser to preview it. To apply the theme to your site, click <strong>Confirm</strong>. To return to your original theme, click <strong>Cancel.</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Change the colour scheme for a theme using the <strong>Theme Colors (1)</strong>. Choose any colour swatch to preview the colours. Click anywhere on the page (2) to confirm the selection."]="Change the colour scheme for a theme using the <strong>Theme Colors (1)</strong>. Choose any colour swatch to preview the colours. Click anywhere on the page (2) to confirm the selection.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["If you want to change the colour of any individual part of the theme you can do that too. See <a class='bold' href='/support/sitebuilder/change_colour_row_column'>Changing the colour of a row or column</a> for more."]="If you want to change the colour of any individual part of the theme you can do that too. See <a class='bold' href='/support/sitebuilder/change_colour_row_column'>Changing the colour of a row or column</a> for more.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Use the <strong>Font set panel</strong> to change the fonts used across your site."]="Use the <strong>Font set panel</strong> to change the fonts used across your site.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["To change the style for any individual piece of text see <a class='bold' href='/support/sitebuilder/creating_editing_type_style'>Creating or editing a type style.</a>"]="To change the style for any individual piece of text see <a class='bold' href='/support/sitebuilder/creating_editing_type_style'>Creating or editing a type style.</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Part 2: Editing a theme"]="Part 2: Editing a theme";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["A theme consists of a background and a collection of row and column styles. You can edit any of these to create a unique theme."]="A theme consists of a background and a collection of row and column styles. You can edit any of these to create a unique theme.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Theme background"]="Theme background";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Click the background button (1) to see the <strong>Theme Background </strong>controls."]="Click the background button (1) to see the <strong>Theme Background </strong>controls.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["You can create background using a solid colour, an image or a gradation. If you use an image you can repeat horizontally, vertically or both. See <a class='bold' href='/support/sitebuilder/site_background'>Site background</a> for more about background editing."]="You can create background using a solid colour, an image or a gradation. If you use an image you can repeat horizontally, vertically or both. See <a class='bold' href='/support/sitebuilder/site_background'>Site background</a> for more about background editing.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Row styles"]="Row styles";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["To change the style applied to a row, click the row settings button (1) and then click on a style (2). Click the <strong>style editing button (3)</strong> to edit the column style. Use the<strong> style menu buttons (4)</strong> to add, duplicate or delete styles."]="To change the style applied to a row, click the row settings button (1) and then click on a style (2). Click the <strong>style editing button (3)</strong> to edit the column style. Use the<strong> style menu buttons (4)</strong> to add, duplicate or delete styles.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["If you edit a style, the change will be applied everywhere that style is used. If you want to edit a style just in one place, create a copy of it and edit that. See <a class='bold' href='/support/sitebuilder/change_colour_row_column'>Changing the colour or a row or column</a> for more."]="If you edit a style, the change will be applied everywhere that style is used. If you want to edit a style just in one place, create a copy of it and edit that. See <a class='bold' href='/support/sitebuilder/change_colour_row_column'>Changing the colour or a row or column</a> for more.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Column styles"]="Column styles";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Columns styles work the same way as row styles. Click the <strong>column settings button (1) </strong>to show the menu. <strong>Click on a style (2)</strong> to apply it . Click the <strong>style editing button (3)</strong> to edit the column style. Use the<strong> style menu buttons (4)</strong> to add, duplicate or delete styles."]="Columns styles work the same way as row styles. Click the <strong>column settings button (1) </strong>to show the menu. <strong>Click on a style (2)</strong> to apply it . Click the <strong>style editing button (3)</strong> to edit the column style. Use the<strong> style menu buttons (4)</strong> to add, duplicate or delete styles.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Column structure"]="Column structure";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["To split a row into columns, click the <strong>row split icon (1)</strong> and select a <strong>column structure (2)</strong>"]="To split a row into columns, click the <strong>row split icon (1)</strong> and select a <strong>column structure (2)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["Next step: Optimise and publish your site"]="Next step: Optimise and publish your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_customizing_your_site.tpl
$STRINGS[$LANG]["If you are now ready to share your site with the world, see <a class='bold' href='/support/sitebuilder/optimizing_publishing_your_site'>Getting Started 3: Optimising and publishing your site</a>"]="If you are now ready to share your site with the world, see <a class='bold' href='/support/sitebuilder/optimizing_publishing_your_site'>Getting Started 3: Optimising and publishing your site</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["Using Widgets: Carousel widget"]="Using Widgets: Carousel widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["Before you start: Carousel widgets FAQs"]="Before you start: Carousel widgets FAQs";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["The Carousel widget lets you create a sequence of images or other elements. The Carousel can advance automatically after a set period or when the user clicks the <strong>next/previous</strong> button"]="The Carousel widget lets you create a sequence of images or other elements. The Carousel can advance automatically after a set period or when the user clicks the <strong>next/previous</strong> button";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["<strong>Q: Can I have more than one carousel on a page?</strong><br /> A: Yes, you can have multiple carousels. The only limitations are to do with observing good practice. You should always consider the usability and loading time of a page and if you include lots of carousels with lots of images the resulting page will be slow to load and may be frustrating to use. This is not a limitation of the Website Builder - that same issue would exist on any website."]="<strong>Q: Can I have more than one carousel on a page?</strong><br /> A: Yes, you can have multiple carousels. The only limitations are to do with observing good practice. You should always consider the usability and loading time of a page and if you include lots of carousels with lots of images the resulting page will be slow to load and may be frustrating to use. This is not a limitation of the Website Builder - that same issue would exist on any website.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["<strong>Q: Is there any limit to the number of pages in a carousel?</strong><br /> A: No, there is no fixed upper limit although we only test the widget to a maximum of 16 pages and we recommend you do not exceed this number. Carousels much over 20 pages may be slow to load."]="<strong>Q: Is there any limit to the number of pages in a carousel?</strong><br /> A: No, there is no fixed upper limit although we only test the widget to a maximum of 16 pages and we recommend you do not exceed this number. Carousels much over 20 pages may be slow to load.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["<strong>Q: Why does the widget keep changing shape?</strong><br /> A: If you use lots of different sizes of images the widget will change to accommodate them. To avoid this, make all your images the same size before importing into your site"]="<strong>Q: Why does the widget keep changing shape?</strong><br /> A: If you use lots of different sizes of images the widget will change to accommodate them. To avoid this, make all your images the same size before importing into your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["<strong>Q: I've added some large images to my carousel and now it's running slowly. Why is this?</strong><br /> A: No matter how you create a website, large images files will take longer to load than small ones. Scale your images to the right size and make appropriate use of JPEG compression to improve performance. Avoid PNG files."]="<strong>Q: I've added some large images to my carousel and now it's running slowly. Why is this?</strong><br /> A: No matter how you create a website, large images files will take longer to load than small ones. Scale your images to the right size and make appropriate use of JPEG compression to improve performance. Avoid PNG files.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["<strong>Q: Can I customise the buttons on the widget?</strong><br /> A: At the moment you can only customise the carousel buttons using the built in CSS editor. This requires some familiarity with CSS."]="<strong>Q: Can I customise the buttons on the widget?</strong><br /> A: At the moment you can only customise the carousel buttons using the built in CSS editor. This requires some familiarity with CSS.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["<strong>Q: The carousel widget doesn't quite do what I want. Is there an alternative?</strong><br /> A: Yes, embed a widget from a third party source like <a href='http://www.widgetbox.com/' target='_blank'>www.widgetbox.com</a> (hint: search on Widgetbox for 'slideshow'). <br /> One example is <a href='http://www.widgetbox.com/widget/slideshow-pro' target='_blank'>http://www.widgetbox.com/widget/slideshow-pro</a><br /> Google also has similar widgets: see <a href='http://bit.ly/gBS51b' target='_blank'>http://bit.ly/gBS51b</a> for an example<br /> <strong>Please note we can not provide support for third party widgets.</strong>"]="<strong>Q: The carousel widget doesn't quite do what I want. Is there an alternative?</strong><br /> A: Yes, embed a widget from a third party source like <a href='http://www.widgetbox.com/' target='_blank'>www.widgetbox.com</a> (hint: search on Widgetbox for 'slideshow'). <br /> One example is <a href='http://www.widgetbox.com/widget/slideshow-pro' target='_blank'>http://www.widgetbox.com/widget/slideshow-pro</a><br /> Google also has similar widgets: see <a href='http://bit.ly/gBS51b' target='_blank'>http://bit.ly/gBS51b</a> for an example<br /> <strong>Please note we can not provide support for third party widgets.</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["How to use the Carousel widget"]="How to use the Carousel widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["1. Drag and drop the widget onto the page"]="1. Drag and drop the widget onto the page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["2. Click the edit button to change the settings"]="2. Click the edit button to change the settings";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["3. Carousel settings"]="3. Carousel settings";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["1: Add pages to the Carousel by clicking the <strong>Add page button (1)</strong>. Remove pages by click the <strong>remove button.</strong><br /> 2: Set the time interval using the <strong>interval slider (2)</strong> . If the Slider is set to 0 seconds the carousel will only advance when the user clicks the next/previous button<br /> 3: Tick the <strong>Next / Prev checkbox (3) </strong>to show the Next and Previous Buttons<br /> 4: Tick the <strong>Thumbs checkbox</strong> <strong>(4)</strong> to show the image thumbnails"]="1: Add pages to the Carousel by clicking the <strong>Add page button (1)</strong>. Remove pages by click the <strong>remove button.</strong><br /> 2: Set the time interval using the <strong>interval slider (2)</strong> . If the Slider is set to 0 seconds the carousel will only advance when the user clicks the next/previous button<br /> 3: Tick the <strong>Next / Prev checkbox (3) </strong>to show the Next and Previous Buttons<br /> 4: Tick the <strong>Thumbs checkbox</strong> <strong>(4)</strong> to show the image thumbnails";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["The Carousel widget supports Text, Images and Columns only. It does not support video. The Carousel widget can not be nested."]="The Carousel widget supports Text, Images and Columns only. It does not support video. The Carousel widget can not be nested.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["4. Add images to the Carousel"]="4. Add images to the Carousel";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_carousel_widget.tpl
$STRINGS[$LANG]["1: Drag an image into the Carousel. <br /> 2: To add another image, click the circular buttons on the Carousel and drag another image"]="1: Drag an image into the Carousel. <br /> 2: To add another image, click the circular buttons on the Carousel and drag another image";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_comments_widget.tpl
$STRINGS[$LANG]["Using Widgets: Manage Comments"]="Using Widgets: Manage Comments";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_comments_widget.tpl
$STRINGS[$LANG]["Using the comments widget"]="Using the comments widget";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_comments_widget.tpl
$STRINGS[$LANG]["The comment form only works in Preview mode or on Published sites. It cannot be used in Design mode."]="The comment form only works in Preview mode or on Published sites. It cannot be used in Design mode.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_comments_widget.tpl
$STRINGS[$LANG]["Drag the comment widget on to your page"]="Drag the comment widget on to your page";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_comments_widget.tpl
$STRINGS[$LANG]["View submitted comments"]="View submitted comments";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_comments_widget.tpl
$STRINGS[$LANG]["Click <strong>Manage (1)</strong> and then <strong>Comments (2)</strong>"]="Click <strong>Manage (1)</strong> and then <strong>Comments (2)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_comments_widget.tpl
$STRINGS[$LANG]["Entries from the widget appear in the Comment listing."]="Entries from the widget appear in the Comment listing.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_comments_widget.tpl
$STRINGS[$LANG]["Here you can choose if you want to Edit, Approve, Decline or Delete the comments your users make in your site."]="Here you can choose if you want to Edit, Approve, Decline or Delete the comments your users make in your site.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_comments_widget.tpl
$STRINGS[$LANG]["Only approved comments will show in your page."]="Only approved comments will show in your page.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Pages & Navigation: Create a page folder"]="Pages & Navigation: Create a page folder";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Creating a page folder"]="Creating a page folder";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Folders are very useful when you need a sub-navigation menu. They will allow you to categorize your pages and add a sub-navigation menu to each folder."]="Folders are very useful when you need a sub-navigation menu. They will allow you to categorize your pages and add a sub-navigation menu to each folder.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Click the pages Tab (1)"]="Click the pages Tab (1)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Click the create folder button (2)"]="Click the create folder button (2)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Give the folder a name"]="Give the folder a name";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Enter a name in the name field <strong>(1)</strong>. It's best to use a short single word where possible. If the <strong>Create Folder Home Page</strong> checkbox is ticked, a page called 'index' will be created inside the folder"]="Enter a name in the name field <strong>(1)</strong>. It's best to use a short single word where possible. If the <strong>Create Folder Home Page</strong> checkbox is ticked, a page called 'index' will be created inside the folder";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["The new folder will be created:"]="The new folder will be created:";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Add new pages to your folder"]="Add new pages to your folder";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Click the <strong>Create Page</strong> button in the pages tab (1)"]="Click the <strong>Create Page</strong> button in the pages tab (1)";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Select the folder (1) you wish the page to be created in."]="Select the folder (1) you wish the page to be created in.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Click the <strong>Create Page</strong> button (2) to create the page."]="Click the <strong>Create Page</strong> button (2) to create the page.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Note: Drag and drop is not supported in the page navigation menu"]="Note: Drag and drop is not supported in the page navigation menu";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Add existing pages to a folder"]="Add existing pages to a folder";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Open the page options"]="Open the page options";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Click the page options button and select <strong>Name and URL (1)</strong>"]="Click the page options button and select <strong>Name and URL (1)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Choose the folder"]="Choose the folder";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_create_page_folder.tpl
$STRINGS[$LANG]["Select the destination folder <strong>(1)</strong> and click <strong>Save (2)</strong>"]="Select the destination folder <strong>(1)</strong> and click <strong>Save (2)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["Design, Themes & Styles: Site background"]="Design, Themes & Styles: Site background";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["1 .How to set background colour"]="1 .How to set background colour";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["Set the background colour"]="Set the background colour";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["Changes to the background apply to the entire theme. You can not set the background of an individual page."]="Changes to the background apply to the entire theme. You can not set the background of an individual page.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["To set the background colour, click the background icon (1) and set the type menu to<strong> colour (2). </strong>Click on the <strong>colour chip (3) </strong>select a colour"]="To set the background colour, click the background icon (1) and set the type menu to<strong> colour (2). </strong>Click on the <strong>colour chip (3) </strong>select a colour";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["If you select a colour from the <strong>colour picker (1)</strong> the colour will stay fixed. If you select a colour from the <strong>theme swatches (2) </strong>the colour will change whenever you change the<strong> colour swatch (3)</strong>"]="If you select a colour from the <strong>colour picker (1)</strong> the colour will stay fixed. If you select a colour from the <strong>theme swatches (2) </strong>the colour will change whenever you change the<strong> colour swatch (3)</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["2. Colour swatches"]="2. Colour swatches";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["Using colour swatches to set background colour"]="Using colour swatches to set background colour";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["A colour swatch is a sampling of colors. You can use the colour swatches panel to set the colours for a theme. To learn more about <a class='bold' href='/support/sitebuilder/set_color_swatch'>Colour Swatches</a> click on the link."]="A colour swatch is a sampling of colors. You can use the colour swatches panel to set the colours for a theme. To learn more about <a class='bold' href='/support/sitebuilder/set_color_swatch'>Colour Swatches</a> click on the link.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["3. Set a background pattern"]="3. Set a background pattern";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["Add or create background patterns"]="Add or create background patterns";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["To set the background texture, click the background icon (1) and set the type menu to<strong> image (2). </strong>"]="To set the background texture, click the background icon (1) and set the type menu to<strong> image (2). </strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["You can create a repeat pattern easily using a tool like <a href='http://bgpatterns.com/' class='bold' target='_blank'>http://bgpatterns.com/</a>"]="You can create a repeat pattern easily using a tool like <a href='http://bgpatterns.com/' class='bold' target='_blank'>http://bgpatterns.com/</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["Drag a pattern from the <strong>images tab (1) </strong>to the <strong>image target (2). </strong>If you want the image to repeat, tick the boxes for <strong>horizontal or vertical repeat (3).</strong>"]="Drag a pattern from the <strong>images tab (1) </strong>to the <strong>image target (2). </strong>If you want the image to repeat, tick the boxes for <strong>horizontal or vertical repeat (3).</strong>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["<strong>HINT: </strong>It's best to avoid using large images in the background - they can make your site very slow to load."]="<strong>HINT: </strong>It's best to avoid using large images in the background - they can make your site very slow to load.";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["You can download different kind of patterns from here: <a class='bold' href='http://patterns.ava7.com/' target='_blank'>http://patterns.ava7.com/</a>"]="You can download different kind of patterns from here: <a class='bold' href='http://patterns.ava7.com/' target='_blank'>http://patterns.ava7.com/</a>";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["4. Background gradient"]="4. Background gradient";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["Add background gradient to your site"]="Add background gradient to your site";

// File: /registrar-platform/www/application/views/default/sitebuilder/support_site_background.tpl
$STRINGS[$LANG]["To set a gradient, set the type menu to <strong>Linear Gradient (1)</strong> and then select two different colour from the <strong>colour chips (2)</strong>"]="To set a gradient, set the type menu to <strong>Linear Gradient (1)</strong> and then select two different colour from the <strong>colour chips (2)</strong>";

// File: /registrar-platform/www/application/views/default/account_html_transfer_code.tpl
$STRINGS[$LANG]["Transfer Code"]="Transfer Code";

// File: /registrar-platform/www/application/views/default/account_html_transfer_code.tpl
$STRINGS[$LANG]["OK"]="OK";

// File: /registrar-platform/www/application/views/default/account_html_transfer_code.tpl
$STRINGS[$LANG]["The auth code will be emailed to the admin contact for the domain: [email]"]="The auth code will be emailed to the admin contact for the domain: [email]";

// File: /registrar-platform/www/application/views/default/account_html_transfer_code.tpl
$STRINGS[$LANG]["Send Transfer Code"]="Send Transfer Code";

// File: /registrar-platform/www/application/views/default/account_html_transfer_code.tpl
$STRINGS[$LANG]["You cannot generate a transfer code as this domain is locked"]="You cannot generate a transfer code as this domain is locked";

// File: /registrar-platform/www/application/views/default/account_html_transfer_code.tpl
// File: /registrar-platform/www/application/views/default/cart_country_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/account_html_notifications.tpl
$STRINGS[$LANG]["Close"]="Cau";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
$STRINGS[$LANG]["Changes will affect the following contacts: [types]"]="Changes will affect the following contacts: [types]";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
$STRINGS[$LANG]["You can only select a contact that is verified."]="You can only select a contact that is verified.";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
$STRINGS[$LANG]["The admin contact for a .Asia domain must have a postal address in the DotAsia Community."]="The admin contact for a .Asia domain must have a postal address in the DotAsia Community.";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
$STRINGS[$LANG]["New Contact:"]="New Contact:";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
$STRINGS[$LANG]["Save"]="Cadw";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
$STRINGS[$LANG]["OR"]="NEU";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
$STRINGS[$LANG]["Create New Contact"]="Create New Contact";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
$STRINGS[$LANG]["After creating a new contact below, a verification email will be sent to the supplied email address. If you do not verify your contact details within [verify_days] days any domain which uses this contact as a Registrant will be suspended."]="After creating a new contact below, a verification email will be sent to the supplied email address. If you do not verify your contact details within [verify_days] days any domain which uses this contact as a Registrant will be suspended.";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
// File: /registrar-platform/www/application/views/default/inc_country_contact.tpl
$STRINGS[$LANG]["Name*:"]="Enw*:";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
$STRINGS[$LANG]["Organisation/Company:"]="Sefydliad/Cwmni:";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/inc_create.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
// File: /registrar-platform/www/application/views/default/inc_country_contact.tpl
$STRINGS[$LANG]["Street Address*:"]="Cyfeiriad Stryd*:";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
$STRINGS[$LANG]["Town/City*:"]="Tref/Dinas*:";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/inc_create.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
$STRINGS[$LANG]["County:"]="Sir:";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/inc_create.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
// File: /registrar-platform/www/application/views/default/inc_country_contact.tpl
$STRINGS[$LANG]["City*:"]="Dinas*:";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
$STRINGS[$LANG]["State, Region or Province:"]="State, Region or Province:";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/inc_create.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
// File: /registrar-platform/www/application/views/default/inc_country_contact.tpl
$STRINGS[$LANG]["Postal/Zip code*:"]="Cod Post/Zip code*:";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/inc_create.tpl
// File: /registrar-platform/www/application/views/default/inc_country_contact.tpl
$STRINGS[$LANG]["Country/Territory*:"]="Gwlad/Tiriogaeth*:";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/inc_create.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
// File: /registrar-platform/www/application/views/default/inc_country_contact.tpl
$STRINGS[$LANG]["Phone*:"]="Ffôn*:";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
$STRINGS[$LANG]["Phone Number Format"]="Phone Number Format";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
$STRINGS[$LANG]["Phone number should be in the format: +DDD.XXXXXXX. 'DDD' is the dial code, which should not exceed 3 digits. 'XXXXXXX' should not exceed 12 digits."]="Phone number should be in the format: +DDD.XXXXXXX. 'DDD' is the dial code, which should not exceed 3 digits. 'XXXXXXX' should not exceed 12 digits.";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/inc_create.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
// File: /registrar-platform/www/application/views/default/inc_country_contact.tpl
$STRINGS[$LANG]["Fax:"]="Ffacs:";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
$STRINGS[$LANG]["Fax Number Format"]="Fax Number Format";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
$STRINGS[$LANG]["Fax number should be in the format: +DDD.XXXXXXX. 'DDD' is the dial code, which should not exceed 3 digits. 'XXXXXXX' should not exceed 12 digits."]="Fax number should be in the format: +DDD.XXXXXXX. 'DDD' is the dial code, which should not exceed 3 digits. 'XXXXXXX' should not exceed 12 digits.";

// File: /registrar-platform/www/application/views/default/account_html_change_contacts.tpl
// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/e_contact.tpl
// File: /registrar-platform/www/application/views/default/inc_create.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
// File: /registrar-platform/www/application/views/default/e_forgot_password.tpl
// File: /registrar-platform/www/application/views/default/inc_country_contact.tpl
$STRINGS[$LANG]["Email address*:"]="Cyfeiriad Ebost*:";

// File: /registrar-platform/www/application/views/default/cart_country_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_html_validation.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Continue"]="Parhau";

// File: /registrar-platform/www/application/views/default/e_terms_registration.tpl
// File: /registrar-platform/www/application/views/default/e_policies.tpl
$STRINGS[$LANG]["Registration Agreement"]="Registration Agreement";

// File: /registrar-platform/www/application/views/default/account_transfer_request.tpl
$STRINGS[$LANG]["Transfer of [domain]"]="Transfer of [domain]";

// File: /registrar-platform/www/application/views/default/account_transfer_request.tpl
$STRINGS[$LANG]["Transfer confirmation"]="Transfer confirmation";

// File: /registrar-platform/www/application/views/default/account_transfer_request.tpl
$STRINGS[$LANG]["I confirm that I have read the Domain Name Transfer Request Message."]="I confirm that I have read the Domain Name Transfer Request Message.";

// File: /registrar-platform/www/application/views/default/account_transfer_request.tpl
$STRINGS[$LANG]["I confirm that I wish to proceed with the transfer of <span class='bold'>[domain]</span> from <span class='bold'>[losing]</span> to <span class='bold'>[gaining]</span>."]="I confirm that I wish to proceed with the transfer of <span class='bold'>[domain]</span> from <span class='bold'>[losing]</span> to <span class='bold'>[gaining]</span>.";

// File: /registrar-platform/www/application/views/default/account_transfer_request.tpl
$STRINGS[$LANG]["Your Name:"]="Your Name:";

// File: /registrar-platform/www/application/views/default/account_transfer_request.tpl
$STRINGS[$LANG]["Approve Request"]="Approve Request";

// File: /registrar-platform/www/application/views/default/account_transfer_request.tpl
$STRINGS[$LANG]["Reject Request"]="Reject Request";

// File: /registrar-platform/www/application/views/default/e_terms_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/e_policies.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Advanced DNS Terms & Conditions"]="Advanced DNS Terms & Conditions";

// File: /registrar-platform/www/application/views/default/e_login.tpl
$STRINGS[$LANG]["Account Login"]="Mewngofnodi i Gyfrif";

// File: /registrar-platform/www/application/views/default/inc_google.tpl
// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
$STRINGS[$LANG]["Power your business workflow with Googles robust infrastructure!"]="Power your business workflow with Googles robust infrastructure!";

// File: /registrar-platform/www/application/views/default/inc_google.tpl
// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
$STRINGS[$LANG]["Work seamlessly regardless of location!"]="Work seamlessly regardless of location!";

// File: /registrar-platform/www/application/views/default/inc_google.tpl
// File: /registrar-platform/www/application/views/default/inc_google.tpl
// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
$STRINGS[$LANG]["user"]="user";

// File: /registrar-platform/www/application/views/default/inc_google.tpl
// File: /registrar-platform/www/application/views/default/inc_google.tpl
// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
$STRINGS[$LANG]["year"]="blwyddyn";

// File: /registrar-platform/www/application/views/default/inc_google.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
$STRINGS[$LANG]["More Info"]="More Info";

// File: /registrar-platform/www/application/views/default/inc_google.tpl
// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]["Add to Cart"]="Add to Cart";

// File: /registrar-platform/www/application/views/default/inc_google.tpl
// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
$STRINGS[$LANG]["Select number of Users:"]="Select number of Users:";

// File: /registrar-platform/www/application/views/default/inc_google.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
$STRINGS[$LANG]["Back to top"]="Back to top";

// File: /registrar-platform/www/application/views/default/inc_google.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
$STRINGS[$LANG]["All packages are available via the 'Add-on Services' page if you decided to purchase at a later date."]="All packages are available via the 'Add-on Services' page if you decided to purchase at a later date.";

// File: /registrar-platform/www/application/views/default/inc_google.tpl
// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
$STRINGS[$LANG]["No thanks"]="No thanks";

// File: /registrar-platform/www/application/views/default/account_html_view_domain_contacts.tpl
$STRINGS[$LANG]["Unable to retrieve contact information at this time, please try again later"]="Unable to retrieve contact information at this time, please try again later";

// File: /registrar-platform/www/application/views/default/account_html_view_domain_contacts.tpl
$STRINGS[$LANG]["Change Contacts"]="Change Contacts";

// File: /registrar-platform/www/application/views/default/support_index.tpl
$STRINGS[$LANG]["If you are unable to find an answer to your query, and require further assistance, please email our support team at <a class='bold' href='mailto:[support]'>[support]</a>. We will endeavour to respond within 24 hours but, if you don't see a reply from us, please check your spam filter."]="Os nad ydych yn medru darganfod ateb i'ch ymholiad, ac angen cymorth pellach, ebostiwch ein tîm cefnogi ar <a class='bold' href='mailto:[support]'>[support]</a>. Fe geisiwn ein gorau i ymateb o fewn 24 awr ond os nad ydych yn gweld ymateb gennym, gwiriwch eich hidlydd spam.";

// File: /registrar-platform/www/application/views/default/support_index.tpl
$STRINGS[$LANG]["Support FAQs"]="Cymorth Cwestiynau Cyffredinol";

// File: /registrar-platform/www/application/views/default/support_index.tpl
$STRINGS[$LANG]["About [store]"]="About [store]";

// File: /registrar-platform/www/application/views/default/support_index.tpl
$STRINGS[$LANG]["How to Setup Email"]="Sut i Osod Ebost";

// File: /registrar-platform/www/application/views/default/account_html_outgoing_transfers.tpl
$STRINGS[$LANG]["Outgoing Transfers"]="Outgoing Transfers";

// File: /registrar-platform/www/application/views/default/account_html_outgoing_transfers.tpl
$STRINGS[$LANG]["A request has been received to transfer the following domain(s) away from your account.<br />If you consent to the transfer please click 'Approve' below. If you ignore this request, the domain will automatically be removed from your account and transferred to the account/registrar shown on the action date."]="A request has been received to transfer the following domain(s) away from your account.<br />If you consent to the transfer please click 'Approve' below. If you ignore this request, the domain will automatically be removed from your account and transferred to the account/registrar shown on the action date.";

// File: /registrar-platform/www/application/views/default/account_html_outgoing_transfers.tpl
$STRINGS[$LANG]["Requested by"]="Requested by";

// File: /registrar-platform/www/application/views/default/account_html_outgoing_transfers.tpl
// File: /registrar-platform/www/application/views/default/account_html_pending_transfers.tpl
$STRINGS[$LANG]["Action Date"]="Action Date";

// File: /registrar-platform/www/application/views/default/account_html_outgoing_transfers.tpl
$STRINGS[$LANG]["Approve"]="Approve";

// File: /registrar-platform/www/application/views/default/account_html_outgoing_transfers.tpl
$STRINGS[$LANG]["Reject"]="Reject";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["Your Account Details"]="Your Account Details";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["<span class='bold'>Please note:</span> For any new domain names registered in this account, we will take the account details below to populate the contact details for the new domain name(s)."]="<span class='bold'>Please note:</span> For any new domain names registered in this account, we will take the account details below to populate the contact details for the new domain name(s).";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["To change the contact details on your existing domain names, please use the <a href='/account/contacts'>Manage Contacts</a> page."]="To change the contact details on your existing domain names, please use the <a href='/account/contacts'>Manage Contacts</a> page.";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["After editing your account details you will need to re-verify your account. An email has been sent to you with instructions on how to verify your account, we will have to suspend your account if you have not verified your account after [verify_days] days."]="After editing your account details you will need to re-verify your account. An email has been sent to you with instructions on how to verify your account, we will have to suspend your account if you have not verified your account after [verify_days] days.";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["Vat Number:"]="Vat Number:";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["A VAT Number is required, to qualify for VAT deduction off your cart total amount. This is only applicable to Businesses."]="A VAT Number is required, to qualify for VAT deduction off your cart total amount. This is only applicable to Businesses.";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/inc_create.tpl
// File: /registrar-platform/www/application/views/default/inc_country_contact.tpl
$STRINGS[$LANG]["Phone number should contain digits only and not exceed 12 characters in length."]="Phone number should contain digits only and not exceed 12 characters in length.";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/inc_create.tpl
// File: /registrar-platform/www/application/views/default/inc_country_contact.tpl
$STRINGS[$LANG]["Fax number should contain digits only and not exceed 12 characters in length."]="Fax number should contain digits only and not exceed 12 characters in length.";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["Confirm Email address*:"]="Cadarnhau Ebost*:";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
// File: /registrar-platform/www/application/views/default/inc_create.tpl
// File: /registrar-platform/www/application/views/default/inc_login.tpl
$STRINGS[$LANG]["Password*:"]="Cyfrinair*:";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["Enter your password to confirm the changes"]="Enter your password to confirm the changes";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["Update Details"]="Update Details";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["Resend Verification Email"]="Resend Verification Email";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["Change Password"]="Change Password";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["Current Password*:"]="Current Password*:";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["New Password*:"]="New Password*:";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["Password must be at least 8 characters long, contain at least one one upper case character, one lower case character and a number or non-alphanumeric character ."]="Password must be at least 8 characters long, contain at least one one upper case character, one lower case character and a number or non-alphanumeric character .";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["Confirm New Password*:"]="Confirm New Password*:";

// File: /registrar-platform/www/application/views/default/account_settings.tpl
$STRINGS[$LANG]["Save Password"]="Save Password";

// File: /registrar-platform/www/application/views/default/e_checkout_login.tpl
$STRINGS[$LANG]["Register Your Domain"]="Register Your Domain";

// File: /registrar-platform/www/application/views/default/e_checkout_login.tpl
$STRINGS[$LANG]["Are you new to [store]?"]="Are you new to [store]?";

// File: /registrar-platform/www/application/views/default/e_checkout_login.tpl
$STRINGS[$LANG]["Join & continue to checkout"]="Join & continue to checkout";

// File: /registrar-platform/www/application/views/default/e_checkout_login.tpl
$STRINGS[$LANG]["Log into existing [store] account"]="Log into existing [store] account";

// File: /registrar-platform/www/application/views/default/account_html_view_bulk_dns.tpl
// File: /registrar-platform/www/application/views/default/cart_html_validation.tpl
// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
$STRINGS[$LANG]["Domain Name"]="Domain Name";

// File: /registrar-platform/www/application/views/default/account_html_view_bulk_dns.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Nameservers"]="Gweinyddwyr Enw";

// File: /registrar-platform/www/application/views/default/account_html_view_bulk_dns.tpl
$STRINGS[$LANG]["Using Advanced DNS Manager"]="Using Advanced DNS Manager";

// File: /registrar-platform/www/application/views/default/account_html_view_bulk_dns.tpl
// File: /registrar-platform/www/application/views/default/account_html_dns_info.tpl
// File: /registrar-platform/www/application/views/default/account_html_domain_dns.tpl
$STRINGS[$LANG]["No Nameservers listed."]="No Nameservers listed.";

// File: /registrar-platform/www/application/views/default/cart_validation.tpl
$STRINGS[$LANG]["Domain Name Validation"]="Domain Name Validation";

// File: /registrar-platform/www/application/views/default/cart_validation.tpl
$STRINGS[$LANG]["The domain name(s) below requires validation, please enter the additional information for each domain using the 'Upload' buttons.<br />You can only continue to the checkout once all required data has been submitted."]="The domain name(s) below requires validation, please enter the additional information for each domain using the 'Upload' buttons.<br />You can only continue to the checkout once all required data has been submitted.";

// File: /registrar-platform/www/application/views/default/cart_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_validation.tpl
// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]["tr"]="tr";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["invalid"]="invalid";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["is available"]="ar gael";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["is unavailable"]="ddim ar gael";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Premium Domain Names"]="Premium Domain Names";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Premium domains are purchased with their existing expiry date, and will need renewing upon expiry at a cost of [curr][price]/year."]="Premium domains are purchased with their existing expiry date, and will need renewing upon expiry at a cost of [curr][price]/year.";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Premium"]="Premium";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Aftermarket"]="Aftermarket";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Purchase through our aftermarket provider, renew upon expiry at a cost of [curr][price]/year."]="Purchase through our aftermarket provider, renew upon expiry at a cost of [curr][price]/year.";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["IDN Domain Names"]="IDN Domain Names";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["The IDN system allows Internet users to use the full alphabet of their language in their domain names. They are no longer restricted to the English A-Z, and can use the full latin character set, as well as characters from other locales, including Chinese and Japanese."]="The IDN system allows Internet users to use the full alphabet of their language in their domain names. They are no longer restricted to the English A-Z, and can use the full latin character set, as well as characters from other locales, including Chinese and Japanese.";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["IDN"]="IDN";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Suggestion"]="Suggestion";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Purchase: <b>[curr][price]</b><br />Expires: [expires]"]="Purchase: <b>[curr][price]</b><br />Expires: [expires]";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["[year] year for [curr][price]"]="[year] blwyddyn am [curr][price]";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["[year] year"]="[year] blwyddyn";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Added"]="Ychwanegwyd";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/account_html_dns_info.tpl
$STRINGS[$LANG]["Add"]="Ychwanegu";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Already registered"]="Already registered";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Choose your registration phase:"]="Choose your registration phase:";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Non-refundable application fee:"]="Non-refundable application fee:";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Learn more"]="Learn more";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Renewal price for [domain] is [currency][price]"]="Pris adnewyddu [domain] yw [currency][price]";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Add all domains to cart"]="Ychwanegwch pob parth i’r cert";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Can't find what you're looking for? Search again..."]="Methu darganfod yr hyn yr ydych yn chwilio amdano? Chwiliwch eto…";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Non-refundable application fee of [currency][fee] is included in the price"]="Non-refundable application fee of [currency][fee] is included in the price";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["You can register for this phase between the following dates:"]="You can register for this phase between the following dates:";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Opens: [open] UTC"]="Opens: [open] UTC";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["Closes: [close] UTC"]="Closes: [close] UTC";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["error"]="error";

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]["disabled"]="disabled";

// File: /registrar-platform/www/application/views/default/e_policies.tpl
$STRINGS[$LANG]["Policies & Agreements"]="Policies & Agreements";

// File: /registrar-platform/www/application/views/default/e_policies.tpl
$STRINGS[$LANG]["Below are policies, terms & conditions and information that apply to products available on this website. If you have any questions or queries, please <a href='/contact-us'>contact us</a>"]="Below are policies, terms & conditions and information that apply to products available on this website. If you have any questions or queries, please <a href='/contact-us'>contact us</a>";

// File: /registrar-platform/www/application/views/default/e_policies.tpl
$STRINGS[$LANG]["Deletion & Auto-Renew Policy"]="Deletion & Auto-Renew Policy";

// File: /registrar-platform/www/application/views/default/e_policies.tpl
// File: /registrar-platform/www/application/views/default/e_dispute.tpl
$STRINGS[$LANG]["Dispute Policy"]="Dispute Policy";

// File: /registrar-platform/www/application/views/default/e_policies.tpl
$STRINGS[$LANG]["Registrant Benefits & Responsibilities"]="Registrant Benefits & Responsibilities";

// File: /registrar-platform/www/application/views/default/e_policies.tpl
$STRINGS[$LANG]["Privacy Policy"]="Privacy Policy";

// File: /registrar-platform/www/application/views/default/e_policies.tpl
// File: /registrar-platform/www/application/views/default/e_terms.tpl
$STRINGS[$LANG]["Legal Information and Terms of Use"]="Legal Information and Terms of Use";

// File: /registrar-platform/www/application/views/default/e_policies.tpl
$STRINGS[$LANG]["Private Whois Agreement"]="Private Whois Agreement";

// File: /registrar-platform/www/application/views/default/e_policies.tpl
// File: /registrar-platform/www/application/views/default/e_terms_sitebuilder.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Website Builder Terms & Conditions"]="Website Builder Terms & Conditions";

// File: /registrar-platform/www/application/views/default/e_policies.tpl
$STRINGS[$LANG]["Web Hosting Terms & Conditions"]="Web Hosting Terms & Conditions";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["I have ordered Google Apps, when can I start using it?"]="Rwyf wedi archebu Google Apps, pryd gallaf ei ddefnyddio?";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["I received my confirmation email, how do I start using Google Apps?"]="Rwyf wedi cael ebost cadarnhau, sut ydw i’n dechrau defnyddio Google Apps??";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["Further support on how to make the most out of your Google Apps account"]="Mae cymorth pellach ynghylch sut i wneud y gorau o’ch cyfrif Google Apps";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["Q: I have ordered Google Apps, when can I start using it?"]="C: Rwyf wedi archebu Google Apps, pryd gallaf ei ddefnyddio?";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["A: After you have completed your order for Google Apps there are several actions we need to take to setup your account.  The most important of these is passing the Google Domain Ownership Verification process, which we do on your behalf.  This process may take up to an hour to complete after which you will receive an email which confirms that the Google Apps is ready for use."]="A: Wedi ichi orffen eich archeb ar gyfer Google Apps mae sawl gweithred sydd angen inni ymgymryd â hwy er mwyn gosod eich cyfrif. Y weithred bwysicaf o’r rhain yw pasio’r broses Dull Gwirio Perchnogaeth Parth Google, y byddwn yn gwneud ar eich rhan. Gall y broses hwn gymryd hyd at awr i’w gwblhau ac yna byddwch yn derbyn ebost sy’n cadarnhau fod y Google Apps yn barod i’w ddefnyddio.";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["Keep an eye out for the confirmation email, if you do not receive this after one hour our support team will pick up the error and contact you once it has been resolved. Please contact <a href='mailto:[support]'>[support]</a> if you encounter any issues."]="Cadwch olwg am yr ebost cadarnhau, os nad ydych yn derbyn hwn ar ôl un awr bydd ein tîm cymorth yn casglu’r nam ac yn cysylltu â chi unwaith y bydd wedi ei ddatrys.  Cysylltwch â ni ar <a href='mailto:[support]'>[support]</a> os ydych yn cael unrhyw broblemau.";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["Q: I received my confirmation email, how do I start using Google Apps?"]="C: Rwyf wedi cael ebost cadarnhau, sut ydw i’n dechrau defnyddio Google Apps?";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["A: The first step in setting up your Google Apps account is to setup your Admin user. This user is the account which will have the authority to create and maintain all other users on your account, as well as add further subscriptions.  Login to your account with [site], find the domain and click on the 'Setup Google Apps' button next to your domain. You will be asked to provide the details of your Admin user, which will then be sent to Google to complete your setup. The setup of all subsequent users, as well as all other actions should then be completed directly at the Google Apps website."]="A:  Y cam cyntaf wrth sefydlu eich cyfrif Google Apps yw i sefydlu eich defnyddiwr Gweinyddu. Y defnyddiwr yw’r cyfrif sydd â’r awdurdod i greu a chynnal pob defnyddiwr arall ar eich cyfrif, yn ogystal âg ychwanegu tanysgrifiadau eraill. Mewngofnodwch i’ch cyfrif gyda cymru.domains, a darganfyddwch y parth a chliciwch ar y botwm ‘Setup Google Apps’ drws nesaf i’r parth. Bydd gofyn i chi ddarparu manylion eith defnyddiwr Gweinyddu, a fydd wedyn yn cael ei ddanfon at Google er mwyn cwblhau eich gosodiad. Wedyn, dylai gosodiadau pob defnyddiwr dilynol, yn ogystal â gweithrediadau eraill gael eu cwblhau’n uniongyrchol ar y wefan Google Apps.";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["Q: Further support on how to make the most out of your Google Apps account"]="C: Mae cymorth pellach ynghylch sut i wneud y gorau o’ch cyfrif Google Apps ";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["Administrator Help Center"]="Administrator Help Center";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["Browse documentation on configuring Google Apps for your organization"]="Porwch ddogfennau ar gyflunio Google Apps ar gyfer eich sefydliad ";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["User guides"]="User guides";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["Get help using core Google Apps"]="Cewch gymorth wrth ddefnyddio Google Apps craidd";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["Support Tools"]="Support Tools";

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]["Analyze logs, headers, and more"]="Dadansoddi logiau, pennyn, a mwy";

// File: /registrar-platform/www/application/views/default/account_html_dns_info.tpl
$STRINGS[$LANG]["DNS Loading Error"]="DNS Loading Error";

// File: /registrar-platform/www/application/views/default/account_html_dns_info.tpl
// File: /registrar-platform/www/application/views/default/account_html_dns_info.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Advanced DNS Manager"]="Advanced DNS Manager";

// File: /registrar-platform/www/application/views/default/e_setup_hosting.tpl
$STRINGS[$LANG]["Setting up your Hosting"]="Setting up your Hosting";

// File: /registrar-platform/www/application/views/default/e_setup_hosting.tpl
$STRINGS[$LANG]["Click here to view the terms and conditions of purchasing a hosting account with us."]="Click here to view the terms and conditions of purchasing a hosting account with us.";

// File: /registrar-platform/www/application/views/default/e_setup_hosting.tpl
$STRINGS[$LANG]["Once you purchase a hosting account with us, if your domain is not already on our Managed DNS service we will set it up automatically to point to the correct hosting servers. If you are already on Managed DNS we will update the records to point to the hosting servers."]="Once you purchase a hosting account with us, if your domain is not already on our Managed DNS service we will set it up automatically to point to the correct hosting servers. If you are already on Managed DNS we will update the records to point to the hosting servers.";

// File: /registrar-platform/www/application/views/default/e_setup_hosting.tpl
$STRINGS[$LANG]["The 'Hosting' link in your account takes you to the 'Manage Your Hosting' page. This page lists the domains you have purchased hosting for and allows you to setup mailboxes, view the bandwidth and storage allowances and manage your website files."]="The 'Hosting' link in your account takes you to the 'Manage Your Hosting' page. This page lists the domains you have purchased hosting for and allows you to setup mailboxes, view the bandwidth and storage allowances and manage your website files.";

// File: /registrar-platform/www/application/views/default/e_setup_hosting.tpl
$STRINGS[$LANG]["To publish content to your website, you can use our FTP client or any other method you prefer. The FTP address to upload to will be shown on the 'Manage Your Hosting' page. The username and password for accessing FTP will be shown on the 'Manage Website Files' page. You can select 'Manage Website Files' from the drop down option on the 'Manage Your Hosting' page."]="To publish content to your website, you can use our FTP client or any other method you prefer. The FTP address to upload to will be shown on the 'Manage Your Hosting' page. The username and password for accessing FTP will be shown on the 'Manage Website Files' page. You can select 'Manage Website Files' from the drop down option on the 'Manage Your Hosting' page.";

// File: /registrar-platform/www/application/views/default/e_setup_hosting.tpl
$STRINGS[$LANG]["To setup an email account select the desired domain and click 'Manage Mailboxes'. You will see a list of mailboxes that have been setup for that domain. Here you can add a new mailbox or delete or change a password for an existing mailbox."]="To setup an email account select the desired domain and click 'Manage Mailboxes'. You will see a list of mailboxes that have been setup for that domain. Here you can add a new mailbox or delete or change a password for an existing mailbox.";

// File: /registrar-platform/www/application/views/default/e_setup_hosting.tpl
$STRINGS[$LANG]["Once a mailbox has been setup, you can access it through the following URL: http://webmail.your-domain.la. Login with the particular email address you want to use (e.g. 'sales@your-domain.la') and type in the password you created when setting up the address. This opens the webmail program for 'sales' incoming mail. If you have other email boxes with the same domain name, you need to login separately for each individual box."]="Once a mailbox has been setup, you can access it through the following URL: http://webmail.your-domain.la. Login with the particular email address you want to use (e.g. 'sales@your-domain.la') and type in the password you created when setting up the address. This opens the webmail program for 'sales' incoming mail. If you have other email boxes with the same domain name, you need to login separately for each individual box.";

// File: /registrar-platform/www/application/views/default/e_setup_hosting.tpl
$STRINGS[$LANG]["If you would like to setup your mailbox using another client, you can get the required settings by going to the 'Managed DNS' link in your account. Clicking 'View/Edit Configuration' for the required domain, and select 'Advance Confirugation' at the bottom of the page. You will see all the DNS records for your domain along with the webmail, POP and SMTP servers."]="If you would like to setup your mailbox using another client, you can get the required settings by going to the 'Managed DNS' link in your account. Clicking 'View/Edit Configuration' for the required domain, and select 'Advance Confirugation' at the bottom of the page. You will see all the DNS records for your domain along with the webmail, POP and SMTP servers.";

// File: /registrar-platform/www/application/views/default/account_private_registration.tpl
// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Private Registration"]="Private Registration";

// File: /registrar-platform/www/application/views/default/account_private_registration.tpl
$STRINGS[$LANG]["When you register a domain your contact information, including name and postal address,<br />will be displayed in the public Whois record."]="When you register a domain your contact information, including name and postal address,<br />will be displayed in the public Whois record.";

// File: /registrar-platform/www/application/views/default/account_private_registration.tpl
$STRINGS[$LANG]["Choose <span class='bold'>Go Private</span> to use Whois Privacy Ltd's contact information instead,<br />and protect yourself from spam, fraud and unsolicited emails."]="Choose <span class='bold'>Go Private</span> to use Whois Privacy Ltd's contact information instead,<br />and protect yourself from spam, fraud and unsolicited emails.";

// File: /registrar-platform/www/application/views/default/account_private_registration.tpl
$STRINGS[$LANG]["<a href='http://www.whoisprivacy.la/agreement' target='_blank'>Privacy terms and conditions apply</a>"]="<a href='http://www.whoisprivacy.la/agreement' target='_blank'>Privacy terms and conditions apply</a>";

// File: /registrar-platform/www/application/views/default/cart_services.tpl
$STRINGS[$LANG]["Additional Services"]="Additional Services";

// File: /registrar-platform/www/application/views/default/cart_services.tpl
$STRINGS[$LANG]["We offer a number of hosting packages to let you set up a website and/or email address for your chosen domain."]="We offer a number of hosting packages to let you set up a website and/or email address for your chosen domain.";

// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/e_hosting.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["[emails] POP3 Mailbox Accounts"]="[emails] POP3 Mailbox Accounts";

// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/e_hosting.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["[storage] Storage Space"]="[storage] Storage Space";

// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/e_hosting.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["[bw] of Bandwidth"]="[bw] of Bandwidth";

// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/e_hosting.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["[curr][price]/month"]="[curr][price]/month";

// File: /registrar-platform/www/application/views/default/cart_services.tpl
// File: /registrar-platform/www/application/views/default/e_hosting.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["* Discounts are available on this package for selected TLDs"]="* Discounts are available on this package for selected TLDs";

// File: /registrar-platform/www/application/views/default/cart_services.tpl
$STRINGS[$LANG]["<span class='bold'>Please note:</span> If you are purchasing or renewing multiple domains, any extra services selected will be added to EACH domain. You can remove any of these services for individual domains on the next page."]="<span class='bold'>Please note:</span> If you are purchasing or renewing multiple domains, any extra services selected will be added to EACH domain. You can remove any of these services for individual domains on the next page.";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Hosting"]="Hosting";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Site Builder"]="Site Builder";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Domain Registraton"]="Domain Registraton";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Hosting packages can be purchased for 1 year only, and are only renewable on an annual basis."]="Hosting packages can be purchased for 1 year only, and are only renewable on an annual basis.";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Private Whois purchases will be billed up to the expiry date of the domain name."]="Private Whois purchases will be billed up to the expiry date of the domain name.";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Website Builder packages can be purchased for 1 year only, and are only renewable on an annual basis."]="Website Builder packages can be purchased for 1 year only, and are only renewable on an annual basis.";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Google Apps purchases will be billed up to the expiry date of the domain name."]="Google Apps purchases will be billed up to the expiry date of the domain name.";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Including restore fee"]="Including restore fee";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Purchase only <br/>Expires [expires]"]="Purchase only <br/>Expires [expires]";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG][" year"]=" blwyddyn";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG][" years"]=" years";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Unlimited"]="Unlimited";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Until current expiry date"]="Until current expiry date";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG][" user"]=" user";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG][" users"]=" users";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Remove item"]="Remove item";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Approximate conversion rate. You will be billed in [currency]."]="Approximate conversion rate. You will be billed in [currency].";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Discount code '[code]' has been applied to this item"]="Discount code '[code]' has been applied to this item";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["If you have a promotional code which entitles you to a discount, please enter it below."]="OS oes cod hyrwyddo sy’n rhoi’r hawl i chi i ostyngiad, rhowch yn y blwch isod.";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Discount applies to total years purchased at initial registration; subsequent renewals will be at full retail price."]="Mae’r gostyngiad yn weithredol i’r cyfanswm blynyddoedd a brynwyd yn y cofrestriad cychwynnol; bydd adnewyddiadau pellach ar y pris manwerthu llawn.";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Promo Code:"]="Cod Hyrwyddo:";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Apply"]="Dodi";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["If you are VAT registered and would like to claim back your VAT or receive exemption, please enter your VAT number below:"]="Os ydych wedi eich cofrestru am TAW a hoffwch hawlio eich TAW yn ôl neu dderbyn esemptiad, rhowch eich rhif VAT isod:";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["VAT Number:"]="VAT Number:";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["If you are VAT registered and would like to claim back your VAT or receive exemption, please proceed to the checkout where you can apply your VAT number."]="If you are VAT registered and would like to claim back your VAT or receive exemption, please proceed to the checkout where you can apply your VAT number.";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["If you are VAT registered and would like to claim back your VAT or receive exemption then please click 'Edit Cart' to apply your VAT number."]="Os ydych wedi eich cofrestru am TAW a hoffwch hawlio eich TAW yn ôl neu dderbyn esemptiad, yna cliciwch ‘Golygu Cert’ er mwyn dodi eich rhif TAW.";

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
$STRINGS[$LANG]["Proceed to Checkout"]="Parhau i’r Ddesg Dalu
";

// File: /registrar-platform/www/application/views/default/e_verification.tpl
$STRINGS[$LANG]["WHOIS Verification"]="WHOIS Verification";

// File: /registrar-platform/www/application/views/default/e_verification.tpl
$STRINGS[$LANG]["Thanks for clicking the link! Your contact details have now been verified and the below are ready for use."]="Thanks for clicking the link! Your contact details have now been verified and the below are ready for use.";

// File: /registrar-platform/www/application/views/default/account_bulk_dns.tpl
$STRINGS[$LANG]["Bulk DNS Update"]="Bulk DNS Update";

// File: /registrar-platform/www/application/views/default/account_bulk_dns.tpl
$STRINGS[$LANG]["Current DNS Servers"]="Current DNS Servers";

// File: /registrar-platform/www/application/views/default/account_bulk_dns.tpl
$STRINGS[$LANG]["Update DNS Servers"]="Update DNS Servers";

// File: /registrar-platform/www/application/views/default/account_bulk_dns.tpl
$STRINGS[$LANG]["This will delete all existing DNS servers for the above domains and replace with new DNS Servers as specified below."]="This will delete all existing DNS servers for the above domains and replace with new DNS Servers as specified below.";

// File: /registrar-platform/www/application/views/default/account_bulk_dns.tpl
$STRINGS[$LANG]["Override domain names that are using Advanced DNS."]="Override domain names that are using Advanced DNS.";

// File: /registrar-platform/www/application/views/default/account_bulk_dns.tpl
// File: /registrar-platform/www/application/views/default/account_html_domain_dns.tpl
$STRINGS[$LANG]["Hostname"]="Hostname";

// File: /registrar-platform/www/application/views/default/account_bulk_dns.tpl
$STRINGS[$LANG]["Another Nameserver"]="Another Nameserver";

// File: /registrar-platform/www/application/views/default/account_bulk_dns.tpl
$STRINGS[$LANG]["Update All Domains"]="Update All Domains";

// File: /registrar-platform/www/application/views/default/inc_sitebuilder_settings.tpl
$STRINGS[$LANG]["Templates"]="Templates";

// File: /registrar-platform/www/application/views/default/inc_sitebuilder_settings.tpl
$STRINGS[$LANG]["Pages"]="Pages";

// File: /registrar-platform/www/application/views/default/inc_sitebuilder_settings.tpl
$STRINGS[$LANG]["Storage"]="Storage";

// File: /registrar-platform/www/application/views/default/inc_sitebuilder_settings.tpl
$STRINGS[$LANG]["Core Features"]="Core Features";

// File: /registrar-platform/www/application/views/default/inc_sitebuilder_settings.tpl
// File: /registrar-platform/www/application/views/default/inc_sitebuilder_settings.tpl
$STRINGS[$LANG]["E-Commerce"]="E-Commerce";

// File: /registrar-platform/www/application/views/default/inc_sitebuilder_settings.tpl
// File: /registrar-platform/www/application/views/default/inc_sitebuilder_settings.tpl
$STRINGS[$LANG]["Email Forwarding<br />Addresses"]="Dargyfeirio Ebost<br />Addresses";

// File: /registrar-platform/www/application/views/default/account_payments.tpl
$STRINGS[$LANG]["Your Payment History"]="Your Payment History";

// File: /registrar-platform/www/application/views/default/account_payments.tpl
$STRINGS[$LANG]["Click 'View Payments' to see details of the payment in a printable invoice format."]="Click 'View Payments' to see details of the payment in a printable invoice format.";

// File: /registrar-platform/www/application/views/default/account_payments.tpl
$STRINGS[$LANG]["ID"]="ID";

// File: /registrar-platform/www/application/views/default/account_payments.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Payment Date"]="Payment Date";

// File: /registrar-platform/www/application/views/default/account_payments.tpl
// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Amount"]="Amount";

// File: /registrar-platform/www/application/views/default/account_payments.tpl
$STRINGS[$LANG]["View"]="View";

// File: /registrar-platform/www/application/views/default/account_payments.tpl
$STRINGS[$LANG]["You have no payments"]="You have no payments";

// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]["Full Name*:"]="Enw Llawn*:";

// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]["Subject*:"]="Pwnc*:";

// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]["Question*:"]="Cwestiwn*:";

// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]["Send Enquiry"]="Danfon Ymholiad";

// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]["Customer Support"]="Cymorth i Gwsmeriaid";

// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]["TLD Registrar Solutions is committed to providing our customers with first class service. If you need support or have a question about any of our products please send an email to <a href='mailto:[support]'>[support]</a> and we will respond promptly. Whilst we endeavor to answer all of your questions ourselves, on occasion we may have to refer you to one of our partners for help, if we do this we will let you know."]="Mae TLD Regsitrar Solutions wedi ymrwymo i ddarparu gwasanaeth o’r radd flaenaf i’n cwsmeriaid.  Os oes angen cefnogaeth neu fod cwestiwn gennych ynghylch unrhyw un o’n cynnyrch, danfonwch ebost at <a href='mailto:[support]'>[support]</a> a fe fyddwn yn ymateb yn chwimwth.  Tra’n bob yn ceisio ateb eich holl gwestiynau ein hunain, ar achlysuron efallai y byddwn yn gorfod eich cyfeirio at un o’n partneriaid am gymorth ac fe fyddwn yn gadael i chi wybod os byddwn yn gwneud hynny.";

// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]["Complaints"]="Cwynion";

// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]["If you would like to report abuse please email <a href='mailto:[abuse]'>[abuse]</a> and we will deal with your correspondence promptly."]="Os hoffech adrodd am ddifrïo ebostiwch <a href='mailto:[abuse]'>[abuse]</a> a byddwn yn ymdrîn â’ch cyfatebiaeth yn chwimwth.";

// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]["Postal Address"]="Cyfeiriad Postio";

// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]["The [site_url] website is operated by TLD Registrar Solutions (TRS), a company registered in England and Wales with Company Number 07629187. TLD Registrar Solutions is a wholly owned subsidiary of CentralNic Group PLC. The company officers include Ben Crawford, Chairman and Glenn Hayward, Director."]="Mae’r wefan [site_url] wedi ei weithredu gan TLD Registrar Solutions (TRS), cwmni sydd wedi’i gofrestru yn Lloegr a Chymru gyda’r Rhif Cwmni 07629187. Mae TLD Regsistrar Solutions LTD yn is-gwmni sydd wedi’i berchen yn llwyr gan CentralNic Group PLC.  Mae swyddogion y cwmni yn cynnwys Ben Crawford, Cadeirydd a Glenn Hayward";

// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]["If you would like to correspond with us by post you can get in touch at:"]="Os hoffwch ohebu gyda ni drwy’r post gallwch gysylltu yn:";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Getting started"]="Getting started";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["FAQs for new users"]="FAQs for new users";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Adding content to your site"]="Adding content to your site";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Customising your site"]="Customising your site";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Glossary"]="Glossary";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Optimising Your Website"]="Optimising Your Website";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Publishing & SEO settings"]="Publishing & SEO settings";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["How to add Google Analytics"]="How to add Google Analytics";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Design, Themes & Styles"]="Design, Themes & Styles";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Font sets"]="Font sets";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Site background"]="Site background";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Change the colour of a row or column"]="Change the colour of a row or column";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Creating or editing a type style"]="Creating or editing a type style";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["The colour picker"]="The colour picker";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Choose a new theme for your site"]="Choose a new theme for your site";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Editing a theme"]="Editing a theme";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Pages & Navigation"]="Pages & Navigation";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Page Navigation"]="Page Navigation";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Create page folders/sub-navigation"]="Create page folders/sub-navigation";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Vertical Page Navigation"]="Vertical Page Navigation";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Navigation panel"]="Navigation panel";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Setting Page options"]="Setting Page options";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Selective page publication"]="Selective page publication";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Page cloning"]="Page cloning";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Text, Images, Files"]="Text, Images, Files";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Images panel"]="Images panel";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Working with images"]="Working with images";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Text editing"]="Text editing";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Image uploads"]="Image uploads";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Get an image URL"]="Get an image URL";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Image lightbox and rollover"]="Image lightbox and rollover";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["How do I change the favicon?"]="How do I change the favicon?";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["How do i add flash content?"]="How do i add flash content?";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Using Widgets"]="Using Widgets";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["How do I test a form?"]="How do I test a form?";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Form field Selection Menu"]="Form field Selection Menu";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Manage Databases"]="Manage Databases";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Video Widget/Panel"]="Video Widget/Panel";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Background widget"]="Background widget";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Template widgets"]="Template widgets";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Comments widget"]="Comments widget";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Twitter widget"]="Twitter widget";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Add a Tweet Button"]="Add a Tweet Button";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Social bookmarking widget"]="Social bookmarking widget";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Site search"]="Site search";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["RSS feed widget"]="RSS feed widget";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Google Maps widget"]="Google Maps widget";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Flickr widget"]="Flickr widget";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Carousel widget"]="Carousel widget";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Sitemap widget"]="Sitemap widget";

// File: /registrar-platform/www/application/views/default/inc_support_sitebuilder_index.tpl
$STRINGS[$LANG]["Google Checkout Widgets"]="Google Checkout Widgets";

// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
$STRINGS[$LANG]["Add Website Builder"]="Add Website Builder";

// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
$STRINGS[$LANG]["Easy to use website builder. Get started today!"]="Easy to use website builder. Get started today!";

// File: /registrar-platform/www/application/views/default/cart_site_builder.tpl
$STRINGS[$LANG]["Choose from a great range of packages. Unlimited FREE trial now available!"]="Choose from a great range of packages. Unlimited FREE trial now available!";

// File: /registrar-platform/www/application/views/default/cart_html_validation.tpl
$STRINGS[$LANG]["Required Data"]="Required Data";

// File: /registrar-platform/www/application/views/default/cart_html_validation.tpl
$STRINGS[$LANG]["Required Action"]="Required Action";

// File: /registrar-platform/www/application/views/default/cart_html_validation.tpl
$STRINGS[$LANG]["Signed Marked Data"]="Signed Marked Data";

// File: /registrar-platform/www/application/views/default/cart_html_validation.tpl
$STRINGS[$LANG]["Uploaded"]="Uploaded";

// File: /registrar-platform/www/application/views/default/cart_html_validation.tpl
$STRINGS[$LANG]["Upload SMD Data"]="Upload SMD Data";

// File: /registrar-platform/www/application/views/default/cart_html_validation.tpl
$STRINGS[$LANG]["Thank you for submitting your validation data, you may now continue with your purchase."]="Thank you for submitting your validation data, you may now continue with your purchase.";

// File: /registrar-platform/www/application/views/default/cart_html_validation.tpl
$STRINGS[$LANG]["Please be aware that your validation data has only passed initial checks. A more thorough check will be performed by the operating registry of the TLD. Should validation checks fail, you will be notified by email and payment will be refunded."]="Please be aware that your validation data has only passed initial checks. A more thorough check will be performed by the operating registry of the TLD. Should validation checks fail, you will be notified by email and payment will be refunded.";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Activate Advanced DNS Manager for [domain]"]="Activate Advanced DNS Manager for [domain]";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Please note: Activating Advanced DNS Manager.."]="Please note: Activating Advanced DNS Manager..";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["will remove any exisitng DNS server entries"]="will remove any exisitng DNS server entries";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["may make your website and e-mail unavailable, until they have been configured through the Advanced DNS Manager"]="may make your website and e-mail unavailable, until they have been configured through the Advanced DNS Manager";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["may take up to 24 hours to propagate"]="may take up to 24 hours to propagate";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Before [domain] can be activated on Advanced DNS Manager, you must read and agree to the terms and conditions of service."]="Before [domain] can be activated on Advanced DNS Manager, you must read and agree to the terms and conditions of service.";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["I have read and accept the Terms and Conditions"]="I have read and accept the Terms and Conditions";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Please be patient as this can take a couple of minutes to set up"]="Please be patient as this can take a couple of minutes to set up";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Activate Domain on Advanced DNS"]="Activate Domain on Advanced DNS";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Current DNS Records for [domain]"]="Current DNS Records for [domain]";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["As your domain is using the website builder, editing or deleting these records may cause your website to stop resolving."]="As your domain is using the website builder, editing or deleting these records may cause your website to stop resolving.";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_html_domain_dns.tpl
$STRINGS[$LANG]["As your domain is hosted with us, editing or deleting these records may cause your website to stop resolving or prevent emails being received and delivered."]="As your domain is hosted with us, editing or deleting these records may cause your website to stop resolving or prevent emails being received and delivered.";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Add New DNS Record"]="Add New DNS Record";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["the Fully-Qualified Domain Name (FQDN) such as <strong>www.example.uk.com</strong>"]="the Fully-Qualified Domain Name (FQDN) such as <strong>www.example.uk.com</strong>";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["just a hostname/subdomain, such as <strong>www</strong>, <strong>ftp</strong>, etc."]="just a hostname/subdomain, such as <strong>www</strong>, <strong>ftp</strong>, etc.";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["<strong>@</strong> (alias), which will be replaced with the domain name"]="<strong>@</strong> (alias), which will be replaced with the domain name";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Web Forwarding"]="Web Forwarding";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Please make sure you enter the full URL including http:// or https://."]="Please make sure you enter the full URL including http:// or https://.";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Domain Redirect - The address you are forwarding to will be shown in the browser"]="Domain Redirect - The address you are forwarding to will be shown in the browser";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Domain Masking - Your [store] domain will be shown in the browser"]="Domain Masking - Your [store] domain will be shown in the browser";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Web Hosting Records"]="Web Hosting Records";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["The following records were setup automatically when you purchased web hosting with us.<br />These records are shown here for informational purposes, or if you change your DNS configuration and wish to revert back."]="The following records were setup automatically when you purchased web hosting with us.<br />These records are shown here for informational purposes, or if you change your DNS configuration and wish to revert back.";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Google Apps Records"]="Google Apps Records";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["The following records were setup automatically when you purchased 'Google Apps' with us.<br />These records are shown here for informational purposes, or if you change your DNS configuration and wish to revert back."]="The following records were setup automatically when you purchased 'Google Apps' with us.<br />These records are shown here for informational purposes, or if you change your DNS configuration and wish to revert back.";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Website Builder Records"]="Website Builder Records";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["The following records were setup automatically when you purchased 'Website Builder' with us.<br />These records are shown here for informational purposes, or if you change your DNS configuration and wish to revert back."]="The following records were setup automatically when you purchased 'Website Builder' with us.<br />These records are shown here for informational purposes, or if you change your DNS configuration and wish to revert back.";

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]["Additional Information"]="Additional Information";

// File: /registrar-platform/www/application/views/default/e_hosting.tpl
$STRINGS[$LANG]["Available Hosting Packages"]="Available Hosting Packages";

// File: /registrar-platform/www/application/views/default/e_hosting.tpl
$STRINGS[$LANG]["One of the simple hosting packages below will enable you to setup a website and/or email address for your chosen domain(s). A hosting package can be added during the checkout process or, if you already own a domain, through your account login."]="One of the simple hosting packages below will enable you to setup a website and/or email address for your chosen domain(s). A hosting package can be added during the checkout process or, if you already own a domain, through your account login.";

// File: /registrar-platform/www/application/views/default/account_html_domain_dns.tpl
$STRINGS[$LANG]["Use <a href='/account/advanced_dns/[domain]'>Advanced DNS Manager</a> to configure your own DNS records<br /> (web forwarding, A, MX, CNAME, TXT and SRV Records)."]="Use <a href='/account/advanced_dns/[domain]'>Advanced DNS Manager</a> to configure your own DNS records<br /> (web forwarding, A, MX, CNAME, TXT and SRV Records).";

// File: /registrar-platform/www/application/views/default/account_html_domain_dns.tpl
$STRINGS[$LANG]["Save All Changes"]="Save All Changes";

// File: /registrar-platform/www/application/views/default/account_html_domain_dns.tpl
$STRINGS[$LANG]["This domain is using the Advanced DNS Manager"]="This domain is using the Advanced DNS Manager";

// File: /registrar-platform/www/application/views/default/account_html_domain_dns.tpl
$STRINGS[$LANG]["Edit Records"]="Edit Records";

// File: /registrar-platform/www/application/views/default/account_html_domain_dns.tpl
$STRINGS[$LANG]["Deactivate Advanced DNS Manager"]="Deactivate Advanced DNS Manager";

// File: /registrar-platform/www/application/views/default/account_manage_files.tpl
$STRINGS[$LANG]["FTP Client"]="FTP Client";

// File: /registrar-platform/www/application/views/default/account_manage_files.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]["Hosting account is locked."]="Hosting account is locked.";

// File: /registrar-platform/www/application/views/default/account_manage_files.tpl
$STRINGS[$LANG]["[domain]"]="[domain]";

// File: /registrar-platform/www/application/views/default/account_manage_files.tpl
$STRINGS[$LANG]["Use the FTP Client below to manage your files and folders on your hosting account.<br />You can also your manage your files and folder using an alternative FTP client using the access information below."]="Use the FTP Client below to manage your files and folders on your hosting account.<br />You can also your manage your files and folder using an alternative FTP client using the access information below.";

// File: /registrar-platform/www/application/views/default/account_manage_files.tpl
$STRINGS[$LANG]["FTP Account Access"]="FTP Account Access";

// File: /registrar-platform/www/application/views/default/account_manage_files.tpl
$STRINGS[$LANG]["FTP Address:"]="FTP Address:";

// File: /registrar-platform/www/application/views/default/account_manage_files.tpl
$STRINGS[$LANG]["Username:"]="Username:";

// File: /registrar-platform/www/application/views/default/account_manage_files.tpl
$STRINGS[$LANG]["Password:"]="Password:";

// File: /registrar-platform/www/application/views/default/account_manage_files.tpl
$STRINGS[$LANG]["Port Number:"]="Port Number:";

// File: /registrar-platform/www/application/views/default/account_manage_files.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG][":hidden"]=":hidden";

// File: /registrar-platform/www/application/views/default/account_transfers.tpl
$STRINGS[$LANG]["To request the transfer of a domain name to this account, please enter the domain and auth/transfer code in the form below. An email will then be sent to the domain's administrative contact to validate the request. Once validated the transfer will be initiated."]="To request the transfer of a domain name to this account, please enter the domain and auth/transfer code in the form below. An email will then be sent to the domain's administrative contact to validate the request. Once validated the transfer will be initiated.";

// File: /registrar-platform/www/application/views/default/account_transfers.tpl
$STRINGS[$LANG]["The transfer will be automatically APPROVED after five calendar days and the domain transferred to this account. If the losing account holder approves the transfer within the five days it will be transferred immediately."]="The transfer will be automatically APPROVED after five calendar days and the domain transferred to this account. If the losing account holder approves the transfer within the five days it will be transferred immediately.";

// File: /registrar-platform/www/application/views/default/account_transfers.tpl
$STRINGS[$LANG]["You will be notified by email when the transfer is complete."]="You will be notified by email when the transfer is complete.";

// File: /registrar-platform/www/application/views/default/account_transfers.tpl
$STRINGS[$LANG]["The above rules also apply to transfers between user accounts"]="The above rules also apply to transfers between user accounts";

// File: /registrar-platform/www/application/views/default/account_transfers.tpl
$STRINGS[$LANG]["Transfer Code:"]="Transfer Code:";

// File: /registrar-platform/www/application/views/default/account_transfers.tpl
$STRINGS[$LANG]["Request Transfer"]="Request Transfer";

// File: /registrar-platform/www/application/views/default/account_html_transfer_history.tpl
$STRINGS[$LANG]["Transfer History"]="Transfer History";

// File: /registrar-platform/www/application/views/default/account_html_transfer_history.tpl
$STRINGS[$LANG]["Action"]="Action";

// File: /registrar-platform/www/application/views/default/account_html_transfer_history.tpl
$STRINGS[$LANG]["Completed"]="Completed";

// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
$STRINGS[$LANG]["Domains with Public Whois Information"]="Domains with Public Whois Information";

// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
$STRINGS[$LANG]["Private whois is available for purchase 90+ days before the domain expiry date. If your domain expires in less than 90 days, you must renew it in order to purchase private whois."]="Private whois is available for purchase 90+ days before the domain expiry date. If your domain expires in less than 90 days, you must renew it in order to purchase private whois.";

// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
$STRINGS[$LANG]["Price *"]="Pris *";

// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
$STRINGS[$LANG]["Added to cart"]="Added to cart";

// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["Go Private"]="Go Private";

// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
$STRINGS[$LANG][" from [currency][price]/year"]=" from [currency][price]/year";

// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
$STRINGS[$LANG]["* You may be charged less or more than the stated amount as the cost is calculated pro rata depending on when the domain name expires. The exact price will be shown in the shopping cart."]="* You may be charged less or more than the stated amount as the cost is calculated pro rata depending on when the domain name expires. The exact price will be shown in the shopping cart.";

// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
$STRINGS[$LANG]["Your Private Whois Domains"]="Your Private Whois Domains";

// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
$STRINGS[$LANG]["If you would like to make your whois information public, you can do so below. You can re-enable your privacy settings anytime before the expiry date."]="If you would like to make your whois information public, you can do so below. You can re-enable your privacy settings anytime before the expiry date.";

// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
$STRINGS[$LANG]["Privacy Expiry"]="Privacy Expiry";

// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
$STRINGS[$LANG]["Re-enable"]="Re-enable";

// File: /registrar-platform/www/application/views/default/account_html_private_registration.tpl
$STRINGS[$LANG]["Go Public"]="Go Public";

// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Use this page to choose additional products/service to add to your domain name.<br />You can purchase multiple products/services, just click <span class='bold'>'Add to cart'</span> after choosing your domain name."]="Use this page to choose additional products/service to add to your domain name.<br />You can purchase multiple products/services, just click <span class='bold'>'Add to cart'</span> after choosing your domain name.";

// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Add Services for:"]="Add Services for:";

// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_html_notifications.tpl
// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]["View Shopping Cart"]="View Shopping Cart";

// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Google Apps Plans"]="Google Apps Plans";


// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Show"]="Show";

// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Hide"]="Hide";

// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Website Builder Packages"]="Website Builder Packages";

// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Create your website within minutes. Unlimited FREE trial now available!"]="Create your website within minutes. Unlimited FREE trial now available!";

// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Setup Now"]="Setup Now";

// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Web Hosting Plans"]="Web Hosting Plans";

// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Choose one of our hosting plans which will enable you to setup a website with personal email addresses."]="Choose one of our hosting plans which will enable you to setup a website with personal email addresses.";

// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["[curr][price] /month"]="[curr][price] /month";

// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Are you sure?"]="Are you sure?";

// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Do you want to continue with the purchase?"]="Do you want to continue with the purchase?";

// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Website Builder Setup"]="Website Builder Setup";

// File: /registrar-platform/www/application/views/default/account_services.tpl
// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["To setup your free website builder package we will need to make changes to the DNS settings of your chosen domain name. If you have any web or email hosting services already setup for this domain name they may stop working."]="To setup your free website builder package we will need to make changes to the DNS settings of your chosen domain name. If you have any web or email hosting services already setup for this domain name they may stop working.";

// File: /registrar-platform/www/application/views/default/account_services.tpl
$STRINGS[$LANG]["Before your domain can be activated on Advanced DNS Manager and Website Builder, you must read and agree to the terms and conditions of service."]="Before your domain can be activated on Advanced DNS Manager and Website Builder, you must read and agree to the terms and conditions of service.";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["The information below is required to register domain name(s):"]="Mae’r wybodaeth isod yn angenrheidiol er mwyn cofrestru enw(au) parth:";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["About you:"]="About you:";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["Your Name*:"]="Eich Enw*:";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["Confirm Email*:"]="Cadarnhau Ebost*:";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["Password must be at least 8 characters long, contain at least one one upper case character, one lower case character and a number or non-alphanumeric character."]="Password must be at least 8 characters long, contain at least one one upper case character, one lower case character and a number or non-alphanumeric character.";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["Confirm Password*:"]="Cadarnhau Cyfrinair*:";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["Contact information:"]="Gwybodaeth Cyswllt:";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["Organization:"]="Sefydliad:";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
// File: /registrar-platform/www/application/views/default/inc_country_contact.tpl
$STRINGS[$LANG]["Organisation:"]="Sefydliad:";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
// File: /registrar-platform/www/application/views/default/inc_country_contact.tpl
$STRINGS[$LANG]["State or Province:"]="Wladwriaeth neu Talaith:";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["Post Code*:"]="Cod Post*:";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["A VAT Number is required, to qualify for VAT exemption. This is only applicable to businesses."]="A VAT Number is required, to qualify for VAT exemption. This is only applicable to businesses.";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["From time to time we may notify you of relevant new products and features on [website] and of promotions from us and our affiliates. By ticking below you consent to receive such communications from us."]="O bryd i’w gilydd efallai y byddwn yn eich gwneud yn hysbys o gynnyrch a nodweddion newydd ar ac o hyrwyddiadau gennym ni neu gwmnïau sydd wedi eu mabwysiadu gennym. Wrth dicio isod rydych yn cydsynio i dderbyn y math hwn o gyfathrebu wrthom.";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["[client], may use the information you provided above contact you about domain names and other services which may be of interest to you."]="Efallai y bydd [client] yn defnyddio’r wybodaeth sydd wedi ei ddarparu gennych i gysylltu â chi ynghylch enwau parth a gwasanaethau eraill a fydd o ddiddordeb i chi.";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["You will need to verify your details within [verify_days] days. We'll send you an email with the details on how to, so make sure you add us to your safe list."]="Bydd yn rhaid i chi gadarnhau eich manylion o fewn [verify_days] diwrnod. Fe fyddwn yn danfon ebost atoch gyda’r manylion ynghylch sut i wneud hyn, felly sicrhewch eich bod yn ein hychwanegu i’ch rhestr diogel.";

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]["Agree & Continue"]="Cytuno a Pharhau";

// File: /registrar-platform/www/application/views/default/inc_login.tpl
$STRINGS[$LANG]["Email Address*:"]="Cyfeiriad Ebost*:";

// File: /registrar-platform/www/application/views/default/inc_login.tpl
$STRINGS[$LANG]["Log into account"]="Mewngofnodi i’r Cyfrif";

// File: /registrar-platform/www/application/views/default/inc_login.tpl
$STRINGS[$LANG]["Forgotten Password?"]="Anghofio Cyfrinair?";

// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
$STRINGS[$LANG]["Unlock your Google Apps account"]="Unlock your Google Apps account";

// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
$STRINGS[$LANG]["This domain is already registered to a different reseller. To transfer your account you need to unlock it and provide the transfer token. Please follow this <a href='https://support.google.com/a/answer/71281?hl=en' target='_blank'>link</a> to do so. Come back here to complete your transaction once you've retrieved the transfer token."]="This domain is already registered to a different reseller. To transfer your account you need to unlock it and provide the transfer token. Please follow this <a href='https://support.google.com/a/answer/71281?hl=en' target='_blank'>link</a> to do so. Come back here to complete your transaction once you've retrieved the transfer token.";

// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
$STRINGS[$LANG]["Your Transfer Token*:"]="Your Transfer Token*:";

// File: /registrar-platform/www/application/views/default/inc_google_addon.tpl
$STRINGS[$LANG]["Send"]="Send";

// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
$STRINGS[$LANG]["Edit Contact Details"]="Edit Contact Details";

// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
$STRINGS[$LANG]["Any updates to this contact will be reflected in the whois record for all associated domains."]="Any updates to this contact will be reflected in the whois record for all associated domains.";


// File: /registrar-platform/www/application/views/default/account_html_edit_contact.tpl
$STRINGS[$LANG]["To update the data below please <a href='/e/contact'>contact us</a>"]="To update the data below please <a href='/e/contact'>contact us</a>";


// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
$STRINGS[$LANG]["You have received the following Trademark Notice(s) because you have applied for domain name(s) which matches at least one trademark record submitted to the Trademark Clearinghouse."]="You have received the following Trademark Notice(s) because you have applied for domain name(s) which matches at least one trademark record submitted to the Trademark Clearinghouse.";

// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
$STRINGS[$LANG]["Please read and accept the 'Trademark Claim Notice(s)' for all your domains below. You can only continue with your purchase once all trademark claim notice(s) have been accepted or declined. If you decline a Trademark Notice for a domain, the domain name will be removed from your shopping cart."]="Please read and accept the 'Trademark Claim Notice(s)' for all your domains below. You can only continue with your purchase once all trademark claim notice(s) have been accepted or declined. If you decline a Trademark Notice for a domain, the domain name will be removed from your shopping cart.";

// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
$STRINGS[$LANG]["Continue with purchase"]="Continue with purchase";

// File: /registrar-platform/www/application/views/default/cart_trademark_claim.tpl
$STRINGS[$LANG]["collapsed"]="collapsed";

// File: /registrar-platform/www/application/views/default/inc_cart_search.tpl
// File: /registrar-platform/www/application/views/default/inc_cart_search.tpl
// File: /registrar-platform/www/application/views/default/inc_cart_search_multiple.tpl
// File: /registrar-platform/www/application/views/default/inc_cart_search_multiple.tpl
// File: /registrar-platform/www/application/views/default/inc_cart_search_multiple.tpl
$STRINGS[$LANG]["input-blurred"]="input-blurred";

// File: /registrar-platform/www/application/views/default/e_forgot_password.tpl
$STRINGS[$LANG]["Forgot Password"]="Forgot Password";

// File: /registrar-platform/www/application/views/default/e_forgot_password.tpl
$STRINGS[$LANG]["If you've forgotten your password, enter the e-mail address you used to register with us into the box below or <br /> one of any domain names you maybe have registered and press the 'Reset' button, and we'll send you a new password in an e-mail."]="If you've forgotten your password, enter the e-mail address you used to register with us into the box below or <br /> one of any domain names you maybe have registered and press the 'Reset' button, and we'll send you a new password in an e-mail.";

// File: /registrar-platform/www/application/views/default/e_forgot_password.tpl
// File: /registrar-platform/www/application/views/default/e_forgot_password.tpl
$STRINGS[$LANG]["Reset"]="Reset";

// File: /registrar-platform/www/application/views/default/e_forgot_password.tpl
$STRINGS[$LANG]["Domain Name*:"]="Domain Name*:";

// File: /registrar-platform/www/application/views/default/e_forgot_password.tpl
$STRINGS[$LANG]["Click here to login"]="Click here to login";

// File: /registrar-platform/www/application/views/default/e_forgot_password.tpl
$STRINGS[$LANG]["or"]="neu";

// File: /registrar-platform/www/application/views/default/e_forgot_password.tpl
$STRINGS[$LANG]["create a new account"]="create a new account";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["(Accepted)"]="(Accepted)";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["You have received this Trademark Notice because you have applied for a domain name which match at least one trademark record submitted to the Trademark Clearinghouse."]="You have received this Trademark Notice because you have applied for a domain name which match at least one trademark record submitted to the Trademark Clearinghouse.";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["You may or may not be entitled to register the domain name depending on your intended use and whether it is the same or significantly overlaps with the trademarks listed below. Your rights to register this domain name may or may not be protected as noncommercial use or “fair use” by the laws of your country."]="You may or may not be entitled to register the domain name depending on your intended use and whether it is the same or significantly overlaps with the trademarks listed below. Your rights to register this domain name may or may not be protected as noncommercial use or “fair use” by the laws of your country.";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Please read the trademark information below carefully, including the trademarks, jurisdictions, and goods and service for which the trademarks are registered. Please be aware that not all jurisdictions review trademark applications closely, so some of the trademark information below may exist in a national or regional registry which does not conduct a thorough or substantive review of trademark rights prior to registration."]="Please read the trademark information below carefully, including the trademarks, jurisdictions, and goods and service for which the trademarks are registered. Please be aware that not all jurisdictions review trademark applications closely, so some of the trademark information below may exist in a national or regional registry which does not conduct a thorough or substantive review of trademark rights prior to registration.";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["If you have questions, you may want to consult an attorney or legal expert on trademarks and intellectual property for guidance."]="If you have questions, you may want to consult an attorney or legal expert on trademarks and intellectual property for guidance.";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["If you continue with this registration, you represent that, you have received and you understand this notice and to the best of your knowledge, your registration and use of the requested domain name will not infringe on the trademark rights listed below."]="If you continue with this registration, you represent that, you have received and you understand this notice and to the best of your knowledge, your registration and use of the requested domain name will not infringe on the trademark rights listed below.";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["The following [number] Trademarks are listed in the Trademark Clearinghouse:"]="The following [number] Trademarks are listed in the Trademark Clearinghouse:";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["The following Trademark is listed in the Trademark Clearinghouse:"]="The following Trademark is listed in the Trademark Clearinghouse:";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Mark:"]="Mark:";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Jurisdiction:"]="Jurisdiction:";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Goods:"]="Goods:";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["International Class of Goods and Services or Equivalent if applicable:"]="International Class of Goods and Services or Equivalent if applicable:";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["N/A"]="N/A";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Trademark Registrant:"]="Trademark Registrant:";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Trademark Registrant Contact:"]="Trademark Registrant Contact:";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["You cannot register this [domain] because an error occured while retrieving the necessary trademark claim information."]="You cannot register this [domain] because an error occured while retrieving the necessary trademark claim information.";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["If you decline the trademark notice, the domain name will be removed from your shopping cart"]="If you decline the trademark notice, the domain name will be removed from your shopping cart";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Decline"]="Decline";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["You cannot register [domain] because the trademark claim information appears to be invalid."]="You cannot register [domain] because the trademark claim information appears to be invalid.";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Please accept or decline the trademark claim notice for [domain]"]="Please accept or decline the trademark claim notice for [domain]";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Accept"]="Accept";

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]["Trademark claim notice for [domain], has been successfully accepted."]="Trademark claim notice for [domain], has been successfully accepted.";

// File: /registrar-platform/www/application/views/default/e_terms_deletion_renewal.tpl
$STRINGS[$LANG]["Deletion & Auto Renewal Policy"]="Deletion & Auto Renewal Policy";

// File: /registrar-platform/www/application/views/default/account_html_notifications.tpl
$STRINGS[$LANG]["Your Notifications"]="Your Notifications";

// File: /registrar-platform/www/application/views/default/account_html_notifications.tpl
$STRINGS[$LANG]["Expiring in the next [renew_days] days:"]="Expiring in the next [renew_days] days:";

// File: /registrar-platform/www/application/views/default/account_html_notifications.tpl
$STRINGS[$LANG]["Transfer Requests:"]="Transfer Requests:";

// File: /registrar-platform/www/application/views/default/account_html_notifications.tpl
$STRINGS[$LANG]["You have received a transfer request(s)."]="You have received a transfer request(s).";

// File: /registrar-platform/www/application/views/default/account_html_notifications.tpl
$STRINGS[$LANG]["View Requests"]="View Requests";

// File: /registrar-platform/www/application/views/default/account_html_notifications.tpl
$STRINGS[$LANG]["Account Verification:"]="Account Verification:";

// File: /registrar-platform/www/application/views/default/account_html_notifications.tpl
$STRINGS[$LANG]["Contact(s) Verification:"]="Contact(s) Verification:";

// File: /registrar-platform/www/application/views/default/account_html_notifications.tpl
$STRINGS[$LANG]["Your Shopping Cart:"]="Eich Cert Siopa:";

// File: /registrar-platform/www/application/views/default/account_html_notifications.tpl
$STRINGS[$LANG]["You have [cart_count] item(s) in your shopping cart."]="You have [cart_count] item(s) in your shopping cart.";

// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]["Payment ID #[id]"]="Payment ID #[id]";

// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]["(Refunded)"]="(Refunded)";

// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]["(Cancelled)"]="(Cancelled)";

// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]["VAT Number"]="Rhif TAW";

// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]["Payment Date:"]="Payment Date:";

// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]["Payment Type:"]="Payment Type:";

// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]["Card Information:"]="Card Information:";

// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]["Service Type"]="Service Type";

// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]["Domain (if applicable)"]="Domain (if applicable)";

// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]["Year(s)"]="Year(s)";

// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]["(Deleted)"]="(Deleted)";

// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]["VAT exemption claimed using VAT number: [vat_number]"]="VAT exemption claimed using VAT number: [vat_number]";

// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]["The transaction will be in the name of [company] on your credit card statement."]="The transaction will be in the name of [company] on your credit card statement.";

// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]["Print Payment"]="Print Payment";

// File: /registrar-platform/www/application/views/default/e_qrcode.tpl
$STRINGS[$LANG]["Free QR Code"]="Free QR Code";

// File: /registrar-platform/www/application/views/default/e_qrcode.tpl
$STRINGS[$LANG]["QR (short for Quick Response) codes are two-dimensional bar codes that can be scanned using a smartphone or other personal media device with a QR Code Reader application."]="QR (short for Quick Response) codes are two-dimensional bar codes that can be scanned using a smartphone or other personal media device with a QR Code Reader application.";

// File: /registrar-platform/www/application/views/default/e_qrcode.tpl
$STRINGS[$LANG]["Below we will generate a QR code for your domain name which will have the URL of your domain encoded in it. You can then save this QR code and place it on flyers, posters, signs, business cards etc. so your customers can scan it and  no longer have to remember or write down your URL."]="Below we will generate a QR code for your domain name which will have the URL of your domain encoded in it. You can then save this QR code and place it on flyers, posters, signs, business cards etc. so your customers can scan it and  no longer have to remember or write down your URL.";

// File: /registrar-platform/www/application/views/default/e_qrcode.tpl
// File: /registrar-platform/www/application/views/default/inc_cart_search_multiple.tpl
$STRINGS[$LANG]["www."]="www.";

// File: /registrar-platform/www/application/views/default/e_qrcode.tpl
$STRINGS[$LANG]["Get Code"]="Get Code";

// File: /registrar-platform/www/application/views/default/e_qrcode.tpl
$STRINGS[$LANG]["QR Code for "]="QR Code for ";

// File: /registrar-platform/www/application/views/default/e_qrcode.tpl
$STRINGS[$LANG]["To download this QR code right click on the image and select 'Save image As'. Then navigate to the directory in which you want to save the image and click 'Save'"]="To download this QR code right click on the image and select 'Save image As'. Then navigate to the directory in which you want to save the image and click 'Save'";

// File: /registrar-platform/www/application/views/default/e_qrcode.tpl
$STRINGS[$LANG]["HTML embed code:"]="HTML embed code:";

// File: /registrar-platform/www/application/views/default/e_qrcode.tpl
$STRINGS[$LANG]["Use this HTML code to diplay your QR code on your website."]="Use this HTML code to diplay your QR code on your website.";

// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["When you register a domain, details like your name and postal address can be seen in a public database. WHOIS privacy hides these details from only [curr][price]/year per domain name. Choose 'Go Private' to protect yourself from spam, fraud and unsolicited emails."]="When you register a domain, details like your name and postal address can be seen in a public database. WHOIS privacy hides these details from only [curr][price]/year per domain name. Choose 'Go Private' to protect yourself from spam, fraud and unsolicited emails.";

// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["Stay Public"]="Stay Public";

// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["Keep your information on the pubic WHOIS database."]="Keep your information on the pubic WHOIS database.";

// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["Hide your information in the public WHOIS database from [curr][price]/year.<br />Protection from spam and unsolicited emails."]="Hide your information in the public WHOIS database from [curr][price]/year.<br />Protection from spam and unsolicited emails.";

// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["<span class='bold'>Please note:</span> If you are purchasing or renewing multiple domains, selecting private WHOIS will be added to EACH domain. You can remove private WHOIS for individual domains on the shopping cart page."]="<span class='bold'>Please note:</span> If you are purchasing or renewing multiple domains, selecting private WHOIS will be added to EACH domain. You can remove private WHOIS for individual domains on the shopping cart page.";

// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["pr_active_box"]="pr_active_box";

// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]["pr_inactive_box"]="pr_inactive_box";

// File: /registrar-platform/www/application/views/default/account_affiliate.tpl
$STRINGS[$LANG]["Sales & Commission"]="Sales & Commission";

// File: /registrar-platform/www/application/views/default/account_affiliate.tpl
$STRINGS[$LANG]["Date"]="Date";

// File: /registrar-platform/www/application/views/default/account_affiliate.tpl
$STRINGS[$LANG]["Sales"]="Sales";

// File: /registrar-platform/www/application/views/default/account_affiliate.tpl
$STRINGS[$LANG]["Commission"]="Commission";

// File: /registrar-platform/www/application/views/default/account_affiliate.tpl
$STRINGS[$LANG]["Paid Date"]="Paid Date";

// File: /registrar-platform/www/application/views/default/account_affiliate.tpl
$STRINGS[$LANG]["Total"]="Total";

// File: /registrar-platform/www/application/views/default/account_affiliate.tpl
$STRINGS[$LANG]["Converted into your payment currency"]="Converted into your payment currency";

// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]["[action] Your Website Builder Package"]="[action] Your Website Builder Package";

// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]["1. Choose your domain"]="1. Choose your domain";

// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]["Domain:"]="Domain:";

// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]["Select a domain name:"]="Select a domain name:";

// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]["Changing your current 'Website Builder' package will take effect immediately once payment has been processed."]="Changing your current 'Website Builder' package will take effect immediately once payment has been processed.";

// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]["I would like to add an additional year to this package"]="I would like to add an additional year to this package";

// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]["2. Your Package Options"]="2. Your Package Options";

// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]["Current Plan"]="Current Plan";

// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["You are downgrading your package.."]="You are downgrading your package..";

// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Certain features available in your '<span id='current_plan'></span>' package will not be available in the '<span id='new_plan'></span>' package. You will need to remove these features in your 'Website Builder Editor' before you can republish your website."]="Certain features available in your '<span id='current_plan'></span>' package will not be available in the '<span id='new_plan'></span>' package. You will need to remove these features in your 'Website Builder Editor' before you can republish your website.";

// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Are you sure you want to add this package to your cart?"]="Are you sure you want to add this package to your cart?";

// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]["sitebuilder_plan_current"]="sitebuilder_plan_current";

// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]["sitebuilder_plan_hidden"]="sitebuilder_plan_hidden";

// File: /registrar-platform/www/application/views/default/account_nameservers.tpl
$STRINGS[$LANG]["Manage Nameservers"]="Manage Nameservers";

// File: /registrar-platform/www/application/views/default/account_nameservers.tpl
// File: /registrar-platform/www/application/views/default/account_nameservers.tpl
$STRINGS[$LANG]["Register Nameserver"]="Register Nameserver";

// File: /registrar-platform/www/application/views/default/account_nameservers.tpl
$STRINGS[$LANG]["The registration of this nameserver requires manual processing. This should take no longer than 48 hours."]="The registration of this nameserver requires manual processing. This should take no longer than 48 hours.";

// File: /registrar-platform/www/application/views/default/account_nameservers.tpl
$STRINGS[$LANG]["You can only register nameservers under domains that you control and you must enter a valid IP address.<br/>If you have been given nameservers by your hosting company and want to add those to your domain you can do so on the settings page for the domain."]="You can only register nameservers under domains that you control and you must enter a valid IP address.<br/>If you have been given nameservers by your hosting company and want to add those to your domain you can do so on the settings page for the domain.";

// File: /registrar-platform/www/application/views/default/account_nameservers.tpl
$STRINGS[$LANG]["Please be aware creating or changing a nameserver can take up to 48 hours."]="Please be aware creating or changing a nameserver can take up to 48 hours.";

// File: /registrar-platform/www/application/views/default/account_nameservers.tpl
$STRINGS[$LANG]["Server Name:"]="Server Name:";

// File: /registrar-platform/www/application/views/default/account_nameservers.tpl
$STRINGS[$LANG]["IP Address:"]="IP Address:";

// File: /registrar-platform/www/application/views/default/account_nameservers.tpl
$STRINGS[$LANG]["Sorry, you cannot manage your nameservers if you haven't registered any domains."]="Sorry, you cannot manage your nameservers if you haven't registered any domains.";

// File: /registrar-platform/www/application/views/default/account_html_view_contacts.tpl
$STRINGS[$LANG]["Contact ID"]="Contact ID";

// File: /registrar-platform/www/application/views/default/account_html_view_contacts.tpl
$STRINGS[$LANG]["Name"]="Name";

// File: /registrar-platform/www/application/views/default/account_html_view_contacts.tpl
$STRINGS[$LANG]["Company"]="Cwmni";

// File: /registrar-platform/www/application/views/default/account_html_view_contacts.tpl
$STRINGS[$LANG]["Contact Verified?"]="Contact Verified?";

// File: /registrar-platform/www/application/views/default/account_html_view_contacts.tpl
$STRINGS[$LANG]["Verified"]="Verified";

// File: /registrar-platform/www/application/views/default/account_html_view_contacts.tpl
$STRINGS[$LANG]["Verify"]="Verify";

// File: /registrar-platform/www/application/views/default/account_html_view_contacts.tpl
$STRINGS[$LANG]["Edit"]="Golygu";

// File: /registrar-platform/www/application/views/default/account_html_view_contacts.tpl
$STRINGS[$LANG]["There are no contacts associated with your account."]="There are no contacts associated with your account.";

// File: /registrar-platform/www/application/views/default/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["TLD Registrar Solutions does not guarantee the renewal of any domain. It is your responsibility to ensure that you arrange payment for renewal prior to the expiry date of your domain."]="TLD Registrar Solutions does not guarantee the renewal of any domain. It is your responsibility to ensure that you arrange payment for renewal prior to the expiry date of your domain.";

// File: /registrar-platform/www/application/views/default/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["Before Expiry"]="Before Expiry";

// File: /registrar-platform/www/application/views/default/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["TLD Registrar Solutions will take issue reminder notices to the Registrant and Account Holder email addresses on record at the following times:"]="TLD Registrar Solutions will take issue reminder notices to the Registrant and Account Holder email addresses on record at the following times:";

// File: /registrar-platform/www/application/views/default/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["[day] days prior to the domain expiry date"]="[day] days prior to the domain expiry date";

// File: /registrar-platform/www/application/views/default/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["After Expiry"]="After Expiry";

// File: /registrar-platform/www/application/views/default/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["TLD Registrar Solutions will take the following action if you have not made payment of your domain renewal:"]="TLD Registrar Solutions will take the following action if you have not made payment of your domain renewal:";

// File: /registrar-platform/www/application/views/default/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["1 day after the domain expiry date we will suspend your domain and your website will cease functioning."]="1 day after the domain expiry date we will suspend your domain and your website will cease functioning.";

// File: /registrar-platform/www/application/views/default/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]["Between 74 and 77 days after the domain expiry date your domain will be deleted and will no longer appear in your account. The domain will be released to the public and available for re-registration."]="Between 74 and 77 days after the domain expiry date your domain will be deleted and will no longer appear in your account. The domain will be released to the public and available for re-registration.";

// File: /registrar-platform/www/application/views/default/inc_cart_search_multiple.tpl
$STRINGS[$LANG]["To check the availability of multiple domains, enter each name on a new line in the box below."]="Er mwyn chwilio am argaeledd parthau lluosog, rhowch eich en war linell newydd yn y blwch isod.";

// File: /registrar-platform/www/application/views/default/inc_cart_search_multiple.tpl
$STRINGS[$LANG]["Domain Search"]="Chwilio parth";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Settings for [domain]"]="Settings for [domain]";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["General Information"]="General Information";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["This domain name is locked."]="This domain name is locked.";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Registered on:"]="Registered on:";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Expires on:"]="Expires on:";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Restore Now"]="Restore Now";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Renew"]="Renew";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Private Whois:"]="Private Whois:";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Your contact details are private until [expiry]. <a href='/account/private_registration/[domain]'>Edit Privacy Settings</a>"]="Your contact details are private until [expiry]. <a href='/account/private_registration/[domain]'>Edit Privacy Settings</a>";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Your contact details are public. <a href='/account/private_registration/[domain]'>Click here to make them private.</a>"]="Your contact details are public. <a href='/account/private_registration/[domain]'>Click here to make them private.</a>";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Auth/Transfer code:"]="Auth/Transfer code:";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["This domain cannot be transferred"]="This domain cannot be transferred";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Generate Code"]="Generate Code";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Your contact details must be visible in the whois record before a transfer can be initiated. Any unused term will be lost upon transfer to another registrar."]="Your contact details must be visible in the whois record before a transfer can be initiated. Any unused term will be lost upon transfer to another registrar.";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Nameserver Configuration"]="Nameserver Configuration";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Website Builder Settings"]="Website Builder Settings";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Website Builder account is locked."]="Website Builder account is locked.";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["You are currently on the <strong>'[sitebuilder_plan]'</strong> website builder package, which expires on [sitebuilder_expires]"]="You are currently on the <strong>'[sitebuilder_plan]'</strong> website builder package, which expires on [sitebuilder_expires]";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["You are currently on the <strong>'[sitebuilder_plan]'</strong> website builder package"]="You are currently on the <strong>'[sitebuilder_plan]'</strong> website builder package";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Check out our FAQ's section on 'How to use Website Builder'"]="Check out our FAQ's section on 'How to use Website Builder'";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Upgrade Package"]="Upgrade Package";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Edit Your Website"]="Edit Your Website";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Web Hosting Settings"]="Web Hosting Settings";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Unfortunately we could not retrieve the usage information for your account at this time. Please try again later."]="Unfortunately we could not retrieve the usage information for your account at this time. Please try again later.";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Advanced DNS Manager has been deactivated. Your hosting account is no longer configured for this domain. <a href='/account/advanced_dns/[domain]'>Click here to re-activate your hosting account settings</a>."]="Advanced DNS Manager has been deactivated. Your hosting account is no longer configured for this domain. <a href='/account/advanced_dns/[domain]'>Click here to re-activate your hosting account settings</a>.";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Expiry Date"]="Dyddiad dod i ben";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Storage Used"]="Storage Used";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Bandwidth Used"]="Bandwidth Used";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Mailboxes"]="Mailboxes";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Manage"]="Manage";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["View hosting terms and conditions"]="View hosting terms and conditions";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Learn more about setting up your hosting"]="Learn more about setting up your hosting";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["For requests regarding any of the following please email <a href='mailto:[support]'>[support]</a>:"]="For requests regarding any of the following please email <a href='mailto:[support]'>[support]</a>:";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Changing your overage policy to charge on a per GB basis"]="Changing your overage policy to charge on a per GB basis";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Setting up a database"]="Setting up a database";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Enabling PHP, PHPSafe, SSI, CGI, FCGI, Phyton, ASP, WebStats and ErrorDocs"]="Enabling PHP, PHPSafe, SSI, CGI, FCGI, Phyton, ASP, WebStats and ErrorDocs";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Contact Associations"]="Contact Associations";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Below are your contact(s) associated with your domain.<br /><span class='bold'>To update domain contacts, they must be public. To do this disable private whois above.</span>"]="Below are your contact(s) associated with your domain.<br /><span class='bold'>To update domain contacts, they must be public. To do this disable private whois above.</span>";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Below are your contact(s) associated with your domain.<br />To change contact(s), tick the appropiate checkboxes and click 'Change Contacts'."]="Below are your contact(s) associated with your domain.<br />To change contact(s), tick the appropiate checkboxes and click 'Change Contacts'.";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Transaction ID"]="Transaction ID";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["View Payment"]="View Payment";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Domain Deletion"]="Domain Deletion";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["If you no longer require your domain you can schedule it for deletion. Once deleted, the status will change to 'Pending Deletion' and, should you wish to restore it, you will be charged a [restore_fee] fee. If the domain is within 5 days of registration it will be fully deleted, and you will not be able to restore it. No refund will be given for the remainder of the registration."]="If you no longer require your domain you can schedule it for deletion. Once deleted, the status will change to 'Pending Deletion' and, should you wish to restore it, you will be charged a [restore_fee] fee. If the domain is within 5 days of registration it will be fully deleted, and you will not be able to restore it. No refund will be given for the remainder of the registration.";

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]["Delete Domain"]="Delete Domain";

// File: /registrar-platform/www/application/views/default/account_html_pending_transfers.tpl
$STRINGS[$LANG]["Pending Transfers In"]="Pending Transfers In";

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]["Manage Email Accounts"]="Manage Email Accounts";

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]["How to Access your Email"]="How to Access your Email";

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]["Access your email through this link: <a href='http://webmail.[domain]'>http://webmail.[domain]</a>."]="Access your email through this link: <a href='http://webmail.[domain]'>http://webmail.[domain]</a>.";

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]["Login with the email address you want to use and type in the password used when setting up the address. If you have forgotten the password you can reset it below."]="Login with the email address you want to use and type in the password used when setting up the address. If you have forgotten the password you can reset it below.";

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]["To view the DNS records for setting up your email accounts in another client please <a href='/account/advanced_dns/[domain]#records'>click here</a>."]="To view the DNS records for setting up your email accounts in another client please <a href='/account/advanced_dns/[domain]#records'>click here</a>.";

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]["Only letters and numbers are allowed and it must contain at least one number and one upper case character."]="Only letters and numbers are allowed and it must contain at least one number and one upper case character.";

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]["Display Name ('From:'):"]="Display Name ('From:'):";

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]["When an administrator logs on to Webmail and goes to the admin page, <br/>he or she can see all mailboxes for this domain name in this Web hosting account,<br/> can reset passwords for any or all mailboxes, and can delete any or all mailboxes"]="When an administrator logs on to Webmail and goes to the admin page, <br/>he or she can see all mailboxes for this domain name in this Web hosting account,<br/> can reset passwords for any or all mailboxes, and can delete any or all mailboxes";

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]["Create Email Account"]="Create Email Account";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Hello, [user_name]"]="Helo, [user_name]";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Your Domain Launch Phase Requests"]="Your Domain Launch Phase Requests";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["All domain requests will be processed at the end of the phase. You will be notified once the domain has been registered."]="All domain requests will be processed at the end of the phase. You will be notified once the domain has been registered.";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Created"]="Created";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Phase"]="Phase";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Your Domain Names"]="Eich Enwau Parth";

// File: /registrar-platform/www/application/views/default/account_index.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Edit Nameservers"]="Edit Nameservers";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["This domain is locked"]="This domain is locked";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["This domain is scheduled for deletion"]="This domain is scheduled for deletion";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["<b>Personal Info</b>: Private"]="<b>Personal Info</b>: Private";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["<b>Personal Info</b>: Public"]="<b>Personal Info</b>: Public";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Expired on [expiry]"]="Expired on [expiry]";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Manage Google Apps"]="Manage Google Apps";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Setup in progress"]="Setup in progress";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Verification Failed"]="Verification Failed";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Order Google Apps"]="Order Google Apps";

// File: /registrar-platform/www/application/views/default/account_index.tpl
// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["If your domain has not been renewed within 45 days of the expiry date it will be deactivated, and you will be charged a [fee] fee to restore it.<br /><br />After a further 30 days the domain will be fully deleted and made available for registration."]="If your domain has not been renewed within 45 days of the expiry date it will be deactivated, and you will be charged a [fee] fee to restore it.<br /><br />After a further 30 days the domain will be fully deleted and made available for registration.";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Restore"]="Restore";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["You have no domains registered, please <a href='/'>click here</a> to register a domain"]="You have no domains registered, please <a href='/'>click here</a> to register a domain";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Add-on Services Overview"]="Add-on Services Overview";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Below is a list of services that you have purchased other than domain names."]="Below is a list of services that you have purchased other than domain names.";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Service"]="Service";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Expires"]="Expires";

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]["Edit Settings"]="Edit Settings";

// File: /registrar-platform/www/application/views/default/inc_google_view_user.tpl
$STRINGS[$LANG]["Google App Panel"]="Google App Panel";

// File: /registrar-platform/www/application/views/default/e_create.tpl
$STRINGS[$LANG]["Account Sign up"]="Cofrestru Cyfrif";

// File: /registrar-platform/www/application/views/default/account_html_view_mailboxes.tpl
$STRINGS[$LANG]["[used] of [quota] email accounts are currently being used."]="[used] of [quota] email accounts are currently being used.";

// File: /registrar-platform/www/application/views/default/account_html_view_mailboxes.tpl
$STRINGS[$LANG]["New Password"]="New Password";

// File: /registrar-platform/www/application/views/default/account_html_view_mailboxes.tpl
$STRINGS[$LANG]["Administrator"]="Administrator";

// File: /registrar-platform/www/application/views/default/account_html_view_mailboxes.tpl
$STRINGS[$LANG]["Use the 'Update' button to update the password or administrator for that record."]="Use the 'Update' button to update the password or administrator for that record.";

// File: /registrar-platform/www/application/helpers/common_helper.php
$STRINGS[$LANG]["%s - Please confirm your contact information."]="%s - Please confirm your contact information.";

// File: /registrar-platform/www/application/helpers/common_helper.php
$STRINGS[$LANG]["Phone number is invalid"]="Phone number is invalid";

// File: /registrar-platform/www/application/helpers/common_helper.php
$STRINGS[$LANG]["Fax number is invalid"]="Fax number is invalid";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["Thank you for signing up to our newsletter."]="Thank you for signing up to our newsletter.";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["Your password has been emailed to {$user->email}"]="Your password has been emailed to {$user->email}";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["Password reset"]="Ail-Osod Cyfrinair";

// File: /registrar-platform/www/application/controllers/e.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Please verify your password"]="Please verify your password";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["Your phone number is invalid"]="Your phone number is invalid";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["You fax number is invalid"]="You fax number is invalid";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["New Account Signup"]="New Account Signup";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["User account for %s has been successfully verified."]="User account for %s has been successfully verified.";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["User account for %s could not be verified (used out-dated link)"]="User account for %s could not be verified (used out-dated link)";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["User account for %s has already been verified."]="User account for %s has already been verified.";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["Contact %s has been successfully verified."]="Contact %s has been successfully verified.";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["Contact %s could not be verified (used out-dated link)."]="Contact %s could not be verified (used out-dated link).";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["Contact %s has already been verified."]="Contact %s has already been verified.";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["Error finding Contact in user account."]="Error finding Contact in user account.";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["Verification link appears to be broken. Please make sure you copy/paste the full link from your email"]="Verification link appears to be broken. Please make sure you copy/paste the full link from your email";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["Please enter a domain name"]="Please enter a domain name";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["Please enter a valid TLD extension"]="Please enter a valid TLD extension";

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]["e_"]="e_";

// File: /registrar-platform/www/application/controllers/support.php
$STRINGS[$LANG]["sitebuilder/support_"]="sitebuilder/support_";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Please limit the amount of search terms to 10."]="Please limit the amount of search terms to 10.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Please enter at least one search term."]="Please enter at least one search term.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Domains can only contain alphanumerics and hyphens."]="Domains can only contain alphanumerics and hyphens.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Domains must be at least %s characters long."]="Domains must be at least %s characters long.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Domains must be less than %s characters long."]="Domains must be less than %s characters long.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Domains must not start or end with a hyphen"]="Domains must not start or end with a hyphen";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Invalid IDN Domain Name"]="Invalid IDN Domain Name";

// File: /registrar-platform/www/application/controllers/cart.php
// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Domain could not be added to cart at this time: %s"]="Domain could not be added to cart at this time: %s";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Please choose a registration phase."]="Please choose a registration phase.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Domain could not be added to cart at this time: Item is already in cart"]="Domain could not be added to cart at this time: Item is already in cart";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Error uploading file. Please check that the file contains data."]="Error uploading file. Please check that the file contains data.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["The upload file doesn't appear to be a valid SMD file. Please try again."]="The upload file doesn't appear to be a valid SMD file. Please try again.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["The upload file is too large. Please try another file."]="The upload file is too large. Please try another file.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["The uploaded SMD file is not valid for the domain %s. You can only register domains that are present in your SMD file."]="The uploaded SMD file is not valid for the domain %s. You can only register domains that are present in your SMD file.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["The uploaded SMD file isn't valid until after %s. Please try again later."]="The uploaded SMD file isn't valid until after %s. Please try again later.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["The uploaded SMD file has expired. You will need to obtain a new file from the relevent provider."]="The uploaded SMD file has expired. You will need to obtain a new file from the relevent provider.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["The uploaded SMD file contains invalid data, please check that the file has not been corrupted."]="The uploaded SMD file contains invalid data, please check that the file has not been corrupted.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["%s has successfully been uploaded and verified."]="%s has successfully been uploaded and verified.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Region specific contact %s created with %s during checkout"]="Region specific contact %s created with %s during checkout";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Declined"]="Declined";

// File: /registrar-platform/www/application/controllers/cart.php
// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Could not update period for %s"]="Could not update period for %s";

// File: /registrar-platform/www/application/controllers/cart.php
// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Period updated for %s"]="Period updated for %s";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Discount code '{$_POST['code']}' has successfully been applied."]="Discount code '{$_POST['code']}' has successfully been applied.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Sorry, this is promotion code is invalid or unavailable at this time."]="Sorry, this is promotion code is invalid or unavailable at this time.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Sorry, you have entered an invalid VAT Number, please try again."]="Sorry, you have entered an invalid VAT Number, please try again.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Please enter your VAT Number and try again."]="Please enter your VAT Number and try again.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Please enter an Organization name to qualify for VAT deduction"]="Please enter an Organization name to qualify for VAT deduction";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Your VAT number has successfully been applied to your account."]="Your VAT number has successfully been applied to your account.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Token parameter doesn't match or PayerID missing, Paypal payment could not be processed"]="Token parameter doesn't match or PayerID missing, Paypal payment could not be processed";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Paypal payment could not be processed. Please use an alernative payment method or try again later."]="Paypal payment could not be processed. Please use an alernative payment method or try again later.";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["There was an error contacting Paypal. Please use an alernative payment method or try again later."]="There was an error contacting Paypal. Please use an alernative payment method or try again later.";

// File: /registrar-platform/www/application/controllers/cart.php
// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["You must read and accept the Terms and Conditions"]="You must read and accept the Terms and Conditions";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["$field cannot be empty"]="$field cannot be empty";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["%s is invalid, please check and try again"]="%s is invalid, please check and try again";

// File: /registrar-platform/www/application/controllers/cart.php
// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Transaction could not be processed"]="Transaction could not be processed";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Paypal Account of %s %s (%s)"]="Paypal Account of %s %s (%s)";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["User #%d (%s) successfully completed the checkout process. %d items have been added to the queue for processing"]="User #%d (%s) successfully completed the checkout process. %d items have been added to the queue for processing";

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]["Hosting could not be added to one or more of your domains at this time"]="Hosting could not be added to one or more of your domains at this time";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Web Hosting ("]="Web Hosting (";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["No matching domain for $dom"]="No matching domain for $dom";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Permission denied"]="Permission denied";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["No Google Apps Account for $dom"]="No Google Apps Account for $dom";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["You've successfully created a user. Please click on the 'Make Admin' button to set this user as admin"]="You've successfully created a user. Please click on the 'Make Admin' button to set this user as admin";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Deletion request for %s"]="Deletion request for %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["No domains in your account"]="No domains in your account";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Additional services are currently unavailable, please try again later."]="Additional services are currently unavailable, please try again later.";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Unable to find domain name: %s. The support team have been notified."]="Unable to find domain name: %s. The support team have been notified.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["%s already has an hosting plan."]="%s already has an hosting plan.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["%s already has a website builder package. Purchasing a hosting plan will de-activate the website builder package."]="%s already has a website builder package. Purchasing a hosting plan will de-activate the website builder package.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["A website builder package for %s is already in your cart. Please remove it first."]="A website builder package for %s is already in your cart. Please remove it first.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Hosting plan for %s is already in your cart. <a href='/cart/view_shopping_cart'>View your cart</a>"]="Hosting plan for %s is already in your cart. <a href='/cart/view_shopping_cart'>View your cart</a>";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error adding '%s' for %s to cart: %s"]="Error adding '%s' for %s to cart: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Hosting plan (%s) for %s has been added to your cart. <a href='/cart/view_shopping_cart'>View your cart</a>"]="Hosting plan (%s) for %s has been added to your cart. <a href='/cart/view_shopping_cart'>View your cart</a>";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["A hosting plan for %s is already in your cart. Please remove it first."]="A hosting plan for %s is already in your cart. Please remove it first.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["%s already has a website builder package"]="%s already has a website builder package";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["%s already has a hosting plan. Purchasing a website builder package will de-activate the hosting plan."]="%s already has a hosting plan. Purchasing a website builder package will de-activate the hosting plan.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Website builder package for {$_POST['domain']} is already in your cart. <a href='/cart/view_shopping_cart'>View your cart</a>"]="Website builder package for {$_POST['domain']} is already in your cart. <a href='/cart/view_shopping_cart'>View your cart</a>";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error adding website builder package: %s"]="Error adding website builder package: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Website builder (%s) for %s has been added to your cart. <a href='/cart/view_shopping_cart'>View your cart</a>"]="Website builder (%s) for %s has been added to your cart. <a href='/cart/view_shopping_cart'>View your cart</a>";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error setting up your new Website builder package: %s"]="Error setting up your new Website builder package: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully added Website Builder (FREE) package to your account."]="Successfully added Website Builder (FREE) package to your account.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Please choose a domain name."]="Please choose a domain name.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["%s doesnt have a Website Builder package to upgrade."]="%s doesnt have a Website Builder package to upgrade.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Website Builder package for {$_POST['domain']} is already in your cart"]="Website Builder package for {$_POST['domain']} is already in your cart";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Website Builder (%s) has been added to cart. <a href='/cart/view_shopping_cart'>View your cart</a>"]="Website Builder (%s) has been added to cart. <a href='/cart/view_shopping_cart'>View your cart</a>";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Website Builder Renewal for %s is already in your cart"]="Website Builder Renewal for %s is already in your cart";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["%s is already registered, plan:  %s"]="%s is already registered, plan:  %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["There's no Google Apps provisioned for the domain (%s)"]="There's no Google Apps provisioned for the domain (%s)";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG][",<br />"]=",<br />";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Refunded on %s"]="Refunded on %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Cancelled"]="Cancelled";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Paid"]="Paid";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["This domain has 'Local Presence' service enabled. Please email us at %s or <a href='' id='lp_update'>update</a> the administrative contact for the domain to disable the service."]="This domain has 'Local Presence' service enabled. Please email us at %s or <a href='' id='lp_update'>update</a> the administrative contact for the domain to disable the service.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Transfer code request for %s"]="Transfer code request for %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Auth/Transfer code has been emailed to {$email}"]="Auth/Transfer code has been emailed to {$email}";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Auth/Transfer code has been emailed to %s"]="Auth/Transfer code has been emailed to %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error renewing domain %s: %s"]="Error renewing domain %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Domain %s can only be renewed %d days after registration date."]="Domain %s can only be renewed %d days after registration date.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error renewing hosting for %s. Domain is locked."]="Error renewing hosting for %s. Domain is locked.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error renewing hosting for %s. Hosting is locked."]="Error renewing hosting for %s. Hosting is locked.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Hosting for %s can only be renewed up to %d days before expiry."]="Hosting for %s can only be renewed up to %d days before expiry.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error renewing website builder for %s. Domain is locked."]="Error renewing website builder for %s. Domain is locked.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Website builder for %s can only be renewed %d months before expiry."]="Website builder for %s can only be renewed %d months before expiry.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error adding %s item %s to cart: %s."]="Error adding %s item %s to cart: %s.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["%s renewal for %s added to cart."]="%s renewal for %s added to cart.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["%s renewal for %s is already in the cart."]="%s renewal for %s is already in the cart.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Domain item %s could not be added to cart: %s"]="Domain item %s could not be added to cart: %s";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Errors may have occured while adding domain(s) to shopping cart"]="Errors may have occured while adding domain(s) to shopping cart";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Domain renewal for %s added to cart. <a href='/cart/view_shopping_cart'>View shopping cart</a>."]="Domain renewal for %s added to cart. <a href='/cart/view_shopping_cart'>View shopping cart</a>.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Domain %s cannot be renewed at this time."]="Domain %s cannot be renewed at this time.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error disabling private registration for %s: %s"]="Error disabling private registration for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error disabling private registration for domain %s for User #%s (%s): %s"]="Error disabling private registration for domain %s for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Unable to disable private registration for %s: %s"]="Unable to disable private registration for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Private registration for %s has been successfully disabled"]="Private registration for %s has been successfully disabled";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Private Registration for %s has been added to cart. <a href='/cart/view_shopping_cart'>View your cart</a>"]="Private Registration for %s has been added to cart. <a href='/cart/view_shopping_cart'>View your cart</a>";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["A transfer is pending for this domain. Your contact details must be visible in the whois record during the transfer process."]="A transfer is pending for this domain. Your contact details must be visible in the whois record during the transfer process.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error adding Private Registration to cart. This domain is locked."]="Error adding Private Registration to cart. This domain is locked.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["You cannot add private registration to %s which expires within %d days.<br /> Please add the domain renewal to the cart first."]="You cannot add private registration to %s which expires within %d days.<br /> Please add the domain renewal to the cart first.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["An error has occured while adding private registration for %s to cart."]="An error has occured while adding private registration for %s to cart.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["An error has occured while adding private registration for %s to cart for User #%s (%s): %s"]="An error has occured while adding private registration for %s to cart for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Private Registration for %s has already been added to cart."]="Private Registration for %s has already been added to cart.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error re-enabling private registration for %s: %s"]="Error re-enabling private registration for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Private registration for %s could not be re-enabled: %s"]="Private registration for %s could not be re-enabled: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Private registration for %s could not be re-enabled for User #%s (%s): %s"]="Private registration for %s could not be re-enabled for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Private registration for %s has been successfully re-enabled"]="Private registration for %s has been successfully re-enabled";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Verification email sent to %s"]="Verification email sent to %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Contact details updated. Verification email sent to %s"]="Contact details updated. Verification email sent to %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Contact details updated successfully."]="Contact details updated successfully.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["No changes have been made to your account details."]="No changes have been made to your account details.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Password must be at least 8 characters long"]="Password must be at least 8 characters long";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Passwords don't match"]="Passwords don't match";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Your account details are unverified. Please click the link in the verification email we have already sent you to verify this account."]="Your account details are unverified. Please click the link in the verification email we have already sent you to verify this account.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Your account is verified. You will need to re-verify your account if you make any changes to the details below."]="Your account is verified. You will need to re-verify your account if you make any changes to the details below.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Domain not found"]="Domain not found";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Could not update contact at this time"]="Could not update contact at this time";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Could not create new contact at this time, please try again later"]="Could not create new contact at this time, please try again later";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Verification link sent to %s for Contact %s"]="Verification link sent to %s for Contact %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Contacts have been successfully updated. Verification link sent to %s"]="Contacts have been successfully updated. Verification link sent to %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error getting contact information from registry"]="Error getting contact information from registry";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Verification link sent to %s for Contact (%s)."]="Verification link sent to %s for Contact (%s).";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Verification link for Contact (%s) sent to %s."]="Verification link for Contact (%s) sent to %s.";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["This contact is locked"]="This contact is locked";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Update to contact %s has been logged and will be visible in the WHOIS shortly"]="Update to contact %s has been logged and will be visible in the WHOIS shortly";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Please update contact details for %s (user %s) at registry.\nError(s): %s"]="Please update contact details for %s (user %s) at registry.\nError(s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Contact %s has been updated successfully"]="Contact %s has been updated successfully";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Verification link sent to %s for Contact (%s)"]="Verification link sent to %s for Contact (%s)";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Contact %s has been successfully updated. Verification link sent to %s"]="Contact %s has been successfully updated. Verification link sent to %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Contact %s has been successfully updated"]="Contact %s has been successfully updated";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Could not delete contact at this time"]="Could not delete contact at this time";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Contact {$contact->reference} deleted"]="Contact {$contact->reference} deleted";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Contact could not be deleted at this time"]="Contact could not be deleted at this time";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Please enter the domain name"]="Please enter the domain name";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Please enter the transfer code"]="Please enter the transfer code";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Domain name is not supported"]="Domain name is not supported";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Incorrect transfer code"]="Incorrect transfer code";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Domain cannot be transferred in"]="Domain cannot be transferred in";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Domain is already in your account"]="Domain is already in your account";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Domain cannot be transferred"]="Domain cannot be transferred";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["A transfer has already been initiated for this domain"]="A transfer has already been initiated for this domain";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Admin Contact"]="Admin Contact";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error getting admin contact information"]="Error getting admin contact information";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Admin email address is not valid. Please update it to initiate the transfer."]="Admin email address is not valid. Please update it to initiate the transfer.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Transfer request received for %s"]="Transfer request received for %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Transfer request email sent to %s"]="Transfer request email sent to %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Broken link! Please check the URL in your email and try again - it may have been wrapped onto another line"]="Broken link! Please check the URL in your email and try again - it may have been wrapped onto another line";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Transfer not found"]="Transfer not found";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["This link has expired. Please login to initiate a transfer."]="This link has expired. Please login to initiate a transfer.";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["the current registrar"]="the current registrar";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Please enter your name"]="Please enter your name";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error initiating the transfer: %s"]="Error initiating the transfer: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Could not find transfer"]="Could not find transfer";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Transfer could not be cancelled at this time: %s"]="Transfer could not be cancelled at this time: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Transfer cancelled for %s"]="Transfer cancelled for %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error approving transfer for %s for User #%s (%s): %s"]="Error approving transfer for %s for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error rejecting transfer for %s: %s"]="Error rejecting transfer for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Contacts updated for transferred domain '%s'"]="Contacts updated for transferred domain '%s'";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Transfer of %s approved by %s"]="Transfer of %s approved by %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error rejecting transfer for %s for User #%s (%s): %s"]="Error rejecting transfer for %s for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Transfer rejected for %s"]="Transfer rejected for %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Nameserver hostname '{$host->name}' is not valid."]="Nameserver hostname '{$host->name}' is not valid.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Nameserver '{$host->name}' must be created manually <a href='/account/nameservers'>here</a> as it requires an IP address"]="Nameserver '{$host->name}' must be created manually <a href='/account/nameservers'>here</a> as it requires an IP address";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error occured while cancelling advanced DNS for %s: %s"]="Error occured while cancelling advanced DNS for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error cancelling Advanced DNS for %s: %s"]="Error cancelling Advanced DNS for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error cancelling Advanced DNS for %s for User #%s (%s): %s"]="Error cancelling Advanced DNS for %s for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully cancelled Advanced DNS for %s"]="Successfully cancelled Advanced DNS for %s";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error removing nameservers for %s: %s"]="Error removing nameservers for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error removing nameservers for %s for User #%s (%s): %s"]="Error removing nameservers for %s for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error adding new nameservers for %s: %s"]="Error adding new nameservers for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error adding new nameservers for %s for User #%s (%s): %s"]="Error adding new nameservers for %s for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully added new nameservers for %s"]="Successfully added new nameservers for %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Nameserver could not be deleted"]="Nameserver could not be deleted";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error deleting nameserver %s: %s"]="Error deleting nameserver %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully deleted nameserver %s: %s"]="Successfully deleted nameserver %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Nameserver could not be updated at this time"]="Nameserver could not be updated at this time";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error retrieving nameserver info for %s for User #%s (%s): %s"]="Error retrieving nameserver info for %s for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Nameserver could not be updated at this time: Domain is locked."]="Nameserver could not be updated at this time: Domain is locked.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Nameserver could not be updated, please try again later"]="Nameserver could not be updated, please try again later";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error updating nameserver %s: %s"]="Error updating nameserver %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Please update the following nameserver:\n {$host->name}: {$ipv4}"]="Please update the following nameserver:\n {$host->name}: {$ipv4}";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Nameserver update for %s has been successfully requested for manual processing"]="Nameserver update for %s has been successfully requested for manual processing";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Domain is not registered with us"]="Domain is not registered with us";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Server Name is missing or empty"]="Server Name is missing or empty";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Server Name is invalid: it can only contain alphanumerics (A-Z, 0-9), hyphens and periods"]="Server Name is invalid: it can only contain alphanumerics (A-Z, 0-9), hyphens and periods";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["IP address is missing or invalid"]="IP address is missing or invalid";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["IP address is invalid"]="IP address is invalid";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Server already registered. To update the IP click on it in the list below."]="Server already registered. To update the IP click on it in the list below.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error creating new nameserver. Domain is locked."]="Error creating new nameserver. Domain is locked.";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Server could not be created, please try again later."]="Server could not be created, please try again later.";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error creating new nameserver %s: %s"]="Error creating new nameserver %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Please register the following nameserver:\n {$host->name}: {$host->v4}"]="Please register the following nameserver:\n {$host->name}: {$host->v4}";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error sending email for manual registration of %s: %s"]="Error sending email for manual registration of %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error sending email for manual registration of %s to %s for User #%s (%s): %s"]="Error sending email for manual registration of %s to %s for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully sent email for manual registration of %s to %s"]="Successfully sent email for manual registration of %s to %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully created new nameserver %s"]="Successfully created new nameserver %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["DNS setting information not available"]="DNS setting information not available";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error removing nameserver %s: %s"]="Error removing nameserver %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error removing nameserver %s for User #%s (%s): %s"]="Error removing nameserver %s for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully removed nameserver %s"]="Successfully removed nameserver %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Hostname '{$host->name}' is not valid"]="Hostname '{$host->name}' is not valid";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Hostname '{$host->name}' must be created manually <a href='/account/nameservers'>here</a> as it requires an IP address"]="Hostname '{$host->name}' must be created manually <a href='/account/nameservers'>here</a> as it requires an IP address";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully set nameserver %s for %s"]="Successfully set nameserver %s for %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["%s is no longer using Advanced DNS Manager. Any website builder, hosting or web forwarding will no longer work."]="%s is no longer using Advanced DNS Manager. Any website builder, hosting or web forwarding will no longer work.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error removing %s from Advanced DNS: %s"]="Error removing %s from Advanced DNS: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error removing %s from Advanced DNS for User #%s (%s): %s"]="Error removing %s from Advanced DNS for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error removing nameserver %s as part of MDNS deactivation: %s"]="Error removing nameserver %s as part of MDNS deactivation: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully removed nameserver %s as part of MDNS deactivation"]="Successfully removed nameserver %s as part of MDNS deactivation";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully removed %s from Advanced DNS"]="Successfully removed %s from Advanced DNS";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["%s is no longer using Advanced DNS Manager. Some nameservers remain which you may wish to remove."]="%s is no longer using Advanced DNS Manager. Some nameservers remain which you may wish to remove.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["%s is now using Advanced DNS Manager. You will be redirected in a few seconds."]="%s is now using Advanced DNS Manager. You will be redirected in a few seconds.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error removing exisiting nameservers: %s"]="Error removing exisiting nameservers: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error removing nameservers (while activating mdns) for %s: %s"]="Error removing nameservers (while activating mdns) for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error removing nameservers (while activating mdns) for %s for User #%s (%s): %s"]="Error removing nameservers (while activating mdns) for %s for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error activating Advanced DNS: %s"]="Error activating Advanced DNS: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error activating %s on Advanced DNS: %s"]="Error activating %s on Advanced DNS: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully activated %s on Advanced DNS"]="Successfully activated %s on Advanced DNS";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error getting hosting records information to add to Advanced DNS: %s"]="Error getting hosting records information to add to Advanced DNS: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error adding %s hosting record on Advanced DNS: %s"]="Error adding %s hosting record on Advanced DNS: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully added %s hosting record on Advanced DNS"]="Successfully added %s hosting record on Advanced DNS";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Forwarding has been successfully setup. You can now set the URL to forward to."]="Forwarding has been successfully setup. You can now set the URL to forward to.";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error removing conflicting '{$type}' record for {$domain->name}"]="Error removing conflicting '{$type}' record for {$domain->name}";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Dropped conflicting CNAME record for %s via Advanced DNS"]="Dropped conflicting CNAME record for %s via Advanced DNS";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error when setting up forwarding for %s: %s"]="Error when setting up forwarding for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Domain %s set up for forwarding via DNS Manager"]="Domain %s set up for forwarding via DNS Manager";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error setting up forwarding for %s: %s"]="Error setting up forwarding for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully changed %s destination for %s to '%s'"]="Successfully changed %s destination for %s to '%s'";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully changed %s destination to %s"]="Successfully changed %s destination to %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["The record has been added successfully."]="The record has been added successfully.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["This domain is not on managed DNS"]="This domain is not on managed DNS";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["The record's value field must be filled"]="The record's value field must be filled";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Invalid host name"]="Invalid host name";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Value is too long"]="Value is too long";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Cannot add record - a CNAME for '%s.%s' already exists, remove it first."]="Cannot add record - a CNAME for '%s.%s' already exists, remove it first.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Invalid IPv4 address"]="Invalid IPv4 address";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Invalid IPv6 address"]="Invalid IPv6 address";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Invalid format for CNAME record"]="Invalid format for CNAME record";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["CNAME records cannot coexist with other records for the same host name"]="CNAME records cannot coexist with other records for the same host name";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Invalid mail server name"]="Invalid mail server name";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Invalid MX record priority"]="Invalid MX record priority";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Invalid record type"]="Invalid record type";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error when adding record: %s"]="Error when adding record: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error adding %s record (value %s) for %s.%s via Advanced DNS: %s"]="Error adding %s record (value %s) for %s.%s via Advanced DNS: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Added a %s record (value %s) for %s.%s via the Advanced DNS manager"]="Added a %s record (value %s) for %s.%s via the Advanced DNS manager";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error when getting records for %s: %s"]="Error when getting records for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error when getting records for %s for User #%s (%s): %s"]="Error when getting records for %s for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error when deleting record for %s: %s"]="Error when deleting record for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error deleting %s record (value %s) for %s from %s via Advanced DNS: %s"]="Error deleting %s record (value %s) for %s from %s via Advanced DNS: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully removed %s record (value %s) for %s from %s via the Advanced DNS manager"]="Successfully removed %s record (value %s) for %s from %s via the Advanced DNS manager";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["The record has been deleted successfully."]="The record has been deleted successfully.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["The records have been updated successfully."]="The records have been updated successfully.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error when getting records for domain: %s"]="Error when getting records for domain: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Value for %s record is too long"]="Value for %s record is too long";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Invalid IPv4 address for %s record"]="Invalid IPv4 address for %s record";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Invalid IPv6 address for %s record"]="Invalid IPv6 address for %s record";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Invalid format for CNAME record for %s"]="Invalid format for CNAME record for %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Invalid format for MX record for %s"]="Invalid format for MX record for %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Invalid MX priority for %s"]="Invalid MX priority for %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error updating record: %s"]="Error updating record: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error updating %s record (value %s) for %s: %s"]="Error updating %s record (value %s) for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Cannot find domain name."]="Cannot find domain name.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Email account for {$_POST['pop_user']} has been deleted successfully."]="Email account for {$_POST['pop_user']} has been deleted successfully.";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Hosting for {$domain->name} is locked."]="Hosting for {$domain->name} is locked.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error deleting mailbox for %s for %s: %s"]="Error deleting mailbox for %s for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully deleted mailbox for %s for %s"]="Successfully deleted mailbox for %s for %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Email account for {$_POST['email']} has been created successfully."]="Email account for {$_POST['email']} has been created successfully.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error creating mailbox: %s"]="Error creating mailbox: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error creating mailbox for %s: %s"]="Error creating mailbox for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error creating mailbox for %s for User #%s (%s): %s"]="Error creating mailbox for %s for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["The MX, POP, SMTP or WebMail value was not set."]="The MX, POP, SMTP or WebMail value was not set.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error(s) have occured while setting up your DNS servers for your mailbox."]="Error(s) have occured while setting up your DNS servers for your mailbox.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["%s record could not added. Record may already exist."]="%s record could not added. Record may already exist.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error adding %s record (value %s) for %s: %s"]="Error adding %s record (value %s) for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error adding %s record (value %s) for %s for User #%s (%s): %s"]="Error adding %s record (value %s) for %s for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully added %s record (value %s) for %s"]="Successfully added %s record (value %s) for %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Email account for {$_POST['pop_user']} has been updated successfully."]="Email account for {$_POST['pop_user']} has been updated successfully.";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error updating password for mailbox user %s for %s: %s"]="Error updating password for mailbox user %s for %s: %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error updating password for mailbox user %s for %s for User #%s (%s): %s"]="Error updating password for mailbox user %s for %s for User #%s (%s): %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Successfully updated password for mailbox user %s for %s"]="Successfully updated password for mailbox user %s for %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error cd-ing to directory '{dir}': {error}"]="Error cd-ing to directory '{dir}': {error}";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error reading directory '{dir}': {error}"]="Error reading directory '{dir}': {error}";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error while creating a directory '{dir}': {error}"]="Error while creating a directory '{dir}': {error}";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error deleting file '{file}': {error}"]="Error deleting file '{file}': {error}";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error downloading file '{file}': {error}"]="Error downloading file '{file}': {error}";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Error uploading file: {error}"]="Error uploading file: {error}";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Email forwarding information not available"]="Email forwarding information not available";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Forwarding has been removed"]="Forwarding has been removed";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Forwarding not found"]="Forwarding not found";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Forwarding for %s to %s deleted"]="Forwarding for %s to %s deleted";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Forwarding details have been updated"]="Forwarding details have been updated";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["One or more of the forward to locations is invalid"]="One or more of the forward to locations is invalid";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Email forwarding updated for '%s' from %s to %s"]="Email forwarding updated for '%s' from %s to %s";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Forwarding address '%s' is not valid"]="Forwarding address '%s' is not valid";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["The email address '%s' is not valid"]="The email address '%s' is not valid";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Forwarding already exists for '%s'"]="Forwarding already exists for '%s'";

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]["Email forwarding created for '%s' to %s"]="Email forwarding created for '%s' to %s";

// File: /registrar-platform/tools/insert_premiums.php
$STRINGS[$LANG]['africacom_prems.txt']='africacom_prems.txt';

// File: /registrar-platform/lib/Platform/User.php
$STRINGS[$LANG]['User #%d (%s) reset password']='User #%d (%s) reset password';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['ERROR processing order']='ERROR processing order';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Error in registration']='Error in registration';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Domain Restore']='Domain Restore';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Error when removing hold or transfer lock for renewed domain %s: %s']='Error when removing hold or transfer lock for renewed domain %s: %s';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Error renewing domain']='Error renewing domain';

// File: /registrar-platform/bin/process_queue.php
// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Premium Domain Registration']='Premiwm Cofrestru Parth';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Domain Registration']='Cofrestru Parth';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Premium Domain Registration of ']='Premiwm Cofrestru Parth o ';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Domain Registration of ']='Cofrestru Parth o ';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Premium Domain Renewal']='Premium Domain Renewal';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Domain Renewal']='Domain Renewal';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Premium Domain Renewal of ']='Premium Domain Renewal of ';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Domain Renewal of ']='Domain Renewal of ';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Private Registration']='Private Registration';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Private Registration of ']='Private Registration of ';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Renew Private Registration']='Renew Private Registration';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Renewal of Private Registration of ']='Renewal of Private Registration of ';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Local Presence']='Local Presence';

// File: /registrar-platform/bin/process_queue.php
// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Local Presence for ']='Local Presence for ';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Renew Local Presence']='Renew Local Presence';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Error setting up MDNS or creating records for newly purchased hosting package for %s: %s']='Error setting up MDNS or creating records for newly purchased hosting package for %s: %s';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Error in hosting setup']='Error in hosting setup';

// File: /registrar-platform/bin/process_queue.php
$STRINGS[$LANG]['Error setting up MDNS/creating records or with domain mapping for newly provisioned sitebuilder package for %s: %s']='Error setting up MDNS/creating records or with domain mapping for newly provisioned sitebuilder package for %s: %s';

// File: /registrar-platform/bin/process_queue.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error in sitebuilder DNS setup']='Error in sitebuilder DNS setup';

// File: /registrar-platform/bin/submit_restore_reports.php
$STRINGS[$LANG]['Error sending restore report for %s to the registry: %s']='Error sending restore report for %s to the registry: %s';

// File: /registrar-platform/bin/submit_restore_reports.php
$STRINGS[$LANG]['Restore report error']='Restore report error';

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]['launch']='launch';

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]['EPP messages to review']='EPP messages to review';

// File: /registrar-platform/bin/process_epp_msgs.php
// File: /registrar-platform/bin/process_inbound_internal.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error setting contacts for transfered domain %s: %s:']='Error setting contacts for transfered domain %s: %s:';

// File: /registrar-platform/bin/process_epp_msgs.php
// File: /registrar-platform/bin/process_inbound_internal.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error setting contacts in transfer']='Error setting contacts in transfer';

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]['Transfer request for non-existing domain']='Transfer request for non-existing domain';

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]['Outgoing transfer requested for %s']='Outgoing transfer requested for %s';

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]['Auto reject transfer error']='Auto reject transfer error';

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]['Outgoing transfer for %s automatically rejected, transfer not allowed']='Outgoing transfer for %s automatically rejected, transfer not allowed';

// File: /registrar-platform/bin/process_epp_msgs.php
$STRINGS[$LANG]['Cancel hosting account']='Cancel hosting account';

// File: /registrar-platform/bin/expired_pending_delete.php
$STRINGS[$LANG]['Error sending deletion request for %s: %s']='Error sending deletion request for %s: %s';

// File: /registrar-platform/bin/expired_pending_delete.php
$STRINGS[$LANG]['Error deleting domain']='Error deleting domain';

// File: /registrar-platform/bin/cancel_googleapps.php
// File: /registrar-platform/bin/cancel_sitebuilder.php
$STRINGS[$LANG]['Error cancelling sitebuilder for domain %s: %s:']='Error cancelling sitebuilder for domain %s: %s:';

// File: /registrar-platform/bin/cancel_googleapps.php
// File: /registrar-platform/bin/cancel_sitebuilder.php
$STRINGS[$LANG]['Error cancelling sitebuilder']='Error cancelling sitebuilder';

// File: /registrar-platform/bin/expired_hold.php
$STRINGS[$LANG]['Expired Hold script error']='Expired Hold script error';

// File: /registrar-platform/bin/expired_hold.php
// File: /registrar-platform/bin/expired_hold.php
$STRINGS[$LANG]['Unable to send on-hold email to %s: %s']='Unable to send on-hold email to %s: %s';

// File: /registrar-platform/bin/expired_hold.php
// File: /registrar-platform/bin/expired_hold.php
$STRINGS[$LANG]['On hold email sent to %s']='On hold email sent to %s';

// File: /registrar-platform/bin/log_epp_msgs.php
$STRINGS[$LANG]['ERROR with EPP message poll req']='ERROR with EPP message poll req';

// File: /registrar-platform/bin/log_epp_msgs.php
$STRINGS[$LANG]['ERROR in EPP msg logging']='ERROR in EPP msg logging';

// File: /registrar-platform/bin/cancel_whois.php
$STRINGS[$LANG]['Private whois cancelled (was already disabled)']='Private whois cancelled (was already disabled)';

// File: /registrar-platform/bin/cancel_whois.php
$STRINGS[$LANG]['Private whois cancelled for %s (was already disabled)']='Private whois cancelled for %s (was already disabled)';

// File: /registrar-platform/bin/cancel_whois.php
$STRINGS[$LANG]['Error cancelling whois for domain %s: %s:']='Error cancelling whois for domain %s: %s:';

// File: /registrar-platform/bin/cancel_whois.php
$STRINGS[$LANG]['Error cancelling whois']='Error cancelling whois';

// File: /registrar-platform/bin/verify_googleapps.php
$STRINGS[$LANG]['Giving up verification after an hour, Domain: %s']='Giving up verification after an hour, Domain: %s';

// File: /registrar-platform/bin/verify_googleapps.php
$STRINGS[$LANG]['users_meta table has been updated : %s']='users_meta table has been updated : %s';

// File: /registrar-platform/bin/verify_googleapps.php
$STRINGS[$LANG]['user has been notified of this issue %s']='user has been notified of this issue %s';

// File: /registrar-platform/bin/set_transfer_prohibited.php
$STRINGS[$LANG]['Transfer prohibited script error']='Transfer prohibited script error';

// File: /registrar-platform/bin/expired_delete.php
$STRINGS[$LANG]['Error when cancelling hosting for deleted domain %s: %s']='Error when cancelling hosting for deleted domain %s: %s';

// File: /registrar-platform/bin/expired_delete.php
// File: /registrar-platform/bin/cancel_hosting.php
$STRINGS[$LANG]['Error cancelling hosting']='Error cancelling hosting';

// File: /registrar-platform/bin/cancel_hosting.php
$STRINGS[$LANG]['Error cancelling hosting for domain %s: %s:']='Error cancelling hosting for domain %s: %s:';

// File: /registrar-platform/bin/renew_premiums.php
$STRINGS[$LANG]['Error renewing the premium domain %s at the registry: %s']='Error renewing the premium domain %s at the registry: %s';

// File: /registrar-platform/bin/renew_premiums.php
$STRINGS[$LANG]['Error renewing premium']='Error renewing premium';

// File: /registrar-platform/bin/whois_on_hold.php
$STRINGS[$LANG]['Whois on hold script error']='Whois on hold script error';

// File: /registrar-platform/bin/post_ibs_epp_msgs.php
$STRINGS[$LANG]['Error while sending EPP polls message to IBS']='Error while sending EPP polls message to IBS';

// File: /registrar-platform/bin/validate_vat_numbers.php
$STRINGS[$LANG]['VAT validation service not responding']='VAT validation service not responding';

// File: /registrar-platform/bin/validate_vat_numbers.php
$STRINGS[$LANG]['Unable to verify VAT number %s for payment #%s by user %s']='Unable to verify VAT number %s for payment #%s by user %s';

// File: /registrar-platform/bin/validate_vat_numbers.php
$STRINGS[$LANG]['Invalid VAT number detected']='Invalid VAT number detected';

// File: /registrar-platform/bin/validate_vat_numbers.php
$STRINGS[$LANG]['Invalid VAT number detected (%s) for payment #%s by user %s']='Invalid VAT number detected (%s) for payment #%s by user %s';

// File: /registrar-platform/www/application/views/wales/e_index.tpl
// File: /registrar-platform/www/application/views/wales/header.tpl
$STRINGS[$LANG]['Search for your name here']='Chwiliwch am eich enw yma';

// File: /registrar-platform/www/application/views/wales/e_index.tpl
$STRINGS[$LANG]['We can hear you say "I have email already" - <span style="color: #29abdf;"><b>Your new cymru.domains name will work with your current email.</b></span>']='Gallwn eich clywed yn datgan “Mae ebost gennyf eisioes” - <span style="color: #29abdf;"><b>Bydd eich enw cymru.domains yn gweithio gyda’ch ebost presennol.</b></span>';

// File: /registrar-platform/www/application/views/wales/header.tpl
// File: /registrar-platform/www/application/views/wales/header.tpl
// File: /registrar-platform/www/application/views/wales/header.tpl
// File: /registrar-platform/www/application/views/wales/header.tpl
$STRINGS[$LANG]['width']='width';

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]['A: There is a Uniform Dispute Resolution Policy (UDRP) which applies to all Generic Top Level Domain (GTLD) names including [store] domain names. Please see <a href="http://www.icann.org/en/help/dndr/udrp" target="_blank">ICANN&#39;s instructions</a> on the UDRP procedure for information on how to submit your claim.']='A: Mae Polisi Datrys Anghydfod Unffurf (UDRP) sy’n weithredol i bob enw Parth Lefel Uchaf Generig (GTLD) yn cynnwys enwau parth [store]. Gwelwch <a href="http://www.icann.org/en/help/dndr/udrp" target="_blank">gyfarwyddiadau ICANN</a> ar y weithdrefn UDRP am wybodaeth ynghlych sut i gyflwyno eich cais.';

// File: /registrar-platform/www/application/views/wales/inc_support_general_index.tpl
$STRINGS[$LANG]['A: If your domain expired more than 44 days ago it will now be pending deletion. To restore your domain you need to login to your account and renew it. To do this, click the renew button next to the domain, in the "Your Domain Name&#39;s" table. A domain can be restored up to 30 days after going to the pending deletion status, and will incur a fee in addition to the renewal fee. For more information about our what happens to your domain after expiry please see our <a href="/e/terms_deletion_renewal">Deletion and Auto-Renewal Policy</a>']='A: Os ydy’ch parth wedi dod i ben dros 44 diwrnod yn ôl, fe fydd erbyn hyn yn aros i gael ei ddileu. Er mwyn adfer eich parth bydd angen i chi fewngofnodi i’ch cyfrif a’i adnewyddu. Er mwyn gweud hyn, cliciwch y botwn ‘renew’ nesaf i’r parth, yn y tabl "Your Domain Name\'s". Gellir adfer parth hyd at 30 diwrnod wedi iddo fynd at y statws o aros am ddilead, a bydd codi ffi yn ychwaneg i’r ffi adnewyddu. Am ragor o wybodaeth ynghylch yr hyn sy’n digwydd i’ch parth wedi iddo ddod i ben gwelwch ein  <a href="/e/terms_deletion_renewal">Polisi Dileu ag Awto-Adnewyddu</a>';

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]['Products on the go']='Products on the go';

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]['Gmail Interface']='Gmail Interface';

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]['Hangout meeting']='Hangout meeting';

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]['Document Collaboration']='Document Collaboration';

// File: /registrar-platform/www/application/views/wales/e_googleapps.tpl
$STRINGS[$LANG]['Search for your name']='Search for your name';

// File: /registrar-platform/www/application/views/default/account_html_view_records.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Host Name']='Host Name';

// File: /registrar-platform/www/application/views/default/account_html_view_records.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Record Type']='Record Type';

// File: /registrar-platform/www/application/views/default/account_html_view_records.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Priority']='Priority';

// File: /registrar-platform/www/application/views/default/account_html_view_records.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Value']='Value';

// File: /registrar-platform/www/application/views/default/account_html_view_records.tpl
$STRINGS[$LANG]['* these records are managed by our system.']='* these records are managed by our system.';

// File: /registrar-platform/www/application/views/default/account_html_view_records.tpl
$STRINGS[$LANG]['Save Changes']='Save Changes';

// File: /registrar-platform/www/application/views/default/account_html_view_records.tpl
$STRINGS[$LANG]['View Zone File']='View Zone File';

// File: /registrar-platform/www/application/views/default/cart_checkout_completed.tpl
$STRINGS[$LANG]['Checkout Completed']='Cwblhawyd y Ddesg Dalu';

// File: /registrar-platform/www/application/views/default/e_parked.tpl
$STRINGS[$LANG]['For registrant details please use our <a href="/e/whois/[domain]">whois service</a>.']='For registrant details please use our <a href="/e/whois/[domain]">whois service</a>.';

// File: /registrar-platform/www/application/views/default/e_parked.tpl
$STRINGS[$LANG]['For registrant details please use our <a href="/e/whois/">whois service</a>.']='For registrant details please use our <a href="/e/whois/">whois service</a>.';

// File: /registrar-platform/www/application/views/default/account_google_apps.tpl
$STRINGS[$LANG]['Google Apps Admin User:']='Google Apps Admin User:';

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]['Payment & Checkout']='Talu & Desg Dalu';

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]['Company:']='Cwmni:';

// File: /registrar-platform/www/application/views/default/cart_checkout.tpl
$STRINGS[$LANG]['More information about security codes']='Mwy o fanylion ynghylch codiau diogelwch';

// File: /registrar-platform/www/application/views/default/cart_checkout_paypal.tpl
// File: /registrar-platform/www/application/views/default/cart_country_validation.tpl
// File: /registrar-platform/www/application/views/default/cart_country_validation.tpl
// File: /registrar-platform/www/application/views/default/account_bulk_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG][':checked']=':checked';

// File: /registrar-platform/www/application/views/default/cart_cv2.tpl
$STRINGS[$LANG]['The security code is usually the last three digits on the signature strip on the back of your card:']='The security code is usually the last three digits on the signature strip on the back of your card:';

// File: /registrar-platform/www/application/views/default/cart_cv2.tpl
$STRINGS[$LANG]['On American Express cards, the security code is the four digits to the right of the long embossed number on the <strong>front</strong> of the card:']='On American Express cards, the security code is the four digits to the right of the long embossed number on the <strong>front</strong> of the card:';

// File: /registrar-platform/www/application/views/default/e_premiums.tpl
$STRINGS[$LANG]['Premium Domains']='Premium Domains';


// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]['You can choose to purchase multiple years for any domain on the "Shopping Cart" page']='Gallwch ddewis prynu blynyddoedd lluosog ar gyfer unrhyw barth yn y dudalen “Cert Siopa”';

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]['in_cart']='in_cart';

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]['div.result_row']='div.result_row';

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]['div.phase_row']='div.phase_row';

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]['active']='active';

// File: /registrar-platform/www/application/views/default/cart_search_results.tpl
$STRINGS[$LANG]['label.phase_box']='label.phase_box';

// File: /registrar-platform/www/application/views/default/inc_support_googleapps_index.tpl
$STRINGS[$LANG]['<a href="https://productforums.google.com/forum/" target="_blank">Community forum for admins</a> or <a href="https://code.google.com/googleapps/support/" target="_blank">developers</a> - Discuss topics with other Google Apps customers.']='<a href="https://productforums.google.com/forum/" target="_blank">Community forum for admins</a> neu <a href="https://code.google.com/googleapps/support/" target="_blank">developers</a> - Trafodwch bynciau gyda chwsmeriaid eraill Google Apps.';

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]['Vat:']='TAW:';

// File: /registrar-platform/www/application/views/default/cart_html_view_shopping_cart.tpl
// File: /registrar-platform/www/application/views/default/account_view_payment.tpl
$STRINGS[$LANG]['Total:']='Cyfanswm:';

// File: /registrar-platform/www/application/views/default/e_verification.tpl
$STRINGS[$LANG]['An error has occured while trying to verify your contact details. Please <a href="/e/contact">contact our support team</a> to resolve this issue.']='An error has occured while trying to verify your contact details. Please <a href="/e/contact">contact our support team</a> to resolve this issue.';

// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]['For information about what will happen if your domain expires please see our <a href="/terms-deletion-renewal">Deletion & Auto-Renewal Policy</a>']='Am wybodaeth ynghylch yr hyn fydd yn digwydd os bydd eich parth yn dod i ben gwelwch ein <a href="/terms-deletion-renewal">Polisi Dilau & Awto-Adnewyddu</a>';

// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]['If you want to submit a complaint about any of our services please submit your request via the email above with the subject line "Complaint about your service".']='Os hoffech gyflwyno cwyn ynghylch unrhyw un o’n gwasanaethau, cyflwynwch eich cais drwy’r ebost uchod gan gynnwys y llinell pwnc “Cwyn ynghylch eich gwasanaeth”.';

// File: /registrar-platform/www/application/views/default/e_contact.tpl
$STRINGS[$LANG]['Know your rights as a Registrant? Go to <a href="http://www.icann.org/en/resources/registrars/registrant-rights/benefits" target="_blank">ICANN&#39;s Registrants&#39; Benefits and Responsibilities</a> and <a href="http://www.icann.org/en/resources/registrars/registrant-rights/educational" target="_blank">Registrant Rights and Responsibilities</a> for more information.']='Adnabod eich hawliau fel Cofrestrydd? Ewch i <a href="http://www.icann.org/en/resources/registrars/registrant-rights/benefits" target="_blank">Buddion a Chyfrifoldebau Cofrestrydd</a> a <a href="http://www.icann.org/en/resources/registrars/registrant-rights/educational" target="_blank">Hawliau a Chyfrifoldebau Cofrestrydd</a> ICANN am fwy o wbodaeth.';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Please note any changes made on this page may take up to ']='Please note any changes made on this page may take up to ';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG][' hours to propagate.']=' hours to propagate.';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Host Name*']='Host Name*';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Add']='Ychwanegu';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['* The following input is allowed:']='* The following input is allowed:';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Please consult the Notes below for the valid values for this field.']='Please consult the Notes below for the valid values for this field.';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Your domain name can be forwarded to any URL you choose. Please enter a URL to forward to below:']='Your domain name can be forwarded to any URL you choose. Please enter a URL to forward to below:';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Warning: Setting forwarding may make your existing website unavailable for up to [hours] hours']='Warning: Setting forwarding may make your existing website unavailable for up to [hours] hours';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Save']='Save';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Your domain name is not configured for web forwarding. To set up web forwarding, we need to add records to your domain which may replace existing records.']='Your domain name is not configured for web forwarding. To set up web forwarding, we need to add records to your domain which may replace existing records.';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Warning: choosing this may make your existing website unavailable!']='Warning: choosing this may make your existing website unavailable!';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Set up Web Forwarding']='Set up Web Forwarding';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Warning: setting forwarding may make your existing website unavailable for up to [hours] hours']='Warning: setting forwarding may make your existing website unavailable for up to [hours] hours';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Host names may only contain the characters a-z, 0-9, periods (to create sub-domains), underscores, and hyphens. If you want to create a <strong>wildcard</strong> record, use the "*" character. If you want to create a record for the domain name itself, use the "@" character.']='Host names may only contain the characters a-z, 0-9, periods (to create sub-domains), underscores, and hyphens. If you want to create a <strong>wildcard</strong> record, use the "*" character. If you want to create a record for the domain name itself, use the "@" character.';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>CNAME records:</strong> a given host name may only have a <strong>single</strong> CNAME record - our system will reject multiple CNAMEs for the same host name. You cannot set a CNAME record for the domain name itself. <strong>CNAME records must point to fully-qualified domain names.</strong>']='<strong>CNAME records:</strong> a given host name may only have a <strong>single</strong> CNAME record - our system will reject multiple CNAMEs for the same host name. You cannot set a CNAME record for the domain name itself. <strong>CNAME records must point to fully-qualified domain names.</strong>';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>MX records:</strong> all MX records should have the <em>priority</em> field set. Mail servers will try to deliver mail to the server with the lowest priority listed, moving on to the next one if the first is unavailable. <strong>MX records should point to fully-qualified domain names, not IP addresses.</strong>']='<strong>MX records:</strong> all MX records should have the <em>priority</em> field set. Mail servers will try to deliver mail to the server with the lowest priority listed, moving on to the next one if the first is unavailable. <strong>MX records should point to fully-qualified domain names, not IP addresses.</strong>';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>SPF records:</strong> to add an SPF record to your domain, <a href="http://old.openspf.org/wizard.html?mydomain=']='<strong>SPF records:</strong> to add an SPF record to your domain, <a href="http://old.openspf.org/wizard.html?mydomain=';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['">click here</a> to use the OpenSPF website to generate your SPF record, and then add it as a <strong>TXT</strong> record to your domain.']='">click here</a> to use the OpenSPF website to generate your SPF record, and then add it as a <strong>TXT</strong> record to your domain.';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>TXT records:</strong> for security reasons, we limit the maximum size of TXT records to 255 characters.']='<strong>TXT records:</strong> for security reasons, we limit the maximum size of TXT records to 255 characters.';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['Key to DNS Record Types']='Key to DNS Record Types';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>Start Of Authority</strong> - the presence of this record in a zone indicates that the server is <em>authoritative</em> for the domain name and includes some basic information, such as the master nameserver, and the e-mail address of the administrator.']='<strong>Start Of Authority</strong> - the presence of this record in a zone indicates that the server is <em>authoritative</em> for the domain name and includes some basic information, such as the master nameserver, and the e-mail address of the administrator.';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>Name Server</strong> - NS records indicate the name servers that are authoritative for the given host name. If the host name is "@" then the servers are authoritative for this domain.']='<strong>Name Server</strong> - NS records indicate the name servers that are authoritative for the given host name. If the host name is "@" then the servers are authoritative for this domain.';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>IPv4 Address</strong> - specifies the IPv4 address of the given host name. If the host name is "@" then the domain name will resolve to this IP address.']='<strong>IPv4 Address</strong> - specifies the IPv4 address of the given host name. If the host name is "@" then the domain name will resolve to this IP address.';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>IPv6 Address</strong> - specifies the IPv6 address of the given host name. If the host name is "@" then the domain name will resolve to this IP address.']='<strong>IPv6 Address</strong> - specifies the IPv6 address of the given host name. If the host name is "@" then the domain name will resolve to this IP address.';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>Canonical Name (Alias)</strong> - specifies that the given host name is an <em>alias</em> for the host name specified.']='<strong>Canonical Name (Alias)</strong> - specifies that the given host name is an <em>alias</em> for the host name specified.';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>Mail Exchanger</strong> - specifies the mail server for the given host/domain name. Multiple MX records can be specified, with mail servers sorting them by priority.']='<strong>Mail Exchanger</strong> - specifies the mail server for the given host/domain name. Multiple MX records can be specified, with mail servers sorting them by priority.';

// File: /registrar-platform/www/application/views/default/account_advanced_dns.tpl
$STRINGS[$LANG]['<strong>Text</strong> - allows the zone operator to insert an arbitrary message into the DNS. This is most commonly used to set SPF records.']='<strong>Text</strong> - allows the zone operator to insert an arbitrary message into the DNS. This is most commonly used to set SPF records.';

// File: /registrar-platform/www/application/views/default/inc_create.tpl
$STRINGS[$LANG]['By clicking "Agree and Continue" you agree to our <a href="/terms" target="_blank">Terms of Service</a> including all TLD Registrar Solutions policies, in addition you may agree to the following:']='Drwy glicio “Cytuno a Pharhau” rydych yn cytuno i’n Telerau Gwasanaeth yn cynnwys holl bolisïau TLD Registrar Solutions, yn ychwaneg mae’r dewis gennych gytuno i’r canlynol:';

// File: /registrar-platform/www/application/views/default/inc_cart_search.tpl
$STRINGS[$LANG]['table']='table';

// File: /registrar-platform/www/application/views/default/cart_html_trademark_claim_items.tpl
$STRINGS[$LANG]['For more information concerning the records included in this notice, please visit <a href="http://claims.clearinghouse.org" target="_blank">http://claims.clearinghouse.org</a>']='For more information concerning the records included in this notice, please visit <a href="http://claims.clearinghouse.org" target="_blank">http://claims.clearinghouse.org</a>';

// File: /registrar-platform/www/application/views/default/account_html_notifications.tpl
$STRINGS[$LANG]['You have not verified your account information yet. You may have already recieved an email to do so. If not, you can resend another verification email on the <a href="/account/settings">account settings</a> page.']='You have not verified your account information yet. You may have already recieved an email to do so. If not, you can resend another verification email on the <a href="/account/settings">account settings</a> page.';

// File: /registrar-platform/www/application/views/default/account_html_notifications.tpl
$STRINGS[$LANG]['You have not verified some of your domain contacts yet. You may have already recieved an email to do so. If not, you can resend another verification email on the <a href="/account/contacts">manage contacts</a> page.']='You have not verified some of your domain contacts yet. You may have already recieved an email to do so. If not, you can resend another verification email on the <a href="/account/contacts">manage contacts</a> page.';

// File: /registrar-platform/www/application/views/default/cart_private_whois.tpl
$STRINGS[$LANG]['Add Private WHOIS']='Add Private WHOIS';

// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
// File: /registrar-platform/www/application/views/default/account_sitebuilder_change.tpl
$STRINGS[$LANG]['table.sitebuilder_plan']='table.sitebuilder_plan';

// File: /registrar-platform/www/application/views/default/inc_terms_deletion_renewal.tpl
$STRINGS[$LANG]['[pending_deletion] days after the domain expiry date your domain will be placed on "Pending Delete" status and it will enter the redemption period. If you wish to renew your domain whilst the domain is in the redemption period you will be required to pay a redemption fee:']='[pending_deletion] days after the domain expiry date your domain will be placed on "Pending Delete" status and it will enter the redemption period. If you wish to renew your domain whilst the domain is in the redemption period you will be required to pay a redemption fee:';

// File: /registrar-platform/www/application/views/default/account_domain_settings.tpl
$STRINGS[$LANG]['You can only use verified contact(s) as your contact data, please visit the <a href="/account/contacts">manage contacts</a> page for information on how to verify your contacts.']='You can only use verified contact(s) as your contact data, please visit the <a href="/account/contacts">manage contacts</a> page for information on how to verify your contacts.';

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]['Existing Email Accounts']='Existing Email Accounts';

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]['Create Email Account']='Create Email Account';

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG][' Denotes a mandatory field']=' Denotes a mandatory field';

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]['Email:']='Email:';

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]['@']='@';

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]['Password:']='Cyfrinair:';

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]['Repeat Password:']='Repeat Password:';

// File: /registrar-platform/www/application/views/default/account_manage_emails.tpl
$STRINGS[$LANG]['Administrator:']='Administrator:';

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]['Use the check boxes to perform quick actions to your domain(s):']='Defnyddiwch eich bocsys gwirio er mwyn perfformio gweithrediadau cyflym ar eich parth(au):';

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]['div.panel']='div.panel';

// File: /registrar-platform/www/application/views/default/account_index.tpl
$STRINGS[$LANG]['center']='center';

// File: /registrar-platform/www/application/views/default/account_html_view_mailboxes.tpl
$STRINGS[$LANG]['You currently have no email accounts setup. Use the form below to create a new account.']='You currently have no email accounts setup. Use the form below to create a new account.';

// File: /registrar-platform/www/application/helpers/common_helper.php
// File: /registrar-platform/www/application/controllers/e.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Please enter your full name']='Please enter your full name';

// File: /registrar-platform/www/application/helpers/common_helper.php
// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Please enter your street name']='Please enter your street name';

// File: /registrar-platform/www/application/helpers/common_helper.php
// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Please enter your city']='Please enter your city';

// File: /registrar-platform/www/application/helpers/common_helper.php
// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Please enter your postcode']='Please enter your postcode';

// File: /registrar-platform/www/application/helpers/common_helper.php
// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Please enter your phone number']='Please enter your phone number';

// File: /registrar-platform/www/application/helpers/common_helper.php
// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Email address is invalid']='Email address is invalid';

// File: /registrar-platform/www/application/controllers/e.php
// File: /registrar-platform/www/application/controllers/e.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Your email address is invalid']='Your email address is invalid';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Please enter your first name']='Please enter your first name';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Please enter your last name']='Please enter your last name';

// File: /registrar-platform/www/application/controllers/e.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['There are errors in your form. Please correct them.']='There are errors in your form. Please correct them.';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Error signing up for newsletter. Please try again later.']='Error signing up for newsletter. Please try again later.';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Please enter your email address']='Please enter your email address';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Please enter your password']='Please enter your password';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Username or password is incorrect']='Username or password is incorrect';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['This account is on hold']='This account is on hold';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['User #%d (%s) logged in from %s (Location: %s)']='User #%d (%s) logged in from %s (Location: %s)';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['A new password has been emailed to you']='A new password has been emailed to you';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['The email address appears to be invalid. Please try again.']='The email address appears to be invalid. Please try again.';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['That email address does not exist. Please try again.']='That email address does not exist. Please try again.';

// File: /registrar-platform/www/application/controllers/e.php
// File: /registrar-platform/www/application/controllers/e.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['An error occured while sending email. Please try again later.']='An error occured while sending email. Please try again later.';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['The domain name appears to be invalid. Please try again.']='The domain name appears to be invalid. Please try again.';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['The domain name does not exist. Please try again.']='The domain name does not exist. Please try again.';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Please verify your email address']='Please verify your email address';

// File: /registrar-platform/www/application/controllers/e.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Password must be at least 8 characters']='Password must be at least 8 characters';

// File: /registrar-platform/www/application/controllers/e.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Password must contain at least one number']='Password must contain at least one number';

// File: /registrar-platform/www/application/controllers/e.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Password must have an upper case character']='Password must have an upper case character';

// File: /registrar-platform/www/application/controllers/e.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Password must have a lower case character']='Password must have a lower case character';

// File: /registrar-platform/www/application/controllers/e.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Please enter an Organization name to qualify for VAT deduction']='Please enter an Organization name to qualify for VAT deduction';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Country name not recognized']='Country name not recognized';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['The VAT Number provided is invalid, please try again!']='The VAT Number provided is invalid, please try again!';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['This email address is already in use']='This email address is already in use';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Error while creating your account. Please try again later']='Error while creating your account. Please try again later';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['User #%d (%s) successfully verified Contact (%s) via verification link']='User #%d (%s) successfully verified Contact (%s) via verification link';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['User #%d (%s) unable to verify Contact (%s) (used out-dated verification link [%s seconds])']='User #%d (%s) unable to verify Contact (%s) (used out-dated verification link [%s seconds])';

// File: /registrar-platform/www/application/controllers/e.php
// File: /registrar-platform/www/application/controllers/cart.php
// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['inc_terms_hosting.tpl']='inc_terms_hosting.tpl';

// File: /registrar-platform/www/application/controllers/e.php
// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['inc_terms_sitebuilder.tpl']='inc_terms_sitebuilder.tpl';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Your enquiry has been sent. We will reply within 24 hours to the email address provided.']='Your enquiry has been sent. We will reply within 24 hours to the email address provided.';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Please enter your name']='Please enter your name';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Please enter a subject']='Please enter a subject';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['Please enter your question']='Please enter your question';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['e_sitebuilder.tpl']='e_sitebuilder.tpl';

// File: /registrar-platform/www/application/controllers/e.php
$STRINGS[$LANG]['e_googleapps.tpl']='e_googleapps.tpl';

// File: /registrar-platform/www/application/controllers/support.php
$STRINGS[$LANG]['inc_support_general_index.tpl']='inc_support_general_index.tpl';

// File: /registrar-platform/www/application/controllers/support.php
$STRINGS[$LANG]['inc_support_sitebuilder_index.tpl']='inc_support_sitebuilder_index.tpl';

// File: /registrar-platform/www/application/controllers/support.php
$STRINGS[$LANG]['inc_support_googleapps_index.tpl']='inc_support_googleapps_index.tpl';

// File: /registrar-platform/www/application/controllers/support.php
$STRINGS[$LANG]['inc_support_about_index.tpl']='inc_support_about_index.tpl';

// File: /registrar-platform/www/application/controllers/support.php
$STRINGS[$LANG]['inc_support_email_index.tpl']='inc_support_email_index.tpl';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Items added!']='Items added!';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Error validating domain']='Error validating domain';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Error contacting provider, please try again later']='Error contacting provider, please try again later';

// File: /registrar-platform/www/application/controllers/cart.php
// File: /registrar-platform/www/application/controllers/cart.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['There are errors in your form:']='There are errors in your form:';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Please choose type of entity']='Please choose type of entity';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Please choose form of identification']='Please choose form of identification';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Please enter your identification number']='Please enter your identification number';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Error creating contact, please try again later']='Error creating contact, please try again later';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Unable to add local presence item to cart']='Unable to add local presence item to cart';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Error accepting trademark claim notice, please try again later']='Error accepting trademark claim notice, please try again later';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Accepted']='Accepted';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Error declining trademark claim notice, please try again later']='Error declining trademark claim notice, please try again later';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Item period changed!']='Item period changed!';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Please supply a VAT Number']='Please supply a VAT Number';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Item removed from cart']='Item removed from cart';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Please enter the card expiry date']='Please enter the card expiry date';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Please complete the highlighted fields before submitting.']='Please complete the highlighted fields before submitting.';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['card number']='rhif carden';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['security code']='cod diogelwch';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['country']='gwald';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['postcode']='cod post';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['address']='address';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['There was a problem with the information']='There was a problem with the information';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['{scheme} card no. {first4}{xes}{last4} (expires {expires})']='{scheme} card no. {first4}{xes}{last4} (expires {expires})';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Private whois added']='Private whois added';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Add Extras']='Add Extras';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Add Hosting Plan']='Add Hosting Plan';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Hosting package added']='Hosting package added';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Successfully added site builder plan to cart']='Successfully added site builder plan to cart';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Error adding the selected plan to your domains. Please try again later']='Error adding the selected plan to your domains. Please try again later';

// File: /registrar-platform/www/application/controllers/cart.php
$STRINGS[$LANG]['Successfully added Google Apps plan to cart']='Successfully added Google Apps plan to cart';

// File: /registrar-platform/www/application/controllers/cart.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Please enter a valid number of seats.']='Please enter a valid number of seats.';

// File: /registrar-platform/www/application/controllers/cart.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['A Google Apps package is already added to cart. Please remove from cart and try again']='A Google Apps package is already added to cart. Please remove from cart and try again';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Domain Name']='Domain Name';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Website Builder (%s)']='Website Builder (%s)';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['This domain is not registered on Google']='This domain is not registered on Google';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['This domain is registered on Google']='This domain is registered on Google';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['This domain is registered on Google to TRS, please contact TRS to proceed']='This domain is registered on Google to TRS, please contact TRS to proceed';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Token was saved successfully']='Token was saved successfully';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Token is already saved']='Token is already saved';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Please provide your Transfer Token']='Please provide your Transfer Token';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['%s has been deleted successfully']='%s has been deleted successfully';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['%s has been setup as an Admin']='%s has been setup as an Admin';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Domain is now set to auto-expire']='Domain is now set to auto-expire';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Domain has been removed from auto-expire']='Domain has been removed from auto-expire';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error deleting domain: %s']='Error deleting domain: %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Domain %s and associated services have been scheduled for deletion. You will be redirected shortly..']='Domain %s and associated services have been scheduled for deletion. You will be redirected shortly..';

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error sending %s to %s: %s']='Error sending %s to %s: %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Successfully requested deletion of domain %s and associated services']='Successfully requested deletion of domain %s and associated services';

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Item added to cart']='Item added to cart';

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Domain name %s could not be found via %s. (User ID: %s)']='Domain name %s could not be found via %s. (User ID: %s)';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error setting up MDNS or removing records for new sitebuilder package for %s: %s']='Error setting up MDNS or removing records for new sitebuilder package for %s: %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error creating new records for new sitebuilder package for %s: %s']='Error creating new records for new sitebuilder package for %s: %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error in sitebuilder mapping']='Error in sitebuilder mapping';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Upgrade package added to cart']='Upgrade package added to cart';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Website Builder (%s) Renewal has been added to cart']='Website Builder (%s) Renewal has been added to cart';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['You have successfully added %s to your cart']='You have successfully added %s to your cart';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['%s is already added to cart. Please remove from cart and try again']='%s is already added to cart. Please remove from cart and try again';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Your Google Apps verification failed']='Your Google Apps verification failed';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Your Google Apps have been verified successfully']='Your Google Apps have been verified successfully';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error getting auth code, please try again later']='Error getting auth code, please try again later';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Selected items have been added to cart']='Selected items have been added to cart';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Please select at least one item to perform this action']='Please select at least one item to perform this action';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Personal information is now public for domain %s. You can re-enable this at any time before expiry.']='Personal information is now public for domain %s. You can re-enable this at any time before expiry.';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Personal information is now private for domain %s.']='Personal information is now private for domain %s.';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Contact details updated successfully.']='Contact details updated successfully.';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Email addresses must match']='Email addresses must match';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['This email address is already in the system']='This email address is already in the system';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Please enter your password to confirm the changes']='Please enter your password to confirm the changes';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Incorrect password']='Incorrect password';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Invalid VAT number']='Invalid VAT number';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Contact details updated successfully']='Contact details updated successfully';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Password has been successfully updated']='Password has been successfully updated';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Please enter your current password']='Please enter your current password';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Current password is incorrect']='Current password is incorrect';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Password updated successfully']='Password updated successfully';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Verification email has been sent to %s']='Verification email has been sent to %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error sending verification email. Please contact us at %s']='Error sending verification email. Please contact us at %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Select new contact']='Select new contact';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Contacts have been successfully updated']='Contacts have been successfully updated';

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Contacts could not be updated at this time: %s']='Contacts could not be updated at this time: %s';

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Contacts could not be updated for %s for User #%s (%s): %s']='Contacts could not be updated for %s for User #%s (%s): %s';

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Contacts could not be updated for %s: %s']='Contacts could not be updated for %s: %s';

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Contacts successfully updated for %s']='Contacts successfully updated for %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error creating new contact for %s: %s']='Error creating new contact for %s: %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error creating new contact for %s for User #%s (%s): %s']='Error creating new contact for %s for User #%s (%s): %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Contacts have successfully been updated']='Contacts have successfully been updated';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Could not get contact information at this time, please try again later']='Could not get contact information at this time, please try again later';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['admin']='admin';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Contact %s does not have an email address set. Please set an email address before verifing the contact.']='Contact %s does not have an email address set. Please set an email address before verifing the contact.';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error finding contact %s']='Error finding contact %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Could not update contact details for %s, please try again later']='Could not update contact details for %s, please try again later';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Could not update contact %s for User #%s (%s): %s']='Could not update contact %s for User #%s (%s): %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Could not update contact %s: %s']='Could not update contact %s: %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['ATTN: Contact Update Required']='ATTN: Contact Update Required';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Contact %s has been successfully deleted']='Contact %s has been successfully deleted';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error deleting contact %s']='Error deleting contact %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['An error occured while requesting the transfer']='An error occured while requesting the transfer';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Transfer #%d requested (incoming) for %s by user #%d (%s)']='Transfer #%d requested (incoming) for %s by user #%d (%s)';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['An error occured while sending transfer request email to admin contact or registant']='An error occured while sending transfer request email to admin contact or registant';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Transfer request email sent to %s']='Transfer request email sent to %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Transfer request has been submitted']='Transfer request has been submitted';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Transfer request has not been submitted']='Transfer request has not been submitted';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Transfer #%d for %s cancelled by user #%d (%s)']='Transfer #%d for %s cancelled by user #%d (%s)';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Cancelled transfer #%d for %s']='Cancelled transfer #%d for %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Transfer cancelled']='Transfer cancelled';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Outgoing']='Outgoing';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Incoming']='Incoming';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Transfer approved']='Transfer approved';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Transfer could not be approved at this time: %s']='Transfer could not be approved at this time: %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Transfer rejected']='Transfer rejected';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Transfer could not be rejected at this time: %s']='Transfer could not be rejected at this time: %s';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Nameservers have successfully been updated for selected domains.']='Nameservers have successfully been updated for selected domains.';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Errors have occured while validating your new nameservers:']='Errors have occured while validating your new nameservers:';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Errors have occured while assigning your new nameservers:']='Errors have occured while assigning your new nameservers:';

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Nameserver deleted']='Nameserver deleted';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['IP address is invalid or empty']='IP address is invalid or empty';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['ATTN: Nameserver Update Required']='ATTN: Nameserver Update Required';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Nameserver registered']='Nameserver registered';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['ATTN: Nameserver Registration Required']='ATTN: Nameserver Registration Required';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error retrieving IP']='Error retrieving IP';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['-']='-';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Domain not found']='Domain not found';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Permission denied']='Permission denied';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Nameservers saved']='Nameservers saved';

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error(s) encountered when updating records:']='Error(s) encountered when updating records:';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Email username is empty']='Email username is empty';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Password is empty']='Password is empty';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['The email username must be a maximum of 30 characters in length']='The email username must be a maximum of 30 characters in length';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['The password fields do not match']='The password fields do not match';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Please fix the following errors:']='Please fix the following errors:';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['You must enter either your old password for verification or set a new password.']='You must enter either your old password for verification or set a new password.';

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['You must supply directory.']='You must supply directory.';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Directory successfully changed.']='Directory successfully changed.';

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error connecting to FTP server: {error}']='Error connecting to FTP server: {error}';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['{bytes} bytes']='{bytes} bytes';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['{bytes} KB']='{bytes} KB';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['{bytes} MB']='{bytes} MB';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Directory successfully created.']='Directory successfully created.';

// File: /registrar-platform/www/application/controllers/account.php
// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['You must supply file.']='You must supply file.';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['File successfully deleted.']='File successfully deleted.';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['File successfully downloaded.']='File successfully downloaded.';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['You must supply path.']='You must supply path.';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['File successfully uploaded.']='File successfully uploaded.';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['You must enter the new email address']='You must enter the new email address';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['You must enter the forwarding address']='You must enter the forwarding address';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['An error occurred when updating the DNS setting for your domain. An administrator has been notified.']='An error occurred when updating the DNS setting for your domain. An administrator has been notified.';

// File: /registrar-platform/www/application/controllers/account.php
$STRINGS[$LANG]['Error creating MX record for %s: %s']='Error creating MX record for %s: %s';

$STRINGS[$LANG]["Email Forwarding"]="Dargyfeirio Ebost";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Email forwarding is currently offered to all cymru.domains customers as a free add-on service. Your free email forwarding account enables you to advertise a professional email address using your cymru.domains name, and forward it to your existing email address.  You can then configure your existing email account to be able to send from your new cymru.domains email.   For example, setup yourname@yourbusiness.CYMRU and forward it to yourname123@gmail.com."]="Cynigir dargyfeirio ebost i bob cwsmer cymru.domains ar hyd o bryd fel gwasanaeth ychwanegol am ddim.  Mae eich cyfrif dargyfeirio ebost am ddim yn caniatau i chi ddefnyddio eich enw cymru.domains, a’i ddargyfeirio i’ch ebost presennol. Gallwch wedyn gyflunio eich cyfrif ebost presennol er mwyn gallu danfon o’ch ebost cymru.domains newydd.  Er Enghraifft, gosod eichenw@eichbusnes.CYMRU a’i ddargyfeirio at eichenw123@gmail.com.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["In order to setup forwarding to your existing email address please follow the below instructions:"]="Er mwyn gosod dargyfeirio i’ch ebost presennol dilynwch y cyfarwyddiadau isod:";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Login to your account at cymru.domains"]="Mewngofnodwch i’ch cyfrif cymru.domains.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["On the main page click on the domain you wish to setup email forwarding for"]="Ar y brif dudalen cliciwch ar y parth y dymunwch osod dargyfeirio ebost ar ei gyfer.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Scroll down until you find the Email Forwarding section and enter your email forwarding details."]="Sgroliwch lawr nes i chi ddarganfod yr adran “Email Forwarding” a rhowch eich manylion dargyfeirio.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["To configure your existing email account to be able to send from your new cymru.domains email please see the instructions for the appropriate email provider below. PLEASE NOTE: You must create your email forwarding through cymru.domains before configuring your existing email account to send from your new address."]="Er mwyn cyflunio eich cyfrif ebost presennol fel y gall ddanfon o’ch ebost cymru.domains newydd, gwelwch y cyfarwyddiadau ar gyfer y darparwr ebost priodol isod.  NODWCH: Mae’n rhaid I chi greu eich dargyfeirio ebost drwy cymru.domains cyn cyflunio eich cyfrif ebost cyfredol fel ei fod yn gallu danfon o’ch ebost newydd.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Blackberry"]="Backberry";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Login to your account on the BlackBerry Internet Service website."]="Mewngofnodwch i’ch cyfrif o’r gwefan Gwasanaeth Rhyngrwyd Blackberry.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["In the left menu, click Email Accounts."]="Yn y fwydlen ar y chwith, cliciwch “Email Accounts”.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Next to your existing email account click Edit."]="Nesaf i’ch cyfrif ebost presennol cliciwch “Edit”.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Change your Email account name, and Reply to email address to your new email address, e.g. yourname@yourdomain.cymru."]="Newidiwch eich enw cyfrif Ebost, a’ch cyfeiriad ebost “Reply to” i’ch ebost newydd e.e. eichenw@eichparth.cymru.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["All emails sent to your existing email address will still appear in your inbox, but when sending email the ‘from’ address will now show as your new address, e.g. yourname@yourdomain.cymru"]="Bydd pob ebost a ddanfonir i’ch cyfeiriad ebost yn parhau i ymddangos yn eich mewnflwch, ond pan yn danfon ebost bydd y cyfeiriad “from” nawr yn dangos fel eich cyfeiriad newydd, e.e. eichenw@eichparth.cymru";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Yahoo! Mail"]="Yahoo! Mail";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Sign in to Yahoo! Mail."]="Mewngofnodwch i Yahoo! Mail";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Select Settings from the “Options” cog wheel on the right hand side of the Yahoo! Mail screen"]="Dewisiwch ‘Settings’ o’r olwyn dannedd  “Options” ar ochr dde sgrîn Yahoo! Mail.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Click Accounts on the menu to the left."]="Cliciwch “Accounts” ar y fwydlen i’r chwith.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Click the Add button next to “Send and receive email from other email services on the Accounts options page."]="Cliciwch y botwm “Add” nesaf i “Send” and receive email from other email services” ar y dudalen “Account options”.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Add your new email address under the “Email Address” section."]="Ychwanegwch eich cyfeiriad ebost newydd o dan yr adran “Email Address”.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Add a description for the mailbox"]="Ychwanegwch ddisgrifiad ar gyfer y blwch negeseuon e-bost";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Add your new email address under the “Reply-to-address” section. Click on Save"]="Ychwanegwch eich cyfeiriad ebost newydd o dan yr adran “Reply-to-address”. Cliciwch ar “Save”.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Yahoo! Mail will send a confirmation message to the email address you just entered, to confirm that you own it. As this email address is forwarding to your Yahoo! Mail address you should receive the confirmation email in your Yahoo! Mail inbox."]="Bydd Yahoo! Mail yn danfon neges gadarnhau i’r cyfeiriad ebost rydych newydd rhoi, er mwyn cadarnhau mai chi sy’n berchen arno. Wrth i’r cyfeiriad ebost ddargyfeirio i’ch cyfeiriad Yahoo! Mail, dylwch dderbyn ebost cadarnhau yn eich mewnflwch Yahoo! Mail.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Open the Yahoo! Email Address Confirmation email and click the confirmation link. A new Yahoo! Mail page will confirming that you have verified your email."]="Agorwch yr ebost “Yahoo! Email Address Confirmation” a chliciwch ar y ddolen gadarnhau. Bydd tudalen Yahoo! Mail newydd yn cadarnhau eich bod wedi dilysu eich ebost.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Your new email address account should now appear on the From drop down when you compose a new email."]="Dylai eich cyfrif ebost newydd nawr ymddangos yn y gwmplen “From” pan yn cyfansoddi ebost newydd.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Thunderbird"]="Thunderbird";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["From the menu, choose Tools -> Account Settings"]="O’r fwydlen, dewisiwch “Tools -> Account Settings”";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Select your existing account."]="Dewisiwch eich cyfrif presennol.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Change your Email Address and Reply-to Address to your new email address. Click Ok."]="Newidiwch eich Cyfeiriad Ebost a Chyfeiriad “Reply to” yn eich cyfeiriad ebost newydd. Cliciwch “Ok”.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Outlook.com/Windows Live/Hotmail"]="Outlook.com/Windows Live/Hotmail";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Sign in to your email account."]="Mewngofnodwch i’ch cyfrif ebost.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Select Options from the 'Options' cog wheel on the right hand side of the Outlook.com toolbar."]="Dewisiwch “Options” o’r olwyn dannedd “Options” ar ochr dde y bar offer Outlook.com.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Under Manage your account, click on Your Email Accounts."]="O dan “Manage your account”, cliciwch “Your Email Accounts”";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Under Your Email accounts you will see the option to Add a send only account. Click on this option."]="O dan “Your Email Accounts” gwelwch yr opsiwn i “Add a send only account”. Cliciwch ar yr opsiwn hwn. ";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["On the next screen click on Advanced Options"]="Ar y sgrîn nesaf cliciwch ar “Advanced Options”.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Select the option Send emails using Outlook.com’s server"]="Dewisiwch yr opsiwn “Send emails using Outlookcom’s server”. ";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Enter your email address where required and click Next"]="Rhowch eich cyfeiriad ebost pan fydd galw a chliciwch “Next”";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Outlook.com will send a verification message to the email address you just added, to confirm that you own it. As this email address is forwarding to your Outlook.com/Windows Live/Hotmail address you should receive the verification email in your this inbox."]="Bydd Outlook.com yn danfon neges dilysu i’r cyfeiried ebost rydych newydd ychwanegu, er mwyn cadarnhau mai chi sydd yn ei berchen.  Wrth i’r cyfeiriad ebost hwn ddargyfeirio i’ch cyfeiriad Outlook.com/Windows Live/Hotmail, dylwch dderbyn ebost dilysu yn eich mewnflwch.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Open the Windows Live Hotmail: Verify your e-mail address email and click the verification link."]="Agorwch eich ebost: “Windows Live Hotmail: Verify our e-mail address” a chliciwch y ddolen ddilysu.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["On your email accounts page your new email address will appear in the list of email accounts that you can send mail from. You can choose to make this your default ‘From’ address by clicking the Use as default link."]="Ar eich tudalen “Your email accounts” bydd eich cyfeiriad ebost newydd yn ymddangos yn y rhestr o gyfrifon ebost y gallwch ddanfon ebost ohonynt. Gallwch ddewis sefydlu hwn fel eich cyfeiriad “From” diofyn drwy glicio y ddolen “Use as default”.";


// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Outlook 2007"]="Outlook 2007";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Select your existing account, and then click Change from the options above."]="Dewisiwch eich cyfrif presennol, yna cliciwch “Change” o’r opsiynau uchod.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Change the Email Address to your new email address, e.g. yourname@yourdomain.cymru."]="Newidiwch y Cyfeiriad Ebost i’ch cyfeiriad ebost newydd e.e. eichenw@eichparth.cymru.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Click Next, then Finish."]="Cliciwch “Next”, yna “Finish”.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Outlook 2010"]="Outlook 2010";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Open an email you want to send"]="Deiwisiwch ebost yr hoffech ei ddanfon.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Click on the Options tab in the Outlook toolbar."]="Cliciwch ar y tab “Options” yn y bar offer Outlook. ";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Be sure that the From button is selected and highlighted."]="Sicrhewch fod y botwm “From” wedi ei ddewis a’i amlygu.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Click on the From dropdown and select “Other E-Mail Address”."]="Cliciwch ar y gwymplen “From” a dewisiwch “Other E-Mail Address”.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Enter your new cymru.domains email address and send a test email."]="Rhowch eich ebost cymru.domains newydd a danfonwch ebost prawf.";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["You should now be able to send from your new email address, this will appear as an option in the From dropdown menu"]="Dylwch erbyn hyn fod yn gallu danfon o’ch cyfeiriad ebost newydd, bydd hwn yn ymddangos fel opsiwn yn y fwydlen gwymplen “From”";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Gmail"]="Gmail";

// File: /home/kwaku/cnic/registrar-platform/www/application/views/wales_tmp/inc_support_email_index.tpl
$STRINGS[$LANG]["Sending from Gmail for a forwarding address is not currently possible.  Please purchase Google Apps in order to use Gmail as your cymru.domains name email client."]="Nid yw danfon cyfeiriad dargyfeirio Gmail yn bosib ar hyn o bryd. Prynwch Google Apps er mwyn archebu Gmail fel eich cleient ebost enw cymru.domains.";




