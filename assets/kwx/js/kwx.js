// JavaScript Document


function KWX(data, server_env){
	'use strict';
	var self = this;
	this.data = data;
	this.connType = "contentType=application/json";
	this.Host = this.data.host !== '' ? this.data.host +"amf.php?" : alert('Please provide amf entry point url');
	this.endpoint = (typeof server_env == 'undefined') ? this.Host + this.connType : this.data.dev +"amf.php?" + this.connType;
	this.errMsg = $('#err_msg');
	this.appName = this.data.appName;
	this.grid = [];
	this.online = navigator.onLine;
	this.store = localStorage;

	this.init = function(str){
					if(str === true){
						return " dataType: 'json'";
					}
				};

	this.showLoading = function()
	{
		$('body').append('<div id="loading" />');
		$('#loading').fadeIn('fast');

	};

	this.removeLoading = function()
	{
		setTimeout(function () {
			$('#loading').fadeOut('fast', function(){});

			$('#loading').remove();
		}, 1000);

	};

	// sendData handles ajax i/o
	this.sendData = function(transmitter_type, url, customData, resultHandler){

		this.showLoading();

		$.ajax({
			type: transmitter_type,
			url:  url,
			data:  customData,
			dataType: 'jsonp',

			success: resultHandler
		});

		this.removeLoading();
	};


	this.connector = function (str,url, callback){

		this.sendData('post', url, str, callback);

	};


	/* This method is used to make amfphp calls to AMFPHP server
	* @param svc The name of the Service to call, eg: MemberService
	* @param mthd The method/function of the Service, eg: addMember();
	* @param param Parametres to send to the server
	* @param success_callback Pass a custom function to handle any return data (Must be json)
	*/
	this.get = function (svc, mthd, resultHandler){
		this.showLoading();

		var callDataObj = {"serviceName":svc, "methodName":mthd};
		var callData = JSON.stringify(callDataObj);

		$.post(this.endpoint, callData, resultHandler, 'json');


	};


	this.post = function (svc, mthd, param, resultHandler){

		this.showLoading();
		var callDataObj = {"serviceName":svc, "methodName":mthd, "parameters":[param]}
		var callData = JSON.stringify(callDataObj);

		$.post(this.endpoint, callData, resultHandler);


	};


	this.formData = function (frmID){

		var str = $(frmID).serialize();
		var obj = this.stringToJson(str);

		return obj;
	};

	this.stringToJson = function (str){

		var arr = str.split('&');
		var obj = {};
		for(var i = 0; i < arr.length; i++) {
				var bits = arr[i].split('=');
				obj[bits[0]] = this.urlencode(bits[1]);
		}

		return obj;
	};

	this.prepJson = function (obj) {
		var json_data = JSON.stringify(obj);
		return json_data;
	};

	this.jsonData = function(obj){
		if (typeof obj != 'undefined' || typeof obj != '') {

			return JSON.parse(obj);
		} else {
			return false;
		}


	};


	// convert object back to string
	this.jsonToString = function (obj){

		var str = '';
		for(var key in obj) {
			str += key + '=' + obj[key] + '&';
		}
		str = str.slice(0, str.length - 1);

		return str;
	};



	this.fixedEncodeURIComponent = function (str) {
		return encodeURIComponent(str);
	};


	this.urlencode = function(str) {

		return escape(str).replace(/\+/g, ' ').replace(/%40/g, '@').replace(/%2540/g, '@').replace(/%27/g, "'").replace(/%252C/g, ",").replace(/%252F/g, "/").replace(/%253C/g, "/").replace(/%2509/g, "/").replace(/Http%253A/g, "http:").replace(/http%253A/g, "http:").replace(/%3A/g, ":").replace(/%2B/g,'+').replace(/%20/g, '+').replace(/%2A/g, '*').replace(/%2F/g, '/').replace(/%250D%250A/g, "<br/>").replace(/%3C/g, "<").replace(/%3E/g, ">").replace(/%23/g, "#").replace(/%25/g, "%").replace(/%7B/g, "{").replace(/%7D/g, "}").replace(/%7C/g, "|").replace(/%5C/g, "'\'").replace(/%5E/g, "^").replace(/%7E/g, "~").replace(/%24/g, "$").replace(/%26/g, "&").replace(/%3D/g, "=").replace(/%3A/g, ":").replace(/%3F/g, "?").replace(/%3B/g, ";").replace(/%60/g, "'").replace(/%5D/g, "]").replace(/%5B/g, "[").replace(/%22/g, '"').replace(/%C2%A3/g, '£').replace(/%28/g, '(').replace(/%29/g, ')');
	};



	this.fetchPage = function(el, active_class, display_el, url ){

		url = (typeof url == 'undefined') ? this.endpoint : url ;
		$(el).click(function(e){
			e.preventDefault();
			self.showLoading();

			var slug = $(this).attr('id');
			var slg = 'pg_slug='+slug;

			$(el).removeClass(active_class)
			$('#'+slug).addClass(active_class)


				self.sendData('post', url, slg, function callServiceHandler(data){

					$(display_el).html(data);
				});

				self.removeLoading();
		});
	};



	// search the menu code for a page name and set that page's link ID to "active"

	this.setActiveItem = function (menu_name, page) {
		// get the menu code from the containing div
		// note that IE capitalizes HTML tags in this value, so we have to use case-insensitive regexps below
		var menu_code = document.getElementById(menu_name).innerHTML;

		// the first line looks for the assembler.php?page=pagename format
		// the second line looks for the URL rewriting format
		// we're including both in the demo site but a real site would just need one or the other


		var re = new RegExp("(\\W" + window.location + "\")", "gi") ;
		menu_code = menu_code.replace(re, "$1 class=\"active\"") ;
		//alert(re1);
		//menu_code = menu_code.replace(re1, "$1 class=\"current\"");

		// put the modified menu code back into the containing div
		document.getElementById(menu_name).innerHTML = menu_code;
	};


	this.handleForm = function (el, success_text, success_callback) {
		//self.showLoading();
		var url = $(el).attr('action'),
			form_data = self.formData(el); //pass form content as key/value pairs

		if (typeof el == 'undefined')
		{
			el = '.kwx-form';
		}

		if (url === '') {
			alert("Please define the Service and Method for this form to continue");

		} else {
			var srv = url.split('/'); // the first array index repr the Service/Class name and the second the method/function

		}

		$(el).submit(function(e) {
			e.preventDefault();

			self.post(srv[0], srv[1], form_data, function(e){
				if (typeof success_callback == 'function') {
						success_callback(e);

				} else {
					if (e.success){
						//// Display custom success text
						if (typeof success_text != 'undefined') {
							alert(success_text);

						} else {
							alert(e.success);
						}
					}
					else{
						alert(e.error);
					}
				}
					self.removeLoading();
			});

		});


	};

	
	this.ajaxForm = function (el, success_text, success_callback) {
		//self.showLoading();
		

		if (typeof el == 'undefined')
		{
			el = '.kwx-form';
		}
		
		var url = $(el).attr('action'),
			form_data = self.formData(el); //pass form content as key/value pairs
		
		console.log(form_data);

		if (url === '') {
			alert("Please define the Service and Method for this form to continue");
			return
		} 

		$(el).submit(function(e) {
			e.preventDefault();

			$.post(url, form_data, function(e){
				if (typeof success_callback == 'function') {
					success_callback(e);

				} else {
					if (e.success){
						//// Display custom success text
						if (typeof success_text != 'undefined') {
							alert(success_text);

						} else {
							alert(e.success);
						}
					}
					else{
						alert(e.error);
					}
				}
				self.removeLoading();
			});

		});


	};
	


	this.success = function(msg) {
		var dom = $("#success");
		var output = '';

		if (typeof msg === 'undefined') {
			output = "Success";
		}
		else {
			output = msg;
		}

		$('#err_msg').append('<div id="success" />');
		$('#success').fadeIn('fast');

		$("#success").html(output);
		$("#success").show();

		setTimeout(function() {
			$("#success").fadeOut();
		}, 5000);

	};


	this.error = function(msg) {
		var dom = $("#error");
		var output = '';

		if (typeof msg == 'undefined') {
			output = "Error";
		}

		if (typeof msg == 'object') {
			for (var key in msg) {
				if (typeof msg[key] == 'object') {
					for (var _key in msg[key]) {
						output += msg[key][_key] + "<br />";
					}
				} else {
					output += msg[key] + "<br />";
				}
			}
		} else {
			output += msg;
		}

		$('#err_msg').append('<div id="error"  />');
		$('#error').fadeIn('fast');

		$("#error").html(output);
		$("#error").show();
		setTimeout(function() {
			$("#error").fadeOut();
		}, 5000);

	};



	this.hide = function(el){
		$('#'+el).hide();

	};

	this.show = function(el){
		$('#'+el).show();

	};


	// this may only work withing a Bootstrap setting.
	this.tooltip = function(el, pos, text)
	{
		$(el).tooltip({
			placement: pos,
			title: text
		});

	};

	// this may only work withing a Bootstrap setting.
	this.genTooltip = function(pos)
	{
		var $this = $('.kwx-tooltip');
		if(typeof pos == 'undefined')
		{
			pos = 'top';

		}

		$('.kwx-tooltip').tooltip({
			placement: pos,
			title: $this.attr('title')
		});

	};


	/**
	* This function allows the selection of all rows in a grid at once
	*/
	this.rowsSelector = function(tbl){
		if (typeof tbl === 'undefined')
		{
			tbl = 'table';

		}

		$(tbl+' th input:checkbox').on('click' , function(){
			var that = this;
			$(this).closest( tbl ).find('tr > td:first-child input:checkbox')
			.each(function(){
				this.checked = that.checked;
				$(this).closest('tr').toggleClass('selected');
			});

		});
	};


	/* load css file dynamically */
	this.loadCSS = function (filename){

		var fileref = document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", filename);

			if (typeof fileref != "undefined")
			{
				document.getElementsByTagName("head")[0].appendChild(fileref);
			}
	};



		/* load js file dynamically */
	this.loadJs = function (filename){

		var fileref = document.createElement('script');
		fileref.setAttribute("type","text/javascript");
		fileref.setAttribute("src", filename);

		if (typeof fileref != "undefined")
		{
			document.getElementsByTagName("head")[0].appendChild(fileref);
		}
	};



	/**
	* Load a page via ajax
	*/
	this.loadPage = function (place_holder, url, callback){

		if (typeof place_holder === 'undefined')
		{
			alert('Argument one is missing');

		}
		else if (typeof url === 'undefined')
		{
			alert('Provide the page url to load');

		}

		$( place_holder ).load( url, function() {

			if (typeof callback === 'function')
			{
				callback();
			}
		});

	};
	
	
	
	this.date = function(str) {
		var date = new Date();
		if(typeof str !== 'undefined') {
			date = new Date(str);
		}		
		
		return date;
	}

}
